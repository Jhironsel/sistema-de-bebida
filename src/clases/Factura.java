package clases;
public class Factura {
    private final int idFactura;
    private final short idLinea;
    private final String idProducto;
    private final String descripcion;
    private final double precio;
    private final double cantidad;

    public Factura(int idFactura, short idLinea, String idProducto, 
            String descripcion, double precio, double cantidad) {
        this.idFactura = idFactura;
        this.idLinea = idLinea;
        this.idProducto = idProducto;
        this.descripcion = descripcion;
        this.precio = precio;
        this.cantidad = cantidad;
    }

    public String getDescripcion() {
        return descripcion;
    }
    
    public int getIdFactura() {
        return idFactura;
    }

    public int getIdLinea() {
        return idLinea;
    }

    public String getIdProducto() {
        return idProducto;
    }

    public double getPrecio() {
        return precio;
    }

    public double getCantidad() {
        return cantidad;
    }    
}
