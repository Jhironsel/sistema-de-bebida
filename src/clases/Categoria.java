package clases;
public class Categoria {
    private short idCategoria;
    private String descripcion;
    private String imagePath;

    public Categoria(short idCategoria, String descripcion, String imagePath) {
        System.out.println("+Clase Categoria");
        this.idCategoria = idCategoria;
        this.descripcion = descripcion;
        this.imagePath = imagePath;
    }
    public short getIdCategoria() {
        return idCategoria;
    }
    public void setIdCategoria(short idCategoria) {
        this.idCategoria = idCategoria;
    }
    public String getDescripcion() {
        return descripcion;
    }
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    public String getImagePath() {
        return imagePath;
    }
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
    @Override
    public String toString() {
        return descripcion;
    }
}