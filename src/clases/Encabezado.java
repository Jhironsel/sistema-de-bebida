package clases;

public class Encabezado {
    private String nombreEmpresa;
    private String direccionEmpresa;
    private String telefonoEmpresa;
    private String mensajeEmpresa;

    public Encabezado(String nombreEmpresa, String direccionEmpresa, 
            String telefonoEmpresa, String mensajeEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
        this.direccionEmpresa = direccionEmpresa;
        this.telefonoEmpresa = telefonoEmpresa;
        this.mensajeEmpresa = mensajeEmpresa;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getDireccionEmpresa() {
        return direccionEmpresa;
    }

    public void setDireccionEmpresa(String direccionEmpresa) {
        this.direccionEmpresa = direccionEmpresa;
    }

    public String getTelefonoEmpresa() {
        return telefonoEmpresa;
    }

    public void setTelefonoEmpresa(String telefonoEmpresa) {
        this.telefonoEmpresa = telefonoEmpresa;
    }

    public String getMensajeEmpresa() {
        return mensajeEmpresa;
    }

    public void setMensajeEmpresa(String mensajeEmpresa) {
        this.mensajeEmpresa = mensajeEmpresa;
    }
    
    
    
}
