package clases;

import java.util.Date;

public class Producto{
    private String idProducto;
    private String descripcion;
    private float costo;
    private float precio;
    private String nota;
    private float entrada;
    private Date fechaVencimiento;
    private int estado;
    private String impuesto;
    private int idCategoria;
    private String path;
    

    public Producto(String idProducto, String descripcion, float costo, 
            float precio, String nota, float entrada, Date fechaVencimiento, 
            int estado, String impuesto, int idCategoria, String path) {
        this.idProducto = idProducto;
        this.descripcion = descripcion;
        this.costo = costo;
        this.precio = precio;
        this.nota = nota;
        this.entrada = entrada;
        this.fechaVencimiento = fechaVencimiento;
        this.estado = estado;
        this.impuesto = impuesto;
        this.idCategoria = idCategoria;
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }
    
    public String getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(String impuesto) {
        this.impuesto = impuesto;
    }
    
    public String getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(String idProducto) {
        this.idProducto = idProducto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    
    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }
    
    public float getCosto(){
        return costo;
    }
    
    public void setCosto(float costo){
        this.costo = costo;
    }
    
    public float getEntrada(){
        return entrada;
    }
    
    public void setEntrada(float entrada){
        this.entrada = entrada;
    }
    
    public int getEstado(){
        return estado;
    }
    
    public void setEstado(int estado){
        this.estado = estado;
    }
    public Date getFechaVencimiento(){
        return fechaVencimiento;
    }
    public void setFechaVencimiento(Date fechaVencimiento){
        this.fechaVencimiento = fechaVencimiento;
    }
    public String ToString() {
        return idProducto + "|"
                + descripcion + "|"
                + precio + "|"
                + nota + "|"
                + costo + "|"
                + entrada + "|"
                + estado + "|"
                + fechaVencimiento;
    }
}
