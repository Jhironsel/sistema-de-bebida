package clases;

public class Deudas {
    private String idCliente;
    private String idUsuario;
    private String concepto;
    private float monto;
    
    public Deudas(String idCliente, String idUsuario, String concepto, 
            float monto) {
        this.idCliente = idCliente;
        this.idUsuario = idUsuario;
        this.concepto = concepto;
        this.monto = monto;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public float getMonto() {
        if(monto == 0f) return 0.0f;
        return monto;
    }

    public void setMonto(float monto) {
        if(monto == 0f) this.monto = 0.0f;
        this.monto = monto;
    }
}
