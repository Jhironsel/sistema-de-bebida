package clases;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class AnimationClass {
    public void jLabelMove(final String where, final short start,
            final short stop, final short delay, final short increment,
            final JLabel jLabel) {
        new Thread() {
            @Override
            public void run() {
                if ("Up".equals(where) || "Down".equals(where)) {
                    if ("Up".equals(where)) {
                        while (jLabel.getY() > stop) {
                            for (short i = start; i >= stop; i -= increment) {
                                try {
                                    Thread.sleep(delay);
                                    jLabel.setLocation(jLabel.getX(), i);
                                } catch (InterruptedException e) {
                                    System.out.println("Error Thread Interrupted: " + e);
                                }
                            }
                        }
                    }
                    if ("Down".equals(where)) {
                        while (jLabel.getY() <= start) {
                            for (short i = start; i <= stop; i += increment) {
                                try {
                                    Thread.sleep(delay);
                                    jLabel.setLocation(jLabel.getX(), i);
                                } catch (InterruptedException e) {
                                    System.out.println("Error Thread Interrupted: " + e);
                                }
                            }
                        }
                    }
                    jLabel.setLocation(jLabel.getX(), stop);
                }

                if ("Left".equals(where) || "Right".equals(where)) {
                    if ("Left".equals(where)) {
                        while (jLabel.getX() > stop) {
                            for (short i = start; i >= stop; i -= increment) {
                                try {
                                    Thread.sleep(delay);
                                    jLabel.setLocation(i, jLabel.getY());
                                } catch (InterruptedException e) {
                                    System.out.println("Error Thread Interrupted: " + e);
                                }
                            }
                        }
                    }
                    if ("Right".equals(where)) {
                        while (jLabel.getX() <= start) {
                            for (short i = start; i <= stop; i += increment) {
                                try {
                                    Thread.sleep(delay);
                                    jLabel.setLocation(i, jLabel.getY());
                                } catch (InterruptedException e) {
                                    System.out.println("Error Thread Interrupted: " + e);
                                }
                            }
                        }
                    }
                    jLabel.setLocation(stop, jLabel.getY());
                }
            }
        }.start();
    }
    public void jPanelMove(final String where, final short start, final short stop,
            final short delay, final short increment, final JPanel jPanel) {
        new Thread() {
            public void run() {
                if ("Right".equals(where) || "Left".equals(where)) {
                    if ("Right".equals(where)) {
                        while (jPanel.getX() <= start) {
                            for (short i = start; i <= stop; i += increment) {
                                try {
                                    Thread.sleep(delay);

                                    jPanel.setLocation(i, jPanel.getY());
                                } catch (InterruptedException e) {
                                    System.out.println("Error Thread Interrupted: " + e);
                                }
                            }
                        }
                    }
                    if ("Left".equals(where)) {
                        while (jPanel.getX() > stop) {
                            for (short i = start; i >= stop; i -= increment) {
                                try {
                                    Thread.sleep(delay);
                                    jPanel.setLocation(i, jPanel.getY());
                                } catch (InterruptedException e) {
                                    System.out.println("Error Thread Interrupted: " + e);
                                }
                            }
                        }
                    }
                    jPanel.setLocation(stop, jPanel.getY());
                }
                if ("Up".equals(where) || "Down".equals(where)) {
                    if ("Up".equals(where)) {
                        while (jPanel.getY() > stop) {
                            for (short i = start; i >= stop; i -= increment) {
                                try {
                                    Thread.sleep(delay);
                                    jPanel.setLocation(jPanel.getX(), i);
                                } catch (InterruptedException e) {
                                    System.out.println("Error Thread Interrupted: " + e);
                                }
                            }
                        }

                    }
                    if ("Down".equals(where)) {
                        while (jPanel.getY() <= start) {
                            for (short i = start; i <= stop; i += increment) {
                                try {
                                    Thread.sleep(delay);

                                    jPanel.setLocation(jPanel.getX(), i);
                                } catch (InterruptedException e) {
                                    System.out.println("Error Thread Interrupted: " + e);
                                }
                            }
                        }
                    }
                    jPanel.setLocation(jPanel.getX(), stop);
                }
            }
        }.start();
    }
}
