package clases;

import static clases.Utilidades.LOGGER;
import java.io.FileReader;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;

public class Datos {

    private static Datos mDatos;
    private static Connection cnn;

    public synchronized static Datos getMDatos(String role) {
        
        if (mDatos == null) {
            mDatos = new Datos(role);
        }
        
        return mDatos;
    }

    private Datos(String role) {
        System.out.println("+Clase Datos");
        Properties props, host;
        String ip = "", puerto = "";

        props = new Properties();
        props.setProperty("user", "sysdba");
        props.setProperty("password", "123uasd");
        props.setProperty("charSet", "ISO8859_1");
        if(!role.isEmpty()){
            System.out.println("Role dectetado: "+role);
            props.setProperty("roleName", role);
        }
        
        host = new Properties();
        
        try {
            host.load(new FileReader(System.getProperty("user.dir") + 
                    "/Propeties/propiedades.properties"));
        } catch (IOException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (Boolean.valueOf(host.getProperty("ProtocoloActivo", "false"))) {
            ip = host.getProperty("Ip_Servidor1") + "."
                    + host.getProperty("Ip_Servidor2") + "."
                    + host.getProperty("Ip_Servidor3") + "."
                    + host.getProperty("Ip_Servidor4");
        }

        if (Boolean.valueOf(host.getProperty("NombreActivo", "false"))) {
            ip = host.getProperty("Nombre_del_Servidor");
        }

        if (Boolean.valueOf(host.getProperty("Con_Puerto", "false"))) {
            puerto = ":" + host.getProperty("Puerto_del_Servidor");
        }
        
        String file = System.getProperty("user.dir") + 
                "/Data/SistemaDeBebida3.0.fdb";
        System.out.println("Ruta de la Base de Datos: "+file);
        if (getCnn() == null) {
            try {
                Class.forName("org.firebirdsql.jdbc.FBDriver");
                cnn = DriverManager.getConnection(
                        "jdbc:firebirdsql://" + ip + puerto + "/"
                        + file, props);
                System.out.println("Conexion en: " + ip + "\nPuerto" + (puerto.isEmpty() ? ": Sin Puerto" : puerto));
                //getCnn().setAutoCommit(true);
            } catch (ClassNotFoundException | SQLException ex) {
                LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("---------------------------------------");
        System.out.println("Conexion Establecida Clase Conexion!!!");        
        System.out.println("---------------------------------------");
    }

    public Connection getCnn() {
        return cnn;
    }

    public void cerrarConexion(boolean salir) {
        try {
            cnn.close();
        } catch (SQLException ex) {
            if(salir) System.exit(0);
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String pathBaseDeDatos() {
        ResultSet rs = getConsulta("SELECT MON$DATABASE_NAME FROM MON$DATABASE");
        try {
            rs.next();
            return rs.getString(1);
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class
                    .getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public boolean validarUsuario(String usuario, String clave) {
        try {
            // Creamos un Statement para poder hacer peticiones a la bd
            Statement st = getCnn().createStatement();
            ResultSet rs = st.executeQuery(
                    "SELECT (1) FROM TABLA_USUARIOS where idUsuario like '"
                    + usuario + "' and clave like '" + clave
                    + "' and borrado like 'n'");
            return rs.next();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean validarUsuarioPro(String idUsuario, String clave) {
        try {
            // Creamos un Statement para poder hacer peticiones a la bd
            Statement st = getCnn().createStatement();
            ResultSet rs = st.executeQuery(
                    "select (1) from TABLA_USUARIOS where idUsuario like '"
                    + idUsuario + "' and clave like '" + clave + "' and "
                    + "estado = 1 and borrado like 'n' and Autorizar like 's'");
            return rs.next();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public String modificarDeuda(String idDeuda, String op) {
        try {
            // Creamos un Statement para poder hacer peticiones a la bd
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(
                    "SELECT p.S_SALIDA FROM UPDATE_DEUDA_ESTADO ('" + idDeuda
                    + "', '" + op + "') p;");
            rs.next();
            return rs.getString("S_SALIDA");
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error a ejecutar Operacion";
        }
    }

    public int getPerfil(String usuario) {
        try {
            Statement st = cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY,
                    ResultSet.HOLD_CURSORS_OVER_COMMIT);
            ResultSet rs = st.executeQuery(
                    "select idPerfil from tabla_usuarios where idUsuario = '"
                    + usuario + "' and borrado like 'n'");
            if (rs.next()) {
                return rs.getInt("idPerfil");
            } else {
                return -1;
            }
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }

    public boolean existeUsuario(String idUsuario) {
        try {
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(
                    "select (1) from tabla_usuarios where idUsuario = '"
                    + idUsuario + "' and borrado like 'n'");
            return rs.next();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean existeIdMaquina(String idMaquina) {
        try {

            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(
                    "SELECT (1) FROM V_FCH_LC a WHERE a.ID = '"
                    + idMaquina + "'");
            return rs.next();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public int periodoMaquina() {
        try {
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery("SELECT r.D FROM V_TIME_LIC r");
            rs.next();
            return rs.getInt(1);
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }

    public boolean existeCategoria(String categoria) {
        try {
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(
                    "select (1) from tabla_CATEGORIA where descripcion like '"
                    + categoria + "'");
            return rs.next();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean existeCategoriaProductos(String idCategoria) {
        try {
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(
                    "select (1) from tabla_PRODUCTOS where idCategoria = "
                    + idCategoria + "");
            return rs.next();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean usuarioTurnoActivo(String idUsuario) {
        try {
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(
                    "select (1) from tabla_turnos where idUsuario like '"
                    + idUsuario + "' and estado = 1");
            return rs.next();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public int idTurnoActivo(String idUsuario) {
        try {
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(
                    "select idTurno from tabla_turnos where idUsuario like '"
                    + idUsuario + "' and estado = 1");
            if (rs.next()) {
                return rs.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }

    public boolean existeCliente(String idCliente) {
        try {
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(
                    "select (1) from tabla_clientes where idCliente = '"
                    + idCliente + "'");
            return rs.next();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean existeProducto(String idProducto) {
        try {
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(
                    "select (1) from tabla_productos where idProducto = '"
                    + idProducto + "'");
            return rs.next();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public String agregarUsuario(Usuario miUsuario) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate("insert into TABLA_USUARIOS (IDUSUARIO, NOMBRES, APELLIDOS, "
                    + "CLAVE, IDPERFIL, ESTADO, BORRADO, AUTORIZAR) values('"
                    + miUsuario.getIdUsuario().replace("'", "") + "', '"
                    + miUsuario.getNombres().replace("'", "") + "', '"
                    + miUsuario.getApellidos().replace("'", "") + "', '"
                    + miUsuario.getClave().replace("'", "") + "', "
                    + miUsuario.getPerfil() + ", "
                    + miUsuario.getEstado() + ", 'n', '"
                    + miUsuario.getAutorizar() + "' )");
            return "Usuario Agregado Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al insertar Usuario...";
        }
    }

    public String agregarCategoria(String descripcion, String imagePath) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate(
                    "INSERT INTO CATEGORIA (DESCRIPCION, IMAGEPATH) "
                    + "VALUES ('" + descripcion.replace("'", "") + "', '" + imagePath + "');");
            return "Cateoria Agregada Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al insertar Categoria...";
        }
    }

    public String agregarProducto(Producto miProducto) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate("INSERT INTO PRODUCTOS (IDPRODUCTO, DESCRIPCION, COSTO, PRECIO, NOTA, ENTRADA, "
                    + "    FECHAVENCIMIENTO, ESTADO, IMPUESTO, IDCATEGORIA, IMAGEPATH) "
                    + "VALUES ( "
                    + "    '" + miProducto.getIdProducto().replace("'", "") + "',  "
                    + "    '" + miProducto.getDescripcion().replace("'", "") + "',  "
                    + "    " + miProducto.getCosto() + ",  "
                    + "    " + miProducto.getPrecio() + ",  "
                    + "    '" + miProducto.getNota().replace("'", "") + "',  "
                    + "    " + miProducto.getEntrada() + ",  "
                    + "    '" + Utilidades.formatDate(
                            miProducto.getFechaVencimiento(), "MM-dd-yyyy") + "',  "
                    + "    " + miProducto.getEstado() + ",  "
                    + "    " + miProducto.getImpuesto() + ", " + miProducto.getIdCategoria()
                    + ", '" + miProducto.getPath() + "');");
            return "Producto Agregado Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al Insertar Producto...";
        }
    }

    public boolean agregarProductoEntrada(int IDENTRADA_PRODUCTO, int numero,
            String numeroFactura, String idProducto, double entrada,
            String idUsuario) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate(
                    "INSERT INTO ENTRADAS_PRODUCTO (IDENTRADA_PRODUCTO, NUMERO, "
                    + "NUMEROFACTURA, IDPRODUCTO, ENTRADA, OP, IDUSUARIO)"
                    + "VALUES (" + IDENTRADA_PRODUCTO + ", " + numero + ", '"
                    + numeroFactura + "', '" + idProducto + "', " + entrada
                    + ", '+', '" + idUsuario + "');");
            return true;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean agregarProductoSalida(int IDENTRADA_PRODUCTO, int numero,
            String cencepto, String idProducto, double entrada, String idUsuario) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate("INSERT INTO ENTRADAS_PRODUCTO (IDENTRADA_PRODUCTO, CONCEPTO, "
                    + "IDPRODUCTO, ENTRADA, OP, IDUSUARIO, NUMERO)"
                    + "VALUES (" + IDENTRADA_PRODUCTO + ", '" + cencepto
                    + "', '" + idProducto + "', " + entrada + ", '-', '"
                    + idUsuario + "', " + numero + " );");
            return true;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public String agregarPerfil(String descripcion) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate(
                    "INSERT INTO T_PERFIL (DESCRIPCION)  "
                    + "VALUES ('" + descripcion.replace("'", "") + "');");
            return "Perfil Agregado Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al Insertar Perfil...";
        }
    }

    public String agregarOrModificarAcesso(int idAcceso, int idPerfil, Perfil miPerfil) {
        try {
            Statement st = cnn.createStatement();
            st.execute(
                    "UPDATE OR INSERT INTO Tabla_ACCESO2 (IDACCESO, IDPERFIL, ARCHIVOS, ARCHIVOSCLIENTES,"
                    + "                    ARCHIVOSPRODUCTOS, ARCHIVOSUSUARIOS, ARCHIVOSCAMBIOCLAVE,"
                    + "                    ARCHIVOSCAMBIOUSUARIO, ARCHIVOSSALIR, MOVIMIENTOS, "
                    + "                    MOVIMIENTOSNUEVAFACTURA,MOVIMIENTOSREPORTEFACTURA, "
                    + "                    MOVIMIENTOSINVENTARIO, MOVIMIENTOSABRILTURNO, "
                    + "                    MOVIMIENTOSCERRARTURNO, MOVIMIENTOSDEUDA) "
                    + "    VALUES (" + idAcceso + ", " + idPerfil + ", '"
                    + miPerfil.getArchivos() + "', '"
                    + miPerfil.getArchivosClientes() + "', '"
                    + miPerfil.getArchivosProductos() + "', '"
                    + miPerfil.getArchivosUsuarios() + "', '"
                    + miPerfil.getArchivosCambioClave() + "', '"
                    + miPerfil.getArchivosCambioUsuario() + "', '"
                    + miPerfil.getArchivosSalir() + "', '"
                    + miPerfil.getMovimientos() + "', '"
                    + miPerfil.getMovimientosNuevaFactura() + "', '"
                    + miPerfil.getMovimientosReporteFactura() + "', '"
                    + miPerfil.getMovimientosInventarios() + "', '"
                    + miPerfil.getMovimientosAbrirTurno() + "', '"
                    + miPerfil.getMovimientosCerrarTurno() + "', '"
                    + miPerfil.getMovimientosDeuda() + "') matching(IDACCESO);");
            return "Acesso Agregado Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al Insertar Acesso...";
        }
    }

    public String agregarCliente(Cliente miCliente) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate(
                    "insert into CLIENTE(IDCLIENTE, NOMBRES, APELLIDOS, CIUDAD,"
                    + "DIRECCION, TELEFONO, FECHANACIMIENTO, FECHAINGRESO, CREDITO, "
                    + "ESTADO, DEUDAACTUAL) values('" + miCliente.getIdCliente() + "','"
                    + miCliente.getNombres().replace("'", "") + "','" + miCliente.getApellidos().replace("'", "")
                    + "', '" + miCliente.getCiudad().replace("'", "") + "',"
                    + " '" + miCliente.getDireccion().replace("'", "") + "', '" + miCliente.getTelefono()
                    + "', '" + Utilidades.formatDate(miCliente.getFechaNacimiento(), "MM-dd-yyyy")
                    + "', '" + Utilidades.formatDate(miCliente.getFechaIngreso(), "MM-dd-yyyy") + "', "
                    + "" + miCliente.getCredito() + ", " + miCliente.getEstado() + ", 0.00)");
            return "Cliente Agregado Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al insertar Cliente...";
        }
    }

    public String modificarUsuario(Usuario miUsuario) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate("update usuario set "
                    + "nombres = '" + miUsuario.getNombres().replace("'", "") + "', "
                    + "apellidos = '" + miUsuario.getApellidos().replace("'", "") + "', "
                    + "clave = '" + miUsuario.getClave().replace("'", "") + "', "
                    + "idPerfil = " + miUsuario.getPerfil() + ", "
                    + "estado = " + miUsuario.getEstado() + ", "
                    + "AUTORIZAR = '" + miUsuario.getAutorizar() + "' "
                    + "where idUsuario = '" + miUsuario.getIdUsuario() + "'");
            return "Usuario Modificado Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al Modificar Usuario...";
        }
    }

    public String modificarCliente(Cliente miCliente) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate(
                    "update CLIENTE "
                    + "set NOMBRES = '" + miCliente.getNombres().replace("'", "") + "', "
                    + "    APELLIDOS = '" + miCliente.getApellidos().replace("'", "") + "', "
                    + "    CIUDAD = '" + miCliente.getCiudad().replace("'", "") + "', "
                    + "    DIRECCION = '" + miCliente.getDireccion().replace("'", "") + "', "
                    + "    TELEFONO = '" + miCliente.getTelefono() + "', "
                    + "    FECHANACIMIENTO = '" + Utilidades.formatDate(miCliente.getFechaNacimiento(), "MM-dd-yyyy") + "', "
                    + "    FECHAINGRESO = '" + Utilidades.formatDate(miCliente.getFechaIngreso(), "MM-dd-yyyy") + "', "
                    + "    CREDITO = " + miCliente.getCredito() + ", "
                    + "    ESTADO = " + miCliente.getEstado() + "  "
                    + "where (IDCLIENTE = '" + miCliente.getIdCliente() + "')");
            return "Cliente Modificado Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al Modificar Cliente...";
        }
    }

    public boolean modificarFactura(Integer idFactura, int tipoCompra,
            double efectivo, double cambio, String estado) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate(
                    "UPDATE FACTURA a "
                    + "SET "
                    + "    a.TIPO_COMPRA = " + tipoCompra + ", "
                    + "    a.EFECTIVO = '" + efectivo + "', "
                    + "    a.CAMBIO = " + cambio + ", "
                    + "    a.ESTADO = '" + estado + "' "
                    + "WHERE "
                    + "    a.IDFACTURA = " + idFactura);
            return true;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public String modificarCategoria(String idCategoria, String descripcion,
            String imagePath) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate(
                    "update Categoria "
                    + "set descripcion = '" + descripcion.replace("'", "") + "', "
                    + "    imagePath = '" + imagePath + "' "
                    + "where (idCategoria = '" + idCategoria + "')");
            return "Categoria Modificada Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al Modificar Categoria...";
        }
    }

    public String modificarOpcionMensaje(String mensaje, String nombreEmpresa,
            String direccionEmpresa, String telefonoEmpresa) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate(
                    "UPDATE OR INSERT INTO OPCIONES(OPCION, RUTA) "
                    + "VALUES('MensajeTickes', '" + mensaje.replace("'", "") + "') "
                    + "matching(OPCION)");
            st = cnn.createStatement();
            st.executeUpdate(
                    "UPDATE OR INSERT INTO OPCIONES(OPCION, RUTA) "
                    + "VALUES('nombreEmpresa', '" + nombreEmpresa.replace("'", "") + "') "
                    + "matching(OPCION)");
            st = cnn.createStatement();
            st.executeUpdate(
                    "UPDATE OR INSERT INTO OPCIONES(OPCION, RUTA) "
                    + "VALUES('direccionEmpresa', '" + direccionEmpresa.replace("'", "") + "') "
                    + "matching(OPCION)");
            st = cnn.createStatement();
            st.executeUpdate(
                    "UPDATE OR INSERT INTO OPCIONES(OPCION, RUTA) "
                    + "VALUES('telefonoEmpresa', '" + telefonoEmpresa.replace("'", "") + "') "
                    + "matching(OPCION)");

            return "Tickes Actualizado";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al Modificar Tickes...";
        }
    }

    public String modificarT_Perfil(int idPerfil, String descripcion) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate(
                    "UPDATE T_PERFIL a "
                    + "SET "
                    + "    a.DESCRIPCION = '" + descripcion.replace("'", "") + "' "
                    + "WHERE "
                    + "    a.IDPERFIL = " + idPerfil + "");
            return "Perfil Modificado Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al Modificar Perfil";
        }
    }

    public String modificarProducto(Producto miProducto) {
        try {//Trabajar con los producto para modificarlo
            Statement st = cnn.createStatement();
            st.executeUpdate(
                    "update productos set "
                    + "descripcion = '" + miProducto.getDescripcion().replace("'", "") + "', "
                    + "costo = " + miProducto.getCosto() + ", "
                    + "precio = " + miProducto.getPrecio() + ", "
                    + "nota = '" + miProducto.getNota().replace("'", "") + "', "
                    + "fechaVencimiento = '" + Utilidades.formatDate(
                            miProducto.getFechaVencimiento(), "MM-dd-yyyy") + "', "
                    + "estado = " + miProducto.getEstado() + ", "
                    + "impuesto = " + miProducto.getImpuesto() + ", "
                    + "idCategoria = " + miProducto.getIdCategoria() + ", "
                    + "imagepath = '" + miProducto.getPath() + "' "
                    + "where idProducto = '" + miProducto.getIdProducto() + "'");
            return "Producto Modificado Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al Modificar Producto...";
        }
    }

    public String borrarUsuario(String usuario) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate("delete from usuario where idUsuario = '" + usuario + "'");
            return "Usuario Borrado Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Ocurrio un error al intentar borrar el Usuario...";
        }
    }

    public String borrarCliente(String idCliente) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate("delete from cliente where idCliente like '"
                    + idCliente + "'");
            return "Cliente Borrado Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "error";
        }
    }

    public String borrarT_Perfil(String idPerfil) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate(
                    "delete from T_Perfil where idPerfil = " + idPerfil + "");
            return "Perfil Borrado Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Ocurrio un error al intentar borrar el Perfil...";
        }
    }

    public String borrarCategoria(String idCategoria) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate(
                    "delete from CATEGORIA where idCategoria = "
                    + idCategoria + "");
            return "Categoria Borrado Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Ocurrio un error al intentar borrar la Categoria...";
        }
    }

    public String borrarFactura(Integer idFactura) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate(
                    "delete from FACTURA where idFactura = " + idFactura);
            return "Factura Borrada Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "!Ocurrio un error al intentar borrar el Factura...!!!";
        }
    }

    public String borrarProducto(String producto) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate(
                    "delete from productos where idproducto = '" + producto + "'");
            return "Producto Borrado Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Ocurrio un error al intentar borrar el Producto...";
        }
    }

    public String borrarPerfil(String idPerfil) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate(
                    "delete from T_Perfil where idPerfil = " + idPerfil + "");
            return "Perfil Borrado Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Ocurrio un error al intentar borrar el Perfil...";
        }
    }

    public ResultSet getUsuarios() {//Trabajar para llenar el comboBox del Usuario en su perfil Creo que es pan Comido...
        try {
            Statement st = cnn.createStatement();
            return st.executeQuery(
                    "select idUsuario, Nombres, Apellidos, u.idPerfil, borrado, "
                    + "      (p.DESCRIPCION) as perfilDescripcion "
                    + "from TABLA_USUARIOS u "
                    + "    LEFT JOIN TABLA_T_PERFIL p "
                    + "        ON p.IDPERFIL = u.IDPERFIL "
                    + "where borrado like 'n' "
                    + "order by idUsuario");
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ResultSet getUsuariosActivo() {
        try {
            Statement st = cnn.createStatement();
            return st.executeQuery(
                    "select u.idUsuario, u.Nombres, u.Apellidos "
                    + "from tabla_Usuarios u "
                    + "left JOIN tabla_TURNOS t "
                    + "    on t.IDUSUARIO like u.IDUSUARIO "
                    + "where u.borrado like 'n' and t.ESTADO = 1 "
                    + "order by idUsuario");
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ResultSet getClientes() {
        try {
            String sql = "select * from tabla_clientes order by 1";
            Statement st = cnn.createStatement();
            return st.executeQuery(sql);
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ResultSet getClientesCombo() {
        try {
            Statement st = cnn.createStatement();
            return st.executeQuery(
                    "select idCliente, nombres, apellidos "
                    + "from tabla_clientes "
                    + "where estado = 1 and idCliente != '00000000000' "
                    + "order by 1");
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ResultSet getClientes(String idCliente) {
        try {
            Statement st = cnn.createStatement();
            return st.executeQuery(
                    "select idCliente, c.nombres ||' '||c.apellidos, credito, deudaactual"
                    + " from tabla_clientes c where c.idCliente like '" + idCliente + "'");
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ResultSet getFacturas() {
        try {
            Statement st = cnn.createStatement();
            return st.executeQuery(
                    "select idFactura from tabla_facturas order by 1");
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public ResultSet getConsulta(String sql) {
        try {
            Statement st = cnn.createStatement();
            return st.executeQuery(sql);
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    //Procedimiento comienzo
    public boolean PagoCumplido(int idFactura) throws SQLException {
        CallableStatement stmt = cnn.prepareCall(
                "EXECUTE PROCEDURE Cajero_PagoCumplido (?)");
        stmt.setInt(1, idFactura);
        return stmt.execute();
    }

    public boolean PagoDeuda(int idDeuda, int idTurno, double monto) throws SQLException {
        CallableStatement stmt = cnn.prepareCall(
                "EXECUTE PROCEDURE INSER_PAGO_DEUDAS_EXT (?, ?, ?)");
        stmt.setInt(1, idDeuda);
        stmt.setInt(2, idTurno);
        stmt.setDouble(3, monto);
        return stmt.execute();
    }
    
    public boolean setLicencia(String fecha, String idMaquina, String clave1, 
            String clave2) throws SQLException {
        CallableStatement stmt = cnn.prepareCall(
                "EXECUTE PROCEDURE SYSTEM_SET_LICENCIA (?, ?, ?, ?)");
        stmt.setString(1, fecha);
        stmt.setString(2, idMaquina);
        stmt.setString(3, clave1);
        stmt.setString(4, clave2);
        return stmt.execute();
    }

    public boolean setLogo(String ruta) throws SQLException {
        CallableStatement stmt = cnn.prepareCall(
                "EXECUTE PROCEDURE CALL SYSTEM_SET_LOGO (?)");
        stmt.setString(1, ruta);
        return stmt.execute();
    }

    public boolean insertDeudas(Deudas miDeuda) throws SQLException {
        CallableStatement stmt = cnn.prepareCall(
                "EXECUTE PROCEDURE INSER_DEUDAS (?, ?, ?, ?)");
        stmt.setString(1, miDeuda.getIdCliente());
        stmt.setString(2, miDeuda.getIdUsuario());
        stmt.setString(3, miDeuda.getConcepto());
        stmt.setDouble(4, miDeuda.getMonto());
        return stmt.execute();
    }

    public boolean habilitarTurno(String idUsuario) {
        try {
            CallableStatement stmt = cnn.prepareCall(
            "EXECUTE PROCEDURE Admin_HabilitarTurno (?)");
            stmt.setString(1, idUsuario);
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean cerrarTurno(String idUsuario) {
        try {
            CallableStatement stmt = cnn.prepareCall(
                    "EXECUTE PROCEDURE Admin_CerrarTurno (?)");
            stmt.setString(1, idUsuario);
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean gasto(int idTurno, String descripcion, double monto) {
        try {
            CallableStatement stmt = cnn.prepareCall(
                    "EXECUTE PROCEDURE Cajero_gasto (?, ?, ?)");
            stmt.setInt(1, idTurno);
            stmt.setString(2, descripcion.replace("'", ""));
            stmt.setDouble(3, monto);
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }//Precedimiento   fin

    public ResultSet getProductos() {
        try {
            Statement st = cnn.createStatement();
            return st.executeQuery(
            "SELECT r.IDPRODUCTO, r.DESCRIPCION, r.COSTO, r.PRECIO, r.NOTA, r.ENTRADA, "
                    + "    r.FECHAVENCIMIENTO, r.ESTADO, r.IMPUESTO, r.CATEGORIA, r.IDCATEGORIA "
                    + "FROM GET_PRODUCTOS r "
                    + "order by 1");
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public int numeroUsuarios() {
        try {
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(
                    "select count(*) as num from tabla_usuarios");
            if (rs.next()) {
                return rs.getInt("num");
            } else {
                return 0;
            }
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public int numeroClientes() {
        try {
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(
                    "SELECT COUNT(*) as num FROM TABLA_CLIENTES");
            if (rs.next()) {
                return rs.getInt("num");
            } else {
                return 0;
            }
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public int numeroDeudas(String lugar) {
        try {
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(
                    "SELECT count(*) as num FROM TABLA_DEUDAS r " + lugar);
            if (rs.next()) {
                return rs.getInt("num");
            } else {
                return 0;
            }
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public int numeroProductos() {
        try {
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(
                    "SELECT count(*) as num FROM TABLA_PRODUCTOS");
            if (rs.next()) {
                return rs.getInt("num");
            } else {
                return 0;
            }
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public Producto getProductos(String idProducto) {
        try {
            Producto miProducto = null;
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(
                    "select * from tabla_productos "
                    + "where idProducto = '" + idProducto + "'");
            if (rs.next()) {
                miProducto = new Producto(
                        rs.getString("idProducto"),
                        rs.getString("descripcion"),
                        rs.getFloat("costo"),
                        rs.getFloat("precio"),
                        rs.getString("nota"),
                        rs.getFloat("entrada"),
                        rs.getDate("fechaVencimiento"),
                        rs.getInt("estado"),
                        rs.getString("impuesto"),
                        rs.getInt("idCategoria"),
                        rs.getString("imagepath")
                );
            }
            return miProducto;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public Perfil getAcceso(int idPerfil) {
        try {
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(
                    "SELECT r.ARCHIVOS, r.ARCHIVOSCLIENTES, "
                    + "r.ARCHIVOSPRODUCTOS, r.ARCHIVOSUSUARIOS, r.ARCHIVOSCAMBIOCLAVE, "
                    + "r.ARCHIVOSCAMBIOUSUARIO, r.ARCHIVOSSALIR, r.MOVIMIENTOS, "
                    + "r.MOVIMIENTOSNUEVAFACTURA, r.MOVIMIENTOSREPORTEFACTURA, "
                    + "r.MOVIMIENTOSINVENTARIO, r.MOVIMIENTOSABRILTURNO, r.MOVIMIENTOSCERRARTURNO, "
                    + "r.MOVIMIENTOSDEUDA "
                    + "FROM TABLA_ACCESO2 r "
                    + "where idPerfil = " + idPerfil);
            System.out.println("Tenemos Filas: " + rs.next());
            Perfil miPerfil = new Perfil(
                    rs.getString("ARCHIVOS"),
                    rs.getString("ARCHIVOSCLIENTES"),
                    rs.getString("ARCHIVOSPRODUCTOS"),
                    rs.getString("ARCHIVOSUSUARIOS"),
                    rs.getString("ARCHIVOSCAMBIOCLAVE"),
                    rs.getString("ARCHIVOSCAMBIOUSUARIO"),
                    rs.getString("ARCHIVOSSALIR"),
                    rs.getString("MOVIMIENTOS"),
                    rs.getString("MOVIMIENTOSNUEVAFACTURA"),
                    rs.getString("MOVIMIENTOSREPORTEFACTURA"),
                    rs.getString("MOVIMIENTOSINVENTARIO"),
                    rs.getString("MOVIMIENTOSABRILTURNO"),
                    rs.getString("MOVIMIENTOSCERRARTURNO"),
                    rs.getString("MOVIMIENTOSDEUDA"));
            return miPerfil;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public Integer getNumFac(String idUsuario, int idTurno) {
        CallableStatement stmt;
        try {
            stmt = cnn.prepareCall("EXECUTE PROCEDURE SYSTEM_FACTURA (?, ?)");
            stmt.setString(1, idUsuario);
            stmt.setInt(2, idTurno);
            if (stmt.execute()) {
                Statement st = cnn.createStatement();
                ResultSet rs = st.executeQuery(
                        "EXECUTE PROCEDURE SYSTEM_FACTURA ('" + idUsuario + "', "
                        + idTurno + ")");
                if (rs.next()) {
                    return rs.getInt("S_RESULTADO");
                } else {
                    return null;
                }
            }
            return null;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public int getNumPerfil() {
        try {
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery("select max(IDPERFIL) from Tabla_T_PERFIL");
            if (rs.next()) {
                return rs.getInt("max");
            } else {
                return 1;
            }
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return 1;
        }
    }

    public int getNumCategoria() {
        try {
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery("select max(IDCategoria) from Tabla_Categoria");
            if (rs.next()) {
                return rs.getInt("max") + 1;
            } else {
                return 1;
            }
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return 1;
        }
    }

    public boolean agregarFacturaNombre(Integer numFactura, String idCliente,
            Date fecha, String UsuarioActual, int TipoCompra,
            double efectivo, double cambio, int idTurno, String estado,
            String nombreCliente) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate("UPDATE OR INSERT INTO factura (IDFACTURA, IDCLIENTE, "
                    + "USUARIO, Tipo_Compra, efectivo, cambio, idTurno, estado,"
                    + " nombreCliente) "
                    + "values (" + numFactura + ", '" + idCliente + "', '"
                    + UsuarioActual + "', " + TipoCompra + ", " + efectivo
                    + ", " + cambio + ", " + idTurno + ", '" + estado + "', '"
                    + nombreCliente + "') matching(IDFACTURA);");
            return true;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean agregarDetalleFactura(int idFactura, int idLinea,
            String idProducto, double precio, double cantidad) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate("UPDATE OR INSERT into DETALLEFACTURA (IDFACTURA, IDLINEA, IDPRODUCTO, "
                    + "PRECIO, CANTIDAD)"
                    + "values ( " + idFactura + ", " + idLinea + ", '" + idProducto + "', "
                    + precio + ", " + cantidad + ")");
            return true;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean agregarOrInsertarDetalleFactura(int idFactura, int idLinea,
            String idProducto, double precio, double cantidad) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate("UPDATE OR INSERT INTO DETALLEFACTURA (IDFACTURA, "
                    + "IDLINEA, IDPRODUCTO, PRECIO, CANTIDAD)"
                    + "values ( '" + idFactura + "', " + idLinea + ", '" + idProducto + "', "
                    + " " + precio + ", " + cantidad + ") matching(IDFACTURA, IDLINEA)");
            return true;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean agregarCategoria(String idCategoria, String descripcion,
            String imagePath) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate("insert into CATEGORIA (IdCategoria, descripcion, imagePath)"
                    + "values ( " + idCategoria + ", '" + descripcion + "', '" + imagePath + "')");
            return true;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public void cambioClave(String usuario, String clave) {
        try {
            Statement st = cnn.createStatement();
            st.executeUpdate("update usuario set "
                    + "clave = '" + clave + "' "
                    + "where idUsuario = '" + usuario + "'");
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void pagarCredito(double credito, double deuda, String idCliente,
            double Valor, int idFactura, int idTurno) {
        //Aqui debemos de actualizar tantos los campos credito y deudaActual
        //Tambien insertar datos a la tabla de pagosDeudas la cual tiene los sig
        //Campos idCliente, idfactura, idTurno, montoPago
        try {
            double Credito = credito + Valor;
            double deudaActual = deuda - Valor;
            Statement st = cnn.createStatement();
            st.executeUpdate("update cliente set "
                    + "credito = " + Credito + ", "
                    + "deudaActual = " + deudaActual + " "
                    + "where idCliente = '" + idCliente + "'");
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            Statement st = cnn.createStatement();
            st.executeUpdate("INSERT INTO PAGODEUDA (IDCLIENTE, IDFACTURA, IDTURNO, MONTOPAGO, ESTADO) "
                    + "VALUES ( "
                    + "    '" + idCliente + "', "
                    + "    " + idFactura + ", "
                    + "    " + idTurno + ", "
                    + "    " + Valor + ", "
                    + "    'A');");
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            PagoCumplido(idFactura);
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
