package clases;
public class Perfil {
    private String archivos;
    private String archivosClientes;    
    private String archivosProductos;
    private String archivosUsuarios;
    private String archivosCambioClave;
    private String archivosCambioUsuario;
    private String archivosSalir;
    private String movimientos;
    private String movimientosNuevaFactura;
    private String movimientosReporteFactura;
    private String movimientosInventarios;
    private String movimientosAbrirTurno;
    private String movimientosCerrarTurno;
    private String movimientosDeuda;

    public Perfil(String archivos, String archivosClientes, 
            String archivosProductos, String archivosUsuarios, 
            String archivosCambioClave, String archivosCambioUsuario, 
            String archivosSalir, String movimientos, 
            String movimientosNuevaFactura, String movimientosReporteFactura, 
            String movimientosInventarios, String movimientosAbrirTurno, 
            String movimientosCerrarTurno, String movimientosDeuda) {
        this.archivos = archivos;
        this.archivosClientes = archivosClientes;
        this.archivosProductos = archivosProductos;
        this.archivosUsuarios = archivosUsuarios;
        this.archivosCambioClave = archivosCambioClave;
        this.archivosCambioUsuario = archivosCambioUsuario;
        this.archivosSalir = archivosSalir;
        this.movimientos = movimientos;
        this.movimientosNuevaFactura = movimientosNuevaFactura;
        this.movimientosReporteFactura = movimientosReporteFactura;
        this.movimientosInventarios = movimientosInventarios;
        this.movimientosAbrirTurno = movimientosAbrirTurno;
        this.movimientosCerrarTurno = movimientosCerrarTurno;
        this.movimientosDeuda = movimientosDeuda;
    }

    

    public String getArchivos() {
        return archivos;
    }

    public void setArchivos(String archivos) {
        this.archivos = archivos;
    }

    public String getArchivosClientes() {
        return archivosClientes;
    }

    public void setArchivosClientes(String archivosClientes) {
        this.archivosClientes = archivosClientes;
    }

    public String getArchivosProductos() {
        return archivosProductos;
    }

    public void setArchivosProductos(String archivosProductos) {
        this.archivosProductos = archivosProductos;
    }

    public String getArchivosUsuarios() {
        return archivosUsuarios;
    }

    public void setArchivosUsuarios(String archivosUsuarios) {
        this.archivosUsuarios = archivosUsuarios;
    }

    public String getArchivosCambioClave() {
        return archivosCambioClave;
    }

    public void setArchivosCambioClave(String archivosCambioClave) {
        this.archivosCambioClave = archivosCambioClave;
    }

    public String getArchivosCambioUsuario() {
        return archivosCambioUsuario;
    }

    public void setArchivosCambioUsuario(String archivosCambioUsuario) {
        this.archivosCambioUsuario = archivosCambioUsuario;
    }

    public String getArchivosSalir() {
        return archivosSalir;
    }

    public void setArchivosSalir(String archivosSalir) {
        this.archivosSalir = archivosSalir;
    }

    public String getMovimientos() {
        return movimientos;
    }

    public void setMovimientos(String movimientos) {
        this.movimientos = movimientos;
    }

    public String getMovimientosNuevaFactura() {
        return movimientosNuevaFactura;
    }

    public void setMovimientosNuevaFactura(String movimientosNuevaFactura) {
        this.movimientosNuevaFactura = movimientosNuevaFactura;
    }

    public String getMovimientosReporteFactura() {
        return movimientosReporteFactura;
    }

    public void setMovimientosReporteFactura(String movimientosReporteFactura) {
        this.movimientosReporteFactura = movimientosReporteFactura;
    }

    public String getMovimientosInventarios() {
        return movimientosInventarios;
    }

    public void setMovimientosInventarios(String movimientosInventarios) {
        this.movimientosInventarios = movimientosInventarios;
    }

    public String getMovimientosAbrirTurno() {
        return movimientosAbrirTurno;
    }

    public void setMovimientosAbrirTurno(String movimientosAbrirTurno) {
        this.movimientosAbrirTurno = movimientosAbrirTurno;
    }

    public String getMovimientosCerrarTurno() {
        return movimientosCerrarTurno;
    }

    public void setMovimientosCerrarTurno(String movimientosCerrarTurno) {
        this.movimientosCerrarTurno = movimientosCerrarTurno;
    }

    public String getMovimientosDeuda() {
        return movimientosDeuda;
    }

    public void setMovimientosDeuda(String movimientosDeuda) {
        this.movimientosDeuda = movimientosDeuda;
    }    
}
