package clases;
import java.util.Date;
public class Cliente {
    private String idCliente;
    private String nombres;
    private String apellidos;
    private String ciudad;
    private String direccion;
    private String telefono;
    private Date fechaNacimiento;
    private Date fechaIngreso;
    private float credito;
    private float deudaActual;
    private byte estado;

    public Cliente(String idCliente, String nombres, String apellidos, 
            String ciudad, String direccion, String telefono, 
            Date fechaNacimiento, Date fechaIngreso, float credito, 
            byte estado) {
        System.out.println("+Clase Cliente");
        this.idCliente = idCliente;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.ciudad = ciudad;
        this.direccion = direccion;
        this.telefono = telefono;
        this.fechaNacimiento = fechaNacimiento;
        this.fechaIngreso = fechaIngreso;
        this.credito = credito;
        this.estado = estado;
    }
    
    public String getIdCliente() {
        if(idCliente.equals('0')){
            System.out.println("Codigo del cliente nuevo....");
            idCliente = "00000000000";
        }
        return idCliente.replace("-", "").replace(" ", "").trim();
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }
    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }
    
    public float getCredito(){
        return credito;
    }
    
    public void setCredito(float credito){
        this.credito = credito;
    }
    
    public float getDeudaActual(){
        return deudaActual;
    }
    
    public void setDeudaActual(float deudaActual){
        this.deudaActual = deudaActual;
    }

    public byte getEstado() {
        return estado;
    }

    public void setEstado(byte estado) {
        this.estado = estado;
    }    
}
