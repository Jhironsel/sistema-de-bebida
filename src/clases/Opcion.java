package clases;
public class Opcion {
    private String  intervalo;
    private final String Valor, descripcion;
    
    public Opcion(String Valor, String descripcion) {
        this.Valor = Valor;
        this.descripcion = descripcion;
    }

    public Opcion(String Valor, String descripcion, String intervalo) {
        this.Valor = Valor;
        this.descripcion = descripcion;
        this.intervalo = intervalo;
    }

    public String getIntervalo() {
        return intervalo;
    }

    public String getValor() {
        return Valor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    @Override
    public String toString() {
        return descripcion;
    }
}
