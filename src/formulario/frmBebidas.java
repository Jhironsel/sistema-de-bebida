package formulario;

import clases.Datos;
import clases.DefaultTableCellHeaderRenderer;
import clases.Factura;
import clases.Opcion;
import clases.Utilidades;
import static clases.Utilidades.LOGGER;
import static formulario.frmPrincipal.mnuMovimientosNuevaFactura;
import hilos.hiloImpresionFactura;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

public final class frmBebidas extends javax.swing.JInternalFrame implements
        Runnable, ActionListener {

    private Datos datos;
    private String idUsuario, idCliente = "00000000000", hora, minutos, segundos, ampm,
            nombreCliente = "", idClienteTemporal = "00000000000";
    private final String titulos[] = {"Cantidad", "Descripcion", "Montos"};
    private int turno;
    private final Thread h1;
    private Thread ct;
    private double valorCredito = 0;
    private JButton btn, boton;
    private ActionEvent e1;
    private boolean temporal;
    private final List<Factura> facturas;
    private DefaultTableModel miTabla = new DefaultTableModel(null, titulos);
    private final Object[] registro = new Object[3];
    private final Properties propiedad;
    private final DefaultTableCellRenderer tcr;

    private Properties getPropiedad() {
        return propiedad;
    }

    private void setPropiedad() {
        String file = System.getProperty("user.dir") + "/Propeties/propiedades.properties";
        try {
//            propiedad.load(new FileInputStream(path));
            getPropiedad().load(new FileReader(file));
        } catch (FileNotFoundException ex) {
            LOGGER.getLogger(frmPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            LOGGER.getLogger(frmPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean isTemporal() {
        return temporal;
    }

    public void setTemporal(boolean temporal) {
        this.temporal = temporal;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public int getTurno() {
        return turno;
    }

    public void setTurno(int turno) {
        this.turno = turno;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    private void setDatos(Datos misDatos) {
        this.datos = misDatos;
    }

    private Datos getDatos() {
        return datos;
    }

    public frmBebidas(Datos datos) {
        System.out.println("Clase Bebidas");
        initComponents();
        tcr = new DefaultTableCellHeaderRenderer();
        setDatos(datos);
        propiedad = new Properties();
        setPropiedad();

        facturas = new ArrayList<>();

        this.temporal = false;
        h1 = new Thread(this);
        h1.start();

        btnGrupoPago.add(rbtContado);
        btnGrupoPago.add(rbtCredito);

        Utilidades.clickOnKey(btnCancelar, "cancelar", KeyEvent.VK_F12);
        Utilidades.clickOnKey(btnGrabar, "factura", KeyEvent.VK_F10);
        Utilidades.clickOnKey(btnEspera, "temporar", KeyEvent.VK_F5);
        Utilidades.clickOnKey(btnBuscarEspera, "temporarBuscar", KeyEvent.VK_F6);
        Utilidades.clickOnKey(btnGastos, "gastos", KeyEvent.VK_F7);
        Utilidades.clickOnKey(btnPagoDeuda, "pagoDeuda", KeyEvent.VK_F8);
        Utilidades.clickOnKey(btnImpresionUltima, "impresionUltima", KeyEvent.VK_F9);

        categoriaR();
    }//Metodo Constructor...

    @Override
    public void run() {
        ct = Thread.currentThread();
        while (ct == h1) {
            tiempo();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                JOptionPane.showMessageDialog(this, e.getLocalizedMessage());
            }
            txtHora.setText(hora + ":" + minutos + ":" + segundos + " " + ampm);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        btnGrupoPago = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        btnEspera = new javax.swing.JButton();
        btnBuscarEspera = new javax.swing.JButton();
        btnGastos = new javax.swing.JButton();
        btnPagoDeuda = new javax.swing.JButton();
        btnImpresionUltima = new javax.swing.JButton();
        btnGrabar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnCancelar1 = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        cbTodos = new javax.swing.JCheckBox();
        cbTodosProductos = new javax.swing.JCheckBox();
        cbPrevista = new javax.swing.JCheckBox();
        jScrollPane3 = new javax.swing.JScrollPane();
        jPanel8 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jpCliente = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        rbtContado = new javax.swing.JRadioButton();
        rbtCredito = new javax.swing.JRadioButton();
        jPanel7 = new javax.swing.JPanel();
        btnBuscarCliente = new javax.swing.JButton();
        cmbCliente = new javax.swing.JComboBox();
        JlCantidad1 = new javax.swing.JLabel();
        JlCantidad2 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tblDetalle = new JTable(){
            @Override
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false; //Las celdas no son editables.
            }
        };
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txtTotalCantidad = new javax.swing.JFormattedTextField();
        jLabel7 = new javax.swing.JLabel();
        txtPrecio = new javax.swing.JFormattedTextField();
        jLabel6 = new javax.swing.JLabel();
        txtTotalValor = new javax.swing.JFormattedTextField();
        jPanel10 = new javax.swing.JPanel();
        jpCategoria = new javax.swing.JPanel();
        jpBusqueda = new javax.swing.JPanel();
        txtCriterio = new javax.swing.JTextField();
        cbCriterio = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jpProductos = new javax.swing.JPanel();
        jPanel11 = new javax.swing.JPanel();
        jPanel13 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txtIdFactura = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtUsuario = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txtTurno = new javax.swing.JTextField();
        jPanel12 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtFecha = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txtHora = new javax.swing.JTextField();

        setClosable(true);
        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Sistema de Facturacion Sophia");
        setToolTipText("");
        setAutoscrolls(true);
        setDoubleBuffered(true);
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameClosing(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameActivated(evt);
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        jPanel5.setAutoscrolls(true);
        jPanel5.setMinimumSize(new java.awt.Dimension(250, 68));
        jPanel5.setLayout(new java.awt.GridLayout(1, 7, 3, 3));

        btnEspera.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnEspera.setForeground(new java.awt.Color(0, 0, 0));
        btnEspera.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/clock 32 x 32.png"))); // NOI18N
        btnEspera.setMnemonic('T');
        btnEspera.setText("<html><center>Temporal<br>F5</center></html>");
        btnEspera.setToolTipText("Poner una factura en espera y seguir con la otra");
        btnEspera.setBorder(null);
        btnEspera.setFocusPainted(false);
        btnEspera.setFocusable(false);
        btnEspera.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnEspera.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, btnEspera, org.jdesktop.beansbinding.ELProperty.create("${selected}"), btnEspera, org.jdesktop.beansbinding.BeanProperty.create("actionCommand"));
        bindingGroup.addBinding(binding);

        btnEspera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEsperaActionPerformed(evt);
            }
        });
        jPanel5.add(btnEspera);

        btnBuscarEspera.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnBuscarEspera.setForeground(new java.awt.Color(0, 0, 0));
        btnBuscarEspera.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Buscar3 32 x 32.png"))); // NOI18N
        btnBuscarEspera.setText("<html><center>Temporal<br>F6</center></html>");
        btnBuscarEspera.setToolTipText("Buscar factura que estan en espera");
        btnBuscarEspera.setBorder(null);
        btnBuscarEspera.setFocusPainted(false);
        btnBuscarEspera.setFocusable(false);
        btnBuscarEspera.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnBuscarEspera.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnBuscarEspera.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarEsperaActionPerformed(evt);
            }
        });
        jPanel5.add(btnBuscarEspera);

        btnGastos.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnGastos.setForeground(new java.awt.Color(0, 0, 0));
        btnGastos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/gasto 32 x 32.png"))); // NOI18N
        btnGastos.setText("<html><center>Gasto<br>F7</center></html>");
        btnGastos.setToolTipText("Pagar gastos del Negocio");
        btnGastos.setBorder(null);
        btnGastos.setFocusPainted(false);
        btnGastos.setFocusable(false);
        btnGastos.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnGastos.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnGastos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGastosActionPerformed(evt);
            }
        });
        jPanel5.add(btnGastos);

        btnPagoDeuda.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnPagoDeuda.setForeground(new java.awt.Color(0, 0, 0));
        btnPagoDeuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Dinero 32 x 32.png"))); // NOI18N
        btnPagoDeuda.setText("<html><center>Deuda<br>F8</center></html>");
        btnPagoDeuda.setToolTipText("Buscar Deuda");
        btnPagoDeuda.setBorder(null);
        btnPagoDeuda.setFocusPainted(false);
        btnPagoDeuda.setFocusable(false);
        btnPagoDeuda.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnPagoDeuda.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnPagoDeuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPagoDeudaActionPerformed(evt);
            }
        });
        jPanel5.add(btnPagoDeuda);

        btnImpresionUltima.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnImpresionUltima.setForeground(new java.awt.Color(0, 0, 0));
        btnImpresionUltima.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/epson32x32.png"))); // NOI18N
        btnImpresionUltima.setText("<html><center>Ultima<br>F9</center></html>");
        btnImpresionUltima.setToolTipText("Ultima Facura");
        btnImpresionUltima.setBorder(null);
        btnImpresionUltima.setFocusPainted(false);
        btnImpresionUltima.setFocusable(false);
        btnImpresionUltima.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnImpresionUltima.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnImpresionUltima.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImpresionUltimaActionPerformed(evt);
            }
        });
        jPanel5.add(btnImpresionUltima);

        btnGrabar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnGrabar.setForeground(new java.awt.Color(0, 0, 0));
        btnGrabar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Aceptar 32 x 32.png"))); // NOI18N
        btnGrabar.setText("<html><center>Terminar<br>F10</center></html>");
        btnGrabar.setToolTipText("Confirmar Factura");
        btnGrabar.setAutoscrolls(true);
        btnGrabar.setBorder(null);
        btnGrabar.setFocusPainted(false);
        btnGrabar.setFocusable(false);
        btnGrabar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnGrabar.setOpaque(false);
        btnGrabar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnGrabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGrabarActionPerformed(evt);
            }
        });
        jPanel5.add(btnGrabar);

        btnCancelar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnCancelar.setForeground(new java.awt.Color(0, 0, 0));
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Cancelar 32 x 32.png"))); // NOI18N
        btnCancelar.setText("<html><center>Limpiar<br>F12</center></html>");
        btnCancelar.setToolTipText("Cancelar Factura Actual");
        btnCancelar.setBorder(null);
        btnCancelar.setFocusPainted(false);
        btnCancelar.setFocusable(false);
        btnCancelar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCancelar.setOpaque(false);
        btnCancelar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        jPanel5.add(btnCancelar);

        btnCancelar1.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnCancelar1.setForeground(new java.awt.Color(0, 0, 0));
        btnCancelar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/devolucion32x32.png"))); // NOI18N
        btnCancelar1.setText("Devoluciones");
        btnCancelar1.setToolTipText("Cancelar Factura Actual");
        btnCancelar1.setBorder(null);
        btnCancelar1.setFocusPainted(false);
        btnCancelar1.setFocusable(false);
        btnCancelar1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnCancelar1.setOpaque(false);
        btnCancelar1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnCancelar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelar1ActionPerformed(evt);
            }
        });
        jPanel5.add(btnCancelar1);

        jPanel6.setBackground(new java.awt.Color(255, 102, 51));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Opciones de busqueda", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 12), new java.awt.Color(0, 0, 0))); // NOI18N
        jPanel6.setAutoscrolls(true);
        jPanel6.setMinimumSize(new java.awt.Dimension(0, 54));
        jPanel6.setLayout(new java.awt.GridLayout(2, 0));

        cbTodos.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        cbTodos.setText("Todas las Categorias");
        cbTodos.setBorder(null);
        cbTodos.setFocusPainted(false);
        cbTodos.setFocusable(false);
        cbTodos.setMinimumSize(new java.awt.Dimension(0, 16));
        cbTodos.setOpaque(false);
        cbTodos.setPreferredSize(new java.awt.Dimension(0, 16));
        cbTodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbTodosActionPerformed(evt);
            }
        });
        jPanel6.add(cbTodos);

        cbTodosProductos.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        cbTodosProductos.setText("Todos los Productos");
        cbTodosProductos.setBorder(null);
        cbTodosProductos.setFocusPainted(false);
        cbTodosProductos.setFocusable(false);
        cbTodosProductos.setMinimumSize(new java.awt.Dimension(0, 16));
        cbTodosProductos.setOpaque(false);
        cbTodosProductos.setPreferredSize(new java.awt.Dimension(0, 16));
        cbTodosProductos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbTodosProductosActionPerformed(evt);
            }
        });
        jPanel6.add(cbTodosProductos);

        cbPrevista.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        cbPrevista.setSelected(true);
        cbPrevista.setText("Prevista");
        cbPrevista.setBorder(null);
        cbPrevista.setMinimumSize(new java.awt.Dimension(0, 16));
        cbPrevista.setOpaque(false);
        cbPrevista.setPreferredSize(new java.awt.Dimension(0, 16));
        jPanel6.add(cbPrevista);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, 673, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 333, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );

        jScrollPane3.setAutoscrolls(true);
        jScrollPane3.setMaximumSize(new java.awt.Dimension(381, 100));
        jScrollPane3.setMinimumSize(new java.awt.Dimension(0, 100));
        jScrollPane3.setPreferredSize(new java.awt.Dimension(381, 100));

        jpCliente.setBackground(new java.awt.Color(0, 153, 255));
        jpCliente.setBorder(javax.swing.BorderFactory.createTitledBorder("Cliente"));
        jpCliente.setAlignmentX(0.0F);
        jpCliente.setAlignmentY(0.0F);
        jpCliente.setMaximumSize(new java.awt.Dimension(265, 135));
        jpCliente.setMinimumSize(new java.awt.Dimension(30, 30));
        jpCliente.setPreferredSize(new java.awt.Dimension(265, 135));

        jPanel9.setBackground(new java.awt.Color(0, 102, 255));
        jPanel9.setMaximumSize(new java.awt.Dimension(153, 35));
        jPanel9.setMinimumSize(new java.awt.Dimension(153, 35));
        jPanel9.setName(""); // NOI18N
        jPanel9.setPreferredSize(new java.awt.Dimension(153, 35));

        jPanel4.setBackground(new java.awt.Color(0, 153, 255));
        jPanel4.setMaximumSize(new java.awt.Dimension(153, 40));
        jPanel4.setName(""); // NOI18N
        jPanel4.setPreferredSize(new java.awt.Dimension(153, 40));

        btnGrupoPago.add(rbtContado);
        rbtContado.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        rbtContado.setForeground(new java.awt.Color(1, 1, 1));
        rbtContado.setSelected(true);
        rbtContado.setText("Contado");
        rbtContado.setBorder(null);
        rbtContado.setFocusPainted(false);
        rbtContado.setFocusable(false);
        rbtContado.setMaximumSize(new java.awt.Dimension(363, 26));
        rbtContado.setOpaque(false);
        rbtContado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtContadoActionPerformed(evt);
            }
        });

        btnGrupoPago.add(rbtCredito);
        rbtCredito.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        rbtCredito.setForeground(new java.awt.Color(1, 1, 1));
        rbtCredito.setText("Credito");
        rbtCredito.setBorder(null);
        rbtCredito.setFocusPainted(false);
        rbtCredito.setFocusable(false);
        rbtCredito.setMaximumSize(new java.awt.Dimension(363, 26));
        rbtCredito.setOpaque(false);
        rbtCredito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtCreditoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(rbtContado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(rbtCredito, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(rbtContado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(rbtCredito, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        jPanel7.setBackground(new java.awt.Color(0, 153, 255));
        jPanel7.setMaximumSize(new java.awt.Dimension(153, 40));
        jPanel7.setPreferredSize(new java.awt.Dimension(153, 40));

        btnBuscarCliente.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnBuscarCliente.setForeground(new java.awt.Color(0, 0, 0));
        btnBuscarCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Cliente 32 x 32.png"))); // NOI18N
        btnBuscarCliente.setMnemonic('c');
        btnBuscarCliente.setText("Cliente");
        btnBuscarCliente.setToolTipText("Busca un Cliente");
        btnBuscarCliente.setBorder(null);
        btnBuscarCliente.setFocusPainted(false);
        btnBuscarCliente.setFocusable(false);
        btnBuscarCliente.setMaximumSize(new java.awt.Dimension(123, 35));
        btnBuscarCliente.setMinimumSize(new java.awt.Dimension(123, 35));
        btnBuscarCliente.setPreferredSize(new java.awt.Dimension(123, 35));
        btnBuscarCliente.setRequestFocusEnabled(false);
        btnBuscarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarClienteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnBuscarCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(btnBuscarCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 127, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                .addGap(0, 0, 0))
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        cmbCliente.setFont(new java.awt.Font("Courier New", 0, 14)); // NOI18N
        cmbCliente.setForeground(new java.awt.Color(0, 0, 255));
        cmbCliente.setEnabled(false);
        cmbCliente.setLightWeightPopupEnabled(false);
        cmbCliente.setMaximumSize(new java.awt.Dimension(363, 26));
        cmbCliente.setOpaque(false);
        cmbCliente.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbClienteItemStateChanged(evt);
            }
        });
        cmbCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbClienteActionPerformed(evt);
            }
        });

        JlCantidad1.setFont(new java.awt.Font("Monospaced", 1, 20)); // NOI18N
        JlCantidad1.setForeground(new java.awt.Color(1, 1, 1));
        JlCantidad1.setText("w");
        JlCantidad1.setToolTipText("");
        JlCantidad1.setMaximumSize(new java.awt.Dimension(360, 24));

        JlCantidad2.setFont(new java.awt.Font("Monospaced", 1, 20)); // NOI18N
        JlCantidad2.setForeground(new java.awt.Color(1, 1, 1));
        JlCantidad2.setText("w");
        JlCantidad2.setToolTipText("");
        JlCantidad2.setMaximumSize(new java.awt.Dimension(360, 26));

        javax.swing.GroupLayout jpClienteLayout = new javax.swing.GroupLayout(jpCliente);
        jpCliente.setLayout(jpClienteLayout);
        jpClienteLayout.setHorizontalGroup(
            jpClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(JlCantidad1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jpClienteLayout.createSequentialGroup()
                .addGroup(jpClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cmbCliente, 0, 255, Short.MAX_VALUE)
                    .addComponent(JlCantidad2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, 255, Short.MAX_VALUE)
        );
        jpClienteLayout.setVerticalGroup(
            jpClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpClienteLayout.createSequentialGroup()
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(cmbCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(JlCantidad1, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(JlCantidad2, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        tblDetalle.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        tblDetalle.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3"
            }
        ));
        tblDetalle.setShowVerticalLines(false);
        tblDetalle.getTableHeader().setReorderingAllowed(false);
        tblDetalle.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblDetalleMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(tblDetalle);

        jPanel3.setBackground(new java.awt.Color(0, 153, 255));
        jPanel3.setLayout(new java.awt.GridLayout(3, 2, 8, 8));

        jLabel5.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Cantidad:");
        jPanel3.add(jLabel5);

        txtTotalCantidad.setEditable(false);
        txtTotalCantidad.setBorder(null);
        txtTotalCantidad.setForeground(new java.awt.Color(0, 51, 255));
        txtTotalCantidad.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("#0.00"))));
        txtTotalCantidad.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtTotalCantidad.setToolTipText("Total Neto de la factura actual");
        txtTotalCantidad.setFont(new java.awt.Font("Ubuntu", 1, 20)); // NOI18N
        txtTotalCantidad.setOpaque(false);
        jPanel3.add(txtTotalCantidad);

        jLabel7.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Precio:");
        jPanel3.add(jLabel7);

        txtPrecio.setEditable(false);
        txtPrecio.setBorder(null);
        txtPrecio.setForeground(new java.awt.Color(0, 51, 255));
        txtPrecio.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("¤#,##0.00"))));
        txtPrecio.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtPrecio.setToolTipText("Precio del articulo seleccionado");
        txtPrecio.setFont(new java.awt.Font("Ubuntu", 1, 20)); // NOI18N
        txtPrecio.setOpaque(false);
        jPanel3.add(txtPrecio);

        jLabel6.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Total Neto:");
        jPanel3.add(jLabel6);

        txtTotalValor.setEditable(false);
        txtTotalValor.setBorder(null);
        txtTotalValor.setForeground(new java.awt.Color(0, 51, 255));
        txtTotalValor.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("¤#,##0.00"))));
        txtTotalValor.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtTotalValor.setToolTipText("Total Neto de la factura actual");
        txtTotalValor.setFont(new java.awt.Font("Ubuntu", 1, 20)); // NOI18N
        txtTotalValor.setOpaque(false);
        jPanel3.add(txtTotalValor);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jpCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        jpCategoria.setBackground(new java.awt.Color(255, 102, 51));
        jpCategoria.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Categoria", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 12), new java.awt.Color(0, 0, 0))); // NOI18N
        jpCategoria.setAutoscrolls(true);
        jpCategoria.setMinimumSize(new java.awt.Dimension(0, 32));

        jpBusqueda.setBackground(new java.awt.Color(255, 102, 51));
        jpBusqueda.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Opciones de busqueda", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 1, 12), new java.awt.Color(0, 0, 0))); // NOI18N
        jpBusqueda.setMinimumSize(new java.awt.Dimension(0, 45));
        jpBusqueda.setPreferredSize(new java.awt.Dimension(360, 45));
        java.awt.FlowLayout flowLayout5 = new java.awt.FlowLayout(java.awt.FlowLayout.LEFT, 5, 0);
        flowLayout5.setAlignOnBaseline(true);
        jpBusqueda.setLayout(flowLayout5);

        txtCriterio.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        txtCriterio.setAutoscrolls(false);
        txtCriterio.setBorder(null);
        txtCriterio.setDoubleBuffered(true);
        txtCriterio.setFocusCycleRoot(true);
        txtCriterio.setFocusTraversalPolicy(null);
        txtCriterio.setFocusTraversalPolicyProvider(true);
        txtCriterio.setMinimumSize(new java.awt.Dimension(0, 25));
        txtCriterio.setOpaque(false);
        txtCriterio.setPreferredSize(new java.awt.Dimension(800, 25));
        txtCriterio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCriterioActionPerformed(evt);
            }
        });
        jpBusqueda.add(txtCriterio);

        cbCriterio.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        cbCriterio.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Opcion Busqueda", "Codigo", "Descripcion", "En Detalle" }));
        cbCriterio.setSelectedIndex(1);
        cbCriterio.setFocusable(false);
        cbCriterio.setMinimumSize(new java.awt.Dimension(150, 25));
        cbCriterio.setOpaque(false);
        cbCriterio.setPreferredSize(new java.awt.Dimension(160, 25));
        jpBusqueda.add(cbCriterio);

        jLabel4.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(0, 0, 0));
        jLabel4.setText("\",\" Codigo \".\" Descripcion \"-\" Detalle Factura");
        jLabel4.setMinimumSize(new java.awt.Dimension(0, 16));
        jpBusqueda.add(jLabel4);

        jpProductos.setBackground(new java.awt.Color(255, 102, 51));
        jpProductos.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Productos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Ubuntu", 0, 14))); // NOI18N
        jpProductos.setPreferredSize(new java.awt.Dimension(20, 1500));
        java.awt.FlowLayout flowLayout1 = new java.awt.FlowLayout();
        flowLayout1.setAlignOnBaseline(true);
        jpProductos.setLayout(flowLayout1);
        jScrollPane4.setViewportView(jpProductos);

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jpBusqueda, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jpCategoria, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane4))
                .addGap(0, 0, 0))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jpCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jpBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jScrollPane3.setViewportView(jPanel8);

        jPanel11.setMinimumSize(new java.awt.Dimension(0, 35));
        jPanel11.setLayout(new java.awt.GridLayout(1, 2, 8, 8));

        jPanel13.setMinimumSize(new java.awt.Dimension(0, 35));
        jPanel13.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        jLabel3.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Fact. no.:");
        jLabel3.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel3.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jLabel3.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        jPanel13.add(jLabel3);

        txtIdFactura.setEditable(false);
        txtIdFactura.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        txtIdFactura.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtIdFactura.setAutoscrolls(false);
        txtIdFactura.setDoubleBuffered(true);
        txtIdFactura.setFocusCycleRoot(true);
        txtIdFactura.setFocusTraversalPolicy(null);
        txtIdFactura.setFocusTraversalPolicyProvider(true);
        txtIdFactura.setName(""); // NOI18N
        jPanel13.add(txtIdFactura);

        jLabel2.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Usuario:");
        jLabel2.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel2.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jLabel2.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        jPanel13.add(jLabel2);

        txtUsuario.setEditable(false);
        txtUsuario.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        txtUsuario.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtUsuario.setAutoscrolls(false);
        txtUsuario.setDoubleBuffered(true);
        txtUsuario.setFocusCycleRoot(true);
        txtUsuario.setFocusTraversalPolicy(null);
        txtUsuario.setFocusTraversalPolicyProvider(true);
        jPanel13.add(txtUsuario);

        jLabel9.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Turno:");
        jLabel9.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel9.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jLabel9.setVerticalTextPosition(javax.swing.SwingConstants.TOP);
        jPanel13.add(jLabel9);

        txtTurno.setEditable(false);
        txtTurno.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        txtTurno.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtTurno.setAutoscrolls(false);
        txtTurno.setDoubleBuffered(true);
        txtTurno.setFocusCycleRoot(true);
        txtTurno.setFocusTraversalPolicyProvider(true);
        jPanel13.add(txtTurno);

        jPanel11.add(jPanel13);

        jPanel12.setMinimumSize(new java.awt.Dimension(0, 35));
        jPanel12.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT));

        jLabel1.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(1, 1, 1));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("Fecha:");
        jPanel12.add(jLabel1);

        txtFecha.setEditable(false);
        txtFecha.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        txtFecha.setDoubleBuffered(true);
        txtFecha.setFocusCycleRoot(true);
        txtFecha.setFocusTraversalPolicy(null);
        txtFecha.setFocusTraversalPolicyProvider(true);
        jPanel12.add(txtFecha);

        jLabel8.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(1, 1, 1));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Hora:");
        jPanel12.add(jLabel8);

        txtHora.setEditable(false);
        txtHora.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        txtHora.setDoubleBuffered(true);
        txtHora.setFocusCycleRoot(true);
        txtHora.setFocusTraversalPolicy(null);
        txtHora.setFocusTraversalPolicyProvider(true);
        txtHora.setOpaque(false);
        jPanel12.add(txtHora);

        jPanel11.add(jPanel12);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, 1006, Short.MAX_VALUE)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 510, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        bindingGroup.bind();

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void btnBuscarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarClienteActionPerformed
        frmBusquedaCliente miBusqueda = new frmBusquedaCliente(null, closable);
        miBusqueda.setDatos(getDatos());
        miBusqueda.setLocationRelativeTo(null);
        miBusqueda.setVisible(true);

        if (miBusqueda.getRespuesta().equals("")) {
            return;
        }

        for (int i = 0; i <= cmbCliente.getItemCount() + 1; i++) {
            if (((Opcion) cmbCliente.getItemAt(i)).getValor().equals(miBusqueda.getRespuesta())) {
                cmbCliente.setSelectedIndex(i);
                return;
            }
        }
    }//GEN-LAST:event_btnBuscarClienteActionPerformed
    private void btnPagoDeudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPagoDeudaActionPerformed
        int resp = JOptionPane.showOptionDialog(this, "<html><big>Tipo de Deuda?</big></html",
                "Elegir el tipo de deuda!!!",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null, new String[]{"<html><big>Credito</big></html>",
                    "<html><big>Externa</big></html",
                    "<html><big>Cancelar</big></html"}, 0);

        if (resp == 0) {
            System.out.println("Abriendo el formulario para Deuda a Credito");
            frmCobrosClientes miPagoCliente = new frmCobrosClientes(null, closable);
            miPagoCliente.setDatos(getDatos());
            miPagoCliente.setNombreCajero(getIdUsuario());
            miPagoCliente.setIdTurno(getTurno());
            miPagoCliente.setLocationRelativeTo(null);
            miPagoCliente.setVisible(true);
            System.out.println("Completo metodo btnPagoDeuda");
            return;
        }
        if (resp == 1) {
            System.out.println("Abriendo el formulario para Deuda Externa");
            frmCobrosDeudas miPagoDeuda = new frmCobrosDeudas(null, closable);
            miPagoDeuda.setDatos(getDatos());
            miPagoDeuda.setNombreCajero(getIdUsuario());
            miPagoDeuda.setIdTurno(getTurno());
            miPagoDeuda.setLocationRelativeTo(null);
            miPagoDeuda.setVisible(true);
            System.out.println("Hecho");
        }
    }//GEN-LAST:event_btnPagoDeudaActionPerformed
    private void formInternalFrameActivated(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameActivated
        Date now = new Date(System.currentTimeMillis());
        SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
        txtFecha.setText(date.format(now));
        txtHora.setText("00:00:00 AM");
        rbtContado.doClick();
        txtUsuario.setText(getIdUsuario());
        txtTurno.setText("" + getTurno());
        txtCriterio.requestFocusInWindow();
    }//GEN-LAST:event_formInternalFrameActivated
    private void cbTodosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbTodosActionPerformed
        jpCategoria = new javax.swing.JPanel();
        jpCategoria.setBorder(javax.swing.BorderFactory.createTitledBorder("Categoria"));
        jpCategoria.setAutoscrolls(true);
        jpCategoria.setMaximumSize(null);
        //getPropiedad().setProperty("Yiro", "Sophia");
        categoriaR();

        getPropiedad().setProperty("cbTodosUsuario", "" + cbTodos.isSelected());
        try {
            getPropiedad().store(new FileWriter(
                    System.getProperty("user.dir")
                    + "/Propeties/propiedades.properties"), "Comentario");
        } catch (IOException ex) {
            LOGGER.getLogger(frmBebidas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_cbTodosActionPerformed
    private void btnGrabarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGrabarActionPerformed
        Integer idFactura = Integer.parseInt(txtIdFactura.getText());

        //Comienzo de Validaciones
        if (rbtCredito.isSelected() && cmbCliente.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(this,
                    "Debe Seleccinar un Cliente...");
            cmbCliente.requestFocusInWindow();
            return;
        }
        if (Utilidades.controlDouble(txtTotalCantidad.getValue()) == 0.0) {
            JOptionPane.showMessageDialog(this,
                    "Debe Ingresar Detalle de la Factura...");
            txtCriterio.requestFocusInWindow();
            return;
        }

        double total = Utilidades.controlDouble(txtTotalValor.getValue());

        if (total > valorCredito && rbtCredito.isSelected()) {
            int resp = JOptionPane.showConfirmDialog(this, "Limite de credito Excedido! \nDesea continuar?",
                    "Autorización de venta sobre el credito", JOptionPane.YES_NO_OPTION);
            if (resp == 1) {
                return;
            } else {
                frmAutorizacion miAut = new frmAutorizacion(null, true);
                miAut.setDatos(getDatos());
                miAut.setLocationRelativeTo(null);
                miAut.setVisible(true);
                if (!miAut.isAceptado()) {
                    JOptionPane.showMessageDialog(this, "Usuario no valido");
                    return;
                }
            }
        }
        frmCalculoEfectivo miEfe = new frmCalculoEfectivo(null, true,
                total, rbtCredito.isSelected());

        miEfe.setLocationRelativeTo(null);
        miEfe.setVisible(true);

        if (miEfe.getResp() == 0) {
            return;
        }

        //Adicionamos un consecutivo a la Factura oh numero d Factura proxima
        String estado = "p";
        if (rbtCredito.isSelected()) {
            estado = "c";
        }
        if (isTemporal()) {
            if (!getDatos().modificarFactura(idFactura,
                    rbtCredito.isSelected() ? 1 : 0,
                    Utilidades.controlDouble(miEfe.txtEfectivo.getValue()),
                    Utilidades.controlDouble(miEfe.txtDevuelta.getValue()),
                    estado)) {
                JOptionPane.showMessageDialog(this, "Ocurrio un error factura Temporal");
                return;
            } else {
                for (int i = 0; i < facturas.size(); i++) {
                    if (!getDatos().agregarOrInsertarDetalleFactura(
                            facturas.get(i).getIdFactura(),
                            i + 1,
                            facturas.get(i).getIdProducto(),
                            facturas.get(i).getPrecio(),
                            facturas.get(i).getCantidad())) {
                        System.out.println(getDatos().borrarFactura(idFactura));
                        JOptionPane.showMessageDialog(this, "Ocurrio un error Temporal Detallle");
                        return;
                    }
                }

            }
        } else {
            if (!getDatos().agregarFacturaNombre(idFactura,
                    ((Opcion) cmbCliente.getSelectedItem()).getValor(),
                    new Date(), getIdUsuario(), (rbtCredito.isSelected() ? 1 : 0),
                    Utilidades.controlDouble(miEfe.txtEfectivo.getValue()),
                    Utilidades.controlDouble(miEfe.txtDevuelta.getValue()),
                    getTurno(), estado, "")) {
                JOptionPane.showMessageDialog(this, "Esta compra no se ha registrado...");
            } else {
                //Detalle de Factura////////////////////////////////////////////////
                for (int i = 0; i < facturas.size(); i++) {
                    System.out.println("Cantidad de:" + facturas.size() + " Valor de i: " + i);
                    if (!getDatos().agregarDetalleFactura(
                            facturas.get(i).getIdFactura(),
                            i + 1,
                            facturas.get(i).getIdProducto(),
                            facturas.get(i).getPrecio(),
                            facturas.get(i).getCantidad())) {
                        System.out.println(getDatos().borrarFactura(idFactura));
                        JOptionPane.showMessageDialog(this, "Esta compra no se ha registrado...");
                        return;
                    }
                }
            }
        }

        txtIdFactura.setText("" + getDatos().getNumFac(getIdUsuario(),
                getTurno()));

        Map parametros = new HashMap();
        parametros.put("idFactura", idFactura);
        hiloImpresionFactura impresionFactura = new hiloImpresionFactura(
                getDatos(),
                cbPrevista.isSelected(),
                rbtCredito.isSelected(),
                System.getProperty("user.dir") + "/Reportes/factura.jasper",
                parametros);
        impresionFactura.start();

        limpiarTabla();
        totales();

        rbtContado.doClick();
        setTemporal(false);
        facturas.clear();
        nombreCliente = "";
        idClienteTemporal = "00000000000";
    }//GEN-LAST:event_btnGrabarActionPerformed
    private void txtCriterioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCriterioActionPerformed
        if (txtCriterio.getText().trim().equals(",")) {//Validaciones....
            cbCriterio.setSelectedIndex(1);
            txtCriterio.setText("");
            return;
        }
        if (txtCriterio.getText().trim().equals(".")) {
            cbCriterio.setSelectedIndex(2);
            txtCriterio.setText("");
            return;
        }
        if (txtCriterio.getText().trim().equals("-")) {
            cbCriterio.setSelectedIndex(3);
            txtCriterio.setText("");
            return;
        }
        if (cbCriterio.getSelectedIndex() == 0) {
            jpProductos.removeAll();
            jpProductos.repaint();
            jpProductos.setLayout(new FlowLayout());
            return;
        }//Fin de Validaciones...

        String sql = "";
        if (cbCriterio.getSelectedIndex() == 1 || cbCriterio.getSelectedIndex() == 2) {
            if (cbCriterio.getSelectedIndex() == 1) {
                sql = "select Descripcion, IdProducto, imagePath, Precio "
                        + "from TABLA_PRODUCTOS "
                        + "where IdProducto LIKE '" + txtCriterio.getText().trim() + "'";
            }
            if (cbCriterio.getSelectedIndex() == 2) {
                sql = "select Descripcion, IdProducto, imagePath, Precio "
                        + "from TABLA_PRODUCTOS "
                        + "where Descripcion CONTAINING '" + txtCriterio.getText().trim() + "'";
            }

            ResultSet rs = getDatos().getConsulta(sql);

            jpProductos.removeAll();
            jpProductos.repaint();
            jpProductos.setLayout(new FlowLayout());
            try {
                while (rs.next()) {
                    boton = new JButton(rs.getString("Descripcion").
                            concat("                   ").substring(0, 17));
                    boton.setToolTipText(rs.getString("IdProducto"));
                    boton.setMnemonic('p');

                    String im = "";

                    if (rs.getString("imagepath") != null) {
                        im = rs.getString("imagepath");
                    }

                    ImageIcon imagen = new ImageIcon(im);

                    if (imagen.getIconHeight() == -1) {
                        imagen = new ImageIcon(System.getProperty("user.dir")
                                + "/images/Sin_imagen 64 x 64.png");
                    }
                    Icon icon = new ImageIcon(imagen.getImage().getScaledInstance(
                            64,
                            64,
                            Image.SCALE_DEFAULT));
                    imagen.getImage().flush();

                    boton.setIcon(icon);

                    boton.addActionListener(this);

                    boton.setIconTextGap(2);
                    boton.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                    boton.setVerticalAlignment(javax.swing.SwingConstants.TOP);
                    boton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
                    boton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

                    int ancho = 170, alto = 100;

                    boton.setPreferredSize(new java.awt.Dimension(ancho, alto));
                    boton.setMinimumSize(new java.awt.Dimension(ancho, alto));
                    boton.setMaximumSize(new java.awt.Dimension(ancho, alto));

                    boton.setLayout(new BorderLayout(8, 8));
                    boton.requestFocusInWindow();
                    jpProductos.add(boton);
                    jpProductos.repaint();
                    jpProductos.validate();
                    boton.requestFocusInWindow();
                }
            } catch (SQLException ex) {
                LOGGER.getLogger(frmBebidas.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if (cbCriterio.getSelectedIndex() == 3) {
            if (txtCriterio.getText().trim().isEmpty()) {
                return;
            }
            if (!getDatos().existeProducto(txtCriterio.getText().trim())) {
                JOptionPane.showMessageDialog(null, "El Producto No Existe...");
                txtCriterio.setText("");
                return;
            }
            //Detalle de Factura
            int num = tblDetalle.getRowCount();
            if (num == 0) {
                JOptionPane.showMessageDialog(this, "No existe producto en detalle!!!");
                txtCriterio.setText("");
                return;
            }
            boolean dime = true;
            for (int i = 0; i < num; i++) {
                if (facturas.get(i).getIdProducto().equals(txtCriterio.getText().trim())) {
                    tblDetalle.setRowSelectionInterval(i, i);
                    dime = false;
                    txtCriterio.setText("");
                    break;
                }
            }
            if (dime) {
                txtCriterio.setText("");
                JOptionPane.showMessageDialog(this, "Producto no encontrado");
            }
        }
    }//GEN-LAST:event_txtCriterioActionPerformed
    private void rbtCreditoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtCreditoActionPerformed
        if (evt == null) {
            System.out.println("Entro En Credito");
            rbtCredito.doClick();
        }
        cmbCliente.setEnabled(true);
        btnBuscarCliente.setEnabled(true);
        cmbCliente.setSelectedIndex(0);
    }//GEN-LAST:event_rbtCreditoActionPerformed
    private void rbtContadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtContadoActionPerformed
        if (evt == null) {
            System.out.println("Entro en Contado");
            rbtContado.doClick();
        }
        cmbCliente.setEnabled(false);
        cmbCliente.setSelectedIndex(0);
        JlCantidad1.setText("Limt Cred.:$0.00");
        JlCantidad2.setText("Deud Actu.:$0.00");
    }//GEN-LAST:event_rbtContadoActionPerformed
    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        frmDetalleQuitarEliminar miForm = new frmDetalleQuitarEliminar(null, true);

        miForm.setLocationRelativeTo(null);
        miForm.setVisible(true);

        if (miForm.getOpcion() == 1) {
            opcion1();
        }
        if (miForm.getOpcion() == 2) {
            opcion2();
        }

    }//GEN-LAST:event_btnCancelarActionPerformed
    private void opcion1() {
        int rta = JOptionPane.showConfirmDialog(this,
                "¿Esta Seguro de Borrar el detalle de la Factura?",
                "Eliminando Detalle de la factura",
                JOptionPane.YES_NO_OPTION);
        if (rta == 1) {
            return;
        }
        nueva("btnCancelarActionPerformed");
    }

    private void opcion2() {
        if (isTemporal()) {
            JOptionPane.showMessageDialog(this,
                    "Es una factura temporal no se permite eliminar",
                    "Negacion a eliminar producto...",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }

        try {
            DefaultTableModel modelo = (DefaultTableModel) tblDetalle.getModel();
            int fila = tblDetalle.getSelectedRow();
            if (fila == -1) {
                JOptionPane.showMessageDialog(this, "Debe seleccionar un articulo del detalle factura");
                return;
            }

            facturas.remove(fila);
            modelo.removeRow(fila);
            totales();
        } catch (HeadlessException ex) {
            LOGGER.getLogger(frmBebidas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void btnEsperaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEsperaActionPerformed
        if (tblDetalle.getRowCount() == 0) {
            JOptionPane.showMessageDialog(this, "Factura Vacia...");
            return;
        }

        if (facturas.get(0) == null) {
            JOptionPane.showMessageDialog(this, "Ha ocurrido un error de factura realice de nuevo...");
            return;
        }

        if (cmbCliente.getSelectedIndex() > 0) {
            nombreCliente = ((Opcion) cmbCliente.getSelectedItem()).getDescripcion();
            idClienteTemporal = ((Opcion) cmbCliente.getSelectedItem()).getValor();
        } else {
            idClienteTemporal = "00000000000";
            if ("".equals(nombreCliente)) {
                nombreCliente = JOptionPane.showInputDialog(this, "Inserte Nombre Cliente: ",
                        "Inserte nombre del Cliente...",
                        JOptionPane.INFORMATION_MESSAGE);
                if (nombreCliente == null || nombreCliente.equals("")) {
                    JOptionPane.showMessageDialog(this,
                            "Nombre vacio, Seleccione un cliente\n para guardarla en temporal... ",
                            "Advertencia", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
        }

        frmTemporal miTemporal = new frmTemporal(null, true);

        miTemporal.setFactura(Integer.parseInt(txtIdFactura.getText()));
        miTemporal.setMiTabla(miTabla);
        miTemporal.setFacturas(facturas);
        miTemporal.setCredicto(rbtCredito.isSelected());
        miTemporal.setIdTurno(getTurno());
        miTemporal.setIdCliente(idClienteTemporal);
        miTemporal.setNombreCliente(nombreCliente);
        miTemporal.setMisDatos(getDatos());
        miTemporal.setIdUsuario(getIdUsuario());
        miTemporal.setAceptar(false);

        miTemporal.setLocationRelativeTo(null);
        miTemporal.setVisible(true);

        if (miTemporal.isAceptar()) {
            nueva("btnEsperaActionPerformed");
        }
    }//GEN-LAST:event_btnEsperaActionPerformed
    private void btnBuscarEsperaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarEsperaActionPerformed
        frmBuscarTemporal miTempo = new frmBuscarTemporal(null, true,
                getDatos());
        setTemporal(false);
        miTempo.setLocationRelativeTo(null);
        miTempo.setVisible(true);

        if (miTempo.isAceptar()) {
            if (miTempo.getFactura().equals(txtIdFactura.getText())) {
                System.out.println("Factura " + miTempo.getFactura() + " cargada..");
                return;
            }
            btnBuscarCliente.setEnabled(false);
            setTemporal(true);
            setTitle("Nueva Factura: " + miTempo.getFactura()
                    + "   Numero del Turno Activo del Cajero: "
                    + getTurno() + " Factura Temporarl activa, Cliente: " + nombreCliente);

            txtIdFactura.setText("" + miTempo.getFactura());

            String sql = "SELECT d.IDFACTURA, d.IDLINEA, d.IDPRODUCTO, p.DESCRIPCION, d.CANTIDAD, d.PRECIO "
                    + "FROM DETALLEFACTURA d "
                    + "    LEFT JOIN PRODUCTOS p "
                    + "        on p.IDPRODUCTO = d.IDPRODUCTO "
                    + "where d.IDFACTURA = " + miTempo.getFactura() + " "
                    + "order by 2";
            ResultSet rs = getDatos().getConsulta(sql);

            try {
                while (rs.next()) {
                    facturas.add(rs.getInt("idLinea") - 1, new Factura(
                            Integer.parseInt(miTempo.getFactura()),
                            rs.getShort("idLinea"),
                            rs.getString("idProducto"), //IdeProducto
                            rs.getString("descripcion"),
                            rs.getDouble("precio"),
                            rs.getDouble("cantidad")));

                    registro[0] = rs.getDouble("cantidad");
                    registro[1] = rs.getString("descripcion");
                    registro[2] = Utilidades.priceWithDecimal(
                            rs.getDouble("precio") * rs.getDouble("cantidad"));

                    miTabla.addRow(registro);

                    tblDetalle.setModel(miTabla);
                    tblDetalle.setRowSelectionInterval(rs.getInt("idLinea") - 1, rs.getInt("idLinea") - 1);
                }
            } catch (SQLException ex) {
                LOGGER.getLogger(frmBebidas.class.getName()).log(Level.SEVERE, null, ex);
            }

            precio();//Precio porque acaba de ser creada

            sql = "SELECT r.IDCLIENTE, nombreCliente "
                    + "FROM FACTURA r "
                    + "where r.IDFACTURA = " + miTempo.getFactura() + " ";
            rs = getDatos().getConsulta(sql);

            try {
                rs.next();
                setIdCliente(rs.getString(1));
                nombreCliente = rs.getString(2);
            } catch (SQLException ex) {
                LOGGER.getLogger(frmBebidas.class.getName()).log(Level.SEVERE, null, ex);
            }

            for (int i = 0; i <= cmbCliente.getItemCount() + 1; i++) {
                if (((Opcion) cmbCliente.getItemAt(i)).getValor().equals(getIdCliente())) {
                    cmbCliente.setSelectedIndex(i);
                    totales();
                    break;
                }
            }
        }
    }//GEN-LAST:event_btnBuscarEsperaActionPerformed
    private void cbTodosProductosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbTodosProductosActionPerformed
        jpProductos = new javax.swing.JPanel();
        jpProductos.setAutoscrolls(true);
        jpProductos.setBackground(new java.awt.Color(255, 102, 51));
        jpProductos.setBorder(javax.swing.BorderFactory.createTitledBorder(null,
                "Productos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
                javax.swing.border.TitledBorder.DEFAULT_POSITION,
                new java.awt.Font("Ubuntu", 0, 14))); // NOI18N
        jpProductos.setMaximumSize(new java.awt.Dimension(350, 1500));
        jpProductos.setMinimumSize(new java.awt.Dimension(350, 1500));
        jpProductos.setPreferredSize(new java.awt.Dimension(350, 1500));
        java.awt.FlowLayout flowLayout1 = new java.awt.FlowLayout();
        flowLayout1.setAlignOnBaseline(true);
        jpProductos.setLayout(flowLayout1);
        jScrollPane4.setViewportView(jpProductos);

        actionPerformed(e1);
    }//GEN-LAST:event_cbTodosProductosActionPerformed
    private void btnGastosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGastosActionPerformed
        int resp = JOptionPane.showOptionDialog(this, "<html><big>Que desea registrar?</big></html",
                "Elija una accion!!!",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null, new String[]{"<html><big>Gasto</big></html>", "<html><big>Deuda</big></html", "<html><big>Cancelar</big></html"}, 0);

        if (resp == 0) {
            System.out.println("Iniciando Formulario");
            frmGasto miGasto = new frmGasto(null, true);
            frmAutorizacion miAut = new frmAutorizacion(null, true);

            System.out.println("Pidiendo confirmacion!!!");
            miAut.setDatos(getDatos());
            miAut.setLocationRelativeTo(null);
            miAut.setVisible(true);

            if (!miAut.isAceptado()) {
                JOptionPane.showMessageDialog(this, "Usuario no valido");
                return;
            }
            System.out.println("Usuario Ok");
            miGasto.setDatos(getDatos());
            miGasto.setIdTurno(getTurno());
            miGasto.setUsuario(txtUsuario.getText());
            miGasto.setLocationRelativeTo(null);
            miGasto.setVisible(true);

        }

        if (resp == 1) {
            System.out.println("Registrar deudas");
        }

    }//GEN-LAST:event_btnGastosActionPerformed
    private void btnImpresionUltimaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImpresionUltimaActionPerformed
        int resp = JOptionPane.showConfirmDialog(this,
                "Desea Imprimir la Ultima Factura?",
                "Confirmacion de impresion", JOptionPane.YES_NO_OPTION);
        if (resp == 1) {
            return;
        }
        frmPrintFacturaConReporte miPrint = new frmPrintFacturaConReporte(null, true);
        miPrint.setCopia(false);
        miPrint.setLocationRelativeTo(null);
        miPrint.setVisible(true);
    }//GEN-LAST:event_btnImpresionUltimaActionPerformed
    private void tblDetalleMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblDetalleMouseClicked
        precio();
    }//GEN-LAST:event_tblDetalleMouseClicked

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
        getClientes();
        txtIdFactura.setText("" + getDatos().getNumFac(getIdUsuario(),
                getTurno()));
        setTemporal(false);
        repararRegistro();
        totales();
    }//GEN-LAST:event_formInternalFrameOpened

    private void formInternalFrameClosing(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameClosing
        if (tblDetalle.getRowCount() != 0) {
            int resp = JOptionPane.showInternalConfirmDialog(this,
                    "Existe una factura, Desea Salir?", "Advertencia, Factura existente",
                    JOptionPane.YES_NO_OPTION);
            if (resp == 0) {
                dispose();
            }
        } else {
            dispose();
        }
    }//GEN-LAST:event_formInternalFrameClosing

    private void cmbClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbClienteActionPerformed
        if (!cmbCliente.isEnabled()) {
            return;
        }
        estadoCliente("cmbClienteActionPerformed");
    }//GEN-LAST:event_cmbClienteActionPerformed

    private void btnCancelar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelar1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnCancelar1ActionPerformed

    private void cmbClienteItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbClienteItemStateChanged
        if(cmbCliente.getSelectedIndex() == 0){
            JlCantidad1.setVisible(false);
            JlCantidad2.setVisible(false);
            jpCliente.setSize(265, 86);
            jpCliente.setPreferredSize(new Dimension(265, 86));
            jpCliente.setMinimumSize(new Dimension(265, 86));
            jpCliente.setMaximumSize(new Dimension(265, 86));
            
        }else{
            JlCantidad1.setVisible(true);
            JlCantidad2.setVisible(true);
            jpCliente.setSize(265, 135);
            jpCliente.setPreferredSize(new Dimension(265, 135));
            jpCliente.setMinimumSize(new Dimension(265, 135));
            jpCliente.setMaximumSize(new Dimension(265, 135));
        }
    }//GEN-LAST:event_cmbClienteItemStateChanged
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e == null) {
            return;
        }

        e1 = e;

        try {
            btn = (JButton) e.getSource();
        } catch (Exception eb) {
            return;
        }

        if (btn.getMnemonic() == 80) {//Incluir elemento a la factura....
            double cantidad = 0, precio = 0;
            boolean bandera;//Cuadro de Texto pidiendo Cantidad

            do {
                try {
                    String es = JOptionPane.showInputDialog("Introduce cantidad: ");
                    if (es == null) {
                        return;
                    }
                    cantidad = Double.parseDouble(es);
                    bandera = true;
                } catch (NumberFormatException ex) {
                    javax.swing.JOptionPane.showMessageDialog(null, "Solo se permiten numeros");
                    bandera = false;
                }
            } while (!bandera);//Fin del cuadro texto pidiendo la cantidad

            //Para obtener el precio                                                --IdProducto
            String sql = "select Precio from TABLA_PRODUCTOS where idProducto like '" + btn.getToolTipText() + "'";
            ResultSet rs = getDatos().getConsulta(sql);
            try {
                rs.next();
                precio = rs.getDouble("Precio");
            } catch (SQLException ex) {
                LOGGER.getLogger(frmBebidas.class.getName()).log(Level.SEVERE, null, ex);
            }//Fin de obtener el precio del producto

            facturas.add(tblDetalle.getRowCount(), new Factura(
                    Integer.parseInt(txtIdFactura.getText()),//IdFactura
                    (short) tblDetalle.getRowCount(),//IdLinea
                    btn.getToolTipText(),//IdProducto
                    btn.getText(),//Descripcion del producto
                    precio,//Precio del producto
                    cantidad));//Cantidad

            txtPrecio.setValue(precio);

            registro[0] = cantidad;
            registro[1] = btn.getText();
            registro[2] = Utilidades.priceWithDecimal(precio * cantidad);

            miTabla.addRow(registro);
            tblDetalle.setModel(miTabla);

            e1 = null;//Porque va tu aqui...

            jpProductos.removeAll();
            jpProductos.repaint();
            jpProductos.setLayout(new FlowLayout());

            tblDetalle.changeSelection(tblDetalle.getRowCount() - 1, 0, false, false);//Seleccionar ultimo elemento
            tblDetalle.requestFocusInWindow();

            totales();
            txtCriterio.setText("");
            txtCriterio.requestFocusInWindow();
            return;
        }//Fin de la inclusion....

        String sql = "select Descripcion, IdProducto, imagePath, Precio "
                + "from TABLA_PRODUCTOS "
                + "where idCategoria = " + btn.getToolTipText() + " and "
                + "entrada > 0 and "
                + "estado = 1";

        if (cbTodosProductos.isSelected()) {
            sql = "select Descripcion, IdProducto, imagePath, Precio "
                    + "from TABLA_PRODUCTOS "
                    + "where idCategoria = " + btn.getToolTipText();
        }

        ResultSet rs = getDatos().getConsulta(sql);

        jpProductos.removeAll();
        jpProductos.repaint();
        jpProductos.setLayout(new FlowLayout());

        try {
            while (rs.next()) {
                boton = new JButton(rs.getString("Descripcion"));
                boton.setToolTipText(rs.getString("IdProducto"));
                boton.setMnemonic('p');

                String im = "";

                if (rs.getString("imagepath") != null) {
                    im = rs.getString("imagepath");
                }

                ImageIcon imagen = new ImageIcon(im);

                if (imagen.getIconHeight() == -1) {
                    imagen = new ImageIcon(System.getProperty("user.dir")
                            + "/images/Sin_imagen 64 x 64.png");
                }
                Icon icon = new ImageIcon(imagen.getImage().getScaledInstance(
                        64,
                        64,
                        Image.SCALE_DEFAULT));
                imagen.getImage().flush();

                boton.setIcon(icon);

                boton.addActionListener(this);

                boton.setIconTextGap(2);
                boton.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                boton.setVerticalAlignment(javax.swing.SwingConstants.TOP);
                boton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
                boton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

                int ancho = 170, alto = 100;

                boton.setPreferredSize(new java.awt.Dimension(ancho, alto));
                boton.setMinimumSize(new java.awt.Dimension(ancho, alto));
                boton.setMaximumSize(new java.awt.Dimension(ancho, alto));

                boton.setLayout(new BorderLayout(8, 8));

                jpProductos.add(boton);
                jpProductos.repaint();
                jpProductos.validate();
            }
            //jpProductos.setPreferredSize(new Dimension(jpProductos.getSize().width, 1500));
        } catch (SQLException ex) {
            LOGGER.getLogger(frmBebidas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//Fin de actionPerformed

    private void repararRegistro() {
        miTabla = new DefaultTableModel(null, titulos);
        tblDetalle.setModel(miTabla);
        TableColumn miTableColumn;
        for (int i = 0; i < 2; i++) {
            miTableColumn = tblDetalle.getColumnModel().getColumn(i);
            if (i == 0) {
                miTableColumn.setPreferredWidth(80); // la tercera columna sera la mas grande
            }
            if (i == 1) {
                miTableColumn.setPreferredWidth(200); // la tercera columna sera la mas grande
            }
            if (i == 2) {
                miTableColumn.setPreferredWidth(15); // la tercera columna sera la mas grande
            }
        }

        tcr.setHorizontalAlignment(SwingConstants.RIGHT);
        tcr.setFont(new java.awt.Font("Ubuntu", 50, 80));
        tcr.setBackground(Color.yellow);
        tblDetalle.getColumnModel().getColumn(2).setCellRenderer(tcr);
    }

    private void categoriaR() {
        //Trabajo
        String sql = "SELECT r.IDCATEGORIA, r.DESCRIPCION, r.IMAGEPATH "
                + "FROM GET_CATEGORIA_ACTIVAS r";

        if (cbTodos.isSelected()) {
            sql = "SELECT r.IDCATEGORIA, r.DESCRIPCION, r.IMAGEPATH "
                    + "FROM TABLA_CATEGORIA r";
        }

        ResultSet rs = getDatos().getConsulta(sql);
        jpCategoria.removeAll();
        jpCategoria.repaint();
//        jpCategoria.setLayout(new FlowLayout());
        try {
            while (rs.next()) {
                boton = new JButton(rs.getString("Descripcion"));
                boton.setToolTipText(rs.getString("idCategoria"));
                boton.setMnemonic('c');

                String im = rs.getString("imagepath");

                ImageIcon imagen;

                imagen = new ImageIcon(im);

                if (imagen.getIconHeight() == -1 || im.isEmpty()) {
                    imagen = new ImageIcon(System.getProperty("user.dir")
                            + "/images/Sin_imagen 64 x 64.png");
                }

                Icon icon = new ImageIcon(imagen.getImage().getScaledInstance(72, 72,
                        Image.SCALE_DEFAULT));
                imagen.getImage().flush();
                boton.setIcon(icon);
                boton.validate();

                boton.setIconTextGap(2);
                boton.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
                boton.setVerticalAlignment(javax.swing.SwingConstants.TOP);
                boton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
                boton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
                boton.addActionListener(this);

                int ancho = 120, alto = 95;
                boton.setPreferredSize(new java.awt.Dimension(ancho, alto));
                boton.setMinimumSize(new java.awt.Dimension(ancho, alto));
                boton.setMaximumSize(new java.awt.Dimension(ancho, alto));

//                boton.setLayout(new FlowLayout());
                jpCategoria.add(boton);
                jpCategoria.repaint();
                jpCategoria.validate();

            }
        } catch (SQLException ex) {
            LOGGER.getLogger(frmBebidas.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void tiempo() {
        Calendar calendario2 = new GregorianCalendar();
        Date fechaHoraActual = new Date();

        calendario2.setTime(fechaHoraActual);
        ampm = calendario2.get(Calendar.AM_PM) == Calendar.AM ? "AM" : "PM";
        if (ampm.equals("PM")) {
            int h = calendario2.get(Calendar.HOUR_OF_DAY) - 12;
            hora = h > 9 ? "" + h : "0" + h;
        } else {
            hora = calendario2.get(Calendar.HOUR_OF_DAY) > 9 ? ""
                    + calendario2.get(Calendar.HOUR_OF_DAY) : "0"
                    + calendario2.get(Calendar.HOUR_OF_DAY);
        }
        minutos = calendario2.get(Calendar.MINUTE) > 9 ? ""
                + calendario2.get(Calendar.MINUTE) : "0"
                + calendario2.get(Calendar.MINUTE);
        segundos = calendario2.get(Calendar.SECOND) > 9 ? ""
                + calendario2.get(Calendar.SECOND) : "0"
                + calendario2.get(Calendar.SECOND);
    }

    private void estadoCliente(String donde) {
        System.out.println("Invocado de: " + donde);
        String valor = "00000000000";
        try {
            valor = ((Opcion) cmbCliente.getSelectedItem()).getValor();
        } catch (Exception ex) {
            System.out.println("No se obtuvo identificacion del cliente: " + valor);
            valor = "00000000000";
        }

        System.out.println("Trabajando con el Cliente: " + valor);

        ResultSet rs = getDatos().getClientes(valor);

        int resp = 2;

        try {
            rs.next();
            JlCantidad1.setText("Limt Cred.:$"
                    + Utilidades.priceWithDecimal(rs.getDouble("credito")));
            JlCantidad2.setText("Deud Actu.:$"
                    + Utilidades.priceWithDecimal(rs.getDouble("deudaActual")));
            valorCredito = rs.getDouble("credito");
            if (!valor.equals("00000000000") && rbtCredito.isSelected()) {
                resp = JOptionPane.showConfirmDialog(this, "Cliente: "
                        + rs.getString("Concatenation")
                        + "\nSu credito es de: " + "RD$" + rs.getString("Credito")
                        + "\nDesea Continuar?",
                        "Credito insuficiente", JOptionPane.YES_NO_OPTION);
            }
        } catch (SQLException ex) {
            LOGGER.getLogger(frmBebidas.class.getName()).log(Level.SEVERE, null, ex);
        }

        switch (resp) {
            case JOptionPane.OK_OPTION:
                frmAutorizacion miAut = new frmAutorizacion(null, true);
                miAut.setDatos(getDatos());
                miAut.setLocationRelativeTo(null);
                miAut.setVisible(true);
                if (!miAut.isAceptado()) {
                    JOptionPane.showMessageDialog(this, "Usuario no valido");
                    rbtContadoActionPerformed(null);
                }
                break;
            case JOptionPane.NO_OPTION:
                rbtContado.setSelected(true);
                rbtContadoActionPerformed(null);
                rbtContado.validate();
                rbtContado.revalidate();
                break;
        }
    }

    private void getClientes() {
        cmbCliente.removeAllItems();
        cmbCliente.repaint();
        Opcion opc = new Opcion("00000000000", "Cliente Generico");
        cmbCliente.addItem(opc);
        try {
            //Cargamos Clientes
            ResultSet rs = getDatos().getClientesCombo();
            while (rs.next()) {
                opc = new Opcion(
                        rs.getString("idCliente"),
                        rs.getString("nombres")
                        + " "
                        + rs.getString("apellidos"));
                cmbCliente.addItem(opc);
            }//fin
            cmbCliente.setSelectedIndex(0);
        } catch (SQLException ex) {
            LOGGER.getLogger(frmBebidas.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        if (isTemporal()) {
            for (int i = 0; i < cmbCliente.getItemCount(); i++) {
                if (((Opcion) cmbCliente.getItemAt(i)).getValor().equals(getIdCliente())) {
                    cmbCliente.setSelectedIndex(i);
                    return;
                }
            }
        }
    }

    private void limpiarTabla() {
        try {
            DefaultTableModel modelo = (DefaultTableModel) tblDetalle.getModel();
            int filas = tblDetalle.getRowCount();
            for (int i = 0; filas > i; i++) {
                modelo.removeRow(0);
            }
        } catch (Exception ex) {
            LOGGER.getLogger(frmBebidas.class.getName()).log(Level.SEVERE, null, ex);
        }
        cmbCliente.setSelectedIndex(0);
    }

    private void totales() {
        int num = tblDetalle.getRowCount();
        double sumCan = 0;
        double sumVal = 0;

        if (num != 0) {
            for (int i = 0; i < num; i++) {
                sumCan += Utilidades.objectToDouble(tblDetalle.getValueAt(i, 0));
                sumVal += Utilidades.objectToDouble(tblDetalle.getValueAt(i, 2));
            }
        } else {
            txtTotalCantidad.setValue(0);
            txtTotalValor.setValue(0);
            txtPrecio.setValue(0);
            return;
        }
        txtTotalCantidad.setValue(sumCan);
        txtTotalValor.setValue(sumVal);
    }

    private void nueva(String donde) {
        System.out.println("·························***De donde: " + donde);
        mnuMovimientosNuevaFactura.doClick();
        nombreCliente = "";
        idClienteTemporal = "00000000000";
        facturas.clear();
    }

    private void precio() {
        int cell;
        try {
            cell = tblDetalle.getSelectedRow();
        } catch (Exception e) {
            cell = -1;
        }
        if (cell == -1) {
            txtPrecio.setValue(0.0);
            return;
        }
        txtPrecio.setValue(facturas.get(cell).getPrecio());
    }

    public Integer numeroCeldas() {
        return tblDetalle.getRowCount();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel JlCantidad1;
    private javax.swing.JLabel JlCantidad2;
    private javax.swing.JButton btnBuscarCliente;
    private javax.swing.JButton btnBuscarEspera;
    public javax.swing.JButton btnCancelar;
    public javax.swing.JButton btnCancelar1;
    private javax.swing.JButton btnEspera;
    private javax.swing.JButton btnGastos;
    private javax.swing.JButton btnGrabar;
    private javax.swing.ButtonGroup btnGrupoPago;
    private javax.swing.JButton btnImpresionUltima;
    private javax.swing.JButton btnPagoDeuda;
    private javax.swing.JComboBox<String> cbCriterio;
    private javax.swing.JCheckBox cbPrevista;
    private javax.swing.JCheckBox cbTodos;
    private javax.swing.JCheckBox cbTodosProductos;
    public javax.swing.JComboBox cmbCliente;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JPanel jpBusqueda;
    private javax.swing.JPanel jpCategoria;
    private javax.swing.JPanel jpCliente;
    private javax.swing.JPanel jpProductos;
    private javax.swing.JRadioButton rbtContado;
    public javax.swing.JRadioButton rbtCredito;
    public javax.swing.JTable tblDetalle;
    public javax.swing.JTextField txtCriterio;
    private javax.swing.JTextField txtFecha;
    private javax.swing.JTextField txtHora;
    public javax.swing.JTextField txtIdFactura;
    private javax.swing.JFormattedTextField txtPrecio;
    private javax.swing.JFormattedTextField txtTotalCantidad;
    private javax.swing.JFormattedTextField txtTotalValor;
    private javax.swing.JTextField txtTurno;
    private javax.swing.JTextField txtUsuario;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables
}
