package formulario;

import clases.Celda_CheckBox;
import clases.Cliente;
import clases.Datos;
import clases.DefaultTableCellHeaderRenderer;
import clases.Render_CheckBox;
import clases.Utilidades;
import static clases.Utilidades.LOGGER;
import com.toedter.calendar.JTextFieldDateEditor;
import static formulario.frmPrincipal.dpnEscritorio;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyVetoException;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.TableColumn;

public final class frmClientes extends javax.swing.JInternalFrame {

    private Datos misDatos;
    private int cliAct = 0;
    private boolean nuevo = false;
    private final JTextFieldDateEditor editor, editor2;
    private final JButton button, button2;
    private frmDetalleFacturaClientes miDetalle;

    public void setDatos(Datos misDatos) {
        this.misDatos = misDatos;
    }

    private Datos getDatos() {
        return misDatos;
    }

    public frmClientes() {
        System.out.println("Clase Clientes");
        initComponents();
        oyenteDeComponentes();
        editor = (JTextFieldDateEditor) dchFechaNacimiento.getDateEditor();
        editor2 = (JTextFieldDateEditor) dchFechaIngreso.getDateEditor();

        button = dchFechaNacimiento.getCalendarButton();
        button2 = dchFechaIngreso.getCalendarButton();

        editor.setEditable(false);
        editor2.setEditable(false);
        button.setEnabled(false);
        button2.setEnabled(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jScrollPane4 = new javax.swing.JScrollPane();
        jPanel4 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        btnPrimero = new javax.swing.JButton();
        btnAnterior = new javax.swing.JButton();
        btnSiguiente = new javax.swing.JButton();
        btnUltimo = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        btnNuevo = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnBorrar = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblClientes = new JTable(){
            @Override
            public boolean isCellEditable(int rowIndex, int colIndex) { 
                return false; //Las celdas no son editables. 
            }
        };
        jPanel7 = new javax.swing.JPanel();
        chbDireccion = new javax.swing.JCheckBox();
        chbTelefono = new javax.swing.JCheckBox();
        chbCiudad = new javax.swing.JCheckBox();
        chbFechaNacimiento = new javax.swing.JCheckBox();
        chbFechaIngreso = new javax.swing.JCheckBox();
        chbSolo6 = new javax.swing.JCheckBox();
        jPanel1 = new javax.swing.JPanel();
        txtIDCliente = new javax.swing.JFormattedTextField();
        txtNombres = new javax.swing.JTextField();
        txtApellidos = new javax.swing.JTextField();
        txtDireccion = new javax.swing.JTextField();
        txtTelefono = new javax.swing.JFormattedTextField();
        txtCiudad = new javax.swing.JTextField();
        dchFechaNacimiento = new com.toedter.calendar.JDateChooser();
        txtCredito = new javax.swing.JFormattedTextField();
        cbActivo = new javax.swing.JCheckBox();
        btnHistorial = new javax.swing.JButton();
        txtDeudaActual = new javax.swing.JFormattedTextField();
        dchFechaIngreso = new com.toedter.calendar.JDateChooser();
        jLabel7 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Clientes");
        setFocusTraversalPolicyProvider(true);
        setMinimumSize(new java.awt.Dimension(915, 535));
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameActivated(evt);
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Botones de Acción"));
        jPanel2.setMaximumSize(new java.awt.Dimension(787, 81));
        jPanel2.setMinimumSize(new java.awt.Dimension(787, 81));

        jPanel5.setLayout(new java.awt.GridLayout(1, 4, 4, 0));

        btnPrimero.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnPrimero.setForeground(new java.awt.Color(1, 1, 1));
        btnPrimero.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Anterior 32 x 32.png"))); // NOI18N
        btnPrimero.setMnemonic('p');
        btnPrimero.setText("Primero");
        btnPrimero.setToolTipText("Va al Primer Registro");
        btnPrimero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrimeroActionPerformed(evt);
            }
        });
        jPanel5.add(btnPrimero);

        btnAnterior.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnAnterior.setForeground(new java.awt.Color(1, 1, 1));
        btnAnterior.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Flecha Izquierda 32 x 32.png"))); // NOI18N
        btnAnterior.setMnemonic('a');
        btnAnterior.setText("Anterior");
        btnAnterior.setToolTipText("Va al Anterior Registro");
        btnAnterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAnteriorActionPerformed(evt);
            }
        });
        jPanel5.add(btnAnterior);

        btnSiguiente.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnSiguiente.setForeground(new java.awt.Color(1, 1, 1));
        btnSiguiente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Flecha Derecha 32 x 32.png"))); // NOI18N
        btnSiguiente.setMnemonic('s');
        btnSiguiente.setText("Siguiente");
        btnSiguiente.setToolTipText("Va al Siguiente Registro");
        btnSiguiente.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnSiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSiguienteActionPerformed(evt);
            }
        });
        jPanel5.add(btnSiguiente);

        btnUltimo.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnUltimo.setForeground(new java.awt.Color(1, 1, 1));
        btnUltimo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Siguiente 32 x 32.png"))); // NOI18N
        btnUltimo.setMnemonic('u');
        btnUltimo.setText("Ultimo");
        btnUltimo.setToolTipText("Va al Ultimo Registro");
        btnUltimo.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnUltimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUltimoActionPerformed(evt);
            }
        });
        jPanel5.add(btnUltimo);

        jPanel6.setLayout(new java.awt.GridLayout(1, 0, 4, 0));

        btnNuevo.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnNuevo.setForeground(new java.awt.Color(1, 1, 1));
        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Documento nuevo 32 x 32.png"))); // NOI18N
        btnNuevo.setMnemonic('n');
        btnNuevo.setText("Nuevo");
        btnNuevo.setToolTipText("Crear un nuevo Registro");
        btnNuevo.setMaximumSize(new java.awt.Dimension(104, 44));
        btnNuevo.setMinimumSize(new java.awt.Dimension(104, 44));
        btnNuevo.setPreferredSize(new java.awt.Dimension(104, 44));
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        jPanel6.add(btnNuevo);

        btnModificar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnModificar.setForeground(new java.awt.Color(1, 1, 1));
        btnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Editar Documento 32 x 32.png"))); // NOI18N
        btnModificar.setMnemonic('m');
        btnModificar.setText("Modificar");
        btnModificar.setToolTipText("Modificar Registro Actual");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });
        jPanel6.add(btnModificar);

        btnGuardar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnGuardar.setForeground(new java.awt.Color(1, 1, 1));
        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Guardar 32 x 32.png"))); // NOI18N
        btnGuardar.setMnemonic('g');
        btnGuardar.setText("Guardar");
        btnGuardar.setToolTipText("Guardar Registro Actual");
        btnGuardar.setEnabled(false);
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        jPanel6.add(btnGuardar);

        btnCancelar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnCancelar.setForeground(new java.awt.Color(1, 1, 1));
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Cancelar 32 x 32.png"))); // NOI18N
        btnCancelar.setMnemonic('c');
        btnCancelar.setText("Cancelar");
        btnCancelar.setToolTipText("Cancela la Operacion del Registro");
        btnCancelar.setEnabled(false);
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        jPanel6.add(btnCancelar);

        btnBorrar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnBorrar.setForeground(new java.awt.Color(1, 1, 1));
        btnBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Borrar 32 x 32.png"))); // NOI18N
        btnBorrar.setMnemonic('b');
        btnBorrar.setText("Borrar");
        btnBorrar.setToolTipText("Borrar Registro Actual");
        btnBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBorrarActionPerformed(evt);
            }
        });
        jPanel6.add(btnBorrar);

        btnBuscar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnBuscar.setForeground(new java.awt.Color(1, 1, 1));
        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Buscar2 32 x 32.png"))); // NOI18N
        btnBuscar.setMnemonic('r');
        btnBuscar.setText("Buscar");
        btnBuscar.setToolTipText("Buscar el Registro");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });
        jPanel6.add(btnBuscar);

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(jPanel6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel2Layout.createSequentialGroup()
                .add(jPanel5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(0, 0, 0)
                .add(jPanel6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(0, 0, 0))
        );

        jScrollPane3.setAutoscrolls(true);
        jScrollPane3.setDoubleBuffered(true);

        tblClientes.setAutoCreateRowSorter(true);
        tblClientes.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        tblClientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11"
            }
        ));
        tblClientes.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        tblClientes.setColumnSelectionAllowed(true);
        tblClientes.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tblClientes.getTableHeader().setReorderingAllowed(false);
        tblClientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblClientesMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tblClientes);
        if (tblClientes.getColumnModel().getColumnCount() > 0) {
            tblClientes.getColumnModel().getColumn(0).setHeaderValue("1");
            tblClientes.getColumnModel().getColumn(1).setHeaderValue("2");
            tblClientes.getColumnModel().getColumn(2).setHeaderValue("3");
            tblClientes.getColumnModel().getColumn(3).setHeaderValue("4");
            tblClientes.getColumnModel().getColumn(4).setHeaderValue("5");
            tblClientes.getColumnModel().getColumn(5).setHeaderValue("6");
            tblClientes.getColumnModel().getColumn(6).setHeaderValue("7");
            tblClientes.getColumnModel().getColumn(7).setHeaderValue("8");
            tblClientes.getColumnModel().getColumn(8).setHeaderValue("9");
            tblClientes.getColumnModel().getColumn(9).setHeaderValue("10");
            tblClientes.getColumnModel().getColumn(10).setHeaderValue("11");
        }

        jPanel7.setBorder(javax.swing.BorderFactory.createTitledBorder("Columna ocultas"));
        java.awt.FlowLayout flowLayout2 = new java.awt.FlowLayout(java.awt.FlowLayout.RIGHT, 10, 0);
        flowLayout2.setAlignOnBaseline(true);
        jPanel7.setLayout(flowLayout2);

        buttonGroup1.add(chbDireccion);
        chbDireccion.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        chbDireccion.setText("Dirección");
        chbDireccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chbDireccionActionPerformed(evt);
            }
        });
        jPanel7.add(chbDireccion);

        buttonGroup1.add(chbTelefono);
        chbTelefono.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        chbTelefono.setText("Telefono");
        chbTelefono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chbTelefonoActionPerformed(evt);
            }
        });
        jPanel7.add(chbTelefono);

        buttonGroup1.add(chbCiudad);
        chbCiudad.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        chbCiudad.setText("Ciudad");
        chbCiudad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chbCiudadActionPerformed(evt);
            }
        });
        jPanel7.add(chbCiudad);

        buttonGroup1.add(chbFechaNacimiento);
        chbFechaNacimiento.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        chbFechaNacimiento.setText("Fecha de Nacimiento");
        chbFechaNacimiento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chbFechaNacimientoActionPerformed(evt);
            }
        });
        jPanel7.add(chbFechaNacimiento);

        buttonGroup1.add(chbFechaIngreso);
        chbFechaIngreso.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        chbFechaIngreso.setText("Fecha de Ingreso");
        chbFechaIngreso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chbFechaIngresoActionPerformed(evt);
            }
        });
        jPanel7.add(chbFechaIngreso);

        buttonGroup1.add(chbSolo6);
        chbSolo6.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        chbSolo6.setSelected(true);
        chbSolo6.setText("Solo 6");
        chbSolo6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chbSolo6ActionPerformed(evt);
            }
        });
        jPanel7.add(chbSolo6);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos Cliente", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP, new java.awt.Font("URW Palladio L", 0, 12), new java.awt.Color(1, 1, 1))); // NOI18N
        jPanel1.setFocusCycleRoot(true);
        jPanel1.setFocusTraversalPolicyProvider(true);
        jPanel1.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jPanel1.setName("Datos2"); // NOI18N

        txtIDCliente.setEditable(false);
        try {
            txtIDCliente.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###-#######-#")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtIDCliente.setToolTipText("Cedula del Cliente");
        txtIDCliente.setDoubleBuffered(true);
        txtIDCliente.setFocusLostBehavior(javax.swing.JFormattedTextField.COMMIT);
        txtIDCliente.setFocusTraversalPolicyProvider(true);
        txtIDCliente.setFont(new java.awt.Font("Ubuntu Mono", 1, 14)); // NOI18N
        txtIDCliente.setMinimumSize(new java.awt.Dimension(0, 0));
        txtIDCliente.setPreferredSize(new java.awt.Dimension(0, 25));
        txtIDCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIDClienteActionPerformed(evt);
            }
        });
        txtIDCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtIDClienteKeyReleased(evt);
            }
        });

        txtNombres.setEditable(false);
        txtNombres.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        txtNombres.setDoubleBuffered(true);
        txtNombres.setFocusTraversalPolicyProvider(true);
        txtNombres.setMinimumSize(new java.awt.Dimension(0, 0));
        txtNombres.setPreferredSize(new java.awt.Dimension(0, 25));
        txtNombres.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombresActionPerformed(evt);
            }
        });
        txtNombres.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNombresKeyReleased(evt);
            }
        });

        txtApellidos.setEditable(false);
        txtApellidos.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        txtApellidos.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtApellidos.setDoubleBuffered(true);
        txtApellidos.setFocusTraversalPolicyProvider(true);
        txtApellidos.setMinimumSize(new java.awt.Dimension(0, 0));
        txtApellidos.setPreferredSize(new java.awt.Dimension(0, 25));
        txtApellidos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtApellidosActionPerformed(evt);
            }
        });
        txtApellidos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtApellidosKeyReleased(evt);
            }
        });

        txtDireccion.setEditable(false);
        txtDireccion.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        txtDireccion.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtDireccion.setDoubleBuffered(true);
        txtDireccion.setFocusTraversalPolicyProvider(true);
        txtDireccion.setMinimumSize(new java.awt.Dimension(0, 0));
        txtDireccion.setPreferredSize(new java.awt.Dimension(0, 25));
        txtDireccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDireccionActionPerformed(evt);
            }
        });
        txtDireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtDireccionKeyReleased(evt);
            }
        });

        txtTelefono.setEditable(false);
        try {
            txtTelefono.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(###) ###-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtTelefono.setToolTipText("Inserte el Telefono del cliente...");
        txtTelefono.setDoubleBuffered(true);
        txtTelefono.setFocusLostBehavior(javax.swing.JFormattedTextField.COMMIT);
        txtTelefono.setFocusTraversalPolicyProvider(true);
        txtTelefono.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        txtTelefono.setMinimumSize(new java.awt.Dimension(0, 0));
        txtTelefono.setPreferredSize(new java.awt.Dimension(0, 25));
        txtTelefono.setSelectionEnd(0);
        txtTelefono.setSelectionStart(0);
        txtTelefono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTelefonoActionPerformed(evt);
            }
        });
        txtTelefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtTelefonoKeyReleased(evt);
            }
        });

        txtCiudad.setEditable(false);
        txtCiudad.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        txtCiudad.setMinimumSize(new java.awt.Dimension(0, 0));
        txtCiudad.setPreferredSize(new java.awt.Dimension(0, 25));
        txtCiudad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCiudadActionPerformed(evt);
            }
        });
        txtCiudad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCiudadKeyReleased(evt);
            }
        });

        dchFechaNacimiento.setForeground(new java.awt.Color(1, 1, 1));
        dchFechaNacimiento.setAutoscrolls(true);
        dchFechaNacimiento.setFocusTraversalPolicyProvider(true);
        dchFechaNacimiento.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        dchFechaNacimiento.setMinimumSize(new java.awt.Dimension(0, 0));
        dchFechaNacimiento.setPreferredSize(new java.awt.Dimension(0, 25));
        dchFechaNacimiento.setVerifyInputWhenFocusTarget(false);
        dchFechaNacimiento.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                dchFechaNacimientoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                dchFechaNacimientoFocusLost(evt);
            }
        });
        dchFechaNacimiento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                dchFechaNacimientoKeyReleased(evt);
            }
        });

        txtCredito.setEditable(false);
        txtCredito.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(new java.text.DecimalFormat("¤#,##0.00"))));
        txtCredito.setToolTipText("Indique el limite de credito del Cliente");
        txtCredito.setDoubleBuffered(true);
        txtCredito.setFocusLostBehavior(javax.swing.JFormattedTextField.COMMIT);
        txtCredito.setFocusTraversalPolicyProvider(true);
        txtCredito.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        txtCredito.setMinimumSize(new java.awt.Dimension(0, 0));
        txtCredito.setPreferredSize(new java.awt.Dimension(0, 25));
        txtCredito.setValue(0);
        txtCredito.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCreditoActionPerformed(evt);
            }
        });
        txtCredito.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCreditoKeyReleased(evt);
            }
        });

        cbActivo.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        cbActivo.setText("Activo");
        cbActivo.setFocusTraversalPolicyProvider(true);
        cbActivo.setMinimumSize(new java.awt.Dimension(0, 0));
        cbActivo.setPreferredSize(new java.awt.Dimension(0, 25));
        cbActivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbActivoActionPerformed(evt);
            }
        });

        btnHistorial.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnHistorial.setForeground(new java.awt.Color(1, 1, 1));
        btnHistorial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Clientes 64 x 64.png"))); // NOI18N
        btnHistorial.setText("Detalles Clientes");
        btnHistorial.setAutoscrolls(true);
        btnHistorial.setDoubleBuffered(true);
        btnHistorial.setFocusPainted(false);
        btnHistorial.setFocusTraversalPolicyProvider(true);
        btnHistorial.setMinimumSize(new java.awt.Dimension(0, 0));
        btnHistorial.setPreferredSize(new java.awt.Dimension(0, 25));
        btnHistorial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHistorialActionPerformed(evt);
            }
        });

        txtDeudaActual.setEditable(false);
        txtDeudaActual.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.NumberFormatter(java.text.NumberFormat.getCurrencyInstance())));
        txtDeudaActual.setFocusLostBehavior(javax.swing.JFormattedTextField.COMMIT);
        txtDeudaActual.setFocusTraversalPolicyProvider(true);
        txtDeudaActual.setFocusable(false);
        txtDeudaActual.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        txtDeudaActual.setMinimumSize(new java.awt.Dimension(0, 0));
        txtDeudaActual.setPreferredSize(new java.awt.Dimension(0, 25));

        dchFechaIngreso.setForeground(new java.awt.Color(1, 1, 1));
        dchFechaIngreso.setAutoscrolls(true);
        dchFechaIngreso.setFocusCycleRoot(true);
        dchFechaIngreso.setFocusTraversalPolicyProvider(true);
        dchFechaIngreso.setFocusable(false);
        dchFechaIngreso.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        dchFechaIngreso.setMinimumSize(new java.awt.Dimension(0, 0));
        dchFechaIngreso.setPreferredSize(new java.awt.Dimension(0, 25));

        jLabel7.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        jLabel7.setText("* Campos Obligatorios");
        jLabel7.setToolTipText("");
        jLabel7.setFocusable(false);
        jLabel7.setPreferredSize(new java.awt.Dimension(52, 21));
        jLabel7.setRequestFocusEnabled(false);
        jLabel7.setVerifyInputWhenFocusTarget(false);

        jLabel1.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel1.setText("*Cedula:");
        jLabel1.setFocusable(false);
        jLabel1.setPreferredSize(new java.awt.Dimension(52, 21));
        jLabel1.setRequestFocusEnabled(false);
        jLabel1.setVerifyInputWhenFocusTarget(false);

        jLabel2.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("*Nombres:");
        jLabel2.setFocusable(false);
        jLabel2.setPreferredSize(new java.awt.Dimension(52, 21));
        jLabel2.setRequestFocusEnabled(false);
        jLabel2.setVerifyInputWhenFocusTarget(false);

        jLabel8.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("*Ciudad:");
        jLabel8.setFocusable(false);
        jLabel8.setPreferredSize(new java.awt.Dimension(52, 21));
        jLabel8.setRequestFocusEnabled(false);
        jLabel8.setVerifyInputWhenFocusTarget(false);

        jLabel6.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel6.setText("Telefono:");
        jLabel6.setFocusable(false);
        jLabel6.setPreferredSize(new java.awt.Dimension(52, 21));
        jLabel6.setRequestFocusEnabled(false);
        jLabel6.setVerifyInputWhenFocusTarget(false);

        jLabel5.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("*Direccion:");
        jLabel5.setFocusable(false);
        jLabel5.setPreferredSize(new java.awt.Dimension(52, 21));
        jLabel5.setRequestFocusEnabled(false);
        jLabel5.setVerifyInputWhenFocusTarget(false);

        jLabel10.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("*Fecha Ingreso:");
        jLabel10.setFocusable(false);
        jLabel10.setPreferredSize(new java.awt.Dimension(52, 21));
        jLabel10.setRequestFocusEnabled(false);
        jLabel10.setVerifyInputWhenFocusTarget(false);

        jLabel9.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("*Fecha Nacimiento:");
        jLabel9.setFocusable(false);
        jLabel9.setPreferredSize(new java.awt.Dimension(52, 21));
        jLabel9.setRequestFocusEnabled(false);
        jLabel9.setVerifyInputWhenFocusTarget(false);

        jLabel3.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("*Apellidos:");
        jLabel3.setFocusable(false);
        jLabel3.setPreferredSize(new java.awt.Dimension(52, 21));
        jLabel3.setRequestFocusEnabled(false);
        jLabel3.setVerifyInputWhenFocusTarget(false);

        jLabel11.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Limite de Credito:");
        jLabel11.setFocusable(false);
        jLabel11.setPreferredSize(new java.awt.Dimension(52, 21));
        jLabel11.setRequestFocusEnabled(false);
        jLabel11.setVerifyInputWhenFocusTarget(false);

        jLabel12.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel12.setText("Deuda Actual:");
        jLabel12.setFocusable(false);
        jLabel12.setPreferredSize(new java.awt.Dimension(52, 21));
        jLabel12.setRequestFocusEnabled(false);
        jLabel12.setVerifyInputWhenFocusTarget(false);

        jLabel14.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel14.setText("Estado:");
        jLabel14.setFocusable(false);
        jLabel14.setPreferredSize(new java.awt.Dimension(52, 21));
        jLabel14.setRequestFocusEnabled(false);
        jLabel14.setVerifyInputWhenFocusTarget(false);

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jLabel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 75, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jLabel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jLabel3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jLabel6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jLabel5, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, txtTelefono, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, txtApellidos, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, txtNombres, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, txtIDCliente, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, txtDireccion, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, btnHistorial, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .add(6, 6, 6)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(jLabel9, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 131, Short.MAX_VALUE)
                    .add(jLabel11, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jLabel10, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jLabel8, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jLabel12, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jLabel14, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .add(0, 0, 0)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel1Layout.createSequentialGroup()
                        .add(cbActivo, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(0, 0, 0)
                        .add(jLabel7, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 157, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(org.jdesktop.layout.GroupLayout.LEADING, dchFechaIngreso, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, dchFechaNacimiento, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, txtCiudad, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, txtDeudaActual, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, txtCredito, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .add(6, 6, 6))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(jLabel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txtIDCliente, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel8, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txtCiudad, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(0, 0, 0)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(jLabel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txtNombres, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel9, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(dchFechaNacimiento, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(0, 0, 0)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(jLabel3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txtApellidos, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(dchFechaIngreso, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(0, 0, 0)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(jLabel5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txtDireccion, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel11, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txtCredito, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(0, 0, 0)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(jLabel6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txtTelefono, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel12, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txtDeudaActual, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(0, 0, 0)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.CENTER)
                    .add(btnHistorial, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 27, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel7, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel14, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cbActivo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(0, 0, 0))
        );

        jPanel1Layout.linkSize(new java.awt.Component[] {dchFechaIngreso, dchFechaNacimiento, txtApellidos, txtCiudad, txtCredito, txtDeudaActual, txtDireccion, txtIDCliente, txtNombres, txtTelefono}, org.jdesktop.layout.GroupLayout.VERTICAL);

        txtIDCliente.getAccessibleContext().setAccessibleParent(this);
        txtNombres.getAccessibleContext().setAccessibleParent(this);
        txtApellidos.getAccessibleContext().setAccessibleParent(this);
        txtDireccion.getAccessibleContext().setAccessibleParent(this);
        dchFechaNacimiento.getAccessibleContext().setAccessibleParent(this);
        btnHistorial.getAccessibleContext().setAccessibleParent(this);
        dchFechaIngreso.getAccessibleContext().setAccessibleParent(this);

        org.jdesktop.layout.GroupLayout jPanel4Layout = new org.jdesktop.layout.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel4Layout.createSequentialGroup()
                .add(0, 0, 0)
                .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jScrollPane3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .add(jPanel7, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 902, Short.MAX_VALUE)
                    .add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .add(0, 0, 0))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel4Layout.createSequentialGroup()
                .add(0, 0, 0)
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(0, 0, 0)
                .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(0, 0, 0)
                .add(jPanel7, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(0, 0, 0)
                .add(jScrollPane3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 173, Short.MAX_VALUE)
                .add(0, 0, 0))
        );

        jScrollPane4.setViewportView(jPanel4);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jScrollPane4)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jScrollPane4)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void btnUltimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUltimoActionPerformed
        if (!tblClientes.isEnabled()) {
            return;
        }
        cliAct = getDatos().numeroClientes() - 1;
        mostrarRegistro();
    }//GEN-LAST:event_btnUltimoActionPerformed
    private void btnBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBorrarActionPerformed
        if (txtNombres.getText().trim().equals("Generico")) {
            JOptionPane.showMessageDialog(this, "Este Cliente no se debe de eliminar...");
            return;
        }

        double deuda = Utilidades.controlDouble(txtDeudaActual.getValue());
        if (deuda != 0) {
            JOptionPane.showMessageDialog(this, "Cliente no puede ser eliminado por DEUDA");
            return;
        }

        int rta = JOptionPane.showConfirmDialog(this,
                "¿Esta Seguro de Eliminar Registro del Cliente?",
                "Eliminar Cliente", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (rta == 1) {
            return;
        }

        String msg = getDatos().borrarCliente(txtIDCliente.getText().replace("-", ""));
        if (!msg.equals("error")) {
            JOptionPane.showMessageDialog(this, msg);
        } else {
            JOptionPane.showMessageDialog(this, "Usuario con facturas Existe, "
                    + "Deberia de cambiar Estado a Cliente");
        }
        cliAct = 0;
        //Actualizamos los cambios en la Tabla
        llenarTabla();
        mostrarRegistro();
        reOrdenar(0);
        reOrdenar(0);
    }//GEN-LAST:event_btnBorrarActionPerformed
    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        String cliente = JOptionPane.showInputDialog("Ingrese la Cedula, Nombre o Apellido del Cliente");
        int num = tblClientes.getRowCount();
        for (int i = 0; i < num; i++) {
            if (Utilidades.objectToString(tblClientes.getValueAt(i, 0)).contains(cliente)) {
                cliAct = i;
                break;
            }
            if (Utilidades.objectToString(tblClientes.getValueAt(i, 1)).contains(cliente)) {
                cliAct = i;
                break;
            }
            if (Utilidades.objectToString(tblClientes.getValueAt(i, 2)).contains(cliente)) {
                cliAct = i;
                break;
            }
            if (cliente.equals("")) {
                return;
            }
            if (!getDatos().existeCliente(cliente)) {
                JOptionPane.showMessageDialog(this, "El Cliente No Existe");
                break;
            }
        }
        mostrarRegistro();
    }//GEN-LAST:event_btnBuscarActionPerformed
    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        //Botones Para habilitar:
        cancelar();
        nuevo = false;
        mostrarRegistro();
        reOrdenar(0);
        reOrdenar(0);
    }//GEN-LAST:event_btnCancelarActionPerformed
    
    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        nuevo(null);
    }//GEN-LAST:event_btnNuevoActionPerformed
    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        if (txtIDCliente.getText().replace(" ", "").replace("-", "").isEmpty()) {
            JOptionPane.showMessageDialog(this,
                    "Debe Digitar un ID de Cliente...");
            txtIDCliente.requestFocusInWindow();
            return;
        }

        if (txtNombres.getText().equals("")) {
            JOptionPane.showMessageDialog(this,
                    "Debe Digitar un Nombre...");
            txtNombres.requestFocusInWindow();
            return;
        }
        if (txtApellidos.getText().equals("")) {
            JOptionPane.showMessageDialog(this,
                    "Debe Digitar un Apellido...");
            txtApellidos.requestFocusInWindow();
            return;
        }
        if (txtDireccion.getText().equals("")) {
            JOptionPane.showMessageDialog(this,
                    "Debe Digitar la Direccion...");
            txtDireccion.requestFocusInWindow();
            return;
        }
        if (dchFechaNacimiento.getDate() == null) {
            JOptionPane.showMessageDialog(this,
                    "Debe Indicar Una Nueva Fecha de Nacimiento...");
            dchFechaNacimiento.requestFocusInWindow();
            return;
        }
        if ("".equals(txtCredito.getText())) {
            int dime = JOptionPane.showConfirmDialog(this,
                    "Cliente no tendra credito! \n Desea Continuar?!",
                    "Confirmacion de credito", JOptionPane.YES_NO_OPTION);
            if (dime == 1) {
                txtCredito.requestFocusInWindow();
                return;
            }
            txtCredito.setValue(0);
        }
        if (dchFechaNacimiento.getDate().after(new Date())) {
            JOptionPane.showMessageDialog(this,
                    "La Fecha de Nacimiento debe ser Anterior a la Fecha Actual");
            dchFechaNacimiento.requestFocusInWindow();
            return;
        }

        // si es nuevo validamos que el Cliente no exista
        if (nuevo) {
            if (getDatos().existeCliente(txtIDCliente.getText().replace("-", ""))) {
                JOptionPane.showMessageDialog(this, "Cliente Ya existe...");
                return;
            }
        } else if (!getDatos().existeCliente(txtIDCliente.getText().replace("-", ""))) {
            JOptionPane.showMessageDialog(this, "Cliente NO existe...");
            txtIDCliente.requestFocusInWindow();
            return;
        }
        byte estado = 0;
        if (cbActivo.isSelected()) {
            estado = 1;
        }

        //Creamos el Objeto Cliente y los agregamos a Datos
        Cliente miCliente = new Cliente(
                txtIDCliente.getText(),
                txtNombres.getText(),
                txtApellidos.getText(),
                txtCiudad.getText(),
                txtDireccion.getText(),
                txtTelefono.getText(),
                dchFechaNacimiento.getDate(),
                dchFechaIngreso.getDate(),
                Utilidades.controlDouble(txtCredito.getValue()), estado);

        String msg, accion = "editar";
        if (nuevo) {
            accion = "crear";
        }
        int resp = JOptionPane.showConfirmDialog(this,
                "<html><b><big>Se va a " + accion + " el Cliente: </big></b><big>" + txtNombres.getText() + " " + txtApellidos.getText() + "</big></html>"
                + "\n<html><b><big>Cedula no.: </big></b><big>" + txtIDCliente.getText() + "</big></html>"
                + "\n<html><b><big>Direccion: </big></b><big>" + txtDireccion.getText() + "</big></html>"
                + "\n<html><b><big>Telefono: </big></b><big>" + txtTelefono.getText() + "</big></html>"
                + "\n<html><b><big>Ciudad: </big></b><big>" + txtCiudad.getText() + "</big></html>"
                + "\n<html><b><big>Fecha Nacimiento: </big></b><big>" + Utilidades.formatDate(dchFechaNacimiento.getDate(), "dd-MM-yyyy") + "</big></html>"
                + "\n<html><b><big>Limite de Credito de: </big></b><big>" + txtCredito.getText() + "</big></html>"
                + "\n<html><b><big>Estado del Cliente: </big></b><big>" + cbActivo.getText() + "</big></html>"
                + "\n<html><b><big>Desea continuar? </big></b></html>",
                "Confirmacion de Usuario",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);
        if (resp == 1) {
            return;
        }
        if (nuevo) {
            msg = getDatos().agregarCliente(miCliente);
        } else {
            msg = getDatos().modificarCliente(miCliente);
        }

        JOptionPane.showMessageDialog(this, msg);

        //Actualizamos los cambios en la Tabla
        llenarTabla();

        btnCancelarActionPerformed(evt);


    }//GEN-LAST:event_btnGuardarActionPerformed
    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        if (txtNombres.getText().equals("Generico")) {
            JOptionPane.showMessageDialog(this,
                    "Cliente Generico no se puede Modificar!!!");
            return;
        }
        //Botones Para Deshabilitar:
        btnPrimero.setEnabled(false);
        btnAnterior.setEnabled(false);
        btnSiguiente.setEnabled(false);
        btnUltimo.setEnabled(false);
        btnNuevo.setEnabled(false);
        btnModificar.setEnabled(false);
        btnBorrar.setEnabled(false);
        btnBuscar.setEnabled(false);
        tblClientes.setEnabled(false);

        btnGuardar.setEnabled(true);
        btnCancelar.setEnabled(true);

        //Caja de Texto Habilitado
        txtNombres.setEditable(true);
        txtApellidos.setEditable(true);
        txtDireccion.setEditable(true);
        txtTelefono.setEditable(true);
        txtCiudad.setEditable(true);
        editor.setEditable(true);
        button.setEnabled(true);

        double deuda = Utilidades.controlDouble(txtDeudaActual.getValue());
        System.out.println("Deuda del cliente actual: " + deuda);

        if (deuda == 0.0) {
            System.out.println("Puede editar campo Credicto");
            txtCredito.setEditable(true);
        }

        //Desactivamos el Flag de registro Nuevo        
        nuevo = false;

        txtNombres.requestFocusInWindow();
    }//GEN-LAST:event_btnModificarActionPerformed
    private void btnAnteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAnteriorActionPerformed
        if (!tblClientes.isEnabled()) {
            return;
        }
        cliAct--;
        if (cliAct == -1) {
            cliAct = getDatos().numeroClientes() - 1;
        }
        mostrarRegistro();
    }//GEN-LAST:event_btnAnteriorActionPerformed
    private void btnPrimeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrimeroActionPerformed
        if (!tblClientes.isEnabled()) {
            return;
        }
        cliAct = 0;
        mostrarRegistro();
    }//GEN-LAST:event_btnPrimeroActionPerformed
    private void btnSiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSiguienteActionPerformed
        if (!tblClientes.isEnabled()) {
            return;
        }
        cliAct++;
        if (cliAct == getDatos().numeroClientes()) {
            cliAct = 0;
        }
        mostrarRegistro();
    }//GEN-LAST:event_btnSiguienteActionPerformed
    private void txtNombresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombresActionPerformed
        txtApellidos.requestFocusInWindow();
    }//GEN-LAST:event_txtNombresActionPerformed

    private void txtApellidosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtApellidosActionPerformed
        txtDireccion.requestFocusInWindow();
    }//GEN-LAST:event_txtApellidosActionPerformed
    private void txtDireccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDireccionActionPerformed
        txtTelefono.requestFocusInWindow();
    }//GEN-LAST:event_txtDireccionActionPerformed
    private void btnHistorialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHistorialActionPerformed
        if(miDetalle == null){
            miDetalle = new frmDetalleFacturaClientes(getDatos());
            dpnEscritorio.add(miDetalle);
        }
        try {
            miDetalle.setMaximum(false);
            miDetalle.setMaximum(true);
        } catch (PropertyVetoException ex) {
            LOGGER.getLogger(frmClientes.class.getName()).log(Level.SEVERE, null, ex);
        }
        miDetalle.setVisible(true);
    }//GEN-LAST:event_btnHistorialActionPerformed
    private void formInternalFrameActivated(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameActivated
        cliAct = 0;
        llenarTabla();
        mostrarRegistro();
        reOrdenar(6);
        reOrdenar(0);
        reOrdenar(0);
        btnCancelarActionPerformed(null);
    }//GEN-LAST:event_formInternalFrameActivated
    private void txtIDClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIDClienteActionPerformed
        txtNombres.requestFocusInWindow();
    }//GEN-LAST:event_txtIDClienteActionPerformed
    private void txtTelefonoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTelefonoActionPerformed
        txtCiudad.requestFocusInWindow();
    }//GEN-LAST:event_txtTelefonoActionPerformed
    private void tblClientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblClientesMouseClicked
        if (!tblClientes.isEnabled()) {
            return;
        }
        cliAct = tblClientes.getSelectedRow();
        mostrarRegistro();
    }//GEN-LAST:event_tblClientesMouseClicked
    private void cbActivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbActivoActionPerformed
        if (!txtNombres.isEditable()) {
            cbActivo.setSelected(!cbActivo.isSelected());
            return;
        }

        if (cbActivo.isSelected()) {
            cbActivo.setText("Activo");
        } else {
            cbActivo.setText("Inactivo");
        }
        btnGuardar.requestFocusInWindow();
    }//GEN-LAST:event_cbActivoActionPerformed
    private void txtCiudadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCiudadActionPerformed
        dchFechaNacimiento.requestFocusInWindow();
    }//GEN-LAST:event_txtCiudadActionPerformed

    private void txtNombresKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombresKeyReleased
        txtNombres.setText(txtNombres.getText().toUpperCase());
    }//GEN-LAST:event_txtNombresKeyReleased

    private void txtApellidosKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtApellidosKeyReleased
        txtApellidos.setText(txtApellidos.getText().toUpperCase());
    }//GEN-LAST:event_txtApellidosKeyReleased

    private void txtDireccionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDireccionKeyReleased
        txtDireccion.setText(txtDireccion.getText().toUpperCase());
    }//GEN-LAST:event_txtDireccionKeyReleased

    private void txtCiudadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCiudadKeyReleased
        txtCiudad.setText(txtCiudad.getText().toUpperCase());
    }//GEN-LAST:event_txtCiudadKeyReleased

    private void txtTelefonoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelefonoKeyReleased
        char caracter = evt.getKeyChar();
        if (caracter == '.') {
            return;
        }
        if (caracter == '(') {
            return;
        }
        if (caracter == ')') {
            return;
        }
        if (caracter == '-') {
            return;
        }
        if (caracter < '0' || (caracter > '9')) {
            evt.consume();  // ignorar el evento de teclado
        }
    }//GEN-LAST:event_txtTelefonoKeyReleased

    private void txtIDClienteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtIDClienteKeyReleased
        char caracter = evt.getKeyChar();

        if (caracter == '-') {
            return;
        }
        if (caracter < '0' || (caracter > '9')) {
            evt.consume();  // ignorar el evento de teclado
        }
    }//GEN-LAST:event_txtIDClienteKeyReleased

    private void txtCreditoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCreditoKeyReleased
        char caracter = evt.getKeyChar();
        if (caracter == '.') {
            return;
        }
        if (caracter < '0' || (caracter > '9')) {
            evt.consume();  // ignorar el evento de teclado
        }
    }//GEN-LAST:event_txtCreditoKeyReleased
    private void txtCreditoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCreditoActionPerformed
        cbActivo.requestFocusInWindow();
    }//GEN-LAST:event_txtCreditoActionPerformed
    private void dchFechaNacimientoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dchFechaNacimientoKeyReleased

        JOptionPane.showMessageDialog(this, evt.getKeyCode() + " " + KeyEvent.VK_ENTER);

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtCredito.requestFocusInWindow();
        }
    }//GEN-LAST:event_dchFechaNacimientoKeyReleased

    private void dchFechaNacimientoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_dchFechaNacimientoFocusGained
//        dchFechaNacimiento.requestFocusInWindow();
    }//GEN-LAST:event_dchFechaNacimientoFocusGained

    private void dchFechaNacimientoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_dchFechaNacimientoFocusLost
//        txtCredito.requestFocusInWindow();
//        dchFechaNacimiento.transferFocus();
    }//GEN-LAST:event_dchFechaNacimientoFocusLost

    private void chbDireccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chbDireccionActionPerformed
        reOrdenar(6);
        reOrdenar(6);
    }//GEN-LAST:event_chbDireccionActionPerformed

    private void chbTelefonoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chbTelefonoActionPerformed
        reOrdenar(7);
        reOrdenar(7);
    }//GEN-LAST:event_chbTelefonoActionPerformed

    private void chbCiudadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chbCiudadActionPerformed
        reOrdenar(8);
        reOrdenar(8);
    }//GEN-LAST:event_chbCiudadActionPerformed

    private void chbFechaNacimientoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chbFechaNacimientoActionPerformed
        reOrdenar(9);
        reOrdenar(9);
    }//GEN-LAST:event_chbFechaNacimientoActionPerformed

    private void chbFechaIngresoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chbFechaIngresoActionPerformed
        reOrdenar(10);
        reOrdenar(10);
    }//GEN-LAST:event_chbFechaIngresoActionPerformed

    private void chbSolo6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chbSolo6ActionPerformed
        reOrdenar(0);
        reOrdenar(0);
    }//GEN-LAST:event_chbSolo6ActionPerformed
    private void cancelar() {
        btnPrimero.setEnabled(true);
        btnAnterior.setEnabled(true);
        btnSiguiente.setEnabled(true);
        btnUltimo.setEnabled(true);
        btnNuevo.setEnabled(true);
        btnModificar.setEnabled(true);
        btnBorrar.setEnabled(true);
        btnBuscar.setEnabled(true);
        tblClientes.setEnabled(true);

        //Caja de Texto Deshabitar
        btnGuardar.setEnabled(false);
        btnCancelar.setEnabled(false);

        txtIDCliente.setEditable(false);
        txtNombres.setEditable(false);
        txtApellidos.setEditable(false);
        txtDireccion.setEditable(false);
        txtTelefono.setEditable(false);
        txtCiudad.setEditable(false);
        editor.setEditable(false);
        button.setEnabled(false);
        txtCredito.setEditable(false);
    }

    private KeyListener limitarCaracteres(final int limite, final JFormattedTextField txt) {

        KeyListener keyListener = new KeyListener() {
            private int suma = 0;

            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
            }

            @Override
            public void keyReleased(KeyEvent e) {
                suma = suma + 1;
                if (suma == limite) {
                    e.consume();
                    JOptionPane.showMessageDialog(null, "El limte se caracteres es " + suma + "\n" + txt.getText());

                }
            }
        };
        return keyListener;
    }

    private void llenarTabla() {
        String titulos[] = {"Cedula Cliente", "Nombres", "Apellidos",
            "Cretido", "Deuda Actual", "Estado", "Direccion", "Telefono",
            "Ciudad", "Fecha Nacimiento", "Fecha Ingreso"};
        Object registro[] = new Object[11];

        try {
            ResultSet rs = getDatos().getClientes();
            DefaultTableModel miTabla = new DefaultTableModel(null, titulos);
            while (rs.next()) {
                registro[0] = rs.getString("idCliente");
                registro[1] = rs.getString("nombres");
                registro[2] = rs.getString("apellidos");
                registro[3] = "RD$" + rs.getString("credito");
                registro[4] = "RD$" + rs.getString("deudaActual");
                registro[5] = rs.getBoolean("Estado");
                registro[6] = rs.getString("direccion");
                registro[7] = rs.getString("telefono");
                registro[8] = rs.getString("ciudad");
                registro[9] = Utilidades.formatDate(rs.getDate("fechaNacimiento"), "dd/MM/yyyy");
                registro[10] = Utilidades.formatDate(rs.getDate("fechaIngreso"), "dd/MM/yyyy");
                miTabla.addRow(registro);
            }
            tblClientes.setModel(miTabla);
        } catch (SQLException ex) {
            LOGGER.getLogger(frmClientes.class.getName()).log(Level.SEVERE, null, ex);
        }

        //Para Alinear el Texto de la Table a la Derecha...
        DefaultTableCellHeaderRenderer tcr = new DefaultTableCellHeaderRenderer();
        
        tcr.setHorizontalAlignment(SwingConstants.RIGHT);
        tblClientes.getColumnModel().getColumn(3).setCellRenderer(tcr);
        tblClientes.getColumnModel().getColumn(4).setCellRenderer(tcr);

        tblClientes.getColumnModel().getColumn(5).setCellEditor(new Celda_CheckBox());
        tblClientes.getColumnModel().getColumn(5).setCellRenderer(new Render_CheckBox());
    }

    private void mostrarRegistro() {
        if (tblClientes.getRowCount() == 0) {
            return;
        }
        
        txtIDCliente.setText(Utilidades.objectToString(tblClientes.getValueAt(cliAct, 0)));
        txtNombres.setText(Utilidades.objectToString(tblClientes.getValueAt(cliAct, 1)));
        txtApellidos.setText(Utilidades.objectToString(tblClientes.getValueAt(cliAct, 2)));
        txtCredito.setValue(Utilidades.objectToDouble(tblClientes.getValueAt(cliAct, 3)));
        txtDeudaActual.setValue(Utilidades.objectToDouble(tblClientes.getValueAt(cliAct, 4)));

        if (tblClientes.getValueAt(cliAct, 5).equals(true)) {
            cbActivo.setSelected(true);
            cbActivo.setText("Activo");
        } else {
            cbActivo.setSelected(false);
            cbActivo.setText("Inactivo");
        }
        txtDireccion.setText(Utilidades.objectToString(tblClientes.getValueAt(cliAct, 6)));
        txtTelefono.setText(Utilidades.objectToString(tblClientes.getValueAt(cliAct, 7)));
        txtCiudad.setText(Utilidades.objectToString(tblClientes.getValueAt(cliAct, 8)));
        dchFechaNacimiento.setDate(Utilidades.objectToDate(tblClientes.getValueAt(cliAct, 9)));
        dchFechaIngreso.setDate(Utilidades.objectToDate(tblClientes.getValueAt(cliAct, 10)));

        //Mover Cursor
        tblClientes.setRowSelectionInterval(cliAct, cliAct);
    }

    private void oyenteDeComponentes() {
        txtCredito.addFocusListener(new java.awt.event.FocusAdapter() {
            @Override
            public void focusGained(java.awt.event.FocusEvent evt) {
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        txtCredito.setSelectionStart(3);
                        txtCredito.setSelectionEnd(txtCredito.getText().length());
                    }
                });
            }
        });
    }

    private void reOrdenar(int colm) {
        if (tblClientes.getRowCount() == 0) {
            return;
        }
        TableColumn miTableColumn;
        for (int i = 0; i <= 10; i++) {
            miTableColumn = tblClientes.getColumnModel().getColumn(i);
            if (i == 0) {
                if (colm == 0) {
                    chbSolo6.setSelected(true);
                }
                miTableColumn.setPreferredWidth(110);
                miTableColumn.setMaxWidth(110);
                miTableColumn.setMinWidth(110);
            }
            if (i == 1) {
                miTableColumn.setPreferredWidth(170);
                miTableColumn.setMaxWidth(270);
                miTableColumn.setMinWidth(170);
            }
            if (i == 2) {
                miTableColumn.setPreferredWidth(170);
                miTableColumn.setMaxWidth(270);
                miTableColumn.setMinWidth(170);
            }
            if (i == 3) {
                miTableColumn.setPreferredWidth(100);
                miTableColumn.setMaxWidth(100);
                miTableColumn.setMinWidth(100);
            }
            if (i == 4) {
                miTableColumn.setPreferredWidth(100);
                miTableColumn.setMaxWidth(100);
                miTableColumn.setMinWidth(100);
            }
            if (i == 5) {
                miTableColumn.setPreferredWidth(100);
                miTableColumn.setMaxWidth(100);
                miTableColumn.setMinWidth(100);
            }
            if (i == 6) {//Desde Aqui
                miTableColumn.setPreferredWidth(0);
                miTableColumn.setMaxWidth(0);
                miTableColumn.setMinWidth(0);
                if (colm == 6) {
                    miTableColumn.setPreferredWidth(400);
                    miTableColumn.setMaxWidth(400);
                    miTableColumn.setMinWidth(400);
                }
            }
            if (i == 7) {
                miTableColumn.setPreferredWidth(0);
                miTableColumn.setMaxWidth(0);
                miTableColumn.setMinWidth(0);
                if (colm == 7) {
                    miTableColumn.setPreferredWidth(120);
                    miTableColumn.setMaxWidth(120);
                    miTableColumn.setMinWidth(120);
                }
            }
            if (i == 8) {
                miTableColumn.setPreferredWidth(0);
                miTableColumn.setMaxWidth(0);
                miTableColumn.setMinWidth(0);
                if (colm == 8) {
                    miTableColumn.setPreferredWidth(200);
                    miTableColumn.setMaxWidth(200);
                    miTableColumn.setMinWidth(200);
                }
            }
            if (i == 9) {
                miTableColumn.setPreferredWidth(0);
                miTableColumn.setMaxWidth(0);
                miTableColumn.setMinWidth(0);
                if (colm == 9) {
                    miTableColumn.setPreferredWidth(150);
                    miTableColumn.setMaxWidth(150);
                    miTableColumn.setMinWidth(150);
                }
            }
            if (i == 10) {//Hasta aqui
                miTableColumn.setPreferredWidth(0);
                miTableColumn.setMaxWidth(0);
                miTableColumn.setMinWidth(0);
                if (colm == 10) {
                    miTableColumn.setPreferredWidth(150);
                    miTableColumn.setMaxWidth(150);
                    miTableColumn.setMinWidth(150);
                }
            }
            tblClientes.repaint();
        }
        tblClientes.setRowSelectionInterval(cliAct, cliAct);
    }
    public void nuevo(Object idCliente) {
        //Botones Para Deshabilitar:
        btnPrimero.setEnabled(false);
        btnAnterior.setEnabled(false);
        btnSiguiente.setEnabled(false);
        btnUltimo.setEnabled(false);
        btnNuevo.setEnabled(false);
        btnModificar.setEnabled(false);
        btnBorrar.setEnabled(false);
        btnBuscar.setEnabled(false);
        tblClientes.setEnabled(false);

        //Caja de Texto Habilitado
        btnGuardar.setEnabled(true);
        btnCancelar.setEnabled(true);

        //Caja de Textos
        txtIDCliente.setEditable(true);
        txtNombres.setEditable(true);
        txtApellidos.setEditable(true);
        txtDireccion.setEditable(true);
        txtTelefono.setEditable(true);
        txtCiudad.setEditable(true);
        editor.setEditable(true);
        button.setEnabled(true);
        txtCredito.setEditable(true);

        //txt Vaciar
        txtNombres.setText("");
        txtApellidos.setText("");
        txtDireccion.setText("");
        txtTelefono.setText("");
        txtCiudad.setText("");
        dchFechaNacimiento.setDate(new Date());
        dchFechaIngreso.setDate(new Date());
        txtCredito.setValue(0);
        txtDeudaActual.setValue(0);
        cbActivo.setSelected(false);
        cbActivo.setText("Inactivo");

        txtIDCliente.grabFocus();
        txtIDCliente.requestFocusInWindow();

        //Activamos el Flag de registro Nuevo        
        nuevo = true;
        txtIDCliente.setValue(idCliente);

        if (idCliente != null) {
            txtIDCliente.setEditable(false);
            txtNombres.grabFocus();
            txtNombres.requestFocusInWindow();
        }

    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAnterior;
    private javax.swing.JButton btnBorrar;
    private javax.swing.JButton btnBuscar;
    protected javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnHistorial;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnPrimero;
    private javax.swing.JButton btnSiguiente;
    private javax.swing.JButton btnUltimo;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JCheckBox cbActivo;
    private javax.swing.JCheckBox chbCiudad;
    private javax.swing.JCheckBox chbDireccion;
    private javax.swing.JCheckBox chbFechaIngreso;
    private javax.swing.JCheckBox chbFechaNacimiento;
    private javax.swing.JCheckBox chbSolo6;
    private javax.swing.JCheckBox chbTelefono;
    private com.toedter.calendar.JDateChooser dchFechaIngreso;
    private com.toedter.calendar.JDateChooser dchFechaNacimiento;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable tblClientes;
    private javax.swing.JTextField txtApellidos;
    private javax.swing.JTextField txtCiudad;
    private javax.swing.JFormattedTextField txtCredito;
    private javax.swing.JFormattedTextField txtDeudaActual;
    private javax.swing.JTextField txtDireccion;
    private javax.swing.JFormattedTextField txtIDCliente;
    private javax.swing.JTextField txtNombres;
    private javax.swing.JFormattedTextField txtTelefono;
    // End of variables declaration//GEN-END:variables

}
