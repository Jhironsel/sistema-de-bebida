package formulario;

import static clases.Utilidades.LOGGER;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import javax.swing.JOptionPane;

public class frmParametros extends javax.swing.JFrame {

    private final Properties propiedades;

    public frmParametros() {
        System.out.println("Clase Parametros");
        initComponents();
        propiedades = new Properties();
        String file = System.getProperty("user.dir") 
                + "/Propeties/propiedades.properties";
        try {
            propiedades.load(new FileReader(file));
        } catch (FileNotFoundException ex) {
            LOGGER.getLogger(frmParametros.class.getName()).log(Level.SEVERE, 
                    null, ex);
        } catch (IOException ex) {
            LOGGER.getLogger(frmParametros.class.getName()).log(Level.SEVERE, 
                    null, ex);
        }
        cargarParamentos("todo");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        btnCancelar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        txtValor2 = new javax.swing.JTextField();
        txtValor3 = new javax.swing.JTextField();
        txtValor4 = new javax.swing.JTextField();
        txtValor1 = new javax.swing.JTextField();
        btnAceptar = new javax.swing.JButton();
        txtPuerto = new javax.swing.JTextField();
        txthost = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        rbtnProtocoloIPV4 = new javax.swing.JRadioButton();
        rbtnNombreServidor = new javax.swing.JRadioButton();
        chBPuerto = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Parametros del Sistema");
        setAlwaysOnTop(true);
        setMinimumSize(new java.awt.Dimension(439, 200));
        setResizable(false);
        setSize(new java.awt.Dimension(439, 210));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(204, 255, 204));

        jLabel2.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        jLabel2.setText("Nombre del Servidor:");

        jLabel3.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        jLabel3.setText("Numero de Puerto:");

        jLabel1.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        jLabel1.setText("Protocolo IPV4:");

        btnCancelar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Cancelar 32 x 32.png"))); // NOI18N
        btnCancelar.setMnemonic('c');
        btnCancelar.setText("Cancelar");
        btnCancelar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCancelarMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCancelarMouseEntered(evt);
            }
        });
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(204, 255, 204));
        jPanel2.setLayout(new java.awt.GridBagLayout());

        txtValor2.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        txtValor2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtValor2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtValor2KeyTyped(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 25;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 6, 0, 6);
        jPanel2.add(txtValor2, gridBagConstraints);

        txtValor3.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        txtValor3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtValor3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtValor3KeyTyped(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 25;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 6, 0, 6);
        jPanel2.add(txtValor3, gridBagConstraints);

        txtValor4.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        txtValor4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtValor4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtValor4KeyTyped(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 27;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 6, 0, 6);
        jPanel2.add(txtValor4, gridBagConstraints);

        txtValor1.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        txtValor1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtValor1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtValor1KeyTyped(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.ipadx = 25;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 6, 0, 6);
        jPanel2.add(txtValor1, gridBagConstraints);

        btnAceptar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnAceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Aceptar 32 x 32.png"))); // NOI18N
        btnAceptar.setMnemonic('a');
        btnAceptar.setText("Aceptar");
        btnAceptar.setToolTipText("");
        btnAceptar.setPreferredSize(new java.awt.Dimension(123, 44));
        btnAceptar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAceptarMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAceptarMouseEntered(evt);
            }
        });
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });

        txtPuerto.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        txtPuerto.setToolTipText("");
        txtPuerto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPuertoKeyReleased(evt);
            }
        });

        txthost.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N

        jPanel1.setBackground(new java.awt.Color(204, 255, 204));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Preferencias", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Ubuntu", 0, 14))); // NOI18N

        buttonGroup1.add(rbtnProtocoloIPV4);
        rbtnProtocoloIPV4.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        rbtnProtocoloIPV4.setText("Protocolo IPV4");
        rbtnProtocoloIPV4.setOpaque(false);
        rbtnProtocoloIPV4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rbtnProtocoloIPV4MouseClicked(evt);
            }
        });
        rbtnProtocoloIPV4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnProtocoloIPV4ActionPerformed(evt);
            }
        });

        buttonGroup1.add(rbtnNombreServidor);
        rbtnNombreServidor.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        rbtnNombreServidor.setText("Nombre del servidor");
        rbtnNombreServidor.setOpaque(false);
        rbtnNombreServidor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                rbtnNombreServidorMouseClicked(evt);
            }
        });
        rbtnNombreServidor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnNombreServidorActionPerformed(evt);
            }
        });

        chBPuerto.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        chBPuerto.setText("Con Puerto");
        chBPuerto.setOpaque(false);
        chBPuerto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                chBPuertoMouseClicked(evt);
            }
        });
        chBPuerto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chBPuertoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rbtnNombreServidor)
                    .addComponent(chBPuerto)
                    .addComponent(rbtnProtocoloIPV4))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(rbtnNombreServidor)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rbtnProtocoloIPV4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(chBPuerto)
                .addGap(0, 0, 0))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtPuerto, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txthost)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                                .addComponent(btnCancelar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(0, 0, 0)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(0, 0, 0)
                        .addComponent(txthost, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jLabel1)
                        .addGap(0, 0, 0)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(txtPuerto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addGap(6, 6, 6)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAceptarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAceptarMouseExited
        btnAceptar.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnAceptarMouseExited
    private void btnAceptarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAceptarMouseEntered
        btnAceptar.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnAceptarMouseEntered
    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        if (rbtnProtocoloIPV4.isSelected()) {
            int valor;
            try {
                valor = Integer.parseInt(txtValor1.getText());
            } catch (NumberFormatException e) {
                valor = -1;
            }

            if (valor < 0 || valor > 255) {
                JOptionPane.showMessageDialog(rootPane, "Valor incorrecto en el Ambito 1");
                txtValor1.setText("");
                txtValor1.requestFocusInWindow();
                return;
            }

            try {
                valor = Integer.parseInt(txtValor2.getText());
            } catch (NumberFormatException e) {
                valor = -1;
            }
            if (valor < 0 || valor > 255) {
                JOptionPane.showMessageDialog(rootPane, "Valor incorrecto en el Ambito 2");
                txtValor2.setText("");
                txtValor2.requestFocusInWindow();
                return;
            }

            try {
                valor = Integer.parseInt(txtValor3.getText());
            } catch (NumberFormatException e) {
                valor = -1;
            }
            if (valor < 0 || valor > 255) {
                JOptionPane.showMessageDialog(rootPane, "Valor incorrecto en el Ambito 3");
                txtValor3.setText("");
                txtValor3.requestFocusInWindow();
                return;
            }

            try {
                valor = Integer.parseInt(txtValor4.getText());
            } catch (NumberFormatException e) {
                valor = -1;
            }
            if (valor < 0 || valor > 255) {
                JOptionPane.showMessageDialog(rootPane, "Valor incorrecto en el Ambito 4");
                txtValor4.setText("");
                txtValor4.requestFocusInWindow();
                return;
            }
        }

        if (rbtnNombreServidor.isSelected()) {
            if (txthost.getText().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Nombre del servidor vacio");
                txthost.requestFocusInWindow();
                return;
            }
        }

        if (chBPuerto.isSelected()) {
            int valor;
            try {
                valor = Integer.parseInt(txtPuerto.getText());
            } catch (NumberFormatException e) {
                valor = -1;
            }
            if (valor < 0 || valor > 65535) {
                JOptionPane.showMessageDialog(rootPane, "Este Puerto no es valido");
                txtPuerto.setText("");
                txtPuerto.requestFocusInWindow();
                return;
            }
        }

        escribirParametros();
        System.exit(0);
    }//GEN-LAST:event_btnAceptarActionPerformed
    private void btnCancelarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancelarMouseExited
        btnCancelar.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnCancelarMouseExited
    private void btnCancelarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancelarMouseEntered
        btnCancelar.setForeground(Color.blue);
    }//GEN-LAST:event_btnCancelarMouseEntered
    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed
    private void rbtnNombreServidorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnNombreServidorActionPerformed
        valoresEstados(false);
        txtValor1.setText("");
        txtValor2.setText("");
        txtValor3.setText("");
        txtValor4.setText("");
    }//GEN-LAST:event_rbtnNombreServidorActionPerformed

    private void rbtnProtocoloIPV4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnProtocoloIPV4ActionPerformed
        valoresEstados(true);
        txthost.setText("");
    }//GEN-LAST:event_rbtnProtocoloIPV4ActionPerformed
    private void txtValor4KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValor4KeyTyped
        int k = (int) evt.getKeyChar();
        if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!", "Ventana Error Datos", JOptionPane.ERROR_MESSAGE);
        }
        if (k == 241 || k == 209) {
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!", "Ventana Error Datos", JOptionPane.ERROR_MESSAGE);
        }
        if (k == 10) {
            txtValor4.transferFocus();
        }
    }//GEN-LAST:event_txtValor4KeyTyped
    private void txtValor3KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValor3KeyTyped
        int k = (int) evt.getKeyChar();
        if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!", "Ventana Error Datos", JOptionPane.ERROR_MESSAGE);
        }
        if (k == 241 || k == 209) {
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!",
                    "Ventana Error Datos", JOptionPane.ERROR_MESSAGE);
        }
        if (k == 10) {
            txtValor3.transferFocus();
        }
    }//GEN-LAST:event_txtValor3KeyTyped
    private void txtValor2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValor2KeyTyped
        int k = (int) evt.getKeyChar();
        if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!",
                    "Ventana Error Datos", JOptionPane.ERROR_MESSAGE);
        }
        if (k == 241 || k == 209) {
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!",
                    "Ventana Error Datos", JOptionPane.ERROR_MESSAGE);
        }
        if (k == 10) {
            txtValor2.transferFocus();
        }
    }//GEN-LAST:event_txtValor2KeyTyped
    private void txtValor1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValor1KeyTyped
        int k = (int) evt.getKeyChar();
        if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!",
                    "Ventana Error Datos", JOptionPane.ERROR_MESSAGE);
        }
        if (k == 241 || k == 209) {
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!",
                    "Ventana Error Datos", JOptionPane.ERROR_MESSAGE);
        }
        if (k == 10) {
            txtValor1.transferFocus();
        }
    }//GEN-LAST:event_txtValor1KeyTyped
    private void txtPuertoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPuertoKeyReleased
        char caracter = evt.getKeyChar();
        if (caracter < '0' || (caracter > '9')) {
            evt.consume();  // ignorar el evento de teclado
        }
    }//GEN-LAST:event_txtPuertoKeyReleased

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        System.gc();
    }//GEN-LAST:event_formWindowClosed

    private void rbtnNombreServidorMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rbtnNombreServidorMouseClicked
        cargarParamentos("nombre");
    }//GEN-LAST:event_rbtnNombreServidorMouseClicked

    private void rbtnProtocoloIPV4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_rbtnProtocoloIPV4MouseClicked
        cargarParamentos("ipv4");
    }//GEN-LAST:event_rbtnProtocoloIPV4MouseClicked

    private void chBPuertoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_chBPuertoMouseClicked
//        cargarParamentos("puerto");
    }//GEN-LAST:event_chBPuertoMouseClicked

    private void chBPuertoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chBPuertoActionPerformed
        cargarParamentos("puerto");
    }//GEN-LAST:event_chBPuertoActionPerformed
    private void valoresEstados(boolean estado) {
        txtValor1.setEnabled(estado);
        txtValor2.setEnabled(estado);
        txtValor3.setEnabled(estado);
        txtValor4.setEnabled(estado);
        txthost.setEnabled(!estado);
        if(estado){
            txtValor1.requestFocusInWindow();
        }else{
            txthost.requestFocusInWindow();
        }
    }

    private void cargarParamentos(String zona) {
        if (zona.equals("todo") || zona.equals("puerto")) {
            if (Boolean.valueOf(propiedades.getProperty("Con_Puerto", "false"))) {
                if (zona.equals("todo")) {
                    txtPuerto.setText(propiedades.getProperty("Puerto_del_Servidor", ""));
                    chBPuerto.doClick();
                }

                if (zona.equals("puerto")) {
                    if (chBPuerto.isSelected()) {
                        txtPuerto.setEnabled(true);
                        if(Boolean.valueOf(propiedades.getProperty("Con_Puerto", "false"))){
                            txtPuerto.setText(propiedades.getProperty("Puerto_del_Servidor", ""));
                        }
                        txtPuerto.requestFocusInWindow();
                    } else {
                        txtPuerto.setEnabled(false);
                        txtPuerto.setText("");
                    }
                }
            } else {
                if (chBPuerto.isSelected()) {
                    txtPuerto.setEnabled(true);
                    txtPuerto.requestFocusInWindow();
                } else {
                    txtPuerto.setEnabled(false);
                    txtPuerto.setText("");
                }
            }
        }

        if (zona.equals("todo") || zona.equals("ipv4")) {
            if (Boolean.valueOf(propiedades.getProperty("ProtocoloActivo", "false"))) {
                txtValor1.setText(propiedades.getProperty("Ip_Servidor1", ""));
                txtValor2.setText(propiedades.getProperty("Ip_Servidor2", ""));
                txtValor3.setText(propiedades.getProperty("Ip_Servidor3", ""));
                txtValor4.setText(propiedades.getProperty("Ip_Servidor4", ""));
                rbtnProtocoloIPV4.doClick();
            }
        }

        if (zona.equals("todo") || zona.equals("nombre")) {
            if (Boolean.valueOf(propiedades.getProperty("NombreActivo", "false"))) {
                txthost.setText(propiedades.getProperty("Nombre_del_Servidor", ""));
                rbtnNombreServidor.doClick();
            }
        }
    }

    private void escribirParametros() {
        propiedades.setProperty("Ip_Servidor1", txtValor1.getText());
        propiedades.setProperty("Ip_Servidor2", txtValor2.getText());
        propiedades.setProperty("Ip_Servidor3", txtValor3.getText());
        propiedades.setProperty("Ip_Servidor4", txtValor4.getText());
        propiedades.setProperty("ProtocoloActivo", "" + rbtnProtocoloIPV4.isSelected());
        propiedades.setProperty("NombreActivo", "" + rbtnNombreServidor.isSelected());
        propiedades.setProperty("Nombre_del_Servidor", txthost.getText());
        propiedades.setProperty("Puerto_del_Servidor", txtPuerto.getText());
        propiedades.setProperty("Con_Puerto", "" + chBPuerto.isSelected());

        try {
            propiedades.store(new FileWriter(System.getProperty("user.dir") + 
                "/Propeties/propiedades.properties"), "Parametros del Servidor");
        } catch (IOException ex) {
            LOGGER.getLogger(frmParametros.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JCheckBox chBPuerto;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JRadioButton rbtnNombreServidor;
    private javax.swing.JRadioButton rbtnProtocoloIPV4;
    private javax.swing.JTextField txtPuerto;
    private javax.swing.JTextField txtValor1;
    private javax.swing.JTextField txtValor2;
    private javax.swing.JTextField txtValor3;
    private javax.swing.JTextField txtValor4;
    private javax.swing.JTextField txthost;
    // End of variables declaration//GEN-END:variables
}
