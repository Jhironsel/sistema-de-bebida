package swingWorker;

import clases.Datos;
import formulario.frmLogin;
import static formulario.frmSplash.pgProceso;
import java.util.List;
import javax.swing.SwingWorker;

public class Trabajo extends SwingWorker<Double, Integer> {

    private Datos datos;
    private String estado, usuario, clave;
    private int perfil;
    private frmLogin login;

    public int getPerfil() {
        return perfil;
    }
    public final void setPerfil(int perfil) {
        this.perfil = perfil;
    }

    public String getUsuario() {
        return usuario;
    }
    public final void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }
    public final void setClave(String clave) {
        this.clave = clave;
    }

    private Datos getDatos() {
        return datos;
    }
    private void setDatos(Datos datos) {
        this.datos = datos;
    }

    public frmLogin getLogin() {
        return login;
    }
    private void setLogin(frmLogin login) {
        this.login = login;
    }
    
    
    
    public Trabajo(int Perfil, 
            String usuario, String clave, Datos datos, frmLogin login) {
        System.out.println("Clase Trabajo");
        setDatos(datos);
        setPerfil(Perfil);
        setUsuario(usuario);
        setClave(clave);
        setLogin(login);        
    }
    
    @Override
    protected Double doInBackground() throws Exception {
        System.out.println("Cargando formularios {Clase Trabajo}...");
        
        publish(4);
        estado = "Cargando Formulario principal";
        
        
        publish(10);
        estado = "Cargando Clientes";
        
        
        publish(13);
        estado = "Cargando Productos";
        
        publish(20);
        estado = "Cargando Usuarios";
        
        publish(25);
        estado = "Cargado Reporte Factura";
        
        publish(28);
        estado = "Turnos Cargados";
        
        publish(29);
        estado = "Limpiando";
        
        publish(35);
        estado = "Formulario de Turno";
        
        publish(41);
        estado = "Leyendo Turno y formulario Deudas";
        
        publish(59);
        estado = "Inventario y Categoria Listo";
        
        
        publish(74);
        estado = "Cargando Sistema Bebida Factura...";
        
        publish(89);
        estado = "Cargando Sistema Bebida Factura...";
        
        publish(96);
        estado = "Cargando Sistema Bebida Factura...";
        return 100.0;
    }
    @Override
    protected void process(List<Integer> chunks) {
        pgProceso.setValue(chunks.get(0));
        pgProceso.setString("" + chunks.get(0) + " %" +"      "+estado);
    }
    @Override
    protected void done() {
        System.out.println("Terminado Clase: Trabajo");
        pgProceso.setValue(100);
        //Maximizar la ventana
    }
}