package hilos;

import clases.Datos;
import clases.ScreenSplash;
import static clases.Utilidades.LOGGER;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

public class hiloRestaurar extends Thread implements hiloMetodos {

    private boolean retaurar = true;
    private String RGBAK, BDR, log, usuarioMaster, BD, claveMaster;
    private JPasswordField pf;

    public hiloRestaurar() {
        if (ScreenSplash.debuger) {
            System.out.println("Clase Hilo Restaurar");
        }
    }

    @Override
    public void run() {
        if (retaurar) {
            if (ScreenSplash.debuger) {
                System.out.println("Hilo Restaurar en el metodo run()");
            }
            Process p;
            BufferedReader stdInput;
            try {
                p = Runtime.getRuntime().exec(RGBAK
                        + " -c -v -USER " + usuarioMaster + " -PASSWORD "
                        + claveMaster + " "
                        + BDR + " "
                        + BD);

                stdInput = new BufferedReader(new InputStreamReader(
                        p.getInputStream()));

                String linea;

                do {
                    linea = stdInput.readLine();
                    if (ScreenSplash.debuger) {
                        System.out.println(linea);
                    }
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ex) {
                        LOGGER.getLogger(hiloRestaurar.class.getName()).log(Level.SEVERE, null, ex);
                    }

                } while (stdInput.ready());

                if (linea == null || linea.equals("null")) {
                    JOptionPane.showMessageDialog(null,
                            "Usuario no valido o no puede realizarse el "
                            + "backup...",
                            "Validacion no completada",
                            JOptionPane.ERROR_MESSAGE);
                    return;

                }
            } catch (IOException ex) {
                LOGGER.getLogger(hiloRestaurar.class
                        .getName()).log(Level.SEVERE, null, ex);
                return;
            }

            JOptionPane.showMessageDialog(null, "Base de Datos restaurada",
                    "sistema de restauracion",
                    JOptionPane.INFORMATION_MESSAGE);

            detener();
        }//Para restaurar una base de datos en segundo plano....
    }

    @Override
    public void detener() {
        retaurar = false;
    }

    @Override
    public void iniciar() {
        retaurar = true;
    }

    @Override
    public void correr() {
        if (ScreenSplash.debuger) {
            System.out.println("Hilo Restaurar en accion!!!");
        }
        BD = Datos.pathBaseDeDatos();

        BDR = System.getProperty("user.dir") + "/Data/respaldo/SistemaDeBebida.FBK";

        log = System.getProperty("user.dir") + "/Data/respaldo/Respaldo.Log";

        RGBAK = System.getProperty("user.dir") + "/respaldo/gbak";

        usuarioMaster = null;
        usuarioMaster = JOptionPane.showInputDialog(null,
                "Inserte el nombre de Usuario: ", "Usuario...",
                JOptionPane.INFORMATION_MESSAGE);
        if (usuarioMaster == null) {
            return;
        }

        pf = new JPasswordField();
        claveMaster = null;
        pf.requestFocusInWindow();
        pf.requestFocus();

        claveMaster = "" + JOptionPane.showConfirmDialog(null, pf,
                "Inserte el nombre de Usuario: ", JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.INFORMATION_MESSAGE);
        pf.requestFocusInWindow();
        pf.requestFocus();

        if (claveMaster.equals(JOptionPane.CANCEL_OPTION) || claveMaster == null) {
            return;
        }

        Process p;
        BufferedReader stdInput;
//            Calendar miCan = Calendar.getInstance();
        try {//RGBAK.getAbsolutePath()+                        
            String comando = "gbak -b -nodbtriggers -l -nt -v -user '"
                    + usuarioMaster
                    + "' -password '" + new String(pf.getPassword())
                    + "' -y " + log + " "
                    + BD + " "
                    + BDR;
//                        + BDR.getAbsolutePath().replace(".FBK", "")
//                        + miCan.get(Calendar.DATE) + "_"
//                        + (miCan.get(Calendar.MONTH) + 1) + "_"
//                        + miCan.get(Calendar.YEAR) + ".FBK";
            if (ScreenSplash.debuger) {
                System.out.println(comando);
            }
            p = Runtime.getRuntime().exec(comando);
            stdInput = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            String linea;
            do {
                linea = stdInput.readLine();
                if (linea != null) {
                    JOptionPane.showMessageDialog(null,
                            "Usuario no valido o no puede realizarse el "
                            + "backup...",
                            "Validacion no completada",
                            JOptionPane.ERROR_MESSAGE);
                }
                if (ScreenSplash.debuger) {
                    System.out.println(linea);
                }
            } while (stdInput.ready());
        } catch (IOException ex) {
            LOGGER.getLogger(hiloRestaurar.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }
}
