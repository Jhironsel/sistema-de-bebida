package hilos;

import clases.ScreenSplash;
import static clases.Utilidades.LOGGER;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import javax.swing.JOptionPane;

public class hiloIp extends Thread implements hiloMetodos {

    private boolean iniciar = true;

    @Override
    public void run() {
        if (ScreenSplash.debuger) {
            System.out.println("+Clase hilo Ip");
        }
        while (iniciar) {
            if (ScreenSplash.debuger) {
                System.out.println("Hilo IP");
            }
            Process p;
            BufferedReader stdInput;
            try {
                p = Runtime.getRuntime().exec("wget http://ipinfo.io/ip -qO -");

            } catch (IOException ex) {
                LOGGER.getLogger(hiloIp.class
                        .getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(null, "No esta conectado a ninguna RED");
                return;
            }
            stdInput = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            try {
                String resp = stdInput.readLine();
                if (resp == null) {
                    JOptionPane.showMessageDialog(null, "Revise su conexion a Internet");
                    return;
                }
                JOptionPane.showMessageDialog(null, resp);

            } catch (IOException ex) {
                LOGGER.getLogger(hiloIp.class
                        .getName()).log(Level.SEVERE, null, ex);
            }
            detener();
        }
    }

    @Override
    public void detener() {
        iniciar = false;
    }

    @Override
    public void iniciar() {
        iniciar = true;
    }

    @Override
    public void correr() {

    }

}
