package slidepanel;

public interface StateListener {
   public void toggleState() ;
   public void reset() ;

}
