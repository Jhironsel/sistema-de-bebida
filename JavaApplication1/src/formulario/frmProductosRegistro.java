package formulario;

import clases.Categoria;
import clases.Datos;
import clases.Producto;
import clases.Utilidades;
import static clases.Utilidades.LOGGER;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class frmProductosRegistro extends javax.swing.JDialog {
    
    private String source;
    private boolean nuevo;
    private Producto p;
    
    public frmProductosRegistro(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        nuevo = true;
    }
    
    public frmProductosRegistro(java.awt.Frame parent, boolean modal, 
            Producto p) {
        super(parent, modal);
        initComponents();
        txtCodigo.setText(p.getCodigo());
        txtDescripcion.setText(p.getDescripcion());
        txtNotas.setText(p.getNota());
        cbActivo.setSelected(p.isEstado());
        this.p = p;
        updateCategoria();
        nuevo = false;
        txtCodigo.setEditable(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtNotas = new javax.swing.JTextArea();
        cbActivo = new javax.swing.JCheckBox();
        cbCategoria = new javax.swing.JComboBox();
        txtCodigo = new rojeru_san.RSMTextFull();
        txtDescripcion = new rojeru_san.RSMTextFull();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jlImagen = new javax.swing.JLabel();
        btnAdmCategorias = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Registro de producto.");
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Detalle del Producto"));

        jLabel7.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        jLabel7.setText("* Campos Obligatorios");
        jLabel7.setToolTipText("");

        jLabel5.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Notas :");

        txtNotas.setColumns(20);
        txtNotas.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        txtNotas.setLineWrap(true);
        txtNotas.setWrapStyleWord(true);
        txtNotas.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtNotasKeyReleased(evt);
            }
        });
        jScrollPane2.setViewportView(txtNotas);

        cbActivo.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        cbActivo.setForeground(new java.awt.Color(0, 0, 0));
        cbActivo.setSelected(true);
        cbActivo.setText("Activo");
        cbActivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbActivoActionPerformed(evt);
            }
        });

        cbCategoria.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbCategoria.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(32, 131, 255), 3, true));
        cbCategoria.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbCategoriaItemStateChanged(evt);
            }
        });
        cbCategoria.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                cbCategoriaFocusGained(evt);
            }
        });
        cbCategoria.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
                cbCategoriaPopupMenuWillBecomeVisible(evt);
            }
        });

        txtCodigo.setPlaceholder("Codigo del Producto...");
        txtCodigo.setxDarkIcon(true);
        txtCodigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCodigoActionPerformed(evt);
            }
        });
        txtCodigo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCodigoKeyTyped(evt);
            }
        });

        txtDescripcion.setToolTipText("");
        txtDescripcion.setPlaceholder("Descripcion del producto...");
        txtDescripcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDescripcionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(txtCodigo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtDescripcion, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(93, 93, 93)
                                .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE))
                            .addComponent(cbActivo, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cbCategoria, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtDescripcion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cbActivo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        btnGuardar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnGuardar.setForeground(new java.awt.Color(1, 1, 1));
        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Guardar 32 x 32.png"))); // NOI18N
        btnGuardar.setMnemonic('g');
        btnGuardar.setText("Guardar");
        btnGuardar.setToolTipText("Guardar Registro Actual");
        btnGuardar.setMaximumSize(new java.awt.Dimension(80, 44));
        btnGuardar.setMinimumSize(new java.awt.Dimension(80, 44));
        btnGuardar.setPreferredSize(new java.awt.Dimension(80, 44));
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnCancelar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnCancelar.setForeground(new java.awt.Color(1, 1, 1));
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Cancelar 32 x 32.png"))); // NOI18N
        btnCancelar.setMnemonic('c');
        btnCancelar.setText("Cancelar");
        btnCancelar.setToolTipText("Cancela la Operacion del Registro");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        jlImagen.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jlImagen.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlImagen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Sin_imagen 64 x 64.png"))); // NOI18N
        jlImagen.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Seleccione Imagen", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 12))); // NOI18N
        jlImagen.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlImagenMouseClicked(evt);
            }
        });

        btnAdmCategorias.setFont(new java.awt.Font("FreeMono", 1, 14)); // NOI18N
        btnAdmCategorias.setForeground(new java.awt.Color(1, 1, 1));
        btnAdmCategorias.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Categoria 32 x 32.png"))); // NOI18N
        btnAdmCategorias.setText("Categoria");
        btnAdmCategorias.setToolTipText("Buscar el Registro");
        btnAdmCategorias.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnAdmCategorias.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAdmCategoriasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jlImagen, javax.swing.GroupLayout.DEFAULT_SIZE, 151, Short.MAX_VALUE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnAdmCategorias, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnGuardar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jlImagen, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnAdmCategorias)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(12, 12, 12))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnAdmCategorias, btnCancelar, btnGuardar});

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtNotasKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNotasKeyReleased
        txtNotas.setText(txtNotas.getText().toUpperCase());
    }//GEN-LAST:event_txtNotasKeyReleased

    private void jlImagenMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlImagenMouseClicked
        JFileChooser file = new JFileChooser();
        
        file.setFileFilter(new FileNameExtensionFilter(
                "Imagenes", 
                "jpg", 
                "png", 
                "PNG", 
                "JPG"));
        
        int returnVal = file.showOpenDialog(this);

        if (returnVal == JFileChooser.CANCEL_OPTION) {
            return;
        }
        
        source = file.getSelectedFile().getAbsolutePath();

        ImageIcon imagen = new ImageIcon(source);
        //Tamaño de icono
        Icon icon = new ImageIcon(imagen.getImage().getScaledInstance(
                132, 
                132,
                Image.SCALE_DEFAULT));
        imagen.getImage().flush();
        jlImagen.setIcon(icon);
        jlImagen.validate();
    }//GEN-LAST:event_jlImagenMouseClicked

    private void cbActivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbActivoActionPerformed
        if (!txtDescripcion.isEditable()) {
            cbActivo.setSelected(!cbActivo.isSelected());
            return;
        }

        if (cbActivo.isSelected()) {
            cbActivo.setText("Activo");
        } else {
            cbActivo.setText("Inactivo");
        }
    }//GEN-LAST:event_cbActivoActionPerformed

    private void cbCategoriaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbCategoriaItemStateChanged
        txtNotas.requestFocusInWindow();
    }//GEN-LAST:event_cbCategoriaItemStateChanged

    private void cbCategoriaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_cbCategoriaFocusGained
        if (txtCodigo.isEditable()) {
            cbCategoria.showPopup();
        }
    }//GEN-LAST:event_cbCategoriaFocusGained

    private void cbCategoriaPopupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_cbCategoriaPopupMenuWillBecomeVisible
        if (!txtDescripcion.isEditable()) {
            cbCategoria.hidePopup();
        }
    }//GEN-LAST:event_cbCategoriaPopupMenuWillBecomeVisible

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        //Validaciones
        if (txtCodigo.getText().equals("")) {
            JOptionPane.showMessageDialog(rootPane, 
                    "Debe Digitar un ID");
            txtCodigo.requestFocus();
            return;
        }
        if (txtDescripcion.getText().equals("")) {
            JOptionPane.showMessageDialog(rootPane, 
                    "Debe Digitar una Descripcion...");
            txtDescripcion.requestFocus();
            return;
        }
        if (cbCategoria.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(rootPane, 
                    "Seleccione una categoria");
            cbCategoria.requestFocus();
            return;
        }


        // si es nuevo validamos que el Producto no exista
        if (nuevo) {
            if (Datos.existeProducto(txtCodigo.getText())) {
                JOptionPane.showMessageDialog(this, 
                        "Producto ID Ya existe...");
                txtCodigo.setText("");
                txtCodigo.requestFocus();
                return;
            }
        }

        Producto miProducto = new Producto(
                (p != null ? p.getIdProducto():0),
                txtCodigo.getText(),
                txtDescripcion.getText(),
                txtNotas.getText(),
                ((Categoria) cbCategoria.getSelectedItem()).getIdCategoria(),
                null,
                source,
                cbActivo.isSelected(),
                null
        );

        String msg = "", accion = "editar";
        if (nuevo) {
            accion = "crear";
        }
        
        if (txtNotas.getText().length() >= 51) {
            msg = txtNotas.getText().substring(0, 49);
        } else {
            msg = txtNotas.getText();
        }

        int resp = JOptionPane.showConfirmDialog(this,
                "<html><b><big>Se va a " + accion + " el Producto: </big></b><big>" + txtDescripcion.getText() + "</big></html>"
                + "\n<html><b><big>Codigo no: </big></b><big>" + txtCodigo.getText() + "</big></html>"
                + "\n<html><b><big>Categoria: </big></b><big>" + ((Categoria) cbCategoria.getSelectedItem()).getDescripcion() + "</big></html>"
                + "\n<html><b><big>Estado: </big></b><big>" + (cbActivo.isSelected() ? "Activo" : "Inactivo") + "</big></html>"
                + "\n<html><b><big>Notas: </big></b><big>" + msg + "</big></html>"
                + "\n<html><b><big>Desea continuar? </big></b></html>",
                "Confirmacion de Usuario",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE, jlImagen.getIcon());
        if (resp != 0) {
            return;
        }

        if (nuevo) {
            msg = Datos.agregarProducto(miProducto);
        } else {
            msg = Datos.modificarProducto(miProducto);
        }

        JOptionPane.showMessageDialog(rootPane, msg);

        if (msg.equals("Error al Insertar Producto...")
                || msg.equals("Error al Modificar Producto...")) {
            return;
        }

        if (source != null) {
            Utilidades.copyFileUsingFileChannels(source, txtCodigo.getText());
            ImageIcon imagen = new ImageIcon(source);
            Icon icon = new ImageIcon(imagen.getImage().getScaledInstance(
                    132, 
                    132,
                    Image.SCALE_DEFAULT));
            imagen.getImage().flush();
            jlImagen.setIcon(icon);
            jlImagen.validate();
        }
        dispose();
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnAdmCategoriasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAdmCategoriasActionPerformed
        frmCategorias miCategoria = new frmCategorias(null, true);
        miCategoria.setLocationRelativeTo(null);
        miCategoria.setVisible(true);
        updateCategoria();
    }//GEN-LAST:event_btnAdmCategoriasActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        updateCategoria();
        ResultSet rs = Datos.getProductoById(null,txtCodigo.getText().trim());
        BufferedImage img = null;
        try {
            while (rs.next()) {
                Blob b = rs.getBlob("image");
                if (b != null) {
                    byte[] data = b.getBytes(1, (int) b.length());
                    img = ImageIO.read(new ByteArrayInputStream(data));
                }

                ImageIcon imagen;

                if (img != null) {
                    imagen = new ImageIcon(img);
                } else {
                    imagen = new ImageIcon(getClass().getResource("/images/Sin_imagen 64 x 64.png"));
                }

                Icon icon = new ImageIcon(imagen.getImage().getScaledInstance(
                        132, 
                        132,
                        Image.SCALE_DEFAULT));
                imagen.getImage().flush();
                jlImagen.setIcon(icon);
                jlImagen.validate();

                for (int i = 0; i < cbCategoria.getItemCount(); i++) {
                    if (((Categoria) cbCategoria.getItemAt(i)).
                            getIdCategoria() == rs.getInt("idCategoria")) {
                        cbCategoria.setSelectedIndex(i);
                        break;
                    }
                }
            }
        } catch (SQLException ex) {
            LOGGER.getLogger(frmProductosRegistro.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            LOGGER.getLogger(frmProductosRegistro.class.getName()).log(Level.SEVERE, null, ex);
        }
        txtCodigo.requestFocus();
    }//GEN-LAST:event_formWindowOpened

    private void txtCodigoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoKeyTyped
        if (txtCodigo.getText().length() >= 35) {
            evt.consume();
            JOptionPane.showMessageDialog(this, "Este codigo debe ser menor a 35 caracteres!!!");
        }
    }//GEN-LAST:event_txtCodigoKeyTyped

    private void txtCodigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCodigoActionPerformed
        txtDescripcion.requestFocusInWindow();
    }//GEN-LAST:event_txtCodigoActionPerformed

    private void txtDescripcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDescripcionActionPerformed
        cbCategoria.requestFocusInWindow();
        cbCategoria.showPopup();
    }//GEN-LAST:event_txtDescripcionActionPerformed

    private void updateCategoria() {
        ResultSet rs = Datos.getCategorias();
        cbCategoria.removeAllItems();
        cbCategoria.addItem(new Categoria(-1,
                "Seleccione una Categoria"));
        try {
            while (rs.next()) {
                cbCategoria.addItem(
                        new Categoria(
                                rs.getInt("idCategoria"),
                                rs.getString("Descripcion")
                        )
                );
            }
        } catch (SQLException ex) {
            LOGGER.getLogger(frmProductos.class.getName()).log(Level.SEVERE, null, ex);
        }//Fin de llenando combo Box
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdmCategorias;
    protected javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JCheckBox cbActivo;
    private javax.swing.JComboBox cbCategoria;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel jlImagen;
    private rojeru_san.RSMTextFull txtCodigo;
    private rojeru_san.RSMTextFull txtDescripcion;
    private javax.swing.JTextArea txtNotas;
    // End of variables declaration//GEN-END:variables
}
