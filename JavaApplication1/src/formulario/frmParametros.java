package formulario;

import clases.ScreenSplash;
import static clases.Utilidades.LOGGER;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import javax.swing.JOptionPane;

public class frmParametros extends javax.swing.JFrame {

    private final Properties propiedades;

    public frmParametros() {
        if (ScreenSplash.debuger) {
            System.out.println("Clase Parametros");
        }
        initComponents();
        propiedades = new Properties();
        try {
            propiedades.load(new FileReader("propiedades.properties"));
        } catch (FileNotFoundException ex) {
            LOGGER.getLogger(frmParametros.class.getName()).log(Level.SEVERE,
                    null, ex);
        } catch (IOException ex) {
            LOGGER.getLogger(frmParametros.class.getName()).log(Level.SEVERE,
                    null, ex);
        }
        cargarParamentos("todo");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        btnCancelar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        txtValor1 = new javax.swing.JTextField();
        txtValor2 = new javax.swing.JTextField();
        txtValor3 = new javax.swing.JTextField();
        txtValor4 = new javax.swing.JTextField();
        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(10, 0), new java.awt.Dimension(10, 0), new java.awt.Dimension(10, 32767));
        txtPuerto = new javax.swing.JTextField();
        btnAceptar = new javax.swing.JButton();
        txthost = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Parametros del Sistema");
        setAlwaysOnTop(true);
        setMinimumSize(new java.awt.Dimension(439, 200));
        setResizable(false);
        setSize(new java.awt.Dimension(439, 210));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(204, 255, 204));

        jLabel2.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 0, 0));
        jLabel2.setText("Nombre del Servidor:");

        jLabel3.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 0, 0));
        jLabel3.setText("Puerto:");

        jLabel1.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("Protocolo IPV4:");

        btnCancelar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnCancelar.setForeground(new java.awt.Color(0, 0, 0));
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Cancelar 32 x 32.png"))); // NOI18N
        btnCancelar.setMnemonic('c');
        btnCancelar.setText("Cancelar");
        btnCancelar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCancelarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCancelarMouseExited(evt);
            }
        });
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(204, 255, 204));
        jPanel2.setLayout(new java.awt.GridLayout(1, 5, 10, 0));

        txtValor1.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        txtValor1.setForeground(new java.awt.Color(0, 0, 0));
        txtValor1.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtValor1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtValor1KeyTyped(evt);
            }
        });
        jPanel2.add(txtValor1);

        txtValor2.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        txtValor2.setForeground(new java.awt.Color(0, 0, 0));
        txtValor2.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtValor2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtValor2KeyTyped(evt);
            }
        });
        jPanel2.add(txtValor2);

        txtValor3.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        txtValor3.setForeground(new java.awt.Color(0, 0, 0));
        txtValor3.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtValor3.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtValor3KeyTyped(evt);
            }
        });
        jPanel2.add(txtValor3);

        txtValor4.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        txtValor4.setForeground(new java.awt.Color(0, 0, 0));
        txtValor4.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtValor4.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtValor4KeyTyped(evt);
            }
        });
        jPanel2.add(txtValor4);
        jPanel2.add(filler1);

        txtPuerto.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        txtPuerto.setForeground(new java.awt.Color(0, 0, 0));
        txtPuerto.setToolTipText("");
        txtPuerto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPuertoKeyReleased(evt);
            }
        });
        jPanel2.add(txtPuerto);

        btnAceptar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnAceptar.setForeground(new java.awt.Color(0, 0, 0));
        btnAceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Aceptar 32 x 32.png"))); // NOI18N
        btnAceptar.setMnemonic('a');
        btnAceptar.setText("Aceptar");
        btnAceptar.setToolTipText("");
        btnAceptar.setPreferredSize(new java.awt.Dimension(123, 44));
        btnAceptar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAceptarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAceptarMouseExited(evt);
            }
        });
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });

        txthost.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        txthost.setForeground(new java.awt.Color(0, 0, 0));
        txthost.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txthostCaretUpdate(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addComponent(jLabel1)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel3Layout.createSequentialGroup()
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel2)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txthost, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 434, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(btnCancelar)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGap(0, 0, Short.MAX_VALUE))))
                .addContainerGap())
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnAceptar, btnCancelar});

        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addGap(0, 0, 0)
                .addComponent(txthost, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnAceptar, btnCancelar});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnAceptarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAceptarMouseExited
        btnAceptar.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnAceptarMouseExited
    private void btnAceptarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAceptarMouseEntered
        btnAceptar.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnAceptarMouseEntered
    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        int valor;
        try {
            valor = Integer.parseInt(txtValor1.getText());
        } catch (NumberFormatException e) {
            valor = -1;
        }

        if (valor < 0 || valor > 255) {
            JOptionPane.showMessageDialog(rootPane, "Valor incorrecto en el Ambito 1");
            txtValor1.setText("");
            txtValor1.requestFocusInWindow();
            return;
        }

        try {
            valor = Integer.parseInt(txtValor2.getText());
        } catch (NumberFormatException e) {
            valor = -1;
        }
        if (valor < 0 || valor > 255) {
            JOptionPane.showMessageDialog(rootPane, "Valor incorrecto en el Ambito 2");
            txtValor2.setText("");
            txtValor2.requestFocusInWindow();
            return;
        }

        try {
            valor = Integer.parseInt(txtValor3.getText());
        } catch (NumberFormatException e) {
            valor = -1;
        }
        if (valor < 0 || valor > 255) {
            JOptionPane.showMessageDialog(rootPane, "Valor incorrecto en el Ambito 3");
            txtValor3.setText("");
            txtValor3.requestFocusInWindow();
            return;
        }

        try {
            valor = Integer.parseInt(txtValor4.getText());
        } catch (NumberFormatException e) {
            valor = -1;
        }
        if (valor < 0 || valor > 255) {
            JOptionPane.showMessageDialog(rootPane, "Valor incorrecto en el Ambito 4");
            txtValor4.setText("");
            txtValor4.requestFocusInWindow();
            return;
        }

        if (txthost.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Nombre del servidor vacio");
            txthost.requestFocusInWindow();
            return;
        }

        try {
            valor = Integer.parseInt(txtPuerto.getText());
        } catch (NumberFormatException e) {
            valor = -1;
        }
        if (valor < 0 || valor > 65535) {
            JOptionPane.showMessageDialog(rootPane, "Este Puerto no es valido");
            txtPuerto.setText("");
            txtPuerto.requestFocusInWindow();
            return;
        }

        escribirParametros();
        dispose();
    }//GEN-LAST:event_btnAceptarActionPerformed
    private void btnCancelarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancelarMouseExited
        btnCancelar.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnCancelarMouseExited
    private void btnCancelarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancelarMouseEntered
        btnCancelar.setForeground(Color.blue);
    }//GEN-LAST:event_btnCancelarMouseEntered
    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void txtValor4KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValor4KeyTyped
        int k = (int) evt.getKeyChar();
        if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!", "Ventana Error Datos", JOptionPane.ERROR_MESSAGE);
        }
        if (k == 241 || k == 209) {
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!", "Ventana Error Datos", JOptionPane.ERROR_MESSAGE);
        }
        if (k == 10) {
            txtValor4.transferFocus();
        }
    }//GEN-LAST:event_txtValor4KeyTyped
    private void txtValor3KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValor3KeyTyped
        int k = (int) evt.getKeyChar();
        if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!", "Ventana Error Datos", JOptionPane.ERROR_MESSAGE);
        }
        if (k == 241 || k == 209) {
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!",
                    "Ventana Error Datos", JOptionPane.ERROR_MESSAGE);
        }
        if (k == 10) {
            txtValor3.transferFocus();
        }
    }//GEN-LAST:event_txtValor3KeyTyped
    private void txtValor2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValor2KeyTyped
        int k = (int) evt.getKeyChar();
        if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!",
                    "Ventana Error Datos", JOptionPane.ERROR_MESSAGE);
        }
        if (k == 241 || k == 209) {
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!",
                    "Ventana Error Datos", JOptionPane.ERROR_MESSAGE);
        }
        if (k == 10) {
            txtValor2.transferFocus();
        }
    }//GEN-LAST:event_txtValor2KeyTyped
    private void txtValor1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtValor1KeyTyped
        int k = (int) evt.getKeyChar();
        if (k >= 97 && k <= 122 || k >= 65 && k <= 90) {
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!",
                    "Ventana Error Datos", JOptionPane.ERROR_MESSAGE);
        }
        if (k == 241 || k == 209) {
            evt.setKeyChar((char) KeyEvent.VK_CLEAR);
            JOptionPane.showMessageDialog(null, "No puede ingresar letras!!!",
                    "Ventana Error Datos", JOptionPane.ERROR_MESSAGE);
        }
        if (k == 10) {
            txtValor1.transferFocus();
        }
    }//GEN-LAST:event_txtValor1KeyTyped
    private void txtPuertoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPuertoKeyReleased
        char caracter = evt.getKeyChar();
        if (caracter < '0' || (caracter > '9')) {
            evt.consume();  // ignorar el evento de teclado
        }
    }//GEN-LAST:event_txtPuertoKeyReleased

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        System.gc();
    }//GEN-LAST:event_formWindowClosed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened

    }//GEN-LAST:event_formWindowOpened

    private void txthostCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txthostCaretUpdate
        if(evt.getDot() != 0){
            /*Deshabilitamos los componentes ya que necesito que esten en este 
            estado no seran necesario para guardar la ip*/
            txtValor1.setEnabled(false);
            txtValor2.setEnabled(false);
            txtValor3.setEnabled(false);
            txtValor4.setEnabled(false);
            
            /*Me aseguro que esto valores esten en blanco...*/
            txtValor1.setText("");
            txtValor2.setText("");
            txtValor3.setText("");
            txtValor4.setText("");
        }else{
            /*Los pongo disponibles ya que no hay razon para dejarlo 
            deshabilitarlo*/
            txtValor1.setEnabled(true);
            txtValor2.setEnabled(true);
            txtValor3.setEnabled(true);
            txtValor4.setEnabled(true);
        }
    }//GEN-LAST:event_txthostCaretUpdate
    private void valoresEstados(boolean estado) {
        txtValor1.setEnabled(estado);
        txtValor2.setEnabled(estado);
        txtValor3.setEnabled(estado);
        txtValor4.setEnabled(estado);
        txthost.setEnabled(!estado);
        if (estado) {
            txtValor1.requestFocusInWindow();
        } else {
            txthost.requestFocusInWindow();
        }
    }

    private void cargarParamentos(String zona) {
        if (zona.equals("todo") || zona.equals("puerto")) {
            if (Boolean.valueOf(propiedades.getProperty("Con_Puerto", "false"))) {
                if (zona.equals("todo")) {
                    txtPuerto.setText(propiedades.getProperty("Puerto_del_Servidor", ""));
                }

                if (zona.equals("puerto")) {
                    txtPuerto.setEnabled(true);
                    if (Boolean.valueOf(propiedades.getProperty("Con_Puerto", "false"))) {
                        txtPuerto.setText(propiedades.getProperty("Puerto_del_Servidor", ""));
                    }
                    txtPuerto.requestFocusInWindow();
                    txtPuerto.setEnabled(false);
                    txtPuerto.setText("");
                }
            } else {
                txtPuerto.setEnabled(true);
                txtPuerto.requestFocusInWindow();
                txtPuerto.setEnabled(false);
                txtPuerto.setText("");
            }
        }

        if (zona.equals("todo") || zona.equals("ipv4")) {
            if (Boolean.valueOf(propiedades.getProperty("ProtocoloActivo", "false"))) {
                txtValor1.setText(propiedades.getProperty("Ip_Servidor1", ""));
                txtValor2.setText(propiedades.getProperty("Ip_Servidor2", ""));
                txtValor3.setText(propiedades.getProperty("Ip_Servidor3", ""));
                txtValor4.setText(propiedades.getProperty("Ip_Servidor4", ""));
            }
        }

        if (zona.equals("todo") || zona.equals("nombre")) {
            if (Boolean.valueOf(propiedades.getProperty("NombreActivo", "false"))) {
                txthost.setText(propiedades.getProperty("Nombre_del_Servidor", ""));
            }
        }
    }

    private void escribirParametros() {
        /**/
        propiedades.setProperty("Ip_Servidor1", txtValor1.getText());
        propiedades.setProperty("Ip_Servidor2", txtValor2.getText());
        propiedades.setProperty("Ip_Servidor3", txtValor3.getText());
        propiedades.setProperty("Ip_Servidor4", txtValor4.getText());
        propiedades.setProperty("Nombre_del_Servidor", txthost.getText());
        propiedades.setProperty("Puerto_del_Servidor", txtPuerto.getText());

        try {
            propiedades.store(new FileWriter("propiedades.properties"), "Parametros del Servidor");
        } catch (IOException ex) {
            LOGGER.getLogger(frmParametros.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JTextField txtPuerto;
    private javax.swing.JTextField txtValor1;
    private javax.swing.JTextField txtValor2;
    private javax.swing.JTextField txtValor3;
    private javax.swing.JTextField txtValor4;
    private javax.swing.JTextField txthost;
    // End of variables declaration//GEN-END:variables
}
