package formulario;

import clases.Cliente;
import clases.Datos;
import clases.RenderCeldas;
import clases.ScreenSplash;
import clases.Utilidades;
import static clases.Utilidades.LOGGER;
import static formulario.frmPrincipal.dpnEscritorio;
import java.awt.Rectangle;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import java.beans.PropertyVetoException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;
import javax.swing.table.TableColumn;
import rojerusan.RSTableMetro;

public final class frmClientes extends javax.swing.JInternalFrame {

    private DefaultTableModel miTabla;
    private int indexBusqueda = 0;
    private static String valorBusqueda = "";

    public frmClientes() {
        if (ScreenSplash.debuger) {
            System.out.println("Clase Clientes");
        }
        initComponents();
        //tblClientes.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        btnNuevo = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        btnHistorial = new javax.swing.JButton();
        btnBuscar = new javax.swing.JButton();
        btnBorrar = new javax.swing.JButton();
        jRendererPanel1 = new org.jdesktop.swing.animation.rendering.JRendererPanel();
        rSScrollPane1 = new necesario.RSScrollPane();
        tblClientes = new RSTableMetro(){
            @Override
            public boolean isCellEditable(int rowIndex, int colIndex) { 
                return false; //Las celdas no son editables. 
            }
        };

        setClosable(true);
        setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Mantenimiento de Clientes");
        setFocusTraversalPolicyProvider(true);
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameActivated(evt);
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
            }
        });

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Botones de Acción"));
        jPanel2.setMaximumSize(new java.awt.Dimension(787, 81));
        jPanel2.setMinimumSize(new java.awt.Dimension(787, 81));

        jPanel6.setLayout(new java.awt.GridLayout(1, 0, 4, 0));

        btnNuevo.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnNuevo.setForeground(new java.awt.Color(1, 1, 1));
        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Documento nuevo 32 x 32.png"))); // NOI18N
        btnNuevo.setMnemonic('n');
        btnNuevo.setText("Nuevo");
        btnNuevo.setToolTipText("Crear un nuevo Registro");
        btnNuevo.setMaximumSize(new java.awt.Dimension(104, 44));
        btnNuevo.setMinimumSize(new java.awt.Dimension(104, 44));
        btnNuevo.setPreferredSize(new java.awt.Dimension(104, 44));
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        jPanel6.add(btnNuevo);

        btnModificar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnModificar.setForeground(new java.awt.Color(1, 1, 1));
        btnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Editar Documento 32 x 32.png"))); // NOI18N
        btnModificar.setMnemonic('m');
        btnModificar.setText("Modificar");
        btnModificar.setToolTipText("Modificar Registro Actual");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });
        jPanel6.add(btnModificar);

        btnHistorial.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnHistorial.setForeground(new java.awt.Color(1, 1, 1));
        btnHistorial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Clientes 64 x 64.png"))); // NOI18N
        btnHistorial.setText("Detalles");
        btnHistorial.setToolTipText("Detalles de clientes por factura.");
        btnHistorial.setAutoscrolls(true);
        btnHistorial.setDoubleBuffered(true);
        btnHistorial.setFocusPainted(false);
        btnHistorial.setFocusTraversalPolicyProvider(true);
        btnHistorial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHistorialActionPerformed(evt);
            }
        });
        jPanel6.add(btnHistorial);

        btnBuscar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnBuscar.setForeground(new java.awt.Color(1, 1, 1));
        btnBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Buscar2 32 x 32.png"))); // NOI18N
        btnBuscar.setMnemonic('r');
        btnBuscar.setText("Buscar");
        btnBuscar.setToolTipText("Buscar el Registro");
        btnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarActionPerformed(evt);
            }
        });
        jPanel6.add(btnBuscar);

        btnBorrar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnBorrar.setForeground(new java.awt.Color(1, 1, 1));
        btnBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Borrar 32 x 32.png"))); // NOI18N
        btnBorrar.setMnemonic('b');
        btnBorrar.setText("Borrar");
        btnBorrar.setToolTipText("Borrar Registro Actual");
        btnBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBorrarActionPerformed(evt);
            }
        });
        jPanel6.add(btnBorrar);

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 777, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .add(0, 0, 0)
                .add(jPanel6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(0, 0, 0))
        );

        tblClientes.setAutoCreateRowSorter(true);
        tblClientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5", "Title 6", "Title 7"
            }
        ));
        tblClientes.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tblClientes.setMultipleSeleccion(false);
        tblClientes.setShowGrid(true);
        tblClientes.getTableHeader().setReorderingAllowed(false);
        rSScrollPane1.setViewportView(tblClientes);

        org.jdesktop.layout.GroupLayout jRendererPanel1Layout = new org.jdesktop.layout.GroupLayout(jRendererPanel1);
        jRendererPanel1.setLayout(jRendererPanel1Layout);
        jRendererPanel1Layout.setHorizontalGroup(
            jRendererPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(rSScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jRendererPanel1Layout.setVerticalGroup(
            jRendererPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(rSScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 276, Short.MAX_VALUE)
        );

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .add(jRendererPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(jRendererPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBorrarActionPerformed
        if (tblClientes.getValueAt(tblClientes.getSelectedRow(), 0).
                toString().equalsIgnoreCase("000-0000000-0")) {
            JOptionPane.showMessageDialog(this,
                    "Cliente Generico no puede ser Borrado.");
            return;
        }
        int rta = JOptionPane.showConfirmDialog(this,
                "¿Esta Seguro de Eliminar Registro del Cliente?",
                "Eliminar Cliente",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE
        );

        if (rta == JOptionPane.NO_OPTION) {
            return;
        }

        JOptionPane.showMessageDialog(this, Datos.borrarCliente(
                tblClientes.getValueAt(tblClientes.getSelectedRow(), 0).
                        toString()));

        //Actualizamos los cambios en la Tabla
        llenarTabla();
    }//GEN-LAST:event_btnBorrarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        frmClientesRegistro c = new frmClientesRegistro(null, true);
        c.setLocationRelativeTo(this);
        c.setVisible(true);
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        /*Se validan que exista un elemento seleccionado*/
        if (tblClientes.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this, "Debe seleccionar un cliente.");
            return;
        }

        /*De la columna 7 obtenemos la fecha de nacimiento*/
        String sFecha = tblClientes.getValueAt(tblClientes.getSelectedRow(), 7).toString();

        Date dFecha = null;
        //Variable que nos permitira guardar la fecha Nacimiento del Cliente.
        
        try {//Obtenemos la fecha.
            dFecha = new SimpleDateFormat("dd.MM.yyyy").parse(sFecha);
        } catch (ParseException ex) {
            Logger.getLogger(frmClientes.class.getName()).log(Level.SEVERE, null, ex);
        }

        frmClientesRegistro c = new frmClientesRegistro(null, true,
                new Cliente(
                        tblClientes.getValueAt(tblClientes.getSelectedRow(), 0).toString(),
                        tblClientes.getValueAt(tblClientes.getSelectedRow(), 1).toString(),
                        tblClientes.getValueAt(tblClientes.getSelectedRow(), 2).toString(),
                        tblClientes.getValueAt(tblClientes.getSelectedRow(), 3).toString(),
                        tblClientes.getValueAt(tblClientes.getSelectedRow(), 4).toString(),
                        tblClientes.getValueAt(tblClientes.getSelectedRow(), 5).toString(),
                        tblClientes.getValueAt(tblClientes.getSelectedRow(), 6).toString(),
                        dFecha,
                        null,
                        Boolean.valueOf(tblClientes.getValueAt(tblClientes.getSelectedRow(), 9).toString())
                )
        );
        
        c.setLocationRelativeTo(this);
        c.setVisible(true);
        
        llenarTabla();
    }//GEN-LAST:event_btnModificarActionPerformed

    private void formInternalFrameActivated(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameActivated
        llenarTabla();
    }//GEN-LAST:event_formInternalFrameActivated

    private void btnHistorialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHistorialActionPerformed
        //        if (miDetalle == null) {
        frmDetalleFacturaClientes miDetalle = new frmDetalleFacturaClientes();
        dpnEscritorio.add(miDetalle);
        //        }

        try {
            miDetalle.setMaximum(false);
            miDetalle.setMaximum(true);
        } catch (PropertyVetoException ex) {
            LOGGER.getLogger(frmClientes.class.getName()).log(Level.SEVERE, null, ex);
        }
        miDetalle.setVisible(true);
    }//GEN-LAST:event_btnHistorialActionPerformed

    private void btnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarActionPerformed
        frmBusquedaTresValoresPersona c = 
                new frmBusquedaTresValoresPersona(null, true);
        
        c.setLocationRelativeTo(this);
        c.setVisible(true);
        
        if (valorBusqueda.equals("")) {
            //Pidiendo datos al usuario para realizar busqueda
            valorBusqueda = JOptionPane.showInternalInputDialog(this,
                    "Ingrese la cedula con guiones, Nombres o Apellidos del Cliente");

            //Validamos que los datos no sean nulo o vacio
            if (valorBusqueda.isEmpty() || valorBusqueda.isBlank() || valorBusqueda == null) {
                return;
            }

            //Verificamos en la base de datos....
            if (!Datos.existeCliente(valorBusqueda)) {
                //Enviale un mensaje al cliente que no existe el cliente...
                JOptionPane.showMessageDialog(this, "El cliente no existe!");
                return;//si no existe el cliente  se devuelve....
            }

            //Buscando en la tabla frmClientes
            if (busquedaTabla()) {
                return;
            }

            llenarTabla();//llenamos la tabla nuevamente para actualizar la tabla.
            //sabemos que existe el usuario pero no esta en la tabla

            //Llamo este mismo metodo porque el dato ya fue insertado a la tabla.
            btnBuscarActionPerformed(null);
            indexBusqueda = 0;
        } else {
            /*Les pregunto al usuario si quiere seguir buscando con esa palabra */
            int resp = JOptionPane.showInternalConfirmDialog(this,
                    "Desea seguir buscando el valor de [ " + valorBusqueda + " ]?",
                    "Confirmacion de busqueda",
                    JOptionPane.YES_NO_OPTION);

            if (resp == JOptionPane.NO_OPTION) {
                /*Si dice que no pues cambio el valorBusqueda y llamo otra vez
                la funcion.*/
                valorBusqueda = "";
                indexBusqueda = 0;
                return;//Me devuelvo porque el dice que no.....
            }

            busquedaTabla();
        }
    }//GEN-LAST:event_btnBuscarActionPerformed

    private boolean busquedaTabla() {
        //for para buscar la informacion en la tabla
        for (; indexBusqueda < tblClientes.getRowCount(); indexBusqueda++) {
            /*Si encuentra un valor lo devuelve true, si no false.*/
 /*Buscando en el campo de cedula*/
            if (tblClientes.getValueAt(indexBusqueda, 0).toString().contains(
                    valorBusqueda) || tblClientes.getValueAt(indexBusqueda, 0).toString().startsWith(
                            valorBusqueda)) {
                tblClientes.setRowSelectionInterval(indexBusqueda, indexBusqueda);

                tblClientes.getSelectionModel().setSelectionInterval(indexBusqueda, indexBusqueda);
                tblClientes.scrollRectToVisible(new Rectangle(tblClientes.getCellRect(indexBusqueda, 0, true)));

                indexBusqueda++;
                return true;
            }
            /*Buscando en el campo de nombres*/
            if (tblClientes.getValueAt(indexBusqueda, 1).toString().toUpperCase().contains(
                    valorBusqueda.toUpperCase())) {
                tblClientes.setRowSelectionInterval(indexBusqueda, indexBusqueda);

                tblClientes.getSelectionModel().setSelectionInterval(indexBusqueda, indexBusqueda);
                tblClientes.scrollRectToVisible(new Rectangle(tblClientes.getCellRect(indexBusqueda, 0, true)));

                indexBusqueda++;
                return true;
            }
            /*Buscando en el campo de apellidos*/
            if (tblClientes.getValueAt(indexBusqueda, 2).toString().toUpperCase().contains(
                    valorBusqueda.toUpperCase())) {
                tblClientes.setRowSelectionInterval(indexBusqueda, indexBusqueda);

                tblClientes.getSelectionModel().setSelectionInterval(indexBusqueda, indexBusqueda);
                tblClientes.scrollRectToVisible(new Rectangle(tblClientes.getCellRect(indexBusqueda, 0, true)));

                indexBusqueda++;
                return true;
            }
        }
        /*Si ya busco en la tabla completa pues no encontro registro y busca del
        principio...*/
        if (indexBusqueda >= tblClientes.getRowCount()) {
            indexBusqueda = 0;
            tblClientes.getSelectionModel().setSelectionInterval(indexBusqueda, indexBusqueda);
            tblClientes.scrollRectToVisible(new Rectangle(tblClientes.getCellRect(indexBusqueda, 0, true)));

        }
        return false;//Devuelve falso porque no encontro registros....
    }

    /*Unico metodo que me llena la tabla de cliente*/
    private void llenarTabla() {
        //Header de las columnas
        String titulos[] = {
            "Cedula",
            "Nombres",
            "Apellidos",
            "Sexo",
            "Ciudad",
            "Direccion",
            "Telefono",
            "<html><div align='center'>Fecha<br>Nacimiento</div></html>",
            "<html><div align='center'>Fecha<br>Ingreso</div></html>",
            "Estado"};

        Object registro[] = new Object[titulos.length];

        try {
            ResultSet rs = Datos.getClientes();
            miTabla = new DefaultTableModel(null, titulos);

            while (rs.next()) {
                registro[0] = rs.getString("idCliente");
                registro[1] = rs.getString("nombres");
                registro[2] = rs.getString("apellidos");
                registro[3] = rs.getString("sexo").equalsIgnoreCase("m") ? "Masculino" : "Femenino";
                registro[4] = rs.getString("ciudad");
                registro[5] = rs.getString("direccion");
                registro[6] = rs.getString("telefono");
                registro[7] = Utilidades.formatDate(rs.getDate("fechaNacimiento"), "dd.MM.yyyy");
                registro[8] = Utilidades.formatDate(rs.getDate("fechaIngreso"), "dd.MM.yyyy");
                registro[9] = rs.getBoolean("Estado");
                miTabla.addRow(registro);
            }
            tblClientes.setModel(miTabla);
        } catch (SQLException ex) {
            LOGGER.getLogger(frmClientes.class.getName()).log(Level.SEVERE, null, ex);
        }
        reOrdenar();
    }

    private void reOrdenar() {
        if (tblClientes.getRowCount() == 0) {
            return;
        }
        TableColumn c;
        for (int i = 0; i < tblClientes.getColumnCount(); i++) {
            c = tblClientes.getColumnModel().getColumn(i);

            tblClientes.getColumnModel().getColumn(i).
                    setCellRenderer(new RenderCeldas());

            if (i == 0) {
                c.setPreferredWidth(130);
                c.setMaxWidth(180);
                c.setMinWidth(120);
            }

            if (i == 1) {
                c.setPreferredWidth(220);
                c.setMaxWidth(280);
                c.setMinWidth(200);
            }
            if (i == 2) {
                c.setPreferredWidth(220);
                c.setMaxWidth(280);
                c.setMinWidth(200);
            }

            if (i == 3) {
                c.setPreferredWidth(90);
                c.setMaxWidth(100);
                c.setMinWidth(50);
            }

            if (i == 4) {
                c.setPreferredWidth(150);
                c.setMaxWidth(400);
                c.setMinWidth(200);
            }

            if (i == 5) {
                c.setPreferredWidth(150);
                c.setMaxWidth(400);
                c.setMinWidth(200);
            }

            if (i == 6) {
                c.setPreferredWidth(140);
                c.setMaxWidth(150);
                c.setMinWidth(120);
            }

            if (i == 7) {
                c.setPreferredWidth(110);
                c.setMaxWidth(120);
                c.setMinWidth(100);
            }

            if (i == 8) {
                c.setPreferredWidth(110);
                c.setMaxWidth(120);
                c.setMinWidth(100);
            }

        }
        if (tblClientes.getSelectedRow() != -1) {
            tblClientes.setRowSelectionInterval(tblClientes.getSelectedRow(),
                    tblClientes.getSelectedRow());
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBorrar;
    private javax.swing.JButton btnBuscar;
    private javax.swing.JButton btnHistorial;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel6;
    private org.jdesktop.swing.animation.rendering.JRendererPanel jRendererPanel1;
    private necesario.RSScrollPane rSScrollPane1;
    private rojerusan.RSTableMetro tblClientes;
    // End of variables declaration//GEN-END:variables
}
