package formulario;

import clases.Categoria;
import clases.Datos;
import clases.Utilidades;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import clases.DefaultTableCellHeaderRenderer;
import clases.Producto;
import clases.RenderCeldas;
import clases.ScreenSplash;
import static clases.Utilidades.LOGGER;
import formulario.productos.frmEntradaProducto;
import formulario.productos.frmSalidaProducto;
import conexiones.Conexion;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Blob;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import rojeru_san.complementos.RSTableMetro;

public class frmProductos extends javax.swing.JInternalFrame {

    private DefaultTableModel miTabla;
    private final Object registro[] = new Object[11];
    private final DefaultTableCellRenderer tcr;

    public frmProductos() {
        if (ScreenSplash.debuger) {
            System.out.println("Clase Productos");
        }
        initComponents();
        tcr = new DefaultTableCellHeaderRenderer();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel5 = new javax.swing.JPanel();
        btnBuscarProducto = new javax.swing.JButton();
        btnImprimirLista = new javax.swing.JButton();
        btnEntradaProducto = new javax.swing.JButton();
        btnSalidaProducto = new javax.swing.JButton();
        btnHistorialES = new javax.swing.JButton();
        jlImagen = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tblTabla = new RSTableMetro(){
            @Override
            public boolean isCellEditable(int rowIndex, int colIndex) { 
                return false; //Las celdas no son editables. 
            }
        };
        jScrollPane3 = new javax.swing.JScrollPane();
        jPanel2 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        btnNuevo = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        btnBorrar = new javax.swing.JButton();

        setClosable(true);
        setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Productos");
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
        });

        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Opciones"));

        btnBuscarProducto.setFont(new java.awt.Font("FreeMono", 1, 14)); // NOI18N
        btnBuscarProducto.setForeground(new java.awt.Color(1, 1, 1));
        btnBuscarProducto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Buscar2 32 x 32.png"))); // NOI18N
        btnBuscarProducto.setMnemonic('r');
        btnBuscarProducto.setText("Buscar        ");
        btnBuscarProducto.setToolTipText("Buscar el Registro");
        btnBuscarProducto.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnBuscarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBuscarProductoActionPerformed(evt);
            }
        });

        btnImprimirLista.setFont(new java.awt.Font("FreeMono", 1, 14)); // NOI18N
        btnImprimirLista.setForeground(new java.awt.Color(1, 1, 1));
        btnImprimirLista.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Impresora.png"))); // NOI18N
        btnImprimirLista.setMnemonic('l');
        btnImprimirLista.setText("Imprimir Lista");
        btnImprimirLista.setToolTipText("Buscar el Registro");
        btnImprimirLista.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnImprimirLista.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirListaActionPerformed(evt);
            }
        });

        btnEntradaProducto.setFont(new java.awt.Font("FreeMono", 1, 14)); // NOI18N
        btnEntradaProducto.setForeground(new java.awt.Color(1, 1, 1));
        btnEntradaProducto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Productos 32 x 32.png"))); // NOI18N
        btnEntradaProducto.setText("Entrada       ");
        btnEntradaProducto.setToolTipText("Buscar el Registro");
        btnEntradaProducto.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnEntradaProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEntradaProductoActionPerformed(evt);
            }
        });

        btnSalidaProducto.setFont(new java.awt.Font("FreeMono", 1, 14)); // NOI18N
        btnSalidaProducto.setForeground(new java.awt.Color(1, 1, 1));
        btnSalidaProducto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Productos 32 x 32.png"))); // NOI18N
        btnSalidaProducto.setText("Salida        ");
        btnSalidaProducto.setToolTipText("Buscar el Registro");
        btnSalidaProducto.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnSalidaProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalidaProductoActionPerformed(evt);
            }
        });

        btnHistorialES.setFont(new java.awt.Font("FreeMono", 1, 14)); // NOI18N
        btnHistorialES.setForeground(new java.awt.Color(1, 1, 1));
        btnHistorialES.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Productos 32 x 32.png"))); // NOI18N
        btnHistorialES.setText("Historial E/S ");
        btnHistorialES.setToolTipText("Buscar el Registro");
        btnHistorialES.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnHistorialES.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnHistorialESActionPerformed(evt);
            }
        });

        jlImagen.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jlImagen.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlImagen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Sin_imagen 64 x 64.png"))); // NOI18N
        jlImagen.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Seleccione Imagen", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 12))); // NOI18N
        jlImagen.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlImagenMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jlImagen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEntradaProducto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnSalidaProducto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnHistorialES, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnBuscarProducto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnImprimirLista, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jlImagen, javax.swing.GroupLayout.PREFERRED_SIZE, 183, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnBuscarProducto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnImprimirLista)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEntradaProducto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSalidaProducto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnHistorialES)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jScrollPane5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(187, 187, 187)));

        tblTabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblTabla.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        tblTabla.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblTablaMouseClicked(evt);
            }
        });
        tblTabla.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tblTablaKeyReleased(evt);
            }
        });
        jScrollPane5.setViewportView(tblTabla);

        jScrollPane3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Navegacion", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 12))); // NOI18N
        jScrollPane3.setToolTipText("");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jPanel4.setLayout(new java.awt.GridLayout(1, 0, 4, 0));

        btnNuevo.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnNuevo.setForeground(new java.awt.Color(1, 1, 1));
        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Documento nuevo 32 x 32.png"))); // NOI18N
        btnNuevo.setMnemonic('n');
        btnNuevo.setText("Nuevo");
        btnNuevo.setToolTipText("Crear un nuevo Registro");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        jPanel4.add(btnNuevo);

        btnModificar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnModificar.setForeground(new java.awt.Color(1, 1, 1));
        btnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Editar Documento 32 x 32.png"))); // NOI18N
        btnModificar.setMnemonic('e');
        btnModificar.setText("Editar");
        btnModificar.setToolTipText("Modificar Registro Actual");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });
        jPanel4.add(btnModificar);

        btnBorrar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnBorrar.setForeground(new java.awt.Color(1, 1, 1));
        btnBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Borrar 32 x 32.png"))); // NOI18N
        btnBorrar.setMnemonic('b');
        btnBorrar.setText("Borrar");
        btnBorrar.setToolTipText("Borrar Registro Actual");
        btnBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBorrarActionPerformed(evt);
            }
        });
        jPanel4.add(btnBorrar);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 536, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jScrollPane3.setViewportView(jPanel2);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5)
                    .addComponent(jScrollPane3))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        if (Datos.cantidadRegistros("TABLA_CATEGORIAS") == 0) {
            int resp = JOptionPane.showInternalConfirmDialog(this,
                    "No existe una categoria de producto.\n"
                    + "Desea crea la primera categoria?",
                    "Sin categoria en el sistema",
                    JOptionPane.YES_NO_OPTION);

            if (resp == JOptionPane.OK_OPTION) {
                frmCategorias c = new frmCategorias(null, true);
                c.setLocationRelativeTo(null);
                c.setVisible(true);
            } else {
                return;
            }
        }

        frmProductosRegistro r = new frmProductosRegistro(null, true);
        r.setLocationRelativeTo(this);
        r.setVisible(true);
        llenarTabla();
        mostrarRegistro();
        reOrdenar();
        reOrdenar();
    }//GEN-LAST:event_btnNuevoActionPerformed
    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        if (tblTabla.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this,
                    "Debe seleccionar un producto");
            return;
        }
        frmProductosRegistro r = new frmProductosRegistro(null, true,
                new Producto(
                        ((Categoria) tblTabla.getValueAt(tblTabla.getSelectedRow(), 0)).getIdProducto(),
                        ((Categoria) tblTabla.getValueAt(tblTabla.getSelectedRow(), 0)).getDescripcion(),
                        tblTabla.getValueAt(tblTabla.getSelectedRow(), 1).toString(),
                        tblTabla.getValueAt(tblTabla.getSelectedRow(), 3).toString(),
                        ((Categoria) tblTabla.getValueAt(tblTabla.getSelectedRow(), 4)).getIdProducto(),
                        null,
                        null,
                        (Boolean) tblTabla.getValueAt(tblTabla.getSelectedRow(), 6),
                        null)
        );

        r.setLocationRelativeTo(this);
        r.setVisible(true);
        llenarTabla();
        mostrarRegistro();
        reOrdenar();
        reOrdenar();
    }//GEN-LAST:event_btnModificarActionPerformed
    private void btnBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBorrarActionPerformed
        if (tblTabla.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this,
                    "Debe seleccionar un producto");
            return;
        }
        int rta = JOptionPane.showConfirmDialog(this,
                "Esta Seguro de Eliminar el Producto {"
                + tblTabla.getValueAt(tblTabla.getSelectedRow(), 1)
                        .toString() + "} de los Registro?",
                "Confirmacion",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);
        if (rta == 1) {
            return;
        }
        String msg;
        try {
            msg = Datos.borrarProducto(((Categoria) tblTabla.getValueAt(tblTabla.getSelectedRow(), 0)).getDescripcion());
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, "No se puede eliminar el producto porque ya ha sido facturado"
                    + "\n!!Te recomendamos cambiar el ESTADO del producto...");
            return;
        }
        JOptionPane.showMessageDialog(rootPane, msg);

        //Actualizamos los cambios en la Tabla
        llenarTabla();
        mostrarRegistro();
        reOrdenar();
        reOrdenar();
    }//GEN-LAST:event_btnBorrarActionPerformed
    private void btnBuscarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBuscarProductoActionPerformed
        if (tblTabla.getRowCount() == 0) {
            JOptionPane.showMessageDialog(this, "No hay producto registrado");
            return;
        }
        String producto = JOptionPane.showInputDialog("Ingrese el Codigo del Producto...");

        if (producto == null || producto.equals("")) {
        } else {
            if (!Datos.existeProducto(producto)) {
                JOptionPane.showMessageDialog(null, "El Producto No Existe...");
                return;
            }
            //Detalle de Factura
            int num = tblTabla.getRowCount();
            for (int i = 0; i < num; i++) {
                if (Utilidades.objectToString(tblTabla.getValueAt(i, 0)).equals(producto)) {
                    tblTabla.setRowSelectionInterval(i, i);
                    break;
                }
            }
            llenarTabla();
            mostrarRegistro();
            reOrdenar();
            reOrdenar();
        }
    }//GEN-LAST:event_btnBuscarProductoActionPerformed
    private void btnImprimirListaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirListaActionPerformed
        if (tblTabla.getRowCount() == 0) {
            JOptionPane.showMessageDialog(this, "No hay producto registrado");
            return;
        }

        try {
            JasperReport masterReporte
                    = (JasperReport) JRLoader.loadObjectFromFile("Reportes/Productos.jasper");
            JasperPrint jp = JasperFillManager.fillReport(masterReporte, null,
                    Conexion.getCnn());

            JasperViewer miView = new JasperViewer(jp, false);
            miView.setTitle("Lista de productos del sistema actual...");
            miView.setLocationRelativeTo(null);
            miView.setVisible(true);
        } catch (JRException ex) {
            LOGGER.getLogger(frmProductos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_btnImprimirListaActionPerformed
    private void btnEntradaProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEntradaProductoActionPerformed
        if (tblTabla.getRowCount() == 0) {
            JOptionPane.showMessageDialog(this, "No hay producto registrado");
            return;
        }
        frmEntradaProducto miEntrada = new frmEntradaProducto(null, true);
        miEntrada.setLocationRelativeTo(null);
        miEntrada.setVisible(true);
    }//GEN-LAST:event_btnEntradaProductoActionPerformed
    private void btnSalidaProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalidaProductoActionPerformed
        if (tblTabla.getRowCount() == 0) {
            JOptionPane.showMessageDialog(this, "No hay producto registrado");
            return;
        }
        frmSalidaProducto miSalida = new frmSalidaProducto(null, true);
        miSalida.setLocationRelativeTo(null);
        miSalida.setVisible(true);
    }//GEN-LAST:event_btnSalidaProductoActionPerformed

    private void btnHistorialESActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnHistorialESActionPerformed

    }//GEN-LAST:event_btnHistorialESActionPerformed

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
        llenarTabla();
        mostrarRegistro();
        reOrdenar();
        reOrdenar();
        reOrdenar();
    }//GEN-LAST:event_formInternalFrameOpened

    private void tblTablaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tblTablaKeyReleased
        mostrarRegistro();
    }//GEN-LAST:event_tblTablaKeyReleased

    private void tblTablaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblTablaMouseClicked
        if (!tblTabla.isEnabled()) {
            return;
        }
        mostrarRegistro();
    }//GEN-LAST:event_tblTablaMouseClicked

    private void jlImagenMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlImagenMouseClicked
        if (tblTabla.getSelectedRow() == -1) {
            return;
        }

        JFileChooser file = new JFileChooser();

        file.setFileFilter(new FileNameExtensionFilter(
                "Imagenes",
                "jpg",
                "png",
                "PNG",
                "JPG")
        );
        int r = file.showOpenDialog(this);

        if (r == JFileChooser.CANCEL_OPTION) {
            return;
        }

        ImageIcon imagen = new ImageIcon(file.getSelectedFile().getAbsolutePath());
        //Tamaño de icono
        Icon icon = new ImageIcon(
                imagen.getImage().getScaledInstance(
                        164,
                        164,
                        Image.SCALE_DEFAULT
                )
        );
        imagen.getImage().flush();
        jlImagen.setIcon(icon);
        jlImagen.validate();
    }//GEN-LAST:event_jlImagenMouseClicked

    private void llenarTabla() {
        try {
            String titulos[] = {"Codigo", "Descripcion", "Existencia", "Nota", "Categoria", "Fecha Creacion", "Estado"};
            tblTabla.removeAll();
            miTabla = new DefaultTableModel(null, titulos);
            ResultSet rs = Datos.getProductos();
            while (rs.next()) {
                registro[0] = new Categoria(rs.getInt("idProducto"),
                        rs.getString("codigo"));
                registro[1] = rs.getString("descripcion");
                registro[2] = rs.getBigDecimal("CANTIDAD");
                registro[3] = rs.getString("nota");
                registro[4] = new Categoria(rs.getInt("idCategoria"),
                        rs.getString("Categoria"));
                registro[5] = rs.getDate("Fecha_Creacion");
                registro[6] = rs.getBoolean("estado");
                miTabla.addRow(registro);
            }
            tblTabla.setModel(miTabla);
        } catch (SQLException ex) {
            LOGGER.getLogger(frmProductos.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void mostrarRegistro() {

        if (tblTabla.getSelectedRow() == -1) {
            tblTabla.setRowSelectionInterval(0, 0);
        }

        ResultSet rs = Datos.getProductoById(null,
                ((Categoria) tblTabla.getValueAt(
                        tblTabla.getSelectedRow(), 0))
                        .getDescripcion().trim());

        BufferedImage img = null;
        try {
            rs.next();
            Blob blob = rs.getBlob("image");
            if (blob != null) {
                byte[] data = blob.getBytes(1, (int) blob.length());
                img = ImageIO.read(new ByteArrayInputStream(data));
            }

            ImageIcon imagen;

            if (img != null) {
                imagen = new ImageIcon(img);
            } else {
                imagen = new ImageIcon(getClass().getResource("/images/Sin_imagen 64 x 64.png"));
            }

            Icon icon = new ImageIcon(imagen.getImage().getScaledInstance(164, 164,
                    Image.SCALE_DEFAULT));
            imagen.getImage().flush();
            jlImagen.setIcon(icon);
            jlImagen.validate();
        } catch (SQLException ex) {
            LOGGER.getLogger(frmProductos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            LOGGER.getLogger(frmProductos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void reOrdenar() {
        if (tblTabla.getRowCount() == 0 || tblTabla.getSelectedRow() == -1) {
            return;
        }
        TableColumn t;
        for (int i = 0; i < tblTabla.getColumnCount(); i++) {
            t = tblTabla.getColumnModel().getColumn(i);

            tblTabla.getColumnModel().getColumn(i).
                    setCellRenderer(new RenderCeldas());

            if (i == 0) {
                t.setPreferredWidth(130);
                t.setMaxWidth(190);
                t.setMinWidth(20);
            }
            if (i == 1) {
                t.setPreferredWidth(380);
                t.setMaxWidth(400);
                t.setMinWidth(20);
            }
            if (i == 2) {
                t.setPreferredWidth(90);
                t.setMaxWidth(120);
                t.setMinWidth(20);
            }

            if (i == 3) {
                t.setPreferredWidth(120);
                t.setMaxWidth(250);
                t.setMinWidth(20);
            }
            if (i == 4) {
                t.setPreferredWidth(150);
                t.setMaxWidth(190);
                t.setMinWidth(20);
            }
            if (i == 5) {
                t.setPreferredWidth(150);
                t.setMaxWidth(190);
                t.setMinWidth(20);
            }
            if (i == 6) {
                t.setPreferredWidth(80);
                t.setMaxWidth(100);
                t.setMinWidth(20);
            }

        }
        //Para Alinear el Texto de la Table a la Derecha...

//        tcr.setHorizontalAlignment(SwingConstants.RIGHT);
//
//        tblTabla.getColumnModel().getColumn(2).setCellRenderer(tcr);
//        tblTabla.getColumnModel().getColumn(3).setCellRenderer(tcr);
//        tblTabla.getColumnModel().getColumn(4).setCellRenderer(tcr);
//
//        tblTabla.getColumnModel().getColumn(6).setCellEditor(new Celda_CheckBox());
//        tblTabla.getColumnModel().getColumn(6).setCellRenderer(new Render_CheckBox());
        tblTabla.setRowSelectionInterval(tblTabla.getSelectedRow(),
                tblTabla.getSelectedRow());
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBorrar;
    private javax.swing.JButton btnBuscarProducto;
    private javax.swing.JButton btnEntradaProducto;
    private javax.swing.JButton btnHistorialES;
    private javax.swing.JButton btnImprimirLista;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnSalidaProducto;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JLabel jlImagen;
    private rojeru_san.complementos.RSTableMetro tblTabla;
    // End of variables declaration//GEN-END:variables
}
