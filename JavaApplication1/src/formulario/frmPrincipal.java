package formulario;

import clases.Datos;
import clases.DesktopConFondo;
import clases.ScreenSplash;
import clases.Utilidades;
import static clases.Utilidades.LOGGER;
import conexiones.Conexion;
import formulario.productos.frmMovimientoEntradaSalida;
import hilos.hiloIp;
import hilos.hiloRestaurar;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.beans.PropertyVetoException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public final class frmPrincipal extends javax.swing.JFrame {

    private int returnVal = JFileChooser.CANCEL_OPTION;
    private String usuarioMaster, rol, idUsuario;

    //Archivos
    private frmClientes cliente;
    private frmProductos p;
    private frmUsuarios u;

    //Movimientos
    private frmBebidas b;
    private frmReporteFacturas r;
    private frmDeudas deudas;

    //Panel Izquierdo
    private frmMovimientoEntradaSalida m;
    private frmGraficos miGraficos;

    //Formularios Modales
    private frmCambioClave c;
    private frmFechaReporte fr;
    private frmCerrarTurno tc;

    public frmPrincipal(String rol) {
        ScreenSplash miSplash = new ScreenSplash();
        miSplash.animar2();
        
        if (ScreenSplash.debuger) {
            System.out.println("Clase Principal");
        }
        
        idUsuario = Datos.getUsuarioActual();
        
        initComponents();

        if (!idUsuario.equalsIgnoreCase("sysdba")) {
            mnuArchivosUsuario.setVisible(Datos.delega(idUsuario));
        } else {
            mnuArchivosUsuario.setVisible(true);
        }

        jPanelImpresion.setVisible(false);//Ocultar barra de progreso...

        this.rol = rol;
        
        if (rol.equalsIgnoreCase("Administrador") || idUsuario.equalsIgnoreCase("sysdba")) {

        } else {

            jlMostrarPanel.setVisible(false);
            jspIzquierdo.setVisible(false);
            mnuMovimientosNuevaFactura.doClick();
        }
        menus();
        
    }

    private void menus() {
        jlUsuarioActual.setText(
                "<html>Usuario: " + idUsuario
                + "<br> Licencia: " + Datos.periodoMaquina() + " días</html>"
        );
        filler1.setMaximumSize(new Dimension(30000, 20));
        jmbMenus.add(filler1);
        jmbMenus.add(jpInfo);

        if (rol.equalsIgnoreCase("administrador")) {
            jspIzquierdo.setVisible(true);
            estado();
        } else {
            jspIzquierdo.setVisible(false);
        }

        //Ocultamiento del administrador
        if (idUsuario.equalsIgnoreCase("sysdba")) {
            jlRestauracion.setVisible(true);
            jlRespaldar.setVisible(true);
            jlRestaurar.setVisible(true);
        } else {
            jlRestauracion.setVisible(false);
            jlRespaldar.setVisible(false);
            jlRestaurar.setVisible(false);
        }

        jmbMenus.add(jPanelImpresion);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        filler1 = new javax.swing.Box.Filler(new java.awt.Dimension(0, 0), new java.awt.Dimension(0, 0), new java.awt.Dimension(500, 0));
        jpInfo = new javax.swing.JPanel();
        jlUsuarioActual = new javax.swing.JLabel();
        jPanelImpresion = new javax.swing.JPanel();
        jLabelImpresion = new javax.swing.JLabel();
        jprImpresion = new javax.swing.JProgressBar();
        jPopupMenu1 = new javax.swing.JPopupMenu();
        Archivos = new javax.swing.JMenu();
        jmClientes = new javax.swing.JMenuItem();
        jmProductos = new javax.swing.JMenuItem();
        jmUsuarios = new javax.swing.JMenuItem();
        jSeparator7 = new javax.swing.JPopupMenu.Separator();
        jmCambioClave = new javax.swing.JMenuItem();
        jmCambioUsuario = new javax.swing.JMenuItem();
        jSeparator8 = new javax.swing.JPopupMenu.Separator();
        jmSalir = new javax.swing.JMenuItem();
        Movimientos = new javax.swing.JMenu();
        jmNuevaFactura = new javax.swing.JMenuItem();
        jSeparator9 = new javax.swing.JPopupMenu.Separator();
        jmReporteFactura = new javax.swing.JMenuItem();
        jmInventario = new javax.swing.JMenuItem();
        jSeparator10 = new javax.swing.JPopupMenu.Separator();
        jmAbrirTurno = new javax.swing.JMenuItem();
        jmCerrarTurno = new javax.swing.JMenuItem();
        jSeparator11 = new javax.swing.JPopupMenu.Separator();
        jmDeuda = new javax.swing.JMenuItem();
        jspIzquierdo = new javax.swing.JScrollPane();
        pEstatus = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jlImagen = new javax.swing.JLabel();
        jlGetIP = new javax.swing.JLabel();
        jlMovimientoES = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtCajero = new JTable(){
            @Override
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false; //Las celdas no son editables.
            }
        };
        jPanel1 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtMensaje = new javax.swing.JTextPane();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtTelefonosEmpresa = new javax.swing.JTextField();
        txtDireccionEmpresa = new javax.swing.JTextField();
        txtNombreEmpresa = new javax.swing.JTextField();
        btnGuardarMensaje = new javax.swing.JLabel();
        jlRespaldar = new javax.swing.JLabel();
        jlRestaurar = new javax.swing.JLabel();
        jlRestauracion = new javax.swing.JLabel();
        jlGrafica = new javax.swing.JLabel();
        jspDerecho = new javax.swing.JScrollPane();
        dpnEscritorio = new DesktopConFondo();
        jlMostrarPanel = new javax.swing.JLabel();
        jmbMenus = new javax.swing.JMenuBar();
        mnuArchivos = new javax.swing.JMenu();
        mnuArchivosCliente = new javax.swing.JMenuItem();
        mnuArchivosProductos = new javax.swing.JMenuItem();
        mnuArchivosUsuario = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        mnuArchivosCambioClave = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        mnuArchivosCambioUsuario = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        mnuArchivosConfiguraciones = new javax.swing.JMenuItem();
        jSeparator12 = new javax.swing.JPopupMenu.Separator();
        mnuArchivosSalir = new javax.swing.JMenuItem();
        mnuMovimientos = new javax.swing.JMenu();
        mnuMovimientosNuevaFactura = new javax.swing.JMenuItem();
        jSeparator5 = new javax.swing.JPopupMenu.Separator();
        mnuMovimientosReporteFactura = new javax.swing.JMenuItem();
        mnuMovimientosInventario = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        mnuMovimientosAbrirTurno = new javax.swing.JMenuItem();
        mnuMovimientosCerrarTurno = new javax.swing.JMenuItem();
        jSeparator6 = new javax.swing.JPopupMenu.Separator();
        mnuMovimientosDeudas = new javax.swing.JMenuItem();
        mnuAyuda = new javax.swing.JMenu();
        mnuAyudaAcercaDe = new javax.swing.JMenuItem();
        mnuAyudaAyuda = new javax.swing.JMenuItem();

        jpInfo.setBackground(new java.awt.Color(4, 255, 0));
        jpInfo.setFocusable(false);
        jpInfo.setMaximumSize(new java.awt.Dimension(170, 55));
        jpInfo.setMinimumSize(new java.awt.Dimension(150, 55));
        jpInfo.setPreferredSize(new java.awt.Dimension(160, 55));
        jpInfo.setRequestFocusEnabled(false);
        jpInfo.setVerifyInputWhenFocusTarget(false);

        jlUsuarioActual.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jlUsuarioActual.setForeground(new java.awt.Color(1, 1, 1));
        jlUsuarioActual.setText("Quireo verlo");

        javax.swing.GroupLayout jpInfoLayout = new javax.swing.GroupLayout(jpInfo);
        jpInfo.setLayout(jpInfoLayout);
        jpInfoLayout.setHorizontalGroup(
            jpInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
            .addGroup(jpInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jpInfoLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jlUsuarioActual)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        jpInfoLayout.setVerticalGroup(
            jpInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
            .addGroup(jpInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jpInfoLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jlUsuarioActual)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        jPanelImpresion.setMaximumSize(new java.awt.Dimension(32767, 30));
        jPanelImpresion.setMinimumSize(new java.awt.Dimension(0, 30));
        jPanelImpresion.setLayout(new java.awt.GridLayout(1, 0, 4, 0));

        jLabelImpresion.setFont(new java.awt.Font("Ubuntu", 0, 18)); // NOI18N
        jLabelImpresion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabelImpresion.setText("Imprimiendo ");
        jLabelImpresion.setMaximumSize(new java.awt.Dimension(120, 25));
        jLabelImpresion.setMinimumSize(new java.awt.Dimension(59, 25));
        jLabelImpresion.setPreferredSize(null);
        jPanelImpresion.add(jLabelImpresion);

        jprImpresion.setFont(new java.awt.Font("Ubuntu", 1, 12)); // NOI18N
        jprImpresion.setDoubleBuffered(true);
        jprImpresion.setMaximumSize(new java.awt.Dimension(32767, 30));
        jprImpresion.setMinimumSize(new java.awt.Dimension(0, 30));
        jprImpresion.setStringPainted(true);
        jPanelImpresion.add(jprImpresion);

        Archivos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Archivos 32 x 32.png"))); // NOI18N
        Archivos.setText("Archivos");
        Archivos.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N

        jmClientes.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jmClientes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Cliente 32 x 32.png"))); // NOI18N
        jmClientes.setText("Clientes");
        jmClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmClientesActionPerformed(evt);
            }
        });
        Archivos.add(jmClientes);

        jmProductos.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jmProductos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Productos 32 x 32.png"))); // NOI18N
        jmProductos.setText("Productos");
        jmProductos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmProductosActionPerformed(evt);
            }
        });
        Archivos.add(jmProductos);

        jmUsuarios.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jmUsuarios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Usuario 32 x 32.png"))); // NOI18N
        jmUsuarios.setText("Usuarios");
        jmUsuarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmUsuariosActionPerformed(evt);
            }
        });
        Archivos.add(jmUsuarios);
        Archivos.add(jSeparator7);

        jmCambioClave.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jmCambioClave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Cambiar Contraseña 32 x 32.png"))); // NOI18N
        jmCambioClave.setText("Cambio de Clave");
        jmCambioClave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmCambioClaveActionPerformed(evt);
            }
        });
        Archivos.add(jmCambioClave);

        jmCambioUsuario.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jmCambioUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Cambio de Usuario 32 x 32.png"))); // NOI18N
        jmCambioUsuario.setText("Cambio de Usuario");
        jmCambioUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmCambioUsuarioActionPerformed(evt);
            }
        });
        Archivos.add(jmCambioUsuario);
        Archivos.add(jSeparator8);

        jmSalir.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jmSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Salir 32 x 32.png"))); // NOI18N
        jmSalir.setText("Salir");
        jmSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmSalirActionPerformed(evt);
            }
        });
        Archivos.add(jmSalir);

        jPopupMenu1.add(Archivos);

        Movimientos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Movimiento 32 x 32.png"))); // NOI18N
        Movimientos.setText("Movimientos");
        Movimientos.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N

        jmNuevaFactura.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jmNuevaFactura.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Factura 32 x 32.png"))); // NOI18N
        jmNuevaFactura.setText("Nueva Factura");
        jmNuevaFactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmNuevaFacturaActionPerformed(evt);
            }
        });
        Movimientos.add(jmNuevaFactura);
        Movimientos.add(jSeparator9);

        jmReporteFactura.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jmReporteFactura.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Reportar Factura 32 x 32.png"))); // NOI18N
        jmReporteFactura.setText("Reporte de Factura");
        jmReporteFactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmReporteFacturaActionPerformed(evt);
            }
        });
        Movimientos.add(jmReporteFactura);

        jmInventario.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jmInventario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/inventario 32 x 32.png"))); // NOI18N
        jmInventario.setText("Inventario");
        jmInventario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmInventarioActionPerformed(evt);
            }
        });
        Movimientos.add(jmInventario);
        Movimientos.add(jSeparator10);

        jmAbrirTurno.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jmAbrirTurno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/billar32x32.png"))); // NOI18N
        jmAbrirTurno.setText("Abrir Turno");
        jmAbrirTurno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmAbrirTurnoActionPerformed(evt);
            }
        });
        Movimientos.add(jmAbrirTurno);

        jmCerrarTurno.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jmCerrarTurno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/noBillar32x32.png"))); // NOI18N
        jmCerrarTurno.setText("Cerrar Turno");
        jmCerrarTurno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmCerrarTurnoActionPerformed(evt);
            }
        });
        Movimientos.add(jmCerrarTurno);
        Movimientos.add(jSeparator11);

        jmDeuda.setFont(new java.awt.Font("Ubuntu", 1, 18)); // NOI18N
        jmDeuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/money32x32.png"))); // NOI18N
        jmDeuda.setText("Deuda");
        jmDeuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmDeudaActionPerformed(evt);
            }
        });
        Movimientos.add(jmDeuda);

        jPopupMenu1.add(Movimientos);

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Ventana principal del sistema");
        setIconImage(Toolkit.getDefaultToolkit().getImage("icon.png"));
        setMinimumSize(new java.awt.Dimension(640, 480));
        setPreferredSize(new java.awt.Dimension(800, 600));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jspIzquierdo.setPreferredSize(new java.awt.Dimension(270, 560));

        pEstatus.setMinimumSize(new java.awt.Dimension(20, 20));
        pEstatus.setPreferredSize(new java.awt.Dimension(250, 660));

        jLabel1.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Datos actuales de la empresa");

        jlImagen.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlImagen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Sin_imagen 64 x 64.png"))); // NOI18N
        jlImagen.setToolTipText("Doble click para cambiar el logo");
        jlImagen.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Seleccione Logo", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Ubuntu", 0, 14))); // NOI18N
        jlImagen.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jlImagen.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jlImagen.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlImagenMouseClicked(evt);
            }
        });

        jlGetIP.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jlGetIP.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlGetIP.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/ip32X32.png"))); // NOI18N
        jlGetIP.setToolTipText("Obtener la IP Publica del Equipo cuando esta conectada a Internet");
        jlGetIP.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        jlGetIP.setDoubleBuffered(true);
        jlGetIP.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jlGetIP.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jlGetIP.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlGetIPMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jlGetIPMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jlGetIPMouseExited(evt);
            }
        });

        jlMovimientoES.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jlMovimientoES.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlMovimientoES.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/inventario 32 x 32.png"))); // NOI18N
        jlMovimientoES.setToolTipText("Reporte e impresion de Entrada y Salida");
        jlMovimientoES.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jlMovimientoES.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jlMovimientoES.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlMovimientoESMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jlMovimientoESMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jlMovimientoESMouseEntered(evt);
            }
        });

        jScrollPane1.setMinimumSize(new java.awt.Dimension(267, 70));
        jScrollPane1.setPreferredSize(new java.awt.Dimension(267, 77));

        jtCajero.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null},
                {null}
            },
            new String [] {
                "Cajero Activo"
            }
        ));
        jtCajero.setFillsViewportHeight(true);
        jtCajero.setMinimumSize(new java.awt.Dimension(267, 70));
        jtCajero.setPreferredSize(new java.awt.Dimension(267, 77));
        jtCajero.setShowGrid(true);
        jtCajero.setShowVerticalLines(false);
        jScrollPane1.setViewportView(jtCajero);

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel5.setText("Nombre de la Empresa:");

        jScrollPane2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Mensaje de Tickes", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Ubuntu", 0, 14))); // NOI18N
        jScrollPane2.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        jScrollPane2.setViewportView(txtMensaje);

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel7.setText("Direccion de la Empresa:");

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel8.setText("Telefonos de la Empresa:");

        btnGuardarMensaje.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnGuardarMensaje.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Guardar 32 x 32.png"))); // NOI18N
        btnGuardarMensaje.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnGuardarMensaje.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnGuardarMensaje.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnGuardarMensaje.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnGuardarMensajeMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNombreEmpresa, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtDireccionEmpresa, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTelefonosEmpresa))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnGuardarMensaje))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jLabel5)
                .addGap(0, 0, 0)
                .addComponent(txtNombreEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jLabel7)
                .addGap(0, 0, 0)
                .addComponent(txtDireccionEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jLabel8)
                .addGap(0, 0, 0)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(btnGuardarMensaje)
                    .addComponent(txtTelefonosEmpresa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        jlRespaldar.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jlRespaldar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlRespaldar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/respaldarBD32x32.png"))); // NOI18N
        jlRespaldar.setToolTipText("Hacer un Respaldo de la Base de Datos...");
        jlRespaldar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jlRespaldar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jlRespaldar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlRespaldarMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jlRespaldarMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jlRespaldarMouseEntered(evt);
            }
        });

        jlRestaurar.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jlRestaurar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlRestaurar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/restaurarBD32x32.png"))); // NOI18N
        jlRestaurar.setToolTipText("Retaurar la Base de Datos desde un Respaldo");
        jlRestaurar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jlRestaurar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jlRestaurar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlRestaurarMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jlRestaurarMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jlRestaurarMouseEntered(evt);
            }
        });

        jlRestauracion.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jlRestauracion.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlRestauracion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/update 32x32.png"))); // NOI18N
        jlRestauracion.setToolTipText("Restaurar una base de datos anterior...");
        jlRestauracion.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        jlRestauracion.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jlRestauracion.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jlRestauracion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlRestauracionMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jlRestauracionMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jlRestauracionMouseEntered(evt);
            }
        });

        jlGrafica.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jlGrafica.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlGrafica.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Grafico32x32.png"))); // NOI18N
        jlGrafica.setToolTipText("Obtener la IP Publica del Equipo cuando esta conectada a Internet");
        jlGrafica.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        jlGrafica.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jlGrafica.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jlGrafica.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlGraficaMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jlGraficaMouseExited(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jlGraficaMouseEntered(evt);
            }
        });

        javax.swing.GroupLayout pEstatusLayout = new javax.swing.GroupLayout(pEstatus);
        pEstatus.setLayout(pEstatusLayout);
        pEstatusLayout.setHorizontalGroup(
            pEstatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pEstatusLayout.createSequentialGroup()
                .addGroup(pEstatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jlImagen, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pEstatusLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(pEstatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pEstatusLayout.createSequentialGroup()
                                .addComponent(jlMovimientoES, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(53, 53, 53)
                                .addComponent(jlGrafica, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGap(52, 52, 52)
                                .addComponent(jlGetIP, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addGroup(pEstatusLayout.createSequentialGroup()
                                .addComponent(jlRestauracion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jlRespaldar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jlRestaurar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addGap(0, 0, 0))
        );
        pEstatusLayout.setVerticalGroup(
            pEstatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pEstatusLayout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(0, 0, 0)
                .addComponent(jlImagen, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 247, Short.MAX_VALUE)
                .addGroup(pEstatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jlMovimientoES)
                    .addComponent(jlGetIP, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jlGrafica))
                .addGroup(pEstatusLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jlRespaldar)
                    .addComponent(jlRestaurar)
                    .addComponent(jlRestauracion))
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
        );

        jspIzquierdo.setViewportView(pEstatus);

        dpnEscritorio.setBackground(new java.awt.Color(0, 102, 102));
        dpnEscritorio.setComponentPopupMenu(jPopupMenu1);
        dpnEscritorio.setPreferredSize(new java.awt.Dimension(510, 531));

        jlMostrarPanel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Flecha Derecha 32 x 32.png"))); // NOI18N
        jlMostrarPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlMostrarPanelMouseClicked(evt);
            }
        });
        dpnEscritorio.add(jlMostrarPanel);
        jlMostrarPanel.setBounds(0, 250, 40, 40);

        jspDerecho.setViewportView(dpnEscritorio);

        jmbMenus.setMaximumSize(new java.awt.Dimension(4000, 150));
        jmbMenus.setMinimumSize(new java.awt.Dimension(4000, 50));
        jmbMenus.setPreferredSize(new java.awt.Dimension(4000, 50));

        mnuArchivos.setBackground(new java.awt.Color(0, 0, 0));
        mnuArchivos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Archivos 32 x 32.png"))); // NOI18N
        mnuArchivos.setText("Archivos  ");
        mnuArchivos.setDoubleBuffered(true);
        mnuArchivos.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N

        mnuArchivosCliente.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F2, 0));
        mnuArchivosCliente.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        mnuArchivosCliente.setForeground(new java.awt.Color(1, 1, 1));
        mnuArchivosCliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Cliente 32 x 32.png"))); // NOI18N
        mnuArchivosCliente.setText("Clientes ...");
        mnuArchivosCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuArchivosClienteActionPerformed(evt);
            }
        });
        mnuArchivos.add(mnuArchivosCliente);

        mnuArchivosProductos.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F3, 0));
        mnuArchivosProductos.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        mnuArchivosProductos.setForeground(new java.awt.Color(1, 1, 1));
        mnuArchivosProductos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Productos 32 x 32.png"))); // NOI18N
        mnuArchivosProductos.setText("Productos ...");
        mnuArchivosProductos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuArchivosProductosActionPerformed(evt);
            }
        });
        mnuArchivos.add(mnuArchivosProductos);

        mnuArchivosUsuario.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, 0));
        mnuArchivosUsuario.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        mnuArchivosUsuario.setForeground(new java.awt.Color(1, 1, 1));
        mnuArchivosUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Usuario 32 x 32.png"))); // NOI18N
        mnuArchivosUsuario.setText("Usuarios ...");
        mnuArchivosUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuArchivosUsuarioActionPerformed(evt);
            }
        });
        mnuArchivos.add(mnuArchivosUsuario);
        mnuArchivos.add(jSeparator1);

        mnuArchivosCambioClave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F5, 0));
        mnuArchivosCambioClave.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        mnuArchivosCambioClave.setForeground(new java.awt.Color(1, 1, 1));
        mnuArchivosCambioClave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Cambiar Contraseña 32 x 32.png"))); // NOI18N
        mnuArchivosCambioClave.setText("Cambio de Clave ...");
        mnuArchivosCambioClave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuArchivosCambioClaveActionPerformed(evt);
            }
        });
        mnuArchivos.add(mnuArchivosCambioClave);
        mnuArchivos.add(jSeparator3);

        mnuArchivosCambioUsuario.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F6, 0));
        mnuArchivosCambioUsuario.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        mnuArchivosCambioUsuario.setForeground(new java.awt.Color(1, 1, 1));
        mnuArchivosCambioUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Cambio de Usuario 32 x 32.png"))); // NOI18N
        mnuArchivosCambioUsuario.setText("Cambio de Usuario ...");
        mnuArchivosCambioUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuArchivosCambioUsuarioActionPerformed(evt);
            }
        });
        mnuArchivos.add(mnuArchivosCambioUsuario);
        mnuArchivos.add(jSeparator2);

        mnuArchivosConfiguraciones.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F7, 0));
        mnuArchivosConfiguraciones.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        mnuArchivosConfiguraciones.setForeground(new java.awt.Color(1, 1, 1));
        mnuArchivosConfiguraciones.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Parametros 32 x 32.png"))); // NOI18N
        mnuArchivosConfiguraciones.setText("Configuraciones");
        mnuArchivosConfiguraciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuArchivosConfiguracionesActionPerformed(evt);
            }
        });
        mnuArchivos.add(mnuArchivosConfiguraciones);
        mnuArchivos.add(jSeparator12);

        mnuArchivosSalir.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        mnuArchivosSalir.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        mnuArchivosSalir.setForeground(new java.awt.Color(1, 1, 1));
        mnuArchivosSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Salir 32 x 32.png"))); // NOI18N
        mnuArchivosSalir.setText("Salir");
        mnuArchivosSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuArchivosSalirActionPerformed(evt);
            }
        });
        mnuArchivos.add(mnuArchivosSalir);

        jmbMenus.add(mnuArchivos);

        mnuMovimientos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Movimiento 32 x 32.png"))); // NOI18N
        mnuMovimientos.setText("Movimientos");
        mnuMovimientos.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N

        mnuMovimientosNuevaFactura.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F7, 0));
        mnuMovimientosNuevaFactura.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        mnuMovimientosNuevaFactura.setForeground(new java.awt.Color(1, 1, 1));
        mnuMovimientosNuevaFactura.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Factura 32 x 32.png"))); // NOI18N
        mnuMovimientosNuevaFactura.setText("Nueva Factura...");
        mnuMovimientosNuevaFactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuMovimientosNuevaFacturaActionPerformed(evt);
            }
        });
        mnuMovimientos.add(mnuMovimientosNuevaFactura);
        mnuMovimientos.add(jSeparator5);

        mnuMovimientosReporteFactura.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F8, 0));
        mnuMovimientosReporteFactura.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        mnuMovimientosReporteFactura.setForeground(new java.awt.Color(1, 1, 1));
        mnuMovimientosReporteFactura.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Reportar Factura 32 x 32.png"))); // NOI18N
        mnuMovimientosReporteFactura.setText("Reporte de Facturas...");
        mnuMovimientosReporteFactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuMovimientosReporteFacturaActionPerformed(evt);
            }
        });
        mnuMovimientos.add(mnuMovimientosReporteFactura);

        mnuMovimientosInventario.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F9, 0));
        mnuMovimientosInventario.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        mnuMovimientosInventario.setForeground(new java.awt.Color(1, 1, 1));
        mnuMovimientosInventario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/inventario 32 x 32.png"))); // NOI18N
        mnuMovimientosInventario.setText("Inventario...");
        mnuMovimientosInventario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuMovimientosInventarioActionPerformed(evt);
            }
        });
        mnuMovimientos.add(mnuMovimientosInventario);
        mnuMovimientos.add(jSeparator4);

        mnuMovimientosAbrirTurno.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F10, 0));
        mnuMovimientosAbrirTurno.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        mnuMovimientosAbrirTurno.setForeground(new java.awt.Color(1, 1, 1));
        mnuMovimientosAbrirTurno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/billar32x32.png"))); // NOI18N
        mnuMovimientosAbrirTurno.setText("Abrir turno...");
        mnuMovimientosAbrirTurno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuMovimientosAbrirTurnoActionPerformed(evt);
            }
        });
        mnuMovimientos.add(mnuMovimientosAbrirTurno);

        mnuMovimientosCerrarTurno.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F11, 0));
        mnuMovimientosCerrarTurno.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        mnuMovimientosCerrarTurno.setForeground(new java.awt.Color(1, 1, 1));
        mnuMovimientosCerrarTurno.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/noBillar32x32.png"))); // NOI18N
        mnuMovimientosCerrarTurno.setText("Cerrar turno...");
        mnuMovimientosCerrarTurno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuMovimientosCerrarTurnoActionPerformed(evt);
            }
        });
        mnuMovimientos.add(mnuMovimientosCerrarTurno);
        mnuMovimientos.add(jSeparator6);

        mnuMovimientosDeudas.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F12, 0));
        mnuMovimientosDeudas.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        mnuMovimientosDeudas.setForeground(new java.awt.Color(1, 1, 1));
        mnuMovimientosDeudas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/money32x32.png"))); // NOI18N
        mnuMovimientosDeudas.setText("Deuda");
        mnuMovimientosDeudas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuMovimientosDeudasActionPerformed(evt);
            }
        });
        mnuMovimientos.add(mnuMovimientosDeudas);

        jmbMenus.add(mnuMovimientos);

        mnuAyuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Sistema de Ayuda 32 x 32.png"))); // NOI18N
        mnuAyuda.setText("Ayuda");
        mnuAyuda.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N

        mnuAyudaAcercaDe.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        mnuAyudaAcercaDe.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        mnuAyudaAcercaDe.setForeground(new java.awt.Color(1, 1, 1));
        mnuAyudaAcercaDe.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Acerca de 32 x 32.png"))); // NOI18N
        mnuAyudaAcercaDe.setText("Acerca de...");
        mnuAyudaAcercaDe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuAyudaAcercaDeActionPerformed(evt);
            }
        });
        mnuAyuda.add(mnuAyudaAcercaDe);

        mnuAyudaAyuda.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F1, 0));
        mnuAyudaAyuda.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        mnuAyudaAyuda.setForeground(new java.awt.Color(1, 1, 1));
        mnuAyudaAyuda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Help 32 x 32.png"))); // NOI18N
        mnuAyudaAyuda.setText("Ayuda...");
        mnuAyudaAyuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuAyudaAyudaActionPerformed(evt);
            }
        });
        mnuAyuda.add(mnuAyudaAyuda);

        jmbMenus.add(mnuAyuda);

        setJMenuBar(jmbMenus);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jspIzquierdo, javax.swing.GroupLayout.PREFERRED_SIZE, 358, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jspDerecho, javax.swing.GroupLayout.DEFAULT_SIZE, 690, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jspIzquierdo, javax.swing.GroupLayout.DEFAULT_SIZE, 664, Short.MAX_VALUE)
            .addComponent(jspDerecho)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void mnuArchivosClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuArchivosClienteActionPerformed
        if (ScreenSplash.debuger) {
            System.out.println("Entrando al formulario de Clientes");
        }
        if (cliente == null) {
            cliente = new frmClientes();
            dpnEscritorio.add(cliente);
        }

        cliente.setVisible(true);

        try {
            cliente.setMaximum(true);
        } catch (PropertyVetoException ex) {
            LOGGER.getLogger(frmPrincipal.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        if (ScreenSplash.debuger)
            System.out.println("Completado Clientes");
    }//GEN-LAST:event_mnuArchivosClienteActionPerformed
    private void mnuArchivosProductosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuArchivosProductosActionPerformed
        if (ScreenSplash.debuger) {
            System.out.println("Entrando al formulario de Producto");
        }
        if (p == null) {
            if (ScreenSplash.debuger) {
                System.out.println("Es Nulo");
            }
            p = new frmProductos();
            dpnEscritorio.add(p);
        }
        try {
            p.setMaximum(false);
            p.setMaximum(true);
        } catch (PropertyVetoException ex) {
            if (ScreenSplash.debuger) {
                System.out.println("Falloooo");
            }
            LOGGER.getLogger(frmPrincipal.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        p.setVisible(true);
        if (ScreenSplash.debuger)
            System.out.println("Completado Productos");
    }//GEN-LAST:event_mnuArchivosProductosActionPerformed
    private void mnuArchivosUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuArchivosUsuarioActionPerformed
        if (ScreenSplash.debuger) {
            System.out.println("Entrando al formulario de Usuarios");
        }

        if (u == null) {
            u = new frmUsuarios();
            dpnEscritorio.add(u);
        }

        try {
            u.setMaximum(false);
            u.setMaximum(true);
        } catch (PropertyVetoException ex) {
            LOGGER.getLogger(frmPrincipal.class
                    .getName()).log(Level.SEVERE, null, ex);
        }

        u.setVisible(true);
        if (ScreenSplash.debuger)
            System.out.println("Completado");
    }//GEN-LAST:event_mnuArchivosUsuarioActionPerformed
    private void mnuArchivosCambioClaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuArchivosCambioClaveActionPerformed
        if (ScreenSplash.debuger) {
            System.out.println("Entrando al formulario de cambio de clave");
        }
        if (c == null) {
            c = new frmCambioClave(this, true);
        }
        
        c.setLocationRelativeTo(this);
        c.setVisible(true);
        if (ScreenSplash.debuger)
            System.out.println("Completado");
    }//GEN-LAST:event_mnuArchivosCambioClaveActionPerformed
    private void mnuArchivosCambioUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuArchivosCambioUsuarioActionPerformed
        if (ScreenSplash.debuger) {
            System.out.println("Cargando el Login de Usuario");
        }

        try {
            Conexion.getCnn().close();
            Conexion.setCnn(null);
        } catch (SQLException ex) {
            LOGGER.getLogger(frmLogin.class.getName()).log(Level.SEVERE, null, ex);
        }

        cerrarFormularios();

        frmLogin login = new frmLogin();
        login.setLocationRelativeTo(this);
        login.setVisible(true);
        dispose();

        if (ScreenSplash.debuger)
            System.out.println("Esperando el Logeo");
    }//GEN-LAST:event_mnuArchivosCambioUsuarioActionPerformed
    private void mnuArchivosSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuArchivosSalirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_mnuArchivosSalirActionPerformed
    private void mnuMovimientosNuevaFacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuMovimientosNuevaFacturaActionPerformed
        if (ScreenSplash.debuger) {
            System.out.println("Entrando al modulo de nueva factura");
        }
        if (!Datos.usuarioTurnoActivo(idUsuario)) {
            if (ScreenSplash.debuger) {
                System.out.println("Usuario Sin turno activo");
            }
            JOptionPane.showMessageDialog(this, "Usuario no cuenta con Turno para Facturar...!");
            return;
        }

        if (b == null) {
            b = new frmBebidas();
            dpnEscritorio.add(b);
        }

        bebidaR();
    }//GEN-LAST:event_mnuMovimientosNuevaFacturaActionPerformed

    private void mnuMovimientosReporteFacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuMovimientosReporteFacturaActionPerformed
        if (ScreenSplash.debuger) {
            System.out.println("Abriendo el modulo de Movimiento de Reportes de Factura");
        }
        if (r == null) {
            r = new frmReporteFacturas();
        }
        if (!dpnEscritorio.isAncestorOf(r)) {
            dpnEscritorio.add(r);
        }
        r.centralizar();
        r.setVisible(true);

        if (ScreenSplash.debuger)
            System.out.println("Completado");
    }//GEN-LAST:event_mnuMovimientosReporteFacturaActionPerformed
    private void mnuMovimientosInventarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuMovimientosInventarioActionPerformed
        if (ScreenSplash.debuger) {
            System.out.println("Abriendo el modulo Inventarios");
        }
        if (fr == null) {
            fr = new frmFechaReporte(this, true);
        }

        fr.setLocationRelativeTo(this);
        fr.setVisible(true);

        if (fr.getFecha() == null) {
            return;
        }
        if (ScreenSplash.debuger) {
            System.out.println("Realizando Reporte");
        }
        imprimirReporte(fr.getFecha());
        if (ScreenSplash.debuger)
            System.out.println("Hecho");
    }//GEN-LAST:event_mnuMovimientosInventarioActionPerformed
    private void mnuMovimientosAbrirTurnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuMovimientosAbrirTurnoActionPerformed
        if (ScreenSplash.debuger) {
            System.out.println("Abriendo el modulo de Abrir Turno a Usuario");
        }

        frmAbrilTurno t = new frmAbrilTurno(this, true);

        t.setLocationRelativeTo(this);
        t.setVisible(true);

        if (t.isAceptar()) {
            mnuArchivosCambioUsuarioActionPerformed(evt);
        }
    }//GEN-LAST:event_mnuMovimientosAbrirTurnoActionPerformed
    private void mnuMovimientosCerrarTurnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuMovimientosCerrarTurnoActionPerformed
        if (ScreenSplash.debuger) {
            System.out.println("Abriendo el modulo de Cerrar Turno");
        }
        if (tc == null) {
            tc = new frmCerrarTurno(this, true);
        }
        tc.setLocationRelativeTo(this);
        tc.setVisible(true);
        if (tc.isAceptar()) {
            mnuArchivosCambioUsuarioActionPerformed(evt);
        }
        if (ScreenSplash.debuger)
            System.out.println("Completado");
    }//GEN-LAST:event_mnuMovimientosCerrarTurnoActionPerformed
    private void mnuAyudaAcercaDeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuAyudaAcercaDeActionPerformed
        frmAcercaDe acerca = new frmAcercaDe(this, true);
        acerca.setLocationRelativeTo(this);
        acerca.setVisible(true);
    }//GEN-LAST:event_mnuAyudaAcercaDeActionPerformed
    private void mnuAyudaAyudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuAyudaAyudaActionPerformed

    }//GEN-LAST:event_mnuAyudaAyudaActionPerformed
    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        if (mnuArchivosSalir.isEnabled()) {
            mnuArchivosCambioUsuarioActionPerformed(null);
        } else {
            frmAutorizacion miAut = new frmAutorizacion(null, true);
            miAut.setLocationRelativeTo(this);
            miAut.setVisible(true);
            if (miAut.isAceptado()) {
                mnuArchivosCambioUsuarioActionPerformed(null);
            } else {
                JOptionPane.showMessageDialog(this, "Usuario no valido");
            }
        }//Estamos cerrando todo desde que finaliza...
        cerrarFormularios();
    }//GEN-LAST:event_formWindowClosing
    private void jlImagenMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlImagenMouseClicked
        if (evt.getClickCount() == 1) {
            return;
        }
        int resp = JOptionPane.showConfirmDialog(this,
                "Desea cambiar el logo de la empresa?", "Proceso de confirmación!!!",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

        if (resp == JOptionPane.NO_OPTION) {
            return;
        }

        JFileChooser file = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Imagenes",
                "jpg", "png", "PNG", "JPG");
        file.setFileFilter(filter);

        returnVal = file.showOpenDialog(this);

        ImageIcon imagen;
        Icon icon;
        if (returnVal == JFileChooser.APPROVE_OPTION) {

            Datos.guardarImagen(file.getSelectedFile().getAbsolutePath(), "Logo");

            imagen = new ImageIcon(Datos.getImagen("Logo"));
            icon = new ImageIcon(imagen.getImage().getScaledInstance(180, 120,
                    Image.SCALE_DEFAULT));
            imagen.getImage().flush();
            jlImagen.setIcon(icon);
            jlImagen.validate();
        }
    }//GEN-LAST:event_jlImagenMouseClicked
    private void jlGetIPMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlGetIPMouseClicked
//        EventQueue.invokeLater(() -> {
//            new UnlockJFrame().setVisible(true);
//        });        
        hiloIp miIp = new hiloIp();
        miIp.start();
    }//GEN-LAST:event_jlGetIPMouseClicked

    private void jlMovimientoESMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlMovimientoESMouseClicked
        if (m == null) {
            m = new frmMovimientoEntradaSalida();
            dpnEscritorio.add(m);
        }
        try {
            m.setMaximum(false);
            m.setMaximum(true);
        } catch (PropertyVetoException ex) {
            LOGGER.getLogger(frmPrincipal.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        m.setVisible(true);
    }//GEN-LAST:event_jlMovimientoESMouseClicked
    private void btnGuardarMensajeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnGuardarMensajeMouseClicked
        JOptionPane.showMessageDialog(this,
                Datos.modificarOpcionMensaje(txtMensaje.getText(),
                        txtNombreEmpresa.getText(),
                        txtDireccionEmpresa.getText(),
                        txtTelefonosEmpresa.getText()));

    }//GEN-LAST:event_btnGuardarMensajeMouseClicked

    private void jlRespaldarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlRespaldarMouseClicked
        hiloRestaurar miRestaurar = new hiloRestaurar();
        miRestaurar.start();
    }//GEN-LAST:event_jlRespaldarMouseClicked

    private void jlRestaurarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlRestaurarMouseClicked
        JFileChooser miFile = new JFileChooser(System.getProperty("user.dir") + "/Data");
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Base de Datos",
                "fbk", "FBK");
        miFile.setFileFilter(filter);

        Integer respuesta = miFile.showOpenDialog(this);

        if (respuesta == JFileChooser.CANCEL_OPTION || respuesta == null) {
            return;
        }//Elegir el backup de la base de datos a restaurar...

        usuarioMaster = null;
        usuarioMaster = JOptionPane.showInputDialog(this,
                "Inserte el nombre de Usuario: ", "Usuario...",
                JOptionPane.INFORMATION_MESSAGE);

        if (usuarioMaster == null || usuarioMaster.equals("")) {
            return;
        }//Coner el usuario que va a realizar la operacion de back up...
        JPasswordField pf = new JPasswordField();

        Integer claveMaster = JOptionPane.showConfirmDialog(this, pf,
                "Inserte el nombre de Usuario: ", JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.INFORMATION_MESSAGE);
        pf.requestFocusInWindow();
        if (claveMaster == JOptionPane.CANCEL_OPTION || claveMaster == null) {
            return;
        }//Obtener la clave del usuario a realizar el backup

//        BDR = miFile.getSelectedFile();
//        RGBAK = System.getProperty("user.dir") + "/respaldo/gbak";

    }//GEN-LAST:event_jlRestaurarMouseClicked

    private void jlMovimientoESMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlMovimientoESMouseEntered
        jlMovimientoES.setText("Reporte");
    }//GEN-LAST:event_jlMovimientoESMouseEntered

    private void jlMovimientoESMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlMovimientoESMouseExited
        jlMovimientoES.setText("");
    }//GEN-LAST:event_jlMovimientoESMouseExited

    private void jlGetIPMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlGetIPMouseEntered
        jlGetIP.setText("Ip Publica");
    }//GEN-LAST:event_jlGetIPMouseEntered

    private void jlGetIPMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlGetIPMouseExited
        jlGetIP.setText("");
    }//GEN-LAST:event_jlGetIPMouseExited

    private void jlRespaldarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlRespaldarMouseEntered
        jlRespaldar.setText("Respadar");
    }//GEN-LAST:event_jlRespaldarMouseEntered

    private void jlRespaldarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlRespaldarMouseExited
        jlRespaldar.setText("");
    }//GEN-LAST:event_jlRespaldarMouseExited

    private void jlRestaurarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlRestaurarMouseEntered
        jlRestaurar.setText("Restaurar");
    }//GEN-LAST:event_jlRestaurarMouseEntered

    private void jlRestaurarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlRestaurarMouseExited
        jlRestaurar.setText("");
    }//GEN-LAST:event_jlRestaurarMouseExited

    private void jlRestauracionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlRestauracionMouseClicked
        frmRestaurarDatos restaurar = new frmRestaurarDatos();
        dpnEscritorio.add(restaurar);
        try {
            restaurar.setMaximum(true);

        } catch (PropertyVetoException ex) {
            LOGGER.getLogger(frmPrincipal.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        restaurar.show();
    }//GEN-LAST:event_jlRestauracionMouseClicked

    private void jlRestauracionMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlRestauracionMouseExited
        jlRestauracion.setText("");
    }//GEN-LAST:event_jlRestauracionMouseExited

    private void jlRestauracionMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlRestauracionMouseEntered
        jlRestauracion.setText("Restauración");
    }//GEN-LAST:event_jlRestauracionMouseEntered

    private void mnuMovimientosDeudasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuMovimientosDeudasActionPerformed
        if (ScreenSplash.debuger) {
            System.out.println("Entrando al formulario de Deuda");
        }
        if (deudas == null) {
            deudas = new frmDeudas();
            dpnEscritorio.add(deudas);
            deudasR();
        } else {
            deudasR();
        }
        if (ScreenSplash.debuger)
            System.out.println("Completado");
    }//GEN-LAST:event_mnuMovimientosDeudasActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        ((DesktopConFondo) dpnEscritorio).setImagen("/images/Fondo.jpg");
    }//GEN-LAST:event_formWindowOpened
    private void jlGraficaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlGraficaMouseClicked
        if (miGraficos == null) {
            miGraficos = new frmGraficos();
            dpnEscritorio.add(miGraficos);
        }
        try {
            miGraficos.setMaximum(false);
            miGraficos.setMaximum(true);
        } catch (PropertyVetoException ex) {
            LOGGER.getLogger(frmPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        miGraficos.setVisible(true);
    }//GEN-LAST:event_jlGraficaMouseClicked

    private void jlGraficaMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlGraficaMouseExited
        jlGrafica.setText("");
    }//GEN-LAST:event_jlGraficaMouseExited

    private void jlGraficaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlGraficaMouseEntered
        jlGrafica.setText("Graficos");
    }//GEN-LAST:event_jlGraficaMouseEntered

    private void jmClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmClientesActionPerformed
        mnuArchivosClienteActionPerformed(evt);
    }//GEN-LAST:event_jmClientesActionPerformed

    private void jmProductosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmProductosActionPerformed
        mnuArchivosProductosActionPerformed(evt);
    }//GEN-LAST:event_jmProductosActionPerformed

    private void jmUsuariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmUsuariosActionPerformed
        mnuArchivosUsuarioActionPerformed(evt);
    }//GEN-LAST:event_jmUsuariosActionPerformed

    private void jmCambioClaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmCambioClaveActionPerformed
        mnuArchivosCambioClaveActionPerformed(evt);
    }//GEN-LAST:event_jmCambioClaveActionPerformed

    private void jmCambioUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmCambioUsuarioActionPerformed
        mnuArchivosCambioUsuarioActionPerformed(evt);
    }//GEN-LAST:event_jmCambioUsuarioActionPerformed

    private void jmSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmSalirActionPerformed
        mnuArchivosSalirActionPerformed(evt);
    }//GEN-LAST:event_jmSalirActionPerformed

    private void jmNuevaFacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmNuevaFacturaActionPerformed
        mnuMovimientosNuevaFacturaActionPerformed(evt);
    }//GEN-LAST:event_jmNuevaFacturaActionPerformed

    private void jmReporteFacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmReporteFacturaActionPerformed
        mnuMovimientosReporteFacturaActionPerformed(evt);
    }//GEN-LAST:event_jmReporteFacturaActionPerformed

    private void jmInventarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmInventarioActionPerformed
        mnuMovimientosInventarioActionPerformed(evt);
    }//GEN-LAST:event_jmInventarioActionPerformed

    private void jmAbrirTurnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmAbrirTurnoActionPerformed
        mnuMovimientosAbrirTurnoActionPerformed(evt);
    }//GEN-LAST:event_jmAbrirTurnoActionPerformed

    private void jmCerrarTurnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmCerrarTurnoActionPerformed
        mnuMovimientosCerrarTurnoActionPerformed(evt);
    }//GEN-LAST:event_jmCerrarTurnoActionPerformed

    private void jmDeudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmDeudaActionPerformed
        mnuMovimientosDeudasActionPerformed(evt);
    }//GEN-LAST:event_jmDeudaActionPerformed

    private void jlMostrarPanelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlMostrarPanelMouseClicked
        if (jspIzquierdo.isVisible()) {
            jspIzquierdo.setVisible(false);
            pEstatus.setVisible(false);
        } else {
            jspIzquierdo.setVisible(true);
            pEstatus.setVisible(true);
        }
        pack();
    }//GEN-LAST:event_jlMostrarPanelMouseClicked

    private void mnuArchivosConfiguracionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuArchivosConfiguracionesActionPerformed
        frmConfiguraciones c = new frmConfiguraciones(this, true);
        c.setLocationRelativeTo(this);
        c.setVisible(true);
    }//GEN-LAST:event_mnuArchivosConfiguracionesActionPerformed
    private void imprimirReporte(Date fecha) {
        try {
            JasperReport masterReporte
                    = (JasperReport) JRLoader.loadObjectFromFile(
                            "Reportes/repSistemaDeBebida.jasper"
                    );

            Map<String, Object> parametros = new HashMap<>();
            parametros.put("fecha", fecha);

            JasperPrint jp = JasperFillManager.fillReport(masterReporte, parametros, Conexion.getCnn());

            JasperViewer miView = new JasperViewer(jp, false);
            miView.setTitle("Reporte de movimiento de inventario...");
            miView.setLocationRelativeTo(this);
            miView.setVisible(true);

        } catch (JRException ex) {
            LOGGER.getLogger(frmPrincipal.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Funciones que estan en el panel de la Ventana Principal
    private void estado() {
        //Trabajando con Imagen
        Icon icon;
        Image i = Datos.getImagen("Logo");
        ImageIcon imagen = null;
        if (i != null) {
            imagen = new ImageIcon(i);
            icon = new ImageIcon(imagen.getImage().getScaledInstance(180, 120,
                    Image.SCALE_DEFAULT));
            imagen.getImage().flush();
            jlImagen.setIcon(icon);
            jlImagen.validate();
        }
//        if (imagen.getIconHeight() == -1) {
//            imagen = new ImageIcon(getClass().getResource("/images/Sin_imagen 64 x 64.png"));
//            icon = new ImageIcon(imagen.getImage().getScaledInstance(180, 120,
//                    Image.SCALE_DEFAULT));
//            imagen.getImage().flush();
//            jlImagen.setIcon(icon);
//            jlImagen.validate();
//        } else {
//            
//        }//Terminado Aqui
        cajeros();
        mensaje();
    }

    private void cajeros() {
        //Saber cuales cajeros estan Activo....
        String titulos[] = {"Cajero Activo"};
        Object[] registro = new Object[1];
        DefaultTableModel miTabla = new DefaultTableModel(null, titulos);
        ResultSet rs = Datos.getCajerosActivos();
        try {
            while (rs.next()) {
                registro[0] = rs.getString("idUsuario").trim() + ": "
                        + Utilidades.formatDate(rs.getDate("Fecha"), "dd-MM-yyyy").trim()
                        + ", "
                        + rs.getString("Hora").substring(0, 5).trim();
                miTabla.addRow(registro);
            }
            jtCajero.setModel(miTabla);

        } catch (SQLException ex) {
            LOGGER.getLogger(frmPrincipal.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void mensaje() {
        ResultSet rs = Datos.getMensajes();
        try {
            rs.next();
            txtMensaje.setText(rs.getString("mensaje"));
            txtNombreEmpresa.setText(rs.getString("nombreEmpresa"));
            txtDireccionEmpresa.setText(rs.getString("direccionEmpresa"));
            txtTelefonosEmpresa.setText(rs.getString("telefonoEmpresa"));
        } catch (SQLException ex) {
            LOGGER.getLogger(frmPrincipal.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }///--------------Hasta Aqui.......

    //Movimientos
    private void bebidaR() {
        try {
            b.setMaximum(false);
            b.setMaximum(true);
        } catch (PropertyVetoException ex) {
            LOGGER.getLogger(frmPrincipal.class.getName()).log(
                    Level.SEVERE, null, ex);
        }
        b.setTurno(Datos.idTurnoActivo(idUsuario));
        b.setVisible(true);
    }

    private void deudasR() {
        try {
            deudas.setMaximum(false);
            deudas.setMaximum(true);
        } catch (PropertyVetoException ex) {
            LOGGER.getLogger(frmPrincipal.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        
        deudas.setVisible(true);
    }

    private void cerrarFormularios() {
//Archivos
        if (cliente != null) {
            dpnEscritorio.getDesktopManager().closeFrame(cliente);
            cliente = null;
        }
        if (p != null) {
            dpnEscritorio.getDesktopManager().closeFrame(p);
            p = null;
        }
        if (u != null) {
            dpnEscritorio.getDesktopManager().closeFrame(u);
            u = null;
        }
        if (c != null) {
            c = null;
        }
//Movimientos
        if (b != null) {
            dpnEscritorio.getDesktopManager().closeFrame(b);
            b = null;
        }
        if (r != null) {
            dpnEscritorio.getDesktopManager().closeFrame(r);
            r = null;
        }
        if (deudas != null) {
            dpnEscritorio.getDesktopManager().closeFrame(deudas);
            deudas = null;
        }

//Panel
        if (m != null) {
            dpnEscritorio.getDesktopManager().closeFrame(m);
            m = null;
        }
        if (miGraficos != null) {
            dpnEscritorio.getDesktopManager().closeFrame(miGraficos);
            miGraficos = null;
        }
        System.gc();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu Archivos;
    private javax.swing.JMenu Movimientos;
    private javax.swing.JLabel btnGuardarMensaje;
    public static javax.swing.JDesktopPane dpnEscritorio;
    private javax.swing.Box.Filler filler1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    public static javax.swing.JLabel jLabelImpresion;
    private javax.swing.JPanel jPanel1;
    public static javax.swing.JPanel jPanelImpresion;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator10;
    private javax.swing.JPopupMenu.Separator jSeparator11;
    private javax.swing.JPopupMenu.Separator jSeparator12;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JPopupMenu.Separator jSeparator5;
    private javax.swing.JPopupMenu.Separator jSeparator6;
    private javax.swing.JPopupMenu.Separator jSeparator7;
    private javax.swing.JPopupMenu.Separator jSeparator8;
    private javax.swing.JPopupMenu.Separator jSeparator9;
    private javax.swing.JLabel jlGetIP;
    private javax.swing.JLabel jlGrafica;
    private javax.swing.JLabel jlImagen;
    private javax.swing.JLabel jlMostrarPanel;
    private javax.swing.JLabel jlMovimientoES;
    private javax.swing.JLabel jlRespaldar;
    private javax.swing.JLabel jlRestauracion;
    private javax.swing.JLabel jlRestaurar;
    private javax.swing.JLabel jlUsuarioActual;
    private javax.swing.JMenuItem jmAbrirTurno;
    private javax.swing.JMenuItem jmCambioClave;
    private javax.swing.JMenuItem jmCambioUsuario;
    private javax.swing.JMenuItem jmCerrarTurno;
    private javax.swing.JMenuItem jmClientes;
    private javax.swing.JMenuItem jmDeuda;
    private javax.swing.JMenuItem jmInventario;
    private javax.swing.JMenuItem jmNuevaFactura;
    private javax.swing.JMenuItem jmProductos;
    private javax.swing.JMenuItem jmReporteFactura;
    private javax.swing.JMenuItem jmSalir;
    private javax.swing.JMenuItem jmUsuarios;
    private javax.swing.JMenuBar jmbMenus;
    private javax.swing.JPanel jpInfo;
    public static javax.swing.JProgressBar jprImpresion;
    private javax.swing.JScrollPane jspDerecho;
    private javax.swing.JScrollPane jspIzquierdo;
    private javax.swing.JTable jtCajero;
    private javax.swing.JMenu mnuArchivos;
    private javax.swing.JMenuItem mnuArchivosCambioClave;
    private javax.swing.JMenuItem mnuArchivosCambioUsuario;
    public static javax.swing.JMenuItem mnuArchivosCliente;
    private javax.swing.JMenuItem mnuArchivosConfiguraciones;
    private javax.swing.JMenuItem mnuArchivosProductos;
    private javax.swing.JMenuItem mnuArchivosSalir;
    private javax.swing.JMenuItem mnuArchivosUsuario;
    private javax.swing.JMenu mnuAyuda;
    private javax.swing.JMenuItem mnuAyudaAcercaDe;
    private javax.swing.JMenuItem mnuAyudaAyuda;
    private javax.swing.JMenu mnuMovimientos;
    private javax.swing.JMenuItem mnuMovimientosAbrirTurno;
    private javax.swing.JMenuItem mnuMovimientosCerrarTurno;
    private javax.swing.JMenuItem mnuMovimientosDeudas;
    private javax.swing.JMenuItem mnuMovimientosInventario;
    public static javax.swing.JMenuItem mnuMovimientosNuevaFactura;
    private javax.swing.JMenuItem mnuMovimientosReporteFactura;
    private javax.swing.JPanel pEstatus;
    private javax.swing.JTextField txtDireccionEmpresa;
    private javax.swing.JTextPane txtMensaje;
    private javax.swing.JTextField txtNombreEmpresa;
    private javax.swing.JTextField txtTelefonosEmpresa;
    // End of variables declaration//GEN-END:variables
}
