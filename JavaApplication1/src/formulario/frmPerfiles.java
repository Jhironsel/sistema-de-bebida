package formulario;

import clases.Categoria;
import clases.Datos;
import clases.Perfil;
import clases.ScreenSplash;
import static clases.Utilidades.LOGGER;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import javax.swing.JOptionPane;

public class frmPerfiles extends javax.swing.JDialog {

    private boolean crear;
    private String descripcion, rol;
    private int idAcesso;

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public frmPerfiles(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        if (ScreenSplash.debuger) {
            System.out.println("Clase Perfiles");
        }
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        cmbRol = new javax.swing.JComboBox<>();
        jPanel1 = new javax.swing.JPanel();
        cbArchivos = new javax.swing.JCheckBox();
        cbArchivosClientes = new javax.swing.JCheckBox();
        cbArchivosProductos = new javax.swing.JCheckBox();
        cbArchivosUsuarios = new javax.swing.JCheckBox();
        cbArchivosCambioUsuario = new javax.swing.JCheckBox();
        cbArchivosCambioClave = new javax.swing.JCheckBox();
        cbArchivosSalir = new javax.swing.JCheckBox();
        jPanel3 = new javax.swing.JPanel();
        cbMovimientos = new javax.swing.JCheckBox();
        cbMovimientosNuevaFactura = new javax.swing.JCheckBox();
        cbMovimientosReporteFactura = new javax.swing.JCheckBox();
        cbMovimientosInventario = new javax.swing.JCheckBox();
        cbMovimientosCerrarTurno = new javax.swing.JCheckBox();
        cbMovimientosAbrirTurno = new javax.swing.JCheckBox();
        cbMovimientosDeuda = new javax.swing.JCheckBox();
        btnCrear = new javax.swing.JButton();
        btnBorrar = new javax.swing.JButton();
        btnEditar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        lMensaje = new javax.swing.JLabel();
        btnGuardar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        lNombrePerfil = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Crear de Perfil de Usuario...");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel1.setText("Perfiles:");

        cmbRol.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        cmbRol.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                cmbRolPopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Archivos"));

        cbArchivos.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        cbArchivos.setText("Activar modulos");
        cbArchivos.setDoubleBuffered(true);
        cbArchivos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cbArchivosMouseClicked(evt);
            }
        });

        cbArchivosClientes.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        cbArchivosClientes.setText("Clientes");
        cbArchivosClientes.setDoubleBuffered(true);

        cbArchivosProductos.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        cbArchivosProductos.setText("Productos");
        cbArchivosProductos.setDoubleBuffered(true);

        cbArchivosUsuarios.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        cbArchivosUsuarios.setText("Usuarios");
        cbArchivosUsuarios.setDoubleBuffered(true);

        cbArchivosCambioUsuario.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        cbArchivosCambioUsuario.setText("Cambio de Usuario");
        cbArchivosCambioUsuario.setDoubleBuffered(true);

        cbArchivosCambioClave.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        cbArchivosCambioClave.setText("Cambio de Clave");
        cbArchivosCambioClave.setDoubleBuffered(true);

        cbArchivosSalir.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        cbArchivosSalir.setText("Salir");
        cbArchivosSalir.setDoubleBuffered(true);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbArchivosProductos)
                            .addComponent(cbArchivosClientes)
                            .addComponent(cbArchivosUsuarios)
                            .addComponent(cbArchivosSalir)
                            .addComponent(cbArchivosCambioClave)
                            .addComponent(cbArchivosCambioUsuario)))
                    .addComponent(cbArchivos))
                .addGap(0, 0, 0))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(cbArchivos)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbArchivosClientes)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbArchivosProductos)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbArchivosUsuarios)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbArchivosCambioClave)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbArchivosCambioUsuario)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbArchivosSalir)
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Movimientos"));

        cbMovimientos.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        cbMovimientos.setText("Activar modulos");
        cbMovimientos.setDoubleBuffered(true);
        cbMovimientos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cbMovimientosMouseClicked(evt);
            }
        });

        cbMovimientosNuevaFactura.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        cbMovimientosNuevaFactura.setText("Nueva Factura");
        cbMovimientosNuevaFactura.setDoubleBuffered(true);

        cbMovimientosReporteFactura.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        cbMovimientosReporteFactura.setText("Reporte de Facturas");
        cbMovimientosReporteFactura.setDoubleBuffered(true);

        cbMovimientosInventario.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        cbMovimientosInventario.setText("Inventario");
        cbMovimientosInventario.setDoubleBuffered(true);

        cbMovimientosCerrarTurno.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        cbMovimientosCerrarTurno.setText("Cerrar turno");
        cbMovimientosCerrarTurno.setDoubleBuffered(true);

        cbMovimientosAbrirTurno.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        cbMovimientosAbrirTurno.setText("Abrir turno");
        cbMovimientosAbrirTurno.setDoubleBuffered(true);

        cbMovimientosDeuda.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        cbMovimientosDeuda.setText("Deudas");
        cbMovimientosDeuda.setDoubleBuffered(true);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbMovimientosReporteFactura)
                            .addComponent(cbMovimientosNuevaFactura)
                            .addComponent(cbMovimientosInventario)
                            .addComponent(cbMovimientosAbrirTurno)
                            .addComponent(cbMovimientosCerrarTurno)
                            .addComponent(cbMovimientosDeuda)))
                    .addComponent(cbMovimientos))
                .addGap(0, 0, 0))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(cbMovimientos)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbMovimientosNuevaFactura)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbMovimientosReporteFactura)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbMovimientosInventario)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbMovimientosAbrirTurno)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbMovimientosCerrarTurno)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(cbMovimientosDeuda)
                .addContainerGap())
        );

        btnCrear.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCrear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Agregar 32 x 32.png"))); // NOI18N
        btnCrear.setText("Crear");
        btnCrear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCrearActionPerformed(evt);
            }
        });

        btnBorrar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Borrar 32 x 32.png"))); // NOI18N
        btnBorrar.setText("Borrar");
        btnBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBorrarActionPerformed(evt);
            }
        });

        btnEditar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Editar Documento 32 x 32.png"))); // NOI18N
        btnEditar.setText("Editar");
        btnEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditarActionPerformed(evt);
            }
        });

        btnGuardar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Guardar 32 x 32.png"))); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.setEnabled(false);
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        btnCancelar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Cancelar 32 x 32.png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lMensaje, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lNombrePerfil, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancelar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnGuardar))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnGuardar)
                    .addComponent(btnCancelar)))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(lMensaje, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(lNombrePerfil, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(3, 3, 3)
                        .addComponent(cmbRol, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnCrear)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEditar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnBorrar)))
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCrear)
                    .addComponent(btnBorrar)
                    .addComponent(btnEditar))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel1)
                    .addComponent(cmbRol, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jPanel1, jPanel3});

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void cmbRolPopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_cmbRolPopupMenuWillBecomeInvisible
//        llenarCheck();
    }//GEN-LAST:event_cmbRolPopupMenuWillBecomeInvisible
    private void cbArchivosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cbArchivosMouseClicked
        if (!cbArchivos.isEnabled()) {
            return;
        }
        if (cbArchivos.isSelected()) {
            cbArchivos.setText("Inactivar modulos");
            cbArchivos.repaint();
            check(2, true);
            check(6, true);
        } else {
            cbArchivos.setText("Activar modulos");
            cbArchivos.repaint();
            check(2, false);
            deseleccionarCheck(2);
        }
    }//GEN-LAST:event_cbArchivosMouseClicked
    private void cbMovimientosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cbMovimientosMouseClicked
        if (!cbMovimientos.isEnabled()) {
            return;
        }
        if (cbMovimientos.isSelected()) {
            cbMovimientos.setText("Inactivar modulos");
            check(4, true);
        } else {
            cbMovimientos.setText("Activar modulos");
            check(4, false);
            deseleccionarCheck(4);
        }
    }//GEN-LAST:event_cbMovimientosMouseClicked
    private void btnCrearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCrearActionPerformed
        crear = true;
        descripcion = JOptionPane.showInputDialog(this,
                "Inserte el nombre del perfil: ");
        if (descripcion == null || descripcion.trim().isEmpty()) {
            return;
        }

        if (descripcion.equalsIgnoreCase("Administrador")) {
            JOptionPane.showMessageDialog(this, "Nombre de Perfil invalido");
            return;
        }

        JOptionPane.showMessageDialog(this,
                "Definir los permisos para el perfil: ( " + descripcion + " )");
        deseleccionarCheck(5);
        check(5, false);
        check(1, true);
        check(3, true);

        lMensaje.setText("Creando el Perfil: ");
        lNombrePerfil.setText("     " + descripcion);

        btnCrear.setEnabled(false);
        btnEditar.setEnabled(false);
        btnBorrar.setEnabled(false);
        cmbRol.setEnabled(false);

        btnGuardar.setEnabled(true);
    }//GEN-LAST:event_btnCrearActionPerformed
    private void btnBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBorrarActionPerformed
        if (((Categoria) cmbRol.getSelectedItem()).getDescripcion().
                equalsIgnoreCase("Administrador")) {
            JOptionPane.showMessageDialog(null,
                    "Perfil no puede ser eliminado");
            return;
        }

        int resp = JOptionPane.showConfirmDialog(this,
                "Deseas eliminar Perfil llamado: "
                + ((Categoria) cmbRol.getSelectedItem()).getDescripcion()
                + " \nDesea continuar?\n\nRECUERDE QUE SI EXISTE UN USUARIO CON ESTE PERFIL DEBE ELIMINAR EL USUARIO PRIMERO",
                "Eliminar perfil", JOptionPane.YES_NO_OPTION);
        if (resp == 1) {
            return;
        } else {
//            JOptionPane.showMessageDialog(this, Datos.borrarPerfil(((Categoria) cmbRol.getSelectedItem()).getIdUsuario()));
        }
        this.setVisible(false);
    }//GEN-LAST:event_btnBorrarActionPerformed
    private void btnEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditarActionPerformed
        if (cmbRol.getSelectedItem().toString().equals("Administrador")) {
            JOptionPane.showMessageDialog(null, "Perfil no puede ser editado");
            return;
        }
        frmDialogoCategoria miDialogo = new frmDialogoCategoria(null, true,
                descripcion, "Desea cambiar nombre del Perfil?", "Nombre Perfil:");
        miDialogo.setLocationRelativeTo(null);
        miDialogo.setVisible(true);

        if (miDialogo.getNombreCategoria() == null) {
            if (ScreenSplash.debuger) {
                System.out.println("Se Cancela la Accion...");
            }
            return;
        }

        descripcion = miDialogo.getNombreCategoria();

        crear = false;
        idAcesso = ((Categoria) cmbRol.getSelectedItem()).getIdCategoria();
        descripcion = ((Categoria) cmbRol.getSelectedItem()).getDescripcion();

        btnCrear.setEnabled(false);
        btnEditar.setEnabled(false);
        btnBorrar.setEnabled(false);
        cmbRol.setEnabled(false);

        btnGuardar.setEnabled(true);

        lMensaje.setText("Editando el Perfil: ");
        lNombrePerfil.setText("     " + descripcion);

        check(1, true);
        check(3, true);
        if (cbArchivos.isSelected()) {
            check(2, true);
        }
        if (cbMovimientos.isSelected()) {
            check(4, true);
        }
    }//GEN-LAST:event_btnEditarActionPerformed
    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        Perfil miPerfil = new Perfil(-1,
                descripcion,
                cbArchivos.isSelected(),
                cbArchivosClientes.isSelected(),
                cbArchivosProductos.isSelected(),
                cbArchivosUsuarios.isSelected(),
                cbArchivosCambioClave.isSelected(),
                cbArchivosCambioUsuario.isSelected(),
                cbArchivosSalir.isSelected(),
                cbMovimientos.isSelected(),
                cbMovimientosNuevaFactura.isSelected(),
                cbMovimientosReporteFactura.isSelected(),
                cbMovimientosInventario.isSelected(),
                cbMovimientosAbrirTurno.isSelected(),
                cbMovimientosCerrarTurno.isSelected(),
                cbMovimientosDeuda.isSelected());

        if (crear) {
//            Datos.agregarPerfil(descripcion);
//            int idPerfil = Datos.getNumPerfil();
            int idPerfil = 0;
            idAcesso = Datos.getMaxIdAcceso();

            String msj = Datos.agregarOrModificarAcesso(idAcesso, idPerfil, miPerfil);

            JOptionPane.showMessageDialog(this, msj);

            if (msj.equals("Error al Insertar Acesso...")) {
                //Datos.borrarT_Perfil("" + idPerfil);
            }
            crear = false;
        } else {
            int num = Datos.getIdAcceso(idAcesso);

//            JOptionPane.showMessageDialog(this,
//                    Datos.agregarOrModificarAcesso(num, idAcesso, miPerfil) + "\n"
//                    + Datos.modificarT_Perfil(idAcesso, descripcion));
        }

        llenarCombo();
//        llenarCheck();

        btnCrear.setEnabled(true);
        btnEditar.setEnabled(true);
        btnBorrar.setEnabled(true);
        cmbRol.setEnabled(true);

        btnGuardar.setEnabled(false);

        lMensaje.setText("");
        lNombrePerfil.setText("");
    }//GEN-LAST:event_btnGuardarActionPerformed
    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        cmbRol.setSelectedIndex(0);
        check(5, false);

        btnCrear.setEnabled(true);
        btnEditar.setEnabled(true);
        btnBorrar.setEnabled(true);
        cmbRol.setEnabled(true);

        btnGuardar.setEnabled(false);

        lMensaje.setText("");
        lNombrePerfil.setText("");

        setVisible(false);
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        llenarCombo();
        for (int i = 1; i < cmbRol.getItemCount(); i++) {
            if (cmbRol.getItemAt(i).equalsIgnoreCase(getRol())) {
                cmbRol.setSelectedIndex(i);
                break;
            }
        }
//        llenarCheck();
    }//GEN-LAST:event_formWindowOpened

    private void llenarCombo() {
        ResultSet rs = Datos.getRoles();
        cmbRol.removeAllItems();
        cmbRol.addItem("Seleccionar un rol");
        try {
            while (rs.next()) {
                cmbRol.addItem(rs.getString(1).trim());
            }
        } catch (SQLException ex) {
            LOGGER.getLogger(frmUsuariosAgregar.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//    private void llenarCheck() {
//        ResultSet rsCheck = Datos.getConsulta("select * from Tabla_ACCESO2 where idPerfil = "
//                + ((Opcion) cmbPerfil.getSelectedItem()).getValor() + "");
//        try {
//            rsCheck.next();
//            cbArchivos.setSelected(permiso(rsCheck.getString("ARCHIVOS")));
//            cbArchivosClientes.setSelected(permiso(rsCheck.getString("ARCHIVOSCLIENTES")));
//            cbArchivosProductos.setSelected(permiso(rsCheck.getString("ARCHIVOSPRODUCTOS")));
//            cbArchivosUsuarios.setSelected(permiso(rsCheck.getString("ARCHIVOSUSUARIOS")));
//            cbArchivosCambioClave.setSelected(permiso(rsCheck.getString("ARCHIVOSCAMBIOCLAVE")));
//            cbArchivosCambioUsuario.setSelected(permiso(rsCheck.getString("ARCHIVOSCAMBIOUSUARIO")));
//            cbArchivosSalir.setSelected(permiso(rsCheck.getString("ARCHIVOSSALIR")));
//
//            cbMovimientos.setSelected(permiso(rsCheck.getString("MOVIMIENTOS")));
//            cbMovimientosNuevaFactura.setSelected(permiso(rsCheck.getString("MOVIMIENTOSNUEVAFACTURA")));
//            cbMovimientosReporteFactura.setSelected(permiso(rsCheck.getString("MOVIMIENTOSREPORTEFACTURA")));
//            cbMovimientosInventario.setSelected(permiso(rsCheck.getString("MOVIMIENTOSINVENTARIO")));
//            cbMovimientosAbrirTurno.setSelected(permiso(rsCheck.getString("MOVIMIENTOSABRILTURNO")));
//            cbMovimientosCerrarTurno.setSelected(permiso(rsCheck.getString("MOVIMIENTOSCERRARTURNO")));
//            cbMovimientosDeuda.setSelected(permiso(rsCheck.getString("MOVIMIENTOSDEUDA")));
//        } catch (SQLException ex) {
//            LOGGER.getLogger(frmPerfiles.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        check(5, false);
//    }
    private void check(int campo, boolean valor) {
        if (campo == 1 || campo == 5) {
            cbArchivos.setEnabled(valor);
        }
        if (campo == 2 || campo == 5) {
            cbArchivosClientes.setEnabled(valor);
            cbArchivosProductos.setEnabled(valor);
            cbArchivosUsuarios.setEnabled(valor);
            cbArchivosCambioClave.setEnabled(valor);
            cbArchivosCambioUsuario.setEnabled(valor);
            cbArchivosSalir.setEnabled(valor);
        }
        if (campo == 3 || campo == 5) {
            cbMovimientos.setEnabled(valor);
        }
        if (campo == 4 || campo == 5) {
            cbMovimientosNuevaFactura.setEnabled(valor);
            cbMovimientosReporteFactura.setEnabled(valor);
            cbMovimientosInventario.setEnabled(valor);
            cbMovimientosAbrirTurno.setEnabled(valor);
            cbMovimientosCerrarTurno.setEnabled(valor);
            cbMovimientosDeuda.setEnabled(valor);
        }
    }

    private void deseleccionarCheck(int campo) {
        if (campo == 1 || campo == 5) {
            cbArchivos.setSelected(false);
        }
        if (campo == 2 || campo == 5) {
            cbArchivosClientes.setSelected(false);
            cbArchivosProductos.setSelected(false);
            cbArchivosUsuarios.setSelected(false);
            cbArchivosCambioClave.setSelected(false);
            cbArchivosCambioUsuario.setSelected(false);
            cbArchivosSalir.setSelected(false);
        }
        if (campo == 3 || campo == 5) {
            cbMovimientos.setSelected(false);
        }
        if (campo == 4 || campo == 5) {
            cbMovimientosNuevaFactura.setSelected(false);
            cbMovimientosReporteFactura.setSelected(false);
            cbMovimientosInventario.setSelected(false);
            cbMovimientosAbrirTurno.setSelected(false);
            cbMovimientosCerrarTurno.setSelected(false);
            cbMovimientosDeuda.setSelected(false);
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBorrar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCrear;
    private javax.swing.JButton btnEditar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JCheckBox cbArchivos;
    private javax.swing.JCheckBox cbArchivosCambioClave;
    private javax.swing.JCheckBox cbArchivosCambioUsuario;
    private javax.swing.JCheckBox cbArchivosClientes;
    private javax.swing.JCheckBox cbArchivosProductos;
    private javax.swing.JCheckBox cbArchivosSalir;
    private javax.swing.JCheckBox cbArchivosUsuarios;
    private javax.swing.JCheckBox cbMovimientos;
    private javax.swing.JCheckBox cbMovimientosAbrirTurno;
    private javax.swing.JCheckBox cbMovimientosCerrarTurno;
    private javax.swing.JCheckBox cbMovimientosDeuda;
    private javax.swing.JCheckBox cbMovimientosInventario;
    private javax.swing.JCheckBox cbMovimientosNuevaFactura;
    private javax.swing.JCheckBox cbMovimientosReporteFactura;
    private javax.swing.JComboBox<String> cmbRol;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JLabel lMensaje;
    private javax.swing.JLabel lNombrePerfil;
    // End of variables declaration//GEN-END:variables
}
