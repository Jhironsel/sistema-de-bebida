package formulario;

import clases.Categoria;
import clases.Datos;
import clases.ScreenSplash;
import clases.Utilidades;
import static clases.Utilidades.LOGGER;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class frmCategorias extends javax.swing.JDialog {

    private boolean nuevo = false;
    private int returnVal = JFileChooser.CANCEL_OPTION;
    private String nombreCategoria = "", source;
    private int idCategoria;

    public frmCategorias(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        if (ScreenSplash.debuger) {
            System.out.println("Clase Categorias");
        }
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jlImagen = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnNuevo = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        btnBorrar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        cbCategoria = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(339, 218));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jlImagen.setFont(new java.awt.Font("Ubuntu", 1, 14)); // NOI18N
        jlImagen.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jlImagen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Sin_imagen 64 x 64.png"))); // NOI18N
        jlImagen.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Seleccione Imagen", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Ubuntu", 0, 14))); // NOI18N
        jlImagen.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jlImagen.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        jPanel2.setMaximumSize(new java.awt.Dimension(504, 102));
        jPanel2.setMinimumSize(new java.awt.Dimension(504, 102));
        jPanel2.setPreferredSize(new java.awt.Dimension(504, 102));
        jPanel2.setLayout(new java.awt.GridLayout(1, 3, 3, 3));

        btnNuevo.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnNuevo.setForeground(new java.awt.Color(0, 0, 0));
        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Documento nuevo 32 x 32.png"))); // NOI18N
        btnNuevo.setMnemonic('n');
        btnNuevo.setText("Nuevo");
        btnNuevo.setToolTipText("Crear un nuevo Registro");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        jPanel2.add(btnNuevo);

        btnModificar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnModificar.setForeground(new java.awt.Color(0, 0, 0));
        btnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Editar Documento 32 x 32.png"))); // NOI18N
        btnModificar.setMnemonic('e');
        btnModificar.setText("Editar");
        btnModificar.setToolTipText("Modificar Registro Actual");
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });
        jPanel2.add(btnModificar);

        btnBorrar.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        btnBorrar.setForeground(new java.awt.Color(0, 0, 0));
        btnBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Borrar 32 x 32.png"))); // NOI18N
        btnBorrar.setMnemonic('b');
        btnBorrar.setText("Borrar");
        btnBorrar.setToolTipText("Borrar Registro Actual");
        btnBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBorrarActionPerformed(evt);
            }
        });
        jPanel2.add(btnBorrar);

        jLabel1.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setText("Nombre de la Categoria");

        cbCategoria.setForeground(new java.awt.Color(0, 0, 0));
        cbCategoria.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbCategoriaItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(cbCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jlImagen, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jLabel1)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cbCategoria, jLabel1});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbCategoria, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jlImagen, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        //Activamos el Flag de registro Nuevo
        nuevo = true;
        //Label de la imagen Habilitado y su texto
        jlImagen.setIcon(null);

        //Buscamos nombre de la categoria
        nombreCategoria = JOptionPane.showInputDialog(this,
                "Inserte nombre de la Categoria:", "Crear nombre de Categoria",
                JOptionPane.QUESTION_MESSAGE);

        nombreCategoria = nombreCategoria.toUpperCase();

        if (nombreCategoria == null || nombreCategoria.trim().isEmpty()) {
            //Si no se digita ningun valor para la variable
            return;
        }

        if (Datos.existeCategoria(nombreCategoria)) {
            JOptionPane.showMessageDialog(this,
                    "Este nombre de Categoria ya existe en el sitema");
            return;//Nos devolvemos si existe esta categoria registrada
        }

        JOptionPane.showMessageDialog(this,
                "Siguiente paso es para elejir una imagen para la Categoria "
                + nombreCategoria);

        if (buscarImagen()) {
            guardar();
        }
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        if (cbCategoria.getItemCount() == 0) {
            JOptionPane.showMessageDialog(this, "Debe de crear una categoria...!!!");
            return;
        }

        nuevo = false;
        idCategoria = ((Categoria) cbCategoria.getSelectedItem()).getIdCategoria();
        nombreCategoria = ((Categoria) cbCategoria.getSelectedItem()).getDescripcion();

        frmDialogoCategoria miCategoria = new frmDialogoCategoria(null, true, nombreCategoria);
        miCategoria.setLocationRelativeTo(null);
        miCategoria.setVisible(true);
        if (miCategoria.isAceptar()) {
            if (!nombreCategoria.equals(miCategoria.getNombreCategoria())) {
                if (Datos.existeCategoria(miCategoria.getNombreCategoria())) {
                    JOptionPane.showMessageDialog(this, "Este nombre de Categoria Existe");
                    return;
                }
            }
            nombreCategoria = miCategoria.getNombreCategoria().toUpperCase();
            if (buscarImagen()) {
                guardar();
            } else {
                int resp = JOptionPane.showConfirmDialog(this,
                        "Desea solo guardar el nombre?",
                        "Confirmacion de Categoria",
                        JOptionPane.YES_NO_OPTION);
                if (resp == 0) {
                    guardar();
                }
            }
        }
        miCategoria.dispose();
    }//GEN-LAST:event_btnModificarActionPerformed

    private void btnBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBorrarActionPerformed
        if (cbCategoria.getItemCount() == 0) {
            return;
        }

        if (Datos.existeCategoriaProductos(
                ((Categoria) cbCategoria.getSelectedItem()).getIdCategoria() + "")) {
            JOptionPane.showMessageDialog(this,
                    "No se permite eliminar categoria porque existe producto Asociados");
            return;
        }

        int rta = JOptionPane.showConfirmDialog(this,
                "Esta Seguro de Eliminar la Categoria {"
                + (((Categoria) cbCategoria.getSelectedItem()).getDescripcion())
                + "} del Sistema? \n\n Este proceso tratara "
                + "de eliminarlo si no ocurre es porque existen"
                + "\nproducto asociado a dicha categoria",
                "Confirmacion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        if (rta == JOptionPane.NO_OPTION) {
            return;
        }

        String msg;
        msg = Datos.borrarCategoria(((Categoria) cbCategoria.getSelectedItem()).getIdCategoria());
        JOptionPane.showMessageDialog(rootPane, msg);
        if (cbCategoria.getItemCount() != 0) {
            cbCategoria.setSelectedIndex(0);
        }
        actualizarCombo();
    }//GEN-LAST:event_btnBorrarActionPerformed

    private void cbCategoriaItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbCategoriaItemStateChanged
        if (cbCategoria.getSelectedIndex() >= 0) {
            try {
                ImageIcon imagen = new ImageIcon(((Categoria) cbCategoria.getSelectedItem()).getImage());
                //Tamaño de icono Comprobar...
                if (imagen.getIconHeight() == -1) {
                    imagen = new ImageIcon(getClass().getResource("/images/Sin_imagen 64 x 64.png"));
                }
                Icon icon = new ImageIcon(imagen.getImage().getScaledInstance(72, 72,
                        Image.SCALE_DEFAULT));
                imagen.getImage().flush();
                jlImagen.setIcon(icon);
                jlImagen.validate();
            } catch (Exception e) {
                LOGGER.getLogger(frmCategorias.class.getName()).log(Level.SEVERE, null, e);
            }
        }

    }//GEN-LAST:event_cbCategoriaItemStateChanged

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        actualizarCombo();
    }//GEN-LAST:event_formWindowOpened

    private void guardar() {
        if (nuevo) {
            if (Datos.agregarCategoria(nombreCategoria.trim(), source)) {
                JOptionPane.showConfirmDialog(this, "Error al insertar Categoria");
                return;
            }
        } else {
            if (Datos.modificarCategoria(
                    new Categoria(Integer.valueOf(idCategoria),
                            nombreCategoria.trim(),
                            source)
            )) {
                JOptionPane.showConfirmDialog(this, "Error al actualizar Categoria");
                return;
            }
        }

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            Utilidades.copyFileUsingFileChannels(source, nombreCategoria);
            ImageIcon imagen = new ImageIcon(source);
            Icon icon = new ImageIcon(imagen.getImage().getScaledInstance(72, 72,
                    Image.SCALE_DEFAULT));
            imagen.getImage().flush();
            jlImagen.setIcon(icon);
            jlImagen.validate();
        }
        returnVal = JFileChooser.CANCEL_OPTION;
        actualizarCombo();
    }

    private void actualizarCombo() {
        ResultSet rs = Datos.getCategorias();
        cbCategoria.removeAllItems();
        try {
            while (rs.next()) {
                BufferedImage img = null;

                Blob blob = rs.getBlob("image");

                byte[] data = blob.getBytes(1, (int) blob.length());

                try {
                    img = ImageIO.read(new ByteArrayInputStream(data));
                } catch (IOException ex) {
                    LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
                }

                cbCategoria.addItem(new Categoria(rs.getInt("idCategoria"),
                        rs.getString("Descripcion"),
                        img));
            }
        } catch (SQLException ex) {
            LOGGER.getLogger(frmCategorias.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private boolean buscarImagen() {
        //Creamos el objeto file para buscar la imagen
        JFileChooser file = new JFileChooser();

        //Creamos un filtro para el objeto file
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Imagenes",
                "jpg", "png", "PNG", "JPG");
        file.setFileFilter(filter);//Le ponemos el filtro

        returnVal = file.showOpenDialog(this);//Abrimos el file y recogemos respuesta

        if (returnVal == JFileChooser.CANCEL_OPTION) {
            return false;
        }

        source = file.getSelectedFile().getAbsolutePath();

        ImageIcon imagen = new ImageIcon(source);
        //Tamaño de icono
        Icon icon = new ImageIcon(imagen.getImage().getScaledInstance(72, 72,
                Image.SCALE_DEFAULT));
        imagen.getImage().flush();
        jlImagen.setIcon(icon);
        jlImagen.validate();
        return true;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBorrar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JComboBox<Categoria> cbCategoria;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel jlImagen;
    // End of variables declaration//GEN-END:variables
}
