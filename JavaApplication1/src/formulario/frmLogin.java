package formulario;

import clases.Datos;
import clases.ScreenSplash;
import static clases.Utilidades.LOGGER;
import conexiones.Conexion;
import java.awt.Color;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public final class frmLogin extends javax.swing.JFrame {

    private String idMaquina = "";
    private boolean txtUsuarioKeyPress = false;

    public frmLogin() {
        if (ScreenSplash.debuger) {
            System.out.println("Clase Login");
        }
        initComponents();
        btnParametros.setVisible(false);//Boton parametros Invisible
        this.setLocationRelativeTo(null);
        
        //No dejar esto aqui nunca...
        txtClave.setText("123uasd");
        txtUsuario.setText("Jhoandry");
        
        jpImagen.setImagen(new javax.swing.ImageIcon(getClass().getResource("/images/FondoLogin.jpg")));
    }

    @Override
    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().getImage(
                ClassLoader.getSystemResource("images/icon.png")
        );
        return retValue;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpImagen = new rojeru_san.rspanel.RSPanelImage();
        btnAceptar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnParametros = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        JLSystema = new javax.swing.JLabel();
        txtUsuario = new necesario.TextField();
        txtClave = new necesario.PassField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Login de Sistema Comercial");
        setFocusTraversalPolicyProvider(true);
        setIconImage(Toolkit.getDefaultToolkit().getImage("icon.png"));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        btnAceptar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnAceptar.setForeground(new java.awt.Color(1, 1, 1));
        btnAceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Aceptar 32 x 32.png"))); // NOI18N
        btnAceptar.setMnemonic('a');
        btnAceptar.setText("Aceptar");
        btnAceptar.setToolTipText("Proceda a autenticarse en el sistema.");
        btnAceptar.setPreferredSize(new java.awt.Dimension(123, 44));
        btnAceptar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnAceptarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnAceptarMouseExited(evt);
            }
        });
        btnAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAceptarActionPerformed(evt);
            }
        });

        btnCancelar.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnCancelar.setForeground(new java.awt.Color(1, 1, 1));
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Cancelar 32 x 32.png"))); // NOI18N
        btnCancelar.setMnemonic('c');
        btnCancelar.setText("Cancelar");
        btnCancelar.setToolTipText("Finalice aqui el programa.");
        btnCancelar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnCancelarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnCancelarMouseExited(evt);
            }
        });
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnParametros.setFont(new java.awt.Font("Ubuntu", 0, 14)); // NOI18N
        btnParametros.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Parametros 32 x 32.png"))); // NOI18N
        btnParametros.setMnemonic(';');
        btnParametros.setText("Opcion de Conexion");
        btnParametros.setToolTipText("Para uso de conexion a la base de Datos Remotamente");
        btnParametros.setOpaque(true);
        btnParametros.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnParametrosActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(1, 1, 1));
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Panel de Control 128 x 128.png"))); // NOI18N

        JLSystema.setFont(new java.awt.Font("Arial", 0, 20)); // NOI18N
        JLSystema.setForeground(new java.awt.Color(254, 254, 254));
        JLSystema.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        txtUsuario.setBorder(null);
        txtUsuario.setColumns(2);
        txtUsuario.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtUsuario.setToolTipText("Ingrese aqui el nombre de usuario");
        txtUsuario.setFont(new java.awt.Font("FreeMono", 1, 18)); // NOI18N
        txtUsuario.setPlaceholder("Ingrese Usuario");
        txtUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUsuarioActionPerformed(evt);
            }
        });
        txtUsuario.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtUsuarioKeyPressed(evt);
            }
        });

        txtClave.setColumns(2);
        txtClave.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtClave.setToolTipText("Ingrese aqui la clave del usuario");
        txtClave.setFont(new java.awt.Font("FreeMono", 1, 18)); // NOI18N
        txtClave.setPlaceholder("Ingrese Clave");
        txtClave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtClaveActionPerformed(evt);
            }
        });
        txtClave.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtClaveKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jpImagenLayout = new javax.swing.GroupLayout(jpImagen);
        jpImagen.setLayout(jpImagenLayout);
        jpImagenLayout.setHorizontalGroup(
            jpImagenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpImagenLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpImagenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(txtClave, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnParametros, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jpImagenLayout.createSequentialGroup()
                        .addComponent(btnCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnAceptar, javax.swing.GroupLayout.DEFAULT_SIZE, 129, Short.MAX_VALUE))
                    .addComponent(txtUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, 135, Short.MAX_VALUE))
            .addComponent(JLSystema, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jpImagenLayout.setVerticalGroup(
            jpImagenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpImagenLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(JLSystema, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpImagenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpImagenLayout.createSequentialGroup()
                        .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtClave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jpImagenLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnParametros, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jpImagenLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnAceptar, btnCancelar, btnParametros});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpImagen, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jpImagen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        /*Si la conexion no es nula, tratamos de cerrar la conexion a la base
        de Datos....*/
        if (Conexion.getCnn() != null) {
            try {
                Conexion.getCnn().close();//Cerramos la conexion
                Conexion.setCnn(null);//Nos aseguramos que la variable de la 
                //conexion sea nula.
            } catch (SQLException ex) {
                LOGGER.getLogger(frmLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.exit(0);//Terminamos el programa.
    }//GEN-LAST:event_btnCancelarActionPerformed
    private void btnAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAceptarActionPerformed
        if (txtUsuario.getText().isEmpty()) {
            JOptionPane.showMessageDialog(this, "Nombre del Usuario Vacio");
            txtUsuario.requestFocusInWindow();
            return;
        }

        if (txtClave.getPassword().length == 0) {
            JOptionPane.showMessageDialog(this, "Inserte una clave");
            txtClave.requestFocusInWindow();
            return;
        }

        ArrayList<String> roles = new ArrayList<String>();

        if (!txtUsuario.getText().equalsIgnoreCase("sysdba")) {
            Conexion c = new Conexion("None", txtUsuario.getText(),
                    new String(txtClave.getPassword()));

            roles = Datos.comprobandoRol(txtUsuario.getText().trim());
            try {
                c.getCnn().close();
            } catch (SQLException ex) {
                LOGGER.getLogger(frmLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
            Conexion.setCnn(null);
        }

        if (roles.isEmpty()) {
            JOptionPane.showMessageDialog(this, "El usuario no cuenta con rol en el sistema");
            return;
        }

        JComboBox<String> comboBox = new JComboBox(roles.toArray());
        
        
        if (roles.size() > 1) {
            JOptionPane.showMessageDialog(null, comboBox, "Seleccione un rol",
                    JOptionPane.INFORMATION_MESSAGE);
        }
        
        String rol = comboBox.getSelectedItem().toString();

        
        if(rol.equalsIgnoreCase("ADMINISTRADOR")){
            rol = "RDB$ADMIN";
        }

        new Conexion(rol, txtUsuario.getText(), new String(txtClave.getPassword()));

        txtClave.setText("");//Blanquear la pass

        if (ScreenSplash.debuger) {
            System.out.println("auxRol: " + roles);
        }

        if (Conexion.getCnn() == null) {
            return;
        }

        //Haciendo verificacion del sistema
        if (!Datos.existeIdMaquina(idMaquina)) {
            //Ver si la maquina esta Registrada si no esta Entra
            int num = JOptionPane.showConfirmDialog(this,
                    "Este equipo no esta Autorizado! \nDesea Registrar?",
                    "No Autorizado", JOptionPane.YES_NO_OPTION);

            if (num == JOptionPane.YES_OPTION) {
                //Implementar logistica para registrar producto en linea
                frmRegistros miRegistros = new frmRegistros(this, true);
                miRegistros.setVisible(true);
                if (miRegistros.getIdMaquina().equals("cancelado")) {
                    return;
                }

                try {
                    Datos.setLicencia(miRegistros.getFecha(),
                            miRegistros.getIdMaquina(),
                            miRegistros.getClave1(),
                            miRegistros.getClave2());
                    JOptionPane.showMessageDialog(this, "Maquina Registradas");
                } catch (SQLException ex) {
                    LOGGER.getLogger(frmLogin.class.getName()).log(Level.SEVERE, null, ex);
                }
                return;
            } else {
                return;
            }
        }

        int dia = Datos.periodoMaquina();
        if (dia < 1) {
            JOptionPane.showMessageDialog(this, "Licencia expirada...");
            return;
        }
        if (dia > 1 && dia < 10) {
            JOptionPane.showMessageDialog(this,
                    "Tiempo de version de prueba se acaba en " + dia + " dias.");
        }
        
        if (rol.equalsIgnoreCase("rdb$admin")) {
            rol = "Administrador";
        }

        frmPrincipal p = new frmPrincipal(rol);
        p.setVisible(true);
        p.setExtendedState(JFrame.MAXIMIZED_BOTH);
        dispose();
    }//GEN-LAST:event_btnAceptarActionPerformed


    private void btnAceptarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAceptarMouseEntered
        btnAceptar.setForeground(Color.BLUE);
    }//GEN-LAST:event_btnAceptarMouseEntered
    private void btnAceptarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnAceptarMouseExited
        btnAceptar.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnAceptarMouseExited
    private void btnCancelarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancelarMouseEntered
        btnCancelar.setForeground(Color.blue);
    }//GEN-LAST:event_btnCancelarMouseEntered
    private void btnCancelarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCancelarMouseExited
        btnCancelar.setForeground(Color.BLACK);
    }//GEN-LAST:event_btnCancelarMouseExited
    private void btnParametrosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnParametrosActionPerformed
        frmParametros miParametros = new frmParametros();
        miParametros.setLocationRelativeTo(null);
        miParametros.setVisible(true);
    }//GEN-LAST:event_btnParametrosActionPerformed
    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        String sistema = System.getProperty("os.name");
        JLSystema.setText("Usted Se encuentra en OS: " + sistema);
        Process p;
        if (sistema.equals("Linux")) {
            try {
                p = Runtime.getRuntime().exec("lsblk -o UUID /dev/sda1");
            } catch (IOException ex) {
                LOGGER.getLogger(frmLogin.class.getName()).log(Level.SEVERE, null, ex);
                return;
            }
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(
                    p.getInputStream()));
            try {
                stdInput.readLine();
                idMaquina = stdInput.readLine().trim();
            } catch (IOException ex) {
                LOGGER.getLogger(frmLogin.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            Scanner sc;
            try {
                p = Runtime.getRuntime().exec("wmic csproduct get uuid");
                sc = new Scanner(p.getInputStream());
                p.getOutputStream().close();
            } catch (IOException ex) {
                LOGGER.getLogger(frmLogin.class.getName()).log(Level.SEVERE, null, ex);
                return;
            }
            if (ScreenSplash.debuger) {
                System.out.println(sc.nextLine().trim());
                System.out.println(sc.nextLine().trim());
            }
            idMaquina = sc.nextLine().trim();
        }
        if (ScreenSplash.debuger) {
            System.out.println(idMaquina);
        }
        txtUsuario.setSelectionStart(0);
        txtUsuario.setSelectionEnd(txtUsuario.getText().length());
        txtUsuario.requestFocus();
    }//GEN-LAST:event_formWindowOpened

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        btnCancelarActionPerformed(null);
    }//GEN-LAST:event_formWindowClosing

    private void txtUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUsuarioActionPerformed
        txtClave.setSelectionStart(0);
        txtClave.setSelectionEnd(txtClave.getPassword().length);
        txtClave.requestFocus();
    }//GEN-LAST:event_txtUsuarioActionPerformed

    private void txtUsuarioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtUsuarioKeyPressed
        presionoTecla(evt);
    }//GEN-LAST:event_txtUsuarioKeyPressed

    private void txtClaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtClaveActionPerformed
        btnAceptarActionPerformed(null);
    }//GEN-LAST:event_txtClaveActionPerformed

    private void txtClaveKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtClaveKeyPressed
        presionoTecla(evt);
    }//GEN-LAST:event_txtClaveKeyPressed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel JLSystema;
    private javax.swing.JButton btnAceptar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnParametros;
    private javax.swing.JLabel jLabel3;
    private rojeru_san.rspanel.RSPanelImage jpImagen;
    private necesario.PassField txtClave;
    public static necesario.TextField txtUsuario;
    // End of variables declaration//GEN-END:variables

    private void presionoTecla(KeyEvent evt) {
        /*En este codigo podemos habilitar el boton avanzado que me permite hacer
        ajuste como por ejemplo la IP del servidor, el puerto que se va a usar,
         */

        /*Ahora se activa con el boton control + Arriba o Abajo*/
        if (evt.getKeyCode() == KeyEvent.VK_CONTROL) {
            txtUsuarioKeyPress = true;
        } else if (txtUsuarioKeyPress && (evt.getKeyCode() == KeyEvent.VK_UP
                || evt.getKeyCode() == KeyEvent.VK_DOWN)) {
            if (btnParametros.isVisible()) {
                btnParametros.setVisible(false);
            } else {
                btnParametros.setVisible(true);
            }
            txtUsuarioKeyPress = false;
        }//Habilitar y Deshabilitar el boton del Parametros en el formulario Login
    }
}
