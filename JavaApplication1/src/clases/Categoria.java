package clases;

import java.awt.Image;

public class Categoria {

    private final int idCategoria;
    private final String descripcion, idUsuario;
    private final Image image;
    private final String ruta;

    public Categoria(String idUsuario, String nombreApellidos) {
        this.idUsuario = idUsuario;
        this.descripcion = nombreApellidos;

        idCategoria = -1;
        image = null;
        ruta = null;
    }

    public Categoria(int idCategoria, String descripcion) {
        this.idCategoria = idCategoria;
        this.descripcion = descripcion;

        this.image = null;
        this.ruta = null;
        this.idUsuario = null;
    }

    public Categoria(int idCategoria, String descripcion, Image image) {
        if (ScreenSplash.debuger) {
            System.out.println("+Clase Categoria");
        }
        this.idCategoria = idCategoria;
        this.descripcion = descripcion;
        this.image = image;

        this.ruta = null;
        this.idUsuario = null;
    }

    public Categoria(int idCategoria, String descripcion, String ruta) {
        this.idCategoria = idCategoria;
        this.descripcion = descripcion;
        this.ruta = ruta;

        this.image = null;
        this.idUsuario = null;
    }

    public Categoria(int idCategoria, String descripcion, Image image,
            String ruta) {
        this.idCategoria = idCategoria;
        this.descripcion = descripcion;
        this.image = image;
        this.ruta = ruta;

        this.idUsuario = null;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public int getIdProducto() {
        return idCategoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getNombreApellidos() {
        return descripcion;
    }

    public Image getImage() {
        return image;
    }

    public String getRuta() {
        return ruta;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public String getIdCliente() {
        return idUsuario;
    }

    @Override
    public String toString() {
        return descripcion;
    }
}
