package clases;

import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Date;

public class EntradaProducto {
    private final int idEntradaProducto;
    private final int numero;
    private final int idProducto;
    private final String idUsuario;
    private final String numeroFactura;
    private final String concepto;
    private final BigDecimal entrada;
    private final Date fecha;
    private final Time hora;
    private final char op;
    private final BigDecimal costo;
    private final BigDecimal precio;
    private final Boolean impuesto;
    private final BigDecimal impuestoPorciento;

    public EntradaProducto(int idEntradaProducto, int idProducto, 
            String idUsuario, int numero, String numeroFactura, String concepto, 
            BigDecimal entrada, Date fecha, Time hora, char op, BigDecimal costo, 
            BigDecimal precio, Boolean impuesto, BigDecimal impuestoPorciento) {
        this.idEntradaProducto = idEntradaProducto;
        this.idProducto = idProducto;
        this.idUsuario = idUsuario;
        this.numero = numero;
        this.numeroFactura = numeroFactura;
        this.concepto = concepto;
        this.entrada = entrada;
        this.fecha = fecha;
        this.hora = hora;
        this.op = op;
        this.costo = costo;
        this.precio = precio;
        this.impuesto = impuesto;
        this.impuestoPorciento = impuestoPorciento;
    }

    public int getIdEntradaProducto() {
        return idEntradaProducto;
    }

    public int getIdProducto() {
        return idProducto;
    }
    
    public String getIdUsuario() {
        return idUsuario;
    }

    public int getNumero() {
        return numero;
    }

    public String getNumeroFactura() {
        return numeroFactura;
    }

    public String getConcepto() {
        return concepto;
    }

    public BigDecimal getEntrada() {
        return entrada;
    }

    public Date getFecha() {
        return fecha;
    }

    public Time getHora() {
        return hora;
    }

    public char getOp() {
        return op;
    }

    public BigDecimal getCosto() {
        return costo;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public Boolean getImpuesto() {
        return impuesto;
    }

    public BigDecimal getImpuestoPorciento() {
        return impuestoPorciento;
    }
}
