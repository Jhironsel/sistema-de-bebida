package clases;

import static clases.Utilidades.LOGGER;
import formulario.frmLogin;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Font;
import java.awt.HeadlessException;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;

public class Reporte {

    public static void reporteFacturas(String archivo, ResultSet rs) {
        Document documento = new Document(PageSize.A7, 10, 10, 10, 10);
        //Document documento = new Document();
        Date now = new Date(System.currentTimeMillis());
        
        //Inicialisar Variables del Tiempo...
        SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat hour = new SimpleDateFormat("HH:mm:ss");
        
        try {

            PdfWriter.getInstance(documento, new FileOutputStream(archivo.replace(" ", "_") + "_" + date.format(now) + "_" + hour.format(now) + ".pdf"));

            //Colocamos el Titulo del Reporte
            Paragraph parrafo;
            documento.open();
            //documento.add(parrafo);
            documento.add(new Paragraph("REPORTE FACTURAS", FontFactory.getFont("arial", 2, Font.PLAIN, BaseColor.BLACK)));             // color
            boolean hayRegistros = rs.next();
            //Declamos Variable de Totales
            double totCanFac;
            double totValFac;
            double totCanGen = 0;
            double totValGen = 0;
            
            PdfPCell cell;
            
            //Iniciamos el Cuerpo del Reporte
            while (hayRegistros) {
                //Imprimimos el encabezado de la Factura
                PdfPTable tabla = new PdfPTable(2);
                documento.add(new Paragraph(" ", FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));
                float[] widths = {0.19f, 0.81f};
                tabla.setWidths(widths);
                tabla.addCell(new Paragraph("Factura #: ", FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));
                tabla.addCell(new Paragraph(rs.getString("idFactura"), FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));

                tabla.addCell(new Paragraph("ID Cliente: ", FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));
                tabla.addCell(new Paragraph(rs.getString("idCliente"), FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));

                tabla.addCell(new Paragraph("Nombres: ", FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));
                tabla.addCell(new Paragraph(rs.getString("nombreFull"), FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));

                tabla.addCell(new Paragraph("Fecha: ", FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));
                tabla.addCell(new Paragraph(rs.getString("fecha"), FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));

                parrafo = new Paragraph();
                parrafo.add(tabla);
                documento.add(new Paragraph(parrafo));

                // Inicializamos totales de Factura
                totCanFac = 0;
                totValFac = 0;

                //Colocamos los Encabezados del Detalle
                tabla = new PdfPTable(6);
                float[] widths2 = {0.03f, 0.16f, 0.38f, 0.15f, 0.14f, 0.13f};

                tabla.setWidths(widths2);
                tabla.addCell(new Paragraph("#", FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));
                tabla.addCell(new Paragraph("IDProducto", FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));
                tabla.addCell(new Paragraph("Descripcion", FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));
                tabla.addCell(new Paragraph("Precio", FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));
                tabla.addCell(new Paragraph("Cantidad", FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));
                tabla.addCell(new Paragraph("Valor", FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));
                parrafo = new Paragraph();
                parrafo.add(tabla);
                documento.add(parrafo);

                int facturaActual = rs.getInt("idFactura");
                while (hayRegistros && facturaActual == rs.getInt("idFactura")) {
                    tabla = new PdfPTable(6);
                    tabla.setWidths(widths2);
                    tabla.addCell(new Paragraph(rs.getString("idLinea"), FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));
                    tabla.addCell(new Paragraph(rs.getString("idProducto"), FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));
                    tabla.addCell(new Paragraph(rs.getString("descripcion"), FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));

                    //parrafo = new Paragraph(rs.getString("precio"));
                    cell = new PdfPCell(new Paragraph(rs.getString("precio"), FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    tabla.addCell(cell);

                    cell = new PdfPCell(new Paragraph(Utilidades.priceWithDecimal(rs.getDouble("cantidad")), FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    tabla.addCell(cell);

                    parrafo = new Paragraph();
                    cell = new PdfPCell(new Paragraph(Utilidades.priceWithDecimal(rs.getDouble("valor")), FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    tabla.addCell(cell);

                    parrafo = new Paragraph();
                    parrafo.add(tabla);

                    documento.add(parrafo);

                    totCanFac += rs.getDouble("cantidad");
                    totValFac += rs.getDouble("valor");

                    hayRegistros = rs.next();
                }
                //Imprimir Totales
                tabla = new PdfPTable(6);
                tabla.setWidths(widths2);
                tabla.addCell(" ");
                tabla.addCell(" ");
                tabla.addCell(" ");
                tabla.addCell(new Paragraph("TOTAL FACT:", FontFactory.getFont("arial", 3, Font.PLAIN, BaseColor.BLACK)));

                
                cell = new PdfPCell(new Paragraph(Utilidades.priceWithDecimal(totCanFac), FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tabla.addCell(cell);
                
                cell = new PdfPCell(new Paragraph(Utilidades.priceWithDecimal(totValFac), FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                tabla.addCell(cell);
                parrafo = new Paragraph();
                parrafo.add(tabla);
                documento.add(parrafo);

                //Acumulamos totales Generales
                totCanGen += Double.parseDouble(Utilidades.priceWithDecimal(totCanFac));
                totValGen += Double.parseDouble(Utilidades.priceWithDecimal(totValFac));

                //Colocamos un Espacio en Blanco
                //parrafo = new Paragraph("     ------------------------------------------------------------Fin----------------------------------------------------------");
                parrafo = new Paragraph(" ");
                documento.add(parrafo);

            }

            //Imprimir Totales Generales
            PdfPTable tabla = new PdfPTable(6);
            float[] widths2 = {0.03f, 0.16f, 0.39f, 0.15f, 0.13f, 0.13f};
            tabla.setWidths(widths2);
            tabla.addCell(" ");
            tabla.addCell(new Paragraph("TOTAL GEN:", FontFactory.getFont("arial", 3, Font.PLAIN, BaseColor.BLACK)));

            cell = new PdfPCell(new Paragraph(Utilidades.priceWithDecimal(totCanGen), FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            tabla.addCell(cell);
            
            cell = new PdfPCell(new Paragraph(Utilidades.priceWithDecimal(totValGen), FontFactory.getFont("arial", 4, Font.PLAIN, BaseColor.BLACK)));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);            
            tabla.addCell(cell);
            
            parrafo = new Paragraph();
            parrafo.add(tabla);
            documento.add(parrafo);
            
        } catch (FileNotFoundException | DocumentException | SQLException | HeadlessException ex) {
            LOGGER.getLogger(Reporte.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            documento.close();
        }
        Process p;

        try {
            p = Runtime.getRuntime().exec("evince "+archivo.replace(" ", "_") + "_" + date.format(now) + "_" + hour.format(now) + ".pdf");
        } catch (IOException ex) {
            LOGGER.getLogger(frmLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
