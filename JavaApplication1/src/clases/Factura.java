package clases;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;

public class Factura {
    private final int idFactura;
    private final String idCliente;
    private final Date fecha;
    private final Time hora;
    private final String idUsuario;
    private final Boolean credito;
    private final int idTurno;
    private final BigDecimal efectivo;
    private final BigDecimal cambio;
    private final char estado;
    private final String NombreCliente;

    public Factura(int idFactura, String idCliente, Date fecha, Time hora, 
            String idUsuario, Boolean credito, int idTurno, BigDecimal efectivo, 
            BigDecimal cambio, char estado, String NombreCliente) {
        this.idFactura = idFactura;
        this.idCliente = idCliente;
        this.fecha = fecha;
        this.hora = hora;
        this.idUsuario = idUsuario;
        this.credito = credito;
        this.idTurno = idTurno;
        this.efectivo = efectivo;
        this.cambio = cambio;
        this.estado = estado;
        this.NombreCliente = NombreCliente;
    }

    public int getIdFactura() {
        return idFactura;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public Date getFecha() {
        return fecha;
    }

    public Time getHora() {
        return hora;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public Boolean getCredito() {
        return credito;
    }

    public int getIdTurno() {
        return idTurno;
    }

    public BigDecimal getEfectivo() {
        return efectivo;
    }

    public BigDecimal getCambio() {
        return cambio;
    }

    public char getEstado() {
        return estado;
    }

    public String getNombreCliente() {
        return NombreCliente;
    }
    
}
