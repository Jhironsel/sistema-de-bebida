package clases;

import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import net.sf.jasperreports.engine.DefaultJasperReportsContext;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;

public class Utilidades {

    public static final Logger LOGGER = Logger.getLogger(Utilidades.class.getName());

    static {
        try {
            File file = new File("TestLog.log");
            boolean append = true;
            FileHandler fh = new FileHandler(file.getAbsolutePath(), append);
            //fh.setFormatter(new XMLFormatter());
            fh.setFormatter(new SimpleFormatter());
            LOGGER.addHandler(fh);
        } catch (IOException e) {
            if(ScreenSplash.debuger){
                System.out.print(e.getLocalizedMessage());
            }
        }
    }

    //Metodo utilizado para definir las teclas de
    //acceso rapido en el formulario principal....
    //Aprender un poco mas de esto....
    public static void clickOnKey(final AbstractButton button, String actionName, 
            int key) {
        button.getInputMap(JButton.WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke(key, 0), actionName);
        button.getActionMap().put(actionName, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button.doClick();
            }
        });
    }

    public static void extractPrintImage(String filePath, JasperPrint print) {
        try {
            OutputStream ouputStream = new FileOutputStream(filePath);

            DefaultJasperReportsContext.getInstance();

            JasperPrintManager printManager
                    = JasperPrintManager.getInstance(
                            DefaultJasperReportsContext.getInstance());

            BufferedImage rendered_image = (BufferedImage) printManager.printToImage(print, 0, 1.6f);
            ImageIO.write(rendered_image, "png", ouputStream);

        } catch (IOException | JRException e) {
            JOptionPane.showMessageDialog(null, e.getLocalizedMessage());
        }
    }

    public static double controlDouble(Object longValue) {
        double valueTwo = -99; // whatever to state invalid!
        if (longValue instanceof Number) {
            valueTwo = ((Number) longValue).doubleValue();
        }
        return valueTwo;
    }

    public static void copyFileUsingFileChannels(String source, String name) {
        try {
            InputStream in = new FileInputStream(source);
            OutputStream out = new FileOutputStream("imagenes/" + name);

            byte[] buf = new byte[1024];
            int len;

            while ((len = in.read(buf)) > 0) {
                //out.flush();
                out.write(buf, 0, len);
            }

            in.close();
            out.close();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, e.getLocalizedMessage());
        }
    }

    public static boolean isNumercFloat(String cadena) {
        String cadena1 = cadena.replace("$", "").replace(" ", "");
        try {
            Float.parseFloat(cadena1);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    public static java.sql.Date javaDateToSqlDate(java.util.Date date) {
        return new java.sql.Date(date.getTime());
    }

    public static String formatDate(Date fecha, String tipo) {
        if (fecha == null) {
            fecha = new Date();
        }
        SimpleDateFormat formatoDelTexto;
        switch (tipo) {
            case "dd-MM-yyyy":
                formatoDelTexto = new SimpleDateFormat("dd-MM-yyyy");
                break;
            case "MM-dd-yyyy":
                formatoDelTexto = new SimpleDateFormat("MM-dd-yyyy");
                break;
            case "dd/MM/yyyy":
                formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");
                break;
            default:
                formatoDelTexto = new SimpleDateFormat("dd.MM.yyyy");
                break;
        }
        return formatoDelTexto.format(fecha);
    }

    public static Date objectToDate(Object obj) {
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy");
        Date aux = null;
        try {
            aux = formatoDelTexto.parse(objectToString(obj));
        } catch (ParseException ex) {
            if(ScreenSplash.debuger){
                System.out.print(ex.getLocalizedMessage());
            }
        }
        return aux;
    }

    public static double objectToDouble(Object Obj) {
        String Str = Obj.toString();
        String aux = Str.replace("R", "").replace("D", "").replace("$", "").trim();
        return Double.parseDouble(aux);
    }

    public static int objectToInt(Object Obj) {
        String Str = Obj.toString();
        return Integer.parseInt(Str);
    }

    public static String objectToString(Object Obj) {
        String Str = "";
        if (Obj != null) {
            Str = Obj.toString();
        }
        return Str;
    }

    public static String priceWithDecimal(double price) {
        DecimalFormat formatter = new DecimalFormat("#0.00");
        return formatter.format(price);
    }

    public static void limpiarPantalla() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
}
