package clases;

import java.math.BigDecimal;
import java.util.Objects;

public class DetalleFactura {
    private final int idFactura;
    private final int idLinea;
    private final int idProducto;
    private final String descripcion;
    private final BigDecimal precio;
    private final BigDecimal cantidad;

    public DetalleFactura(int idFactura, int idLinea, int idProducto, 
            String descripcion, BigDecimal precio, BigDecimal cantidad) {
        this.idFactura = idFactura;
        this.idLinea = idLinea;
        this.idProducto = idProducto;
        this.descripcion = descripcion;
        this.precio = precio;
        this.cantidad = cantidad;
    }

    public int getIdFactura() {
        return idFactura;
    }

    public int getIdLinea() {
        return idLinea;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.idFactura;
        hash = 97 * hash + this.idLinea;
        hash = 97 * hash + this.idProducto;
        hash = 97 * hash + Objects.hashCode(this.descripcion);
        hash = 97 * hash + Objects.hashCode(this.precio);
        hash = 97 * hash + Objects.hashCode(this.cantidad);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DetalleFactura other = (DetalleFactura) obj;
        if (this.idFactura != other.idFactura) {
            return false;
        }
        if (this.idLinea != other.idLinea) {
            return false;
        }
        return true;
    }
    
}
