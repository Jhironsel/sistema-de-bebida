package clases;

import java.sql.Date;

public class Producto{
    private final int idProducto;
    private final String codigo;
    private final String descripcion;
    private final String nota;
    private final int idCategoria;
    private final Date FechaCreacion;
    private final String imagen;
    private final boolean estado;
    private final String idUsuario;

    public Producto(int idProducto, String codigo, String descripcion, 
            String nota, int idCategoria, Date FechaCreacion, String imagen, 
            boolean estado, String idUsuario) {
        this.idProducto = idProducto;
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.nota = nota;
        this.idCategoria = idCategoria;
        this.FechaCreacion = FechaCreacion;
        this.imagen = imagen;
        this.estado = estado;
        this.idUsuario = idUsuario;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public String getCodigo() {
        return codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getNota() {
        return nota;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public Date getFechaCreacion() {
        return FechaCreacion;
    }

    public String getImagen() {
        return imagen;
    }

    public boolean isEstado() {
        return estado;
    }

    public String getIdUsuario() {
        return idUsuario;
    }
    
    
    
    public String ToString() {
        return idProducto + "|"
                + descripcion + "|"
                + nota + "|"
                + estado;
    }
}
