package clases;

public class Usuario {
    private final String idUsuario;
    private final String nombres;
    private final String apellidos;
    private final String clave;
    private final String rol;
    private final boolean estado;
    private final boolean delega;

    public Usuario(String idUsuario, String nombres, String apellidos, 
            String clave, String rol, boolean estado, boolean delega) {
        this.idUsuario = idUsuario;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.clave = clave;
        this.rol = rol;
        this.estado = estado;
        this.delega = delega;
    }

    public boolean getDelega() {
        return delega;
    }
    public boolean getEstado() {
        return estado;
    }
    public String getIdUsuario() {
        return idUsuario;
    }
    public String getNombres() {
        return nombres;
    }
    public String getApellidos() {
        return apellidos;
    }
    public String getClave() {
        return clave;
    }
    public String getRol() {
        return rol;
    }
    @Override
    public String toString() {
        return "Usuario{" + "idUsuario=" + idUsuario + ", nombres=" + nombres + 
                ", apellidos=" + apellidos + ", clave=" + clave + ", rol=" + rol + 
                ", estado=" + estado + ", delega=" + delega + '}';
    }
}
