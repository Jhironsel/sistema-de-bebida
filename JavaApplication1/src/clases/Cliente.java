package clases;

import java.util.Date;
import java.util.Objects;

public class Cliente {

    private String idCliente;
    private final String nombres;
    private final String apellidos;
    private final String sexo;
    private final String ciudad;
    private final String direccion;
    private final String telefono;
    private final Date fechaNacimiento;
    private final Date fechaIngreso;
    private final Boolean estado;

    public Cliente(String idCliente, String nombres, String apellidos, String sexo,
            String ciudad, String direccion, String telefono,
            Date fechaNacimiento, Date fechaIngreso, Boolean estado) {
        if (ScreenSplash.debuger) {
            System.out.println("+Clase Cliente");
        }
        this.idCliente = idCliente;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.sexo = sexo;
        this.ciudad = ciudad;
        this.direccion = direccion;
        this.telefono = telefono;
        this.fechaNacimiento = fechaNacimiento;
        this.fechaIngreso = fechaIngreso;
        this.estado = estado;
    }

    public String getIdCliente() {
        if (idCliente.equals('0')) {
            if(ScreenSplash.debuger){
            System.out.println("Codigo del cliente nuevo....");
            }
            idCliente = "000-0000000-0";
        }
        return idCliente;
    }

    public String getNombres() {
        return nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getCiudad() {
        return ciudad;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public Boolean getEstado() {
        return estado;
    }
    
    public String getSexo() {
        return sexo;
    }
    

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.idCliente);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente other = (Cliente) obj;
        if (!Objects.equals(this.idCliente, other.idCliente)) {
            return false;
        }
        return true;
    }

}
