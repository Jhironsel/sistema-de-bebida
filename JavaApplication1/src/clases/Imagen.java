package clases;

import java.awt.Image;

public class Imagen {
    private final Image imagen;
    private final String nombre;
    public Imagen(Image imagen, String nombre) {
        this.imagen = imagen;
        this.nombre = nombre;
    }
    public Image getImagen() {
        return imagen;
    }
    public String getNombre() {
        return nombre;
    }
}