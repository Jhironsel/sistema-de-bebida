package clases;

import static clases.Utilidades.LOGGER;
import static conexiones.Conexion.getCnn;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import javax.imageio.ImageIO;

public class Datos {

    public synchronized static String pathBaseDeDatos() {

        try {
            PreparedStatement ps = getCnn().prepareStatement(
                    "SELECT MON$DATABASE_NAME FROM MON$DATABASE");
            ResultSet rs = ps.executeQuery();
            rs.next();
            return rs.getString(1);
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class
                    .getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static String getUsuarioActual() {

        try {
            PreparedStatement ps = getCnn().prepareStatement(
                    "SELECT CURRENT_USER FROM MON$DATABASE");
            ResultSet rs = ps.executeQuery();
            rs.next();
            return rs.getString(1);
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class
                    .getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static String modificarDeuda(String idDeuda, String op) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT S_SALIDA "
                    + "FROM UPDATE_DEUDA_ESTADO (?,?)");

            st.setInt(1, Integer.valueOf(idDeuda));
            st.setString(2, op);

            ResultSet rs = st.executeQuery();
            rs.next();
            return rs.getString("S_SALIDA");
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error a ejecutar Operacion";
        }
    }

    public static boolean existeUsuario(String idUsuario) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "select (1) "
                    + "from GET_USUARIOS "
                    + "where TRIM(IDUSUARIO) = ?");

            st.setString(1, idUsuario.toUpperCase().trim());

            ResultSet rs = st.executeQuery();
            return rs.next();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public synchronized static boolean existeIdMaquina(String idMaquina) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT (1) "
                    + "FROM V_FCH_LC a "
                    + "WHERE a.ID = ?");
            st.setString(1, idMaquina.trim());
            ResultSet rs = st.executeQuery();
            return rs.next();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public synchronized static boolean delega(String idUsuario) {

        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "select (1) "
                    + "from GET_usuarios u "
                    + "where trim(u.idusuario) = upper(trim(?)) and "
                    + "u.autorizado");
            st.setString(1, idUsuario);
            ResultSet rs = st.executeQuery();
            return rs.next();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public synchronized static int periodoMaquina() {
        String sql = "SELECT r.D FROM V_TIME_LIC r";
        try {
            PreparedStatement st = getCnn().prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            rs.next();
            return rs.getInt(1);
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }

    public synchronized static boolean existeCategoria(String categoria) {
        String sql = "select (1) from TABLA_CATEGORIAS where descripcion like ?";
        try {
            PreparedStatement st = getCnn().prepareStatement(sql);
            st.setString(1, categoria);
            ResultSet rs = st.executeQuery();
            if (ScreenSplash.debuger) {
                System.out.println("Resultado: " + rs.toString());
            }
            return rs.next();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public synchronized static boolean existeCategoriaProductos(String idCategoria) {
        String sql = "select (1) from tabla_PRODUCTOS where idCategoria = ?";
        try {
            PreparedStatement st = getCnn().prepareStatement(sql);
            st.setInt(1, Integer.valueOf(idCategoria));
            ResultSet rs = st.executeQuery();
            return rs.next();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public synchronized static boolean usuarioTurnoActivo(String idUsuario) {
        try {

            PreparedStatement st = getCnn().prepareStatement(
                    "select (1) "
                    + "from tabla_turnos "
                    + "where TRIM(idUsuario) = ? and estado");

            st.setString(1, idUsuario.trim().toUpperCase());

            ResultSet rs = st.executeQuery();
            return rs.next();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public synchronized static ResultSet getBuscarTemporal(Integer idFactura) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT IDFACTURA, IDLINEA, IDPRODUCTO, DESCRIPCION, "
                    + "          CANTIDAD, PRECIO "
                    + "   FROM GET_DETALLEFACTURA "
                    + "   WHERE IDFACTURA = ?"
                    + "order by 2");

            st.setInt(1, idFactura);

            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getCategorias() {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT idCategoria, Descripcion, Image "
                    + "FROM tabla_categorias "
                    + "ORDER BY 1");
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE,
                    null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getCategoriaActivas() {
        //Para que sirve este metodo...
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT r.IDCATEGORIA, r.DESCRIPCION, r.IMAGE "
                    + "FROM GET_CATEGORIA_ACTIVAS r");
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getClientesPorCriterios(String f, String c) {
        //Para que sirve este metodo...
        String sql = "SELECT r.IDCATEGORIA, r.DESCRIPCION, r.IMAGE "
                + "FROM GET_CATEGORIA_ACTIVAS r "
                + f;
        if (ScreenSplash.debuger) {
            System.out.println("+++++SQL: " + sql);
            System.out.println("+++++Filtro: " + f);
            System.out.println("+++++Criterio: " + c);
        }
        try {
            PreparedStatement st = getCnn().prepareStatement(sql);
            st.setString(1, c);
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getUsuariosActivo() {
        //Se está utilizando para llenar los comboBox de los usuarios activos.
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "select u.idUsuario, u.NombreUNO, u.Apellidos "
                    + "from GET_Usuarios u "
                    + "left JOIN tabla_TURNOS t on t.IDUSUARIO like u.IDUSUARIO "
                    + "where TRIM(u.Rol) like 'CAJERO' and u.ESTADO"
            );
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getUsuariosActivos() {
        //Para que sirve este metodo...
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT r.IDUSUARIO, (r.NOMBREUNO||' '||r.APELLIDOS) AS Nombre "
                    + "FROM GET_USUARIOS r "
                    + "WHERE r.ESTADO");
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getMensajes() {
        //Para que sirve este metodo...
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "select MENSAJE, NOMBREEMPRESA, DIRECCIONEMPRESA, TELEFONOEMPRESA "
                    + "from GET_MENSAJES "
            );
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getCajerosActivos() {
        //Para que sirve este metodo...
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT r.IDUSUARIO, r.FECHA, r.HORA "
                    + "FROM GET_USUARIO_ACTIVO r"
            );
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static int idTurnoActivo(String idUsuario) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "select idTurno "
                    + "from tabla_turnos "
                    + "where TRIM(idUsuario) like ? and estado"
            );
            st.setString(1, idUsuario.trim().toUpperCase());
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }

    public synchronized static boolean existeCliente(String criterio) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "select (1) "
                    + "from tabla_clientes "
                    + "where idCliente like ? or "
                    + "idCliente starting ? or "
                    + "nombres CONTAINING ? or "
                    + "apellidos CONTAINING ?");
            st.setString(1, criterio.trim());
            st.setString(2, criterio.trim());
            st.setString(3, criterio.trim());
            st.setString(4, criterio.trim());
            ResultSet rs = st.executeQuery();
            return rs.next();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public synchronized static ArrayList<String> comprobandoRol(String idUsuario) {
        ArrayList<String> roles = new ArrayList<String>();
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT distinct r.ROL "
                    + "FROM GET_ROL r "
                    + "where TRIM(r.idUsuario) like TRIM(upper(?))");
            st.setString(1, idUsuario.trim().toUpperCase());
            ResultSet rs = st.executeQuery();

            String aux;
            rs.next();
            do {
                aux = rs.getString("ROL");
                if (aux.equalsIgnoreCase("RDB$ADMIN")) {
                    aux = "ADMINISTRADOR";
                }
                roles.add(aux);
            } while (rs.next());

            return roles;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static boolean existeProducto(String codigo) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "select (1) "
                    + "from tabla_productos "
                    + "where codigo = ?");
            st.setString(1, codigo);
            ResultSet rs = st.executeQuery();
            return rs.next();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public static String agregarUsuario(Usuario u) {
        try {

            Statement st = getCnn().createStatement();

            st.executeUpdate(
                    "CREATE USER " + u.getIdUsuario()
                    + " PASSWORD '" + u.getClave() + "' "
                    + " FIRSTNAME '" + u.getNombres() + "' "
                    + " LASTNAME '" + u.getApellidos() + "' "
                    + (u.getEstado() ? " ACTIVE " : " INACTIVE ")
                    + (u.getDelega() ? " GRANT ADMIN ROLE " : "")
            );

            st.executeUpdate("GRANT " + u.getRol() + " TO " + u.getIdUsuario());

            CallableStatement cs = getCnn().prepareCall(
                    "execute procedure sp_actualizar_reccount('I', 'GET_USUARIOS')");

            cs.executeUpdate();

            return "Usuario agregado correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al insertar usuario.";
        }
    }

    public synchronized static boolean agregarCategoria(String descripcion, String imagePath) {
        try {
            File file = new File(imagePath);
            FileInputStream fis = new FileInputStream(file);

            PreparedStatement st = getCnn().prepareStatement(
                    "INSERT INTO TABLA_CATEGORIAS (DESCRIPCION, IMAGE) "
                    + "VALUES (?,?)"
            );

            st.setString(1, descripcion);
            st.setBinaryStream(2, fis, (int) file.length());

            st.executeUpdate();

            return false;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    public synchronized static String agregarProducto(Producto p) {
        try {

            PreparedStatement st = getCnn().prepareStatement(
                    "INSERT INTO TABLA_PRODUCTOS (codigo, DESCRIPCION, "
                    + "NOTA, IDCATEGORIA, IMAGE, ESTADO) "
                    + "VALUES (?, ?, ?, ?, ?, ? )"
            );
            st.setString(1, p.getCodigo());
            st.setString(2, p.getDescripcion());
            st.setString(3, p.getNota());
            st.setInt(4, p.getIdCategoria());
            if (p.getImagen() != null) {
                File file = new File(p.getImagen());
                FileInputStream fis = new FileInputStream(file);
                st.setBinaryStream(5, fis, (int) file.length());
            } else {
                st.setBinaryStream(5, null, 0);
            }
            st.setBoolean(6, p.isEstado());

            st.executeUpdate();
            return "Producto Agregado Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Error al Insertar Producto...";
    }

    public synchronized static boolean agregarProductoEntrada(EntradaProducto e) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "INSERT INTO TABLA_ENTRADAS_PRODUCTO (IDENTRADA_PRODUCTO, "
                    + "IDPRODUCTO, NUMERO, NUMEROFACTURA, ENTRADA, OP, COSTO, "
                    + "PRECIO, IMPUESTO, IMPUESTOPORCIENTO, CONCEPTO) "
                    + "VALUES(?,?,?,?,?,?,?,?,?,?,?);"
            );
            st.setInt(1, e.getIdEntradaProducto());
            st.setInt(2, e.getIdProducto());
            st.setInt(3, e.getNumero());
            st.setString(4, e.getNumeroFactura());
            st.setBigDecimal(5, e.getEntrada());
            st.setString(6, "" + e.getOp());
            st.setBigDecimal(7, e.getCosto());
            st.setBigDecimal(8, e.getPrecio());
            st.setBoolean(9, e.getImpuesto());
            st.setBigDecimal(10, e.getImpuestoPorciento());
            st.setString(11, e.getConcepto());
            st.executeUpdate();
            return true;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public synchronized static String agregarCliente(Cliente c) {
        try {
            String sql = "insert into TABLA_CLIENTES (IDCLIENTE, NOMBRES, "
                    + "APELLIDOS, CIUDAD, DIRECCION, TELEFONO, FECHANACIMIENTO, "
                    + "ESTADO) "
                    + "values (?,?,?,?,?,?,?,?)";

            PreparedStatement st = getCnn().prepareStatement(sql);
            st.setString(1, c.getIdCliente());
            st.setString(2, c.getNombres());
            st.setString(3, c.getApellidos());
            st.setString(4, c.getCiudad());
            st.setString(5, c.getDireccion());
            st.setString(6, c.getTelefono());
            st.setDate(7, Utilidades.javaDateToSqlDate(c.getFechaNacimiento()));
            st.setBoolean(8, c.getEstado());
            st.executeUpdate();
            return "Cliente Agregado Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al insertar Cliente...";
        }
    }

    public static String modificarUsuario(Usuario u, String revokeRol) {
        try {
            Statement st = getCnn().createStatement();

            String sql = "ALTER USER " + u.getIdUsuario() + " "
                    + (u.getEstado() ? " ACTIVE " : " INACTIVE ")
                    + "FIRSTNAME '" + u.getNombres() + "' "
                    + "LASTNAME '" + u.getApellidos() + "' "
                    + (u.getDelega() ? "GRANT ADMIN ROLE" : " REVOKE ADMIN ROLE ");
            if (ScreenSplash.debuger) {
                System.out.println("sql = " + sql);
            }
            st.executeUpdate(sql);

            if (!u.getClave().isEmpty()) {
                sql = "ALTER USER " + u.getIdUsuario() + " "
                        + "PASSWORD '" + u.getClave() + "'";
                st.executeUpdate(sql);
            }

            sql = "REVOKE " + revokeRol + " FROM " + u.getIdUsuario()
                    + " granted by " + getCreadorUsuario(u.getIdUsuario());
            st.executeUpdate(sql);

            sql = "GRANT " + u.getRol() + " TO " + u.getIdUsuario();
//                    + (u.getDelega() ? " WITH ADMIN OPTION " : "");
            st.executeUpdate(sql);

            return "Usuario modificado correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al Modificar Usuario...";
        }
    }

    public synchronized static String modificarCliente(Cliente c) {
        try {
            String sql = "update Tabla_CLIENTES "
                    + "set NOMBRES = ?, "
                    + "    APELLIDOS = ?, "
                    + "    CIUDAD = ?, "
                    + "    DIRECCION = ?, "
                    + "    TELEFONO = ?, "
                    + "    FECHANACIMIENTO = ?, "
                    + "    ESTADO = ?  "
                    + "where (IDCLIENTE = ?)";

            PreparedStatement st = getCnn().prepareStatement(sql);
            st.setString(1, c.getNombres());
            st.setString(2, c.getApellidos());
            st.setString(3, c.getCiudad());
            st.setString(4, c.getDireccion());
            st.setString(5, c.getTelefono());
            st.setDate(6, Utilidades.javaDateToSqlDate(c.getFechaNacimiento()));
            st.setBoolean(7, c.getEstado());
            st.setString(8, c.getIdCliente());
            st.executeUpdate();
            return "Cliente Modificado Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al Modificar Cliente...";
        }
    }

    public synchronized static boolean modificarFactura(Factura f) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "UPDATE TABLA_FACTURA a "
                    + "SET "
                    + "    a.CREDITO = ?, "
                    + "    a.EFECTIVO = ?, "
                    + "    a.CAMBIO = ?, "
                    + "    a.ESTADO = ? "
                    + "WHERE "
                    + "    a.IDFACTURA = ?");
            st.setString(1, "" + f.getCredito());
            st.setBigDecimal(2, f.getEfectivo());
            st.setBigDecimal(3, f.getCambio());
            st.setString(4, "" + f.getEstado());
            st.setInt(5, f.getIdFactura());
            st.executeUpdate();
            return true;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public synchronized static boolean modificarCategoria(Categoria c) {
        try {
            File file = new File(c.getRuta());
            FileInputStream fis = new FileInputStream(file);

            PreparedStatement st = getCnn().prepareStatement(
                    "update TABLA_CATEGORIAS "
                    + "set descripcion = ?, "
                    + "    image = ? "
                    + "where (idCategoria = ?)"
            );
            st.setString(1, c.getDescripcion());
            st.setBinaryStream(2, fis, (int) file.length());
            st.setInt(3, c.getIdCategoria());

            st.executeUpdate();
            return false;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return true;
    }

    public synchronized static String modificarOpcionMensaje(String mensaje, String nombreEmpresa,
            String direccionEmpresa, String telefonoEmpresa) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "UPDATE OR INSERT INTO OPCIONES(OPCION, RUTA) "
                    + "VALUES('MensajeTickes', ?) "
                    + "matching(OPCION)");
            st.setString(1, mensaje);
            st.executeUpdate();

            st = getCnn().prepareStatement(
                    "UPDATE OR INSERT INTO OPCIONES(OPCION, RUTA) "
                    + "VALUES('nombreEmpresa', ?) "
                    + "matching(OPCION)");
            st.setString(1, nombreEmpresa);
            st.executeUpdate();

            st = getCnn().prepareStatement(
                    "UPDATE OR INSERT INTO OPCIONES(OPCION, RUTA) "
                    + "VALUES('direccionEmpresa', ?) "
                    + "matching(OPCION)");
            st.setString(1, direccionEmpresa);
            st.executeUpdate();

            st = getCnn().prepareStatement(
                    "UPDATE OR INSERT INTO OPCIONES(OPCION, RUTA) "
                    + "VALUES('telefonoEmpresa', ?) "
                    + "matching(OPCION)");
            st.setString(1, telefonoEmpresa);
            st.executeUpdate();

            return "Tickes Actualizado";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Error al Modificar Tickes...";
        }
    }

    public synchronized static String modificarProducto(Producto p) {
        FileInputStream fis = null;
        PreparedStatement st = null;
        try {//Trabajar con los producto para modificarlo
            st = getCnn().prepareStatement(
                    "update tabla_productos set "
                    + "descripcion = ?, "
                    + "nota = ?, "
                    + "estado = ?, "
                    + "idCategoria = ? "
                    + (p.getImagen() != null ? ", image = ? " : "")
                    + "where idProducto = ? ");

            st.setString(1, p.getDescripcion());
            st.setString(2, p.getNota());
            st.setBoolean(3, p.isEstado());
            st.setInt(4, p.getIdCategoria());

            File file;
            if (p.getImagen() != null) {
                file = new File(p.getImagen());
                fis = new FileInputStream(file);
                st.setBinaryStream(5, fis, (int) file.length());
                st.setInt(6, p.getIdProducto());
            } else {
                st.setInt(5, p.getIdProducto());
            }

            st.executeUpdate();
            return "Producto Modificado Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Error al Modificar Producto...";
    }

    public synchronized static String borrarUsuario(String idUsuario, String rol) {
        try {
            Statement st = getCnn().createStatement();

            String sql = "REVOKE " + rol + " FROM " + idUsuario
                    + " granted by " + getCreadorUsuario(idUsuario);
            if (ScreenSplash.debuger) {
                System.out.println("sql = " + sql);
            }
            st.executeUpdate(sql);

            sql = "DROP USER " + idUsuario;

            if (ScreenSplash.debuger) {
                System.out.println("sql = " + sql);
            }

            st.executeUpdate(sql);

            return "Usuario borrado correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Ocurrio un error al intentar borrar el Usuario.";
        }
    }

    public synchronized static String borrarCliente(String idCliente) {
        try {
            String sql = "DELETE FROM TABLA_CLIENTES a WHERE a.IDCLIENTE = ?";

            PreparedStatement st = getCnn().prepareStatement(sql);
            st.setString(1, idCliente);
            st.executeUpdate();
            return "Cliente borrado correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Cliente no puede ser borrado";
        }
    }

    public synchronized static String borrarCategoria(int idCategoria) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "delete from TABLA_CATEGORIAS where idCategoria = ?"
            );
            st.setInt(1, idCategoria);
            st.executeUpdate();
            return "Categoria Borrado Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Ocurrio un error al intentar borrar la Categoria...";
        }
    }

    public synchronized static String borrarFactura(Integer idFactura) {
        //Este metodo hay que analizarlo, Creo que no debe borrarse factura
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "delete from  where idFactura = ?");
            st.setInt(1, idFactura);
            st.executeUpdate();
            return "Factura borrada correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "!Ocurrio un error al intentar borrar el Factura...!!!";
        }
    }

    public synchronized static String borrarProducto(String idProducto) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "delete from TABLA_PRODUCTOS "
                    + "where idproducto = ? "
            );
            st.setString(1, idProducto);
            st.executeUpdate();
            return "Producto Borrado Correctamente";
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return "Ocurrio un error al intentar borrar el Producto...";
        }
    }

    public synchronized static ResultSet getUsuarios() {
        //Se esta utilizando para llenar la tabla de usuarios
        String sql = "SELECT r.IDUSUARIO, r.NOMBREUNO, r.NOMBREDOS, "
                + "       r.APELLIDOS, r.ESTADO, r.AUTORIZADO, r.ROL "
                + "FROM GET_USUARIOS r";
        try {
            PreparedStatement st = getCnn().prepareStatement(sql);
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getRoles() {//Trabajar para llenar el comboBox del Usuario en su perfil Creo que es pan Comido...
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "select ROL "
                    + "from GET_ROLES");
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getTemporales() {//Trabajar para llenar el comboBox del Usuario en su perfil Creo que es pan Comido...
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "select IDFACTURA, NOMBRECLIENTE, NOMBREAPELLIDOS, FECHA, IDUSUARIO, HORA, "
                    + "        TIPO_COMPRA, IDTURNO, EFECTIVO, CAMBIO, ESTADO, MONTO, IDCLIENTE "
                    + "from GET_TEMPORALES "
                    + "order by 1"
            );
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getClientes() {
        try {
            String sql = "select CODIGO, IDCLIENTE, NOMBRES, APELLIDOS, SEXO, CIUDAD, "
                    + "DIRECCION, TELEFONO, FECHANACIMIENTO, FECHAINGRESO, ESTADO "
                    + "from TABLA_CLIENTES "
                    + "order by 1 ";
            PreparedStatement st = getCnn().prepareStatement(sql);
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getClientesFiltrados(String criterio,
            String filtro) {
        try {
            String sql = "SELECT r.IDCLIENTE, r.NOMBRES, r.APELLIDOS, D.MONTO "
                    + "FROM Tabla_Clientes r "
                    + "INNER JOIN TABLA_DEUDAS D ON r.IDCLIENTE like D.IDCLIENTE "
                    + filtro;
            if (ScreenSplash.debuger) {
                System.out.println("SQL: " + sql);
                System.out.println("Criterio: " + criterio);
            }
            PreparedStatement st = getCnn().prepareStatement(sql);
            if (criterio != null) {
                st.setString(1, criterio);
            }

            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getReporteFacturas(String filtro) {
        try {
            String sql = "SELECT f.idFactura, f.idCliente, (c.nombres||' '||c.apellidos) AS nombreFull, "
                    + "        f.fecha, d.idLinea, p.idProducto, p.descripcion, "
                    + "        precio,   d.cantidad, precio * d.cantidad AS Valor "
                    + "FROM tabla_facturas f "
                    + "INNER JOIN tabla_clientes c ON f.idCliente = c.idCliente "
                    + "INNER JOIN detalleFactura d ON f.idFactura = d.idFactura "
                    + "INNER JOIN tabla_productos p ON p.idproducto = d.idproducto "
                    + filtro;
            if (ScreenSplash.debuger) {
                System.out.println("SQL: " + sql);
            }
            PreparedStatement st = getCnn().prepareStatement(sql);
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getCobrosClientesFactura(String idCliente) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT r.IDFACTURA, CAST(sum(d.CANTIDAD * d.PRECIO) as DECIMAL(15,2)) as Total, "
                    + "       r.FECHA, r.ESTADO "
                    + "FROM TABLA_FACTURAS r "
                    + "JOIN TABLA_DETALLEFACTURA d ON d.IDFACTURA = r.IDFACTURA "
                    + "where r.IDCLIENTE like ? and r.ESTADO in('c', 'a') "
                    + "GROUP BY r.IDFACTURA, r.FECHA, r.ESTADO"
            );
            st.setString(1, idCliente);
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getFacturasNombreClientes(int idFactura) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT r.IDCLIENTE, nombreCliente "
                    + "FROM TABLA_FACTURAS r "
                    + "where r.IDFACTURA = ?"
                    + "order by 1");
            st.setInt(1, idFactura);
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getDeudaClientes(String estado) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT SUM(r.MONTO), case r.ESTADO "
                    + "when 'i' then 'Inicial:' "
                    + "when 'a' then 'Abonado:' "
                    + "when 'p' then 'Pagado:' "
                    + "when 'n' then 'Nulado:' "
                    + "end "
                    + "FROM GET_SUMA_DEUDA r "
                    + estado);
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getDeudaClientesEstado(String estado) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT r.IDDEUDAS, IIF(r.IDCLIENTE = '0', '000-0000000-0', "
                    + "r.IDCLIENTE) as IDCLIENTE, c.NOMBRES, c.APELLIDOS, "
                    + "r.CONCEPTO, r.MONTO, r.FECHA, "
                    + "        (IIF(r.ESTADO = 'i', 'Inicial', "
                    + "         IIF(r.ESTADO = 'p', 'Pagada', "
                    + "         IIF(r.ESTADO = 'a', 'Abonada', "
                    + "         IIF(r.ESTADO = 'n','Nula','No Definida'))))) as ESTADO "
                    + "FROM TABLA_DEUDAS r "
                    + "LEFT JOIN TABLA_CLIENTES c"
                    + "    ON c.IDCLIENTE LIKE r.IDCLIENTE "
                    + estado
                    + "ORDER by 1");
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getDeudaClientes() {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT r.IDCLIENTE, (c.NOMBRES||' '||c.APELLIDOS) as nombres "
                    + "FROM TABLA_DEUDAS r "
                    + "LEFT JOIN TABLA_CLIENTES c"
                    + "    ON c.IDCLIENTE LIKE r.IDCLIENTE "
                    + "WHERE r.ESTADO IN('i', 'a') "
                    + "GROUP BY r.IDCLIENTE, c.NOMBRES, c.APELLIDOS ");
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getFacturasDetalladas(String idFactura) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT factura.idFactura, factura.idCliente, nombres||' '||apellidos AS nombreFull, \n"
                    + "        fecha, idLinea, (select p.Descripcion \n"
                    + "                            from TABLA_PRODUCTOS p \n"
                    + "                            where p.idProducto = DETALLEFACTURA.IDPRODUCTO ) as Descripcion, \n"
                    + "        idProducto, precio, cantidad, precio * cantidad AS Valor \n"
                    + "FROM TABLA_FACTURAS\n"
                    + "INNER JOIN TABLA_CLIENTES ON factura.idCliente = cliente.idCliente \n"
                    + "INNER JOIN TABLA_DETALLEFACTURA ON factura.idFactura = detalleFactura.idFactura\n"
                    + "WHERE factura.idFactura = ? ");
            st.setString(1, idFactura);
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getFacturasDetalladaPorCliente(String idCliente) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT f.idFactura, f.estado , f.fecha, f.USUARIO, "
                    + "COALESCE(SUM(d.precio * d.cantidad), 0.00) AS Valor "
                    + "FROM TABLA_FACTURAS f "
                    + "LEFT JOIN TABLA_CLIENTES c "
                    + "    ON f.idCliente = c.idCliente "
                    + "LEFT JOIN TABLA_DETALLEFACTURA d "
                    + "    ON f.idFactura = d.idFactura "
                    + "WHERE f.idCliente = ? "
                    + "GROUP BY f.idFactura, f.estado , f.fecha, f.USUARIO "
                    + "order by 1"
            );
            st.setString(1, idCliente);
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getFacturasDetalladaPorCliente(
            String idCliente, int idFactura) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT f.idFactura, f.estado , f.fecha, f.USUARIO, "
                    + "COALESCE(SUM(d.precio * d.cantidad), 0.00) AS Valor "
                    + "FROM TABLA_FACTURAS f "
                    + "LEFT JOIN TABLA_CLIENTES c "
                    + "    ON f.idCliente = c.idCliente "
                    + "LEFT JOIN TABLA_DETALLEFACTURA d "
                    + "    ON f.idFactura = d.idFactura "
                    + "WHERE f.idCliente = ? "
                    + "and f.idFactura = ? "
                    + "GROUP BY f.idFactura, f.estado , f.fecha, f.USUARIO "
                    + "order by 1"
            );
            st.setString(1, idCliente);
            st.setInt(2, idFactura);
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getDeudaCliente(String idCliente) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT r.IDDEUDAS, r.CONCEPTO, r.MONTO, r.FECHA, r.ESTADO "
                    + "FROM TABLA_DEUDAS r "
                    + "WHERE r.IDCLIENTE LIKE ? AND r.ESTADO NOT IN('n', 'p')"
            );
            st.setString(1, idCliente);

            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getDeudaClienteExterna(String idDeuda) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT r.CODIGO, r.FECHA, r.HORA, r.MONTO "
                    + "FROM TABLA_PAGO_DEUDAS_EXTERNA r "
                    + "WHERE r.IDDEUDA = ?"
            );
            st.setString(1, idDeuda);

            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getClientesCombo(String filtro,
            String criterio) {
//        
        if (filtro == null) {
            filtro = "where estado and idCliente != '000-0000000-0' "
                    + "order by 1";
        }

        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "select c.idCliente, c.nombres, c.apellidos "
                    + "from tabla_clientes c "
                    + filtro);
            if (criterio != null) {
                st.setString(1, criterio);
            }

            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getClientesCobros() {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT r.IDCLIENTE, (r.NOMBRES||' '||r.APELLIDOS) as nombre "
                    + "FROM TABLA_CLIENTES r "
                    + "WHERE r.DEUDAACTUAL > 0");

            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getClientes(String idCliente) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "select idCliente, (c.nombres ||' '||c.apellidos) as NombreCompleto, "
                    + "deudaactual "
                    + "from tabla_clientes c "
                    + "where c.idCliente like ? "
            );
            st.setString(1, idCliente);
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getProductosCriterios(int tipo, String criterio, boolean image) {
        String sql = "select p.IdProducto, p.Descripcion, p.Codigo " + (image ? ", image " : "")
                + "from TABLA_PRODUCTOS p ";
        switch (tipo) {
            case 1:
                sql = sql + "where p.Codigo CONTAINING ? and estado order by 1";
                break;
            case 2:
                sql = sql + "where p.Descripcion CONTAINING ? and estado order by 2";
                break;
            case 3:
                sql = sql
                        + "inner join tabla_entradas_Producto e on p.idProducto = e.idProducto "
                        + "where p.idCategoria = ? and e.entrada > 0 and estado";
                break;
            case 4:
                sql = sql + "where idCategoria = ? ";
                break;
            default:
                sql = sql + "where estado order by 1 ";
        }

        if (ScreenSplash.debuger) {
            System.out.println("SQL: " + sql);
            System.out.println("Criterio: " + criterio);
        }

        try {
            PreparedStatement ps = getCnn().prepareStatement(sql);
            if (tipo != 0) {
                ps.setString(1, criterio);
            }
            return ps.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }

    }

    public synchronized static ResultSet getFacturas() {
        //Para que sirve este metodo...
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "select idFactura "
                    + "from tabla_facturas "
                    + "order by 1");
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    //Procedimiento comienzo
    public synchronized static boolean pagoCumplido(int idFactura) {
        CallableStatement stmt;
        try {
            stmt = getCnn().prepareCall(
                    "EXECUTE PROCEDURE Cajero_PagoCumplido (?)");
            stmt.setInt(1, idFactura);
            return stmt.execute();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public synchronized static Boolean pagoDeuda(int idDeuda, int idTurno,
            BigDecimal monto) {
        CallableStatement stmt;
        try {
            stmt = getCnn().prepareCall(
                    "EXECUTE PROCEDURE INSER_PAGO_DEUDAS_EXT (?, ?, ?)");
            stmt.setInt(1, idDeuda);
            stmt.setInt(2, idTurno);
            stmt.setBigDecimal(3, monto);
            return stmt.execute();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public synchronized static boolean setLicencia(String fecha, String idMaquina, String clave1,
            String clave2) throws SQLException {
        CallableStatement stmt = getCnn().prepareCall(
                "EXECUTE PROCEDURE SYSTEM_SET_LICENCIA (?, ?, ?, ?)");
        stmt.setString(1, fecha);
        stmt.setString(2, idMaquina);
        stmt.setString(3, clave1);
        stmt.setString(4, clave2);
        return stmt.execute();
    }

    public synchronized static boolean setLogo(String ruta) throws SQLException {
        CallableStatement stmt = getCnn().prepareCall(
                "EXECUTE PROCEDURE CALL SYSTEM_SET_LOGO (?)");
        stmt.setString(1, ruta);
        return stmt.execute();
    }

    public synchronized static boolean insertDeudas(Deudas miDeuda) throws SQLException {
        CallableStatement stmt = getCnn().prepareCall(
                "EXECUTE PROCEDURE INSER_DEUDAS (?, ?, ?)");
        stmt.setString(1, miDeuda.getIdCliente());
        stmt.setString(3, miDeuda.getConcepto());
        stmt.setBigDecimal(4, miDeuda.getMonto());
        return stmt.execute();
    }

    public synchronized static boolean habilitarTurno(String idUsuario) {
        try {
            CallableStatement stmt = getCnn().prepareCall(
                    "EXECUTE PROCEDURE Admin_HabilitarTurno (?)");
            stmt.setString(1, idUsuario);
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public synchronized static boolean cerrarTurno(String idUsuario) {
        try {
            CallableStatement stmt = getCnn().prepareCall(
                    "EXECUTE PROCEDURE Admin_CerrarTurno (?)");
            stmt.setString(1, idUsuario);
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public synchronized static boolean gasto(int idTurno, String descripcion, double monto) {
        try {
            CallableStatement stmt = getCnn().prepareCall(
                    "EXECUTE PROCEDURE Cajero_gasto (?, ?, ?)");
            stmt.setInt(1, idTurno);
            stmt.setString(2, descripcion.replace("'", ""));
            stmt.setDouble(3, monto);
            stmt.execute();
            return true;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }//Precedimiento   fin

    public synchronized static ResultSet getProductos() {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT r.IDPRODUCTO, codigo,r.DESCRIPCION, "
                    + "COALESCE(r.CANTIDAD, 0.00) as Cantidad, r.NOTA, r.FECHA_CREACION, "
                    + "r.ESTADO, r.CATEGORIA, r.IDCATEGORIA "
                    + "FROM GET_PRODUCTOS r "
                    + "order by 1");
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getProductoByID(String codigo) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "select IDCATEGORIA, IMAGE "
                    + "from GET_PRODUCTOS "
                    + "where codigo like ? ");
            st.setString(1, codigo);
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getProductoById(Integer idProducto,
            String codigo) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "select idProducto, codigo, descripcion, estado, image, "
                    + "idCategoria, Cantidad "
                    + "from tabla_Productos "
                    + "where codigo = ? or IDPRODUCTO = ?"
            );

            st.setString(1, codigo);
            if (idProducto == null) {
                idProducto = 0;
            }
            st.setInt(2, idProducto);
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getPagoDeudasExterna(int idDeuda) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT r.CODIGO, r.MONTO, r.FECHA, r.HORA "
                    + "FROM TABLA_PAGO_DEUDAS_EXTERNA r "
                    + "WHERE r.IDDEUDA = ?");
            st.setInt(1, idDeuda);
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getPagoDeudas(int idFactura) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT r.IDPAGODEUDA, r.FECHA, r.HORA, r.MONTOPAGO "
                    + "FROM TABLA_PAGODEUDA r "
                    + "where r.IDFACTURA = ?");
            st.setInt(1, idFactura);
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static ResultSet getEntradaProducto(int mes, int year) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT r.FECHAENTRADA, IIF(r.OP = '+', 'Entrada', 'Salida') as operacion, r.IDUSUARIO "
                    + "FROM TABLA_ENTRADAS_PRUDUCTO r "
                    + "WHERE EXTRACT(MONTH FROM r.FECHAENTRADA) = ? "
                    + "and EXTRACT(YEAR FROM r.FECHAENTRADA) = ? "
                    + "GROUP BY r.FECHAENTRADA,  r.OP, r.IDUSUARIO");
            st.setInt(1, mes);
            st.setInt(2, year);
            return st.executeQuery();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static int cantidadRegistros(String tabla) {
        try {//Seguir con el conteos de las tablas....
            PreparedStatement st = getCnn().prepareStatement(
                    "select COALESCE(cantidad, 0) as num "
                    + "from tabla_reccount "
                    + "where tabla = ?;"
            );
            st.setString(1, tabla);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("num");
            } else {
                return 0;
            }
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public synchronized static int idMaximoRegistro() {
        try {//Seguir con el conteos de las tablas....
            PreparedStatement st = getCnn().prepareStatement(
                    "select MAX(IDENTRADA_PRODUCTO) as num "
                    + "from TABLA_ENTRADAS_PRODUCTO "
            );
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("num");
            } else {
                return 0;
            }
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public synchronized static Producto getProductos(String idProducto) {
        try {
            Producto miProducto = null;
            Statement st = getCnn().createStatement();
            ResultSet rs = st.executeQuery(
                    "SELECT IDPRODUCTO, CODIGO, DESCRIPCION, NOTA, IDCATEGORIA, "
                    + "FECHACREACION, IMAGE, ESTADO, IDUSUARIO "
                    + "FROM TABLA_PRODUCTOS "
                    + "where idProducto = '" + idProducto + "'");
            if (rs.next()) {
                miProducto = new Producto(
                        rs.getInt("idProducto"),
                        rs.getString("codigo"),
                        rs.getString("descripcion"),
                        rs.getString("nota"),
                        rs.getInt("idCategoria"),
                        rs.getDate("fechaCreacion"),
                        rs.getString("image"),
                        rs.getBoolean("estado"),
                        rs.getString("idUsuario"));
            }
            return miProducto;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static Perfil getAcceso(int idPerfil) {
        try {
            Statement st = getCnn().createStatement();
            ResultSet rs = st.executeQuery(
                    "SELECT r.ARCHIVOS, r.ARCHIVOSCLIENTES, "
                    + "r.ARCHIVOSPRODUCTOS, r.ARCHIVOSUSUARIOS, r.ARCHIVOSCAMBIOCLAVE, "
                    + "r.ARCHIVOSCAMBIOUSUARIO, r.ARCHIVOSSALIR, r.MOVIMIENTOS, "
                    + "r.MOVIMIENTOSNUEVAFACTURA, r.MOVIMIENTOSREPORTEFACTURA, "
                    + "r.MOVIMIENTOSINVENTARIO, r.MOVIMIENTOSABRILTURNO, r.MOVIMIENTOSCERRARTURNO, "
                    + "r.MOVIMIENTOSDEUDA "
                    + "FROM TABLA_ACCESO2 r "
                    + "where idPerfil = " + idPerfil);
            boolean filas = rs.next();

            if (ScreenSplash.debuger) {
                System.out.println("Tenemos Filas: " + filas);
            }

            if (!filas) {
                return null;
            }

            Perfil miPerfil = new Perfil(
                    idPerfil,
                    null,
                    rs.getBoolean("ARCHIVOS"),
                    rs.getBoolean("ARCHIVOSCLIENTES"),
                    rs.getBoolean("ARCHIVOSPRODUCTOS"),
                    rs.getBoolean("ARCHIVOSUSUARIOS"),
                    rs.getBoolean("ARCHIVOSCAMBIOCLAVE"),
                    rs.getBoolean("ARCHIVOSCAMBIOUSUARIO"),
                    rs.getBoolean("ARCHIVOSSALIR"),
                    rs.getBoolean("MOVIMIENTOS"),
                    rs.getBoolean("MOVIMIENTOSNUEVAFACTURA"),
                    rs.getBoolean("MOVIMIENTOSREPORTEFACTURA"),
                    rs.getBoolean("MOVIMIENTOSINVENTARIO"),
                    rs.getBoolean("MOVIMIENTOSABRILTURNO"),
                    rs.getBoolean("MOVIMIENTOSCERRARTURNO"),
                    rs.getBoolean("MOVIMIENTOSDEUDA"));
            return miPerfil;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static Integer getNumFac(int idTurno) {
        CallableStatement stmt;
        try {
            stmt = getCnn().prepareCall("EXECUTE PROCEDURE SYSTEM_FACTURA(?)");
            stmt.setInt(1, idTurno);

            if (stmt.execute()) {
                PreparedStatement st = getCnn().prepareStatement(
                        "EXECUTE PROCEDURE SYSTEM_FACTURA(?)");

                st.setInt(1, idTurno);

                ResultSet rs = st.executeQuery();
                if (rs.next()) {
                    return rs.getInt("S_RESULTADO");
                } else {
                    return null;
                }
            }
            return null;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);

//            if (ex.getMessage().contains("Turno no encontrado....!!!")) {
//                JOptionPane.showMessageDialog(this, "Turno no encontrado.");
//            }
            return null;
        }
    }

    public synchronized static BigDecimal getPrecioProducto(int idProducto) {

        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "select precio "
                    + "from TABLA_ENTRADAS_PRODUCTO "
                    + "where idProducto = ?");

            st.setInt(1, idProducto);

            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                return rs.getBigDecimal("precio");
            } else {
                return new BigDecimal(0);
            }
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE,
                    null, ex);
            return new BigDecimal(-1);
        }
    }

    public synchronized static BigDecimal getDeudaActual(String idCliente) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT r.DEUDAACTUAL "
                    + "FROM TABLA_DEUDAS r "
                    + "WHERE r.IDCLIENTE LIKE ?");
            st.setString(1, idCliente);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getBigDecimal("DEUDAACTUAL");
            } else {
                return new BigDecimal(0);
            }
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE,
                    null, ex);
            return new BigDecimal(-1);
        }
    }

    public synchronized static int getMaxIdAcceso() {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT COALESCE(MAX(r.IDACCESO) + 1, 1) FROM Tabla_ACCESO2 r");
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            } else {
                return 1;
            }
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }

    public synchronized static int getIdAcceso(int idAcceso) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT r.IDACCESO "
                    + "FROM Tabla_ACCESO2 r "
                    + "WHERE r.IDPERFIL = ?");
            st.setInt(1, idAcceso);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            } else {
                return 1;
            }
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return -1;
        }
    }

    public synchronized static BigDecimal sumaMontoPagoDeudaExterna(int idDeuda) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "SELECT SUM(r.MONTO) "
                    + "FROM TABLA_PAGO_DEUDAS_EXTERNA r "
                    + "WHERE r.IDDEUDA = ?");
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getBigDecimal(1);
            } else {
                return new BigDecimal(0);
            }
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return new BigDecimal(-1);
        }
    }

    public synchronized static String getCreadorUsuario(String idUsuario) {
        try {
            PreparedStatement st = getCnn().prepareStatement(
                    "select creador "
                    + "from get_creador "
                    + "where TRIM(usuario) like ?;"
            );
            st.setString(1, idUsuario);

            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                return rs.getString("creador");
            } else {
                return null;
            }

        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public synchronized static boolean agregarFacturaNombre(Factura f) {
        String sql = "UPDATE OR INSERT INTO TABLA_FACTURAS (IDFACTURA, IDCLIENTE, "
                + "Tipo_Compra, idTurno, efectivo, cambio, estado, nombreCliente) "
                + "values (?, ?, ?, ?, ?, ?, ?, ?) matching(IDFACTURA);";
        try {
            PreparedStatement st = getCnn().prepareStatement(sql);
            st.setInt(1, f.getIdFactura());
            st.setString(2, f.getIdCliente());
            st.setBoolean(3, f.getCredito());
            st.setInt(4, f.getIdTurno());
            st.setBigDecimal(5, f.getEfectivo());
            st.setBigDecimal(6, f.getCambio());
            st.setString(7, String.valueOf(f.getEstado()));
            st.setString(8, f.getNombreCliente());
            st.executeUpdate();
            return true;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public synchronized static boolean agregarDetalleFactura(DetalleFactura d) {

        String sql = "UPDATE OR INSERT into TABLA_DETALLEFACTURA (IDFACTURA, "
                + "IDLINEA, IDPRODUCTO, PRECIO, CANTIDAD) "
                + "values ( ?, ?, ?, ?, ?) matching(IDFACTURA, IDLINEA);";

        try {
            PreparedStatement st = getCnn().prepareStatement(sql);
            st.setInt(1, d.getIdFactura());
            st.setInt(2, d.getIdLinea());
            st.setInt(3, d.getIdProducto());
            st.setBigDecimal(4, d.getPrecio());
            st.setBigDecimal(5, d.getCantidad());
            st.executeUpdate();
            return true;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public synchronized static boolean agregarCategoria(Categorias c) {
        FileInputStream fis = null;
        File file = new File(c.getRutaImagen());
        try {
            fis = new FileInputStream(file);
            PreparedStatement st = getCnn().prepareStatement(
                    "insert into TABLA_CATEGORIAS (descripcion, image, estado)"
                    + "values (?,?,?)");
            st.setString(1, c.getDescripcion());
            st.setBinaryStream(0, fis, (int) file.length());
            st.setBoolean(3, c.getEstado());
            st.executeUpdate();
            return true;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);

        } catch (FileNotFoundException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public synchronized static void cambioClave(String clave, String usuario) {
        try {
            Statement st = getCnn().createStatement();
            st.executeUpdate("ALTER USER '" + usuario + "' PASSWORD '" + clave + "'");
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public synchronized static void pagarCredito(BigDecimal deuda,
            String idCliente, BigDecimal Valor, int idFactura, int idTurno) {
        //Aqui debemos de actualizar tantos los campos credito y deudaActual
        //Tambien insertar datos a la tabla de pagosDeudas la cual tiene los sig
        //Campos idCliente, idfactura, idTurno, montoPago
        BigDecimal deudaActual = deuda.subtract(Valor);

        String sql = "update clientes set "
                + "deudaActual = ? "
                + "where idCliente = ?;";
        try {

            PreparedStatement st = getCnn().prepareStatement(sql);
            st.setBigDecimal(1, deudaActual);
            st.setString(2, idCliente);

            st.executeUpdate();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }

        sql = "INSERT INTO PAGODEUDA (IDCLIENTE, IDFACTURA, IDTURNO, MONTOPAGO, ESTADO) "
                + "VALUES ( ?, ?, ?, ?, 'A');";
        try {
            PreparedStatement st = getCnn().prepareStatement(sql);
            st.setString(1, idCliente);
            st.setInt(2, idFactura);
            st.setInt(3, idTurno);
            st.setBigDecimal(4, Valor);
            st.executeUpdate();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
        pagoCumplido(idFactura);
    }

    public synchronized static boolean guardarImagen(String ruta, String nombre) {
        String sql = "update or insert into TABLA_IMAGENES(imagen,idUsuario) "
                + "values(?,?) "
                + "matching (idUsuario)";
        PreparedStatement ps = null;
        try {

            File file = new File(ruta);
            FileInputStream fis = new FileInputStream(file);

            getCnn().setAutoCommit(false);
            ps = getCnn().prepareStatement(sql);
            ps.setBinaryStream(1, fis, (int) file.length());
            ps.setString(2, nombre);
            ps.executeUpdate();
            getCnn().commit();

            return true;
        } catch (Exception ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static synchronized ArrayList<Imagen> getImagenes() {
        ArrayList<Imagen> lista = new ArrayList<Imagen>();
        PreparedStatement ps = null;
        try {
            ps = getCnn().prepareStatement("SELECT imagen,nombre FROM tabla_Imagenes");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Blob blob = rs.getBlob("imagen");
                String nombre = rs.getObject("nombre").toString();
                byte[] data = blob.getBytes(1, (int) blob.length());
                BufferedImage img = null;
                try {
                    img = ImageIO.read(new ByteArrayInputStream(data));
                } catch (IOException ex) {
                    LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
                }

                lista.add(new Imagen(img, nombre));
            }
            rs.close();
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    public static synchronized Image getImagen(String idUsuario) {
        PreparedStatement ps = null;
        try {
            ps = getCnn().prepareStatement(
                    "SELECT imagen "
                    + "FROM Tabla_Imagenes "
                    + "WHERE TRIM(idUsuario) like ?");
            ps.setString(1, idUsuario);

            ResultSet rs = ps.executeQuery();

            BufferedImage img = null;
            while (rs.next()) {
                Blob blob = rs.getBlob("imagen");
                byte[] data = blob.getBytes(1, (int) blob.length());
                try {
                    img = ImageIO.read(new ByteArrayInputStream(data));
                } catch (IOException ex) {
                    LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            rs.close();
            return img;
        } catch (SQLException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static String agregarOrModificarAcesso(int idAcesso, int idPerfil, Perfil miPerfil) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
