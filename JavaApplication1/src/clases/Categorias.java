package clases;

import javax.swing.ImageIcon;

public class Categorias {
    private final int idCategoria;
    private final String descripcion;
    private final String rutaImagen;
    private final ImageIcon image;
    private final Boolean estado;
    private final String idUsuario;

    public Categorias(int idCategoria, String descripcion, String rutaImagen, 
            ImageIcon image, Boolean estado, String idUsuario) {
        this.idCategoria = idCategoria;
        this.descripcion = descripcion;
        this.rutaImagen = rutaImagen;
        this.image = image;
        this.estado = estado;
        this.idUsuario = idUsuario;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getRutaImagen() {
        return rutaImagen;
    }

    public ImageIcon getImage() {
        return image;
    }

    public Boolean getEstado() {
        return estado;
    }

    public String getIdUsuario() {
        return idUsuario;
    }
    
}
