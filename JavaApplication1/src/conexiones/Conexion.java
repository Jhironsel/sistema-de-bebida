package conexiones;

import clases.Datos;
import clases.ScreenSplash;
import static clases.Utilidades.LOGGER;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import javax.swing.JOptionPane;
import org.firebirdsql.event.DatabaseEvent;
import org.firebirdsql.event.EventListener;
import org.firebirdsql.event.FBEventManager;

public class Conexion extends FBEventManager {

    private static Connection cnn = null;

    public synchronized static Connection getCnn() {
        return cnn;
    }

    public synchronized static void setCnn(Connection cnn) {
        Conexion.cnn = cnn;
    }

    public Conexion(String role, String user, String clave) {
        if (ScreenSplash.debuger) {
            System.out.println("+Clase Conexion");
        }
        final String baseDeDatos = "sistemaDeBebida3";

        Properties props, host;
        String ip = "", puerto = "";

        props = new Properties();
        props.setProperty("user", user);
        props.setProperty("password", clave);
        props.setProperty("charSet", "UTF8");
        props.setProperty("roleName", role);

        host = new Properties();
        try {
            host.load(new FileReader("propiedades.properties"));
        } catch (IOException ex) {
            LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (Boolean.valueOf(host.getProperty("ProtocoloActivo", "false"))) {
            ip = host.getProperty("Ip_Servidor1") + "."
                    + host.getProperty("Ip_Servidor2") + "."
                    + host.getProperty("Ip_Servidor3") + "."
                    + host.getProperty("Ip_Servidor4");
        }

        if (Boolean.valueOf(host.getProperty("NombreActivo", "false"))) {
            ip = host.getProperty("Nombre_del_Servidor");
        }

        if (Boolean.valueOf(host.getProperty("Con_Puerto", "false"))) {
            puerto = ":" + host.getProperty("Puerto_del_Servidor");
        }

        String file = "jdbc:firebirdsql://" + ip + puerto + "/" + baseDeDatos;
        if (ScreenSplash.debuger) {
            System.out.println("Dirección: " + file);
        }

        if (getCnn() == null) {
            try {
                Class.forName("org.firebirdsql.jdbc.FBDriver");
                cnn = DriverManager.getConnection(file, props);
                if (ScreenSplash.debuger) {
                    System.out.println("Datos: " + cnn.getMetaData().getURL());
                    System.out.println("Datos: " + cnn.getMetaData().getDatabaseProductName());
                    System.out.println("Datos: " + cnn.getMetaData().getDatabaseProductVersion());
                    System.out.println("Conexion en: " + ip + "\nPuerto : " + (puerto.isEmpty() ? "Sin Puerto" : puerto));
                }
            } catch (ClassNotFoundException ex) {
                if(ScreenSplash.debuger){
                    System.out.println(ex.getMessage());
                }

                LOGGER.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(null, "Libreria del driver no encontrada");

                return;
            } catch (SQLException ex) {
                if(ScreenSplash.debuger){
                    System.out.println(ex.getMessage());
                }

                if (ex.getMessage().contains("password")) {
                    JOptionPane.showMessageDialog(null, "Usuario no identificado");

                }

                if (ex.getMessage().contains("Unable to complete network request to host")) {
                    JOptionPane.showMessageDialog(null, "No es posible conectarse al servidor: " + ip);
                }

                LOGGER.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
                return;
            }
        }

        if (!role.equalsIgnoreCase("none")) {
            setHost(ip);
            setUser(user);
            setPassword(clave);
            setDatabase(baseDeDatos);
            try {
                if(ScreenSplash.debuger){
                    System.out.println(
                        "/*/*Para eventos tenemos la ip " + ip
                        + " el usuario " + user + " clave "
                        + clave + " la base de datos " + baseDeDatos);
                }
                
                connect();
                addEventListener("new_item",
                        new EventListener() {
                    @Override
                    public void eventOccurred(DatabaseEvent event) {
                        if(ScreenSplash.debuger){
                            System.out.println("Event ["
                                + event.getEventName() + "] occured "
                                + event.getEventCount() + " time(s)");
                        }
                    }
                }
                );
                addEventListener("new_client",
                        new EventListener() {
                    @Override
                    public void eventOccurred(DatabaseEvent event) {
                        if(ScreenSplash.debuger){
                            System.out.println("Event ["
                                + event.getEventName() + "] occured "
                                + event.getEventCount() + " time(s)");
                        }
                    }
                }
                );
            } catch (SQLException ex) {
                LOGGER.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
            }
            return;
        }

        System.out.println("---------------------------------------");
        System.out.println("Conexion Establecida Clase Conexion!!!");
        System.out.println("---------------------------------------");

    }
}
